FROM php:7.4-apache

RUN apt-get update -y && apt-get install -y libpng-dev libonig-dev libfreetype6-dev libjpeg62-turbo-dev
RUN docker-php-ext-configure gd --with-freetype --with-jpeg 
RUN a2enmod rewrite && a2enmod ssl && docker-php-ext-install mysqli pdo pdo_mysql gd mbstring opcache gd exif
RUN apt-get install -y \
    libzip-dev \
    zip \
    && docker-php-ext-install zip

RUN apt-get update && apt-get install -y \
    libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
    && docker-php-ext-enable imagick

