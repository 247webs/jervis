// =========================================================================================
//     File Name: invoice-template.js
//     Description: Treeview.
//     --------------------------------------------------------------------------------------
//     Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
//     
//     
// ==========================================================================================

$(document).ready(function () {
  // print invoice with button
  $(".btn-print").click(function () {
    window.print();
  });
});
