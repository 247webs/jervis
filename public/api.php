<?php

if (!isset($_SERVER['HTTP_UUID'])) {
    header('HTTP/1.1 403 Forbidden');

    $res['type'] = 'error';
    $res['message'] = 'uuid is missing';
    $response[] = $res;

    $finalOutput['message'] = '';
    $finalOutput['errors'] = $response;
    exit(json_encode($finalOutput));
}

require dirname(__DIR__).'/app/config/settings.php';

require ROOT_DIR.'/app/autoload.php';

session_start();

use Respect\Rest\Router;

$_POST = json_decode(file_get_contents('php://input'), true);

$r3 = new Router('/api/v1');

$r3->get('/users/properties/*', 'ApiUsersController');
//$r3->get('/users/*', 'ApiUsersController');
$r3->delete('/users/*/properties/*', 'ApiUsersController');
$r3->any('/users/*/*', 'ApiUsersController');
$r3->any('/users/*', 'ApiUsersController');

$r3->any('/users/invite/*', 'ApiUsersInviteController');

$r3->any('/oauth/access_token/*', 'ApiOauthAccessTokenController');

$r3->any('/checklists/*', 'ApiChecklistsController');
$r3->get('/checklists/properties/*', 'ApiChecklistsPropertiesController');
$r3->any('/checklists/fill/*', 'ApiChecklistsFillController');

$r3->any('/checklists/templates/*', 'ApiChecklistsTemplatesController');
$r3->any('/checklists/templates/properties/*', 'ApiChecklistsTemplatesPropertiesController');

$r3->any('/properties/*', 'ApiPropertiesController');

$r3->post('/password/reset/*', 'ApiPasswordController');

$r3->any('/roles/*', 'ApiRolesController');

$r3->get('/countries/*', 'ApiCountriesController');
$r3->get('/countries/*/states/*', 'ApiCountriesController');
$r3->get('/states/*/cities/*', 'ApiStatesController');
