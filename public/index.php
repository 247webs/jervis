<?php

require dirname(__DIR__).'/app/config/settings.php';
require ROOT_DIR.'/app/config/routes.php';
require ROOT_DIR.'/app/autoload.php';
session_start();
$app = new App($routes);
