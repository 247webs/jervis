# Jervis

## Setting up the IDE

### Visual Studio Code

-   Install [Visual Studio Code](https://code.visualstudio.com/).
-   Then install the extension `vscode-php-cs-fixer`. It is already configured for the project.
-   Then install the extension `Prettier - Code formatter`. It is already configured for the project.

### Other IDE

If you need to setup another IDE, you need to understand the following things:

-   We rely on `PHP-CS-Fixer` to make sure we are compliant with [PSR2](https://www.php-fig.org/psr/psr-2/) and that our PHP code is consistent.
-   We rely on `prettier` to make sure our JavaScript is consistent.
-   Also, we use 4 spaces for identation.
-   The configuration we use for `PHP-CS-Fixer` and `prettier` can be seen in the file `jervis.code-workspace` at the values called `"vscode-php-cs-fixer.rules"` and `prettier.*`.

## Installation

### Dependencies

-   Install [Node.js](https://nodejs.org/).
-   Install Parcel:
    `npm install -g parcel`
-   Install [PHP](https://php.net/).
-   Install [Composer](https://getcomposer.org/download/) and `mv composer.phar /usr/local/bin/composer`.

### Build

    cd jervis
    cd app/core/3rdParty
    # Installs our PHP dependencies
    composer install

    cd ../../..

    cd js
    npm install

    # Compile our JS (Vue and other scripts)

    # Build for development (recompiles on file changes)
    npm run dev

    # Build for production
    npm run build

### Database tables

Run the following command:

    cd app
    php ./core/3rdParty/vendor/bin/phinx migrate

Or using Docker:

    docker-compose exec www bash -c "cd /var/www/app && php ./core/3rdParty/vendor/bin/phinx migrate"

If you are running on an install that previously used the `install.sql` file, you will need to clear your database and start over. You might be able to export all data from the database (without the table definitions), run the migration and insert the data. The tables are more or less the same but more consistent.

To read more about the database migration system, follow these links:

    - http://docs.phinx.org/en/latest/index.html
    - https://helgesverre.com/blog/database-migrations-in-php-with-phinx/

#### Modify the database

To create a new database migration (basically a change in the layout or insert of metadata), run the following command (with a proper name):

    ./core/3rdParty/vendor/bin/phinx create TheNewFeatureTables

And then modify the file called `####_the_new_feature_tables.php` inside of `app/db/migrations`.

Test the migration:

    ./core/3rdParty/vendor/bin/phinx migrate -x

Finally run the migration:

    ./core/3rdParty/vendor/bin/phinx migrate

Rollback if you want to add more changes to the migration:

    ./core/3rdParty/vendor/bin/phinx rollback

### Setup cron

Setup cron for sms/email notification every 5 minutes with below command

cd <DIR-OF-PROJECT> && docker-compose exec -T www /usr/local/bin/php /var/www/app/notificationCron.php send:notification


Setup cron for creating checklist every 30 minutes with below command

cd <DIR-OF-PROJECT> && docker-compose exec -T www /usr/local/bin/php /var/www/app/scheduleChecklistCron.php schedule:checklist


Setup cron to sync calendar with third party booking websites every 5 minutes with below command

cd <DIR-OF-PROJECT> && docker-compose exec -T www /usr/local/bin/php /var/www/app/calendarCron.php sync:calendar


### Run (with Docker)

After running the steps in `Build` you can run Jervis in Docker.

Make a copy of `docker-compose.sample.yml` to `docker-compose.yml` and modify to your needs. Pay close attention to the section about `MYSQL_ROOT_PASSWORD`.

Run the following commands:

    cd jervis
    docker-compose up -d

A folder called `./docker-data` should now show up. Put a database dump in this folder to be accessible inside the `db` container.

    # Start shell in db container
    docker-compose exec db bash

    # Create database
    mysql -uroot -p -e 'create database jervis'

    # Import database dump
    mysql -uroot -p jervis < "/dump/my-database-dump.sql"

Now visit [http://localhost:83](http://localhost:83) in your browser, unless you configured `docker-compose.yml` to listen on a different port.

## For Designers

Our new MVC system installed on the Cleaner Assistant and the Jervis Systems websites for now but the near feature I will apply this for all sites that we have.

Basically, the files which related with designers are in /public/ folder.

To clarify; the file structure is here.

### /public/html/pages/

This folder contains all html files of the site. The file name of the Home page is index.html. If you put a test/index.html file into this folder; you can see that from https://www.cleanerassistant.com/test/ (we don't put the .html extension into urls anymore with this template system)

### /public/html/templates/site.html and /public/html/templates/manager.html

It is the template file that contains header and footer. The file contains all meta tags and styles. If you want to add a css file, javascript file and the copyright texts, you can add it from there. When you change something in that page; it applies to all site pages or manager(admin) pages.

### /public/html/css/

You can put the css files into this folder.

### /public/html/js/

You can put the javascript files into this folder.

### /public/html/manager/css/

You can put the css files into this folder.

### /public/html/manager/js/

You can put the javascript files into this folder.

## For Programmers

### /app/

This folder contains all php files and there is no file to edit by you. But I will explain the folders/files for to make documentation with this message.

### /app/settings/config.php

contains base url and database connection information

### /app/settings/routes.php

I can use later for SEO optimized urls. I can route a SEO url to real url.
Example: xxxx.com/our-test-services/ -> xxxx.com/test/

### /app/core/App.php

The core of php MVC system. Handles the all requests and runs necessary classes.

### /app/core/3rdParty/

I will use this folder for the 3rdparty php classes like phpmailer etc. Will be come composer files as well.

### /app/core/helpers/Helpers.php

I will use this folder for to create general classes. like string sanitization, redirection functions etc.

### /app/core/Model.php

Core model class in there. For now the database connection functions in there. All the other model classes extends from this class.

### /app/core/View.php

Core view class in there. For now the render functions to call header and footer in there. All the other view classes extends from this class.

### /app/core/Controller.php

Core controller class in there. It has the functions to call the core model and the core view php file. All the other page controllers extends from this class.

### /app/mvc/controllers/

All the page controllers will be placed in this file. The file names will be PageController.php , TestController.php or XxxxxxxController.php like that..(CamelCase Notation)

### /app/mvc/models/

All the models using in the page placed in this file. The file names will be Page.php , Test.php or Xxxxxxx.php like that..(First character is uppercase)

### /app/mvc/views/

Linked to /public/html/ folder. Explanations are above.

### /app/autoload.php

Autoloader function file. We just include this file in index.php file. The file, contains the autoloader function that auto include the necessary file if a class called.
