import Vue from "vue";

const datastore = {
    drag: false,
    current_password: "",
    new_password: "",
    confirm_password: "",
    error: "",
    isSaving: false,
};

function getPostValues() {
    return {
        current_password: datastore.current_password,
        new_password: datastore.new_password,
        confirm_password: datastore.confirm_password,
    };
}

const el = "#edit-manager";

const existingChangePassword = $(el).data("manager");
const existingAccess = $(el).data("access");
if (existingChangePassword) {
    datastore.current_password = existingChangePassword.current_password;
    datastore.new_password = existingChangePassword.new_password;
    datastore.confirm_password = existingChangePassword.confirm_password;
}

let deleteAccessCallback = null;

const vm = new Vue({
    el,
    data: datastore,
    methods: {
        save: (evt) => {
            evt.preventDefault();
            const url = $(el).attr("action");
            datastore.isSaving = true;
            $.ajax({
                method: "POST",
                url,
                data: { ...getPostValues() },
                success: (data, status, jqXhr) => {
                    datastore.isSaving = false;
                    $("#reset").trigger("click");
                    console.log({ data });
                    var error_body = $("form").find(".error");
                    if (data.success) {
                        showErrorMsg(error_body, "success", data.success);
                    } else {
                        showErrorMsg(error_body, "warning", data.error);
                    }
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                    datastore.isSaving = false;
                    if (jqXHR.responseJSON) {
                        datastore.error = jqXHR.responseJSON.errMsg;
                    }
                },
                dataType: "json",
            });
        },
    },
});

$(document).ready(function() {
    var password = document.getElementById("new_password"),
        confirm_password = document.getElementById("confirm_password");

    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity("");
        }
    }

    $(document).on("change", "#new_password", function() {
        validatePassword();
    });

    $(document).on("keyup", "#confirm_password", function() {
        validatePassword();
    });
});

function showErrorMsg(form, type, msg) {
    var alert = $(
        '<div class="alert alert-' +
            type +
            ' alert-dismissible" role="alert">\
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times"></i></button>\
                    <span></span>\
            </div>'
    );

    form.find(".alert").remove();
    alert.prependTo(form);
    //    alert.animateClass('fadeIn animated');
    alert.find("span").html(msg);
}
