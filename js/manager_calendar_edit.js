import Vue from "vue";

const datastore = {
    drag: false,
    booking_source: "",
    url: "",
    color: "",
    error: "",
    isSaving: false,
};

function getPostValues() {
    return {
        bookingSource: datastore.booking_source,
        url: datastore.url,
        color: $("input[name=color]").val(),
        propertyId: $("#calendarPropertyId").val(),
    };
}

const el = "#edit-calendar";

const existingCalendar = $(el).data("calendar");
const existingAccess = $(el).data("access");
if (existingCalendar) {
    datastore.existingCalendar = true;
    datastore.booking_source = existingCalendar.booking_source;
    datastore.url = existingCalendar.url;
    datastore.color = existingCalendar.color;
}

let deleteAccessCallback = null;

const vm = new Vue({
    el,
    data: datastore,
    methods: {
        save: evt => {
            evt.preventDefault();
            const url = $(el).attr("action");
            datastore.isSaving = true;
            $.ajax({
                method: "POST",
                url,
                data: { ...getPostValues() },
                success: (data, status, jqXhr) => {
                    console.log({ data });
                    var calendarPropertyId = $("#calendarPropertyId").val();
                    window.location.href = "/manager/calendar/index?property_id=" + calendarPropertyId;
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                    datastore.isSaving = false;
                    if (jqXHR.responseJSON) {
                        datastore.error = jqXHR.responseJSON.errMsg;
                    }
                },
                dataType: "json",
            });
        },
    },
});

// Delete role modal
$("#deleteModal").on("show.bs.modal", function(event) {
    const modal = $(this);
    modal.find("button.do-delete").click(evt => {
        evt.preventDefault();
        $("#deleteModal").modal("hide");
    });
});
