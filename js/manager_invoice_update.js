$(document).ready(function () {
    $(".listItem").on("click", function () {
        $(".datastructer").append(
            '<div class="abc form-row"><div class="form-group col-md-4"><input type="text" class="form-control"  name="name[]" value=""></div><div class="form-group col-md-2"><input type="text" class="form-control quantity" name="quantity[]"  value="0"></div><div class="form-group col-md-2"><input type="text" class="form-control rat"  name="rate[]"  value="0"></div><div class="form-group col-md-3"><input type="text" class="form-control amount" name="amount[]" value="0"></div><div class="form-group col-md-1"><i class="icon icon-trash Remove" style="font-size:33px"></i></div></div>'
        );
    });
    $(".listItemdeduct").on("click", function () {
        $(".deductdatastructer").append(
            '<div class="form-row invoicestruct"><div class="form-group col-md-6"><input type="text" class="form-control" name="deductitemname[]"  value=""></div><div class="form-group col-md-2"><select class="form-control form-control-md pervalue" name="per[]" value=""><option value="$">Flat($)</option><option value="%">Percent(%)</option></select></div><div class="form-group col-md-3"><input type="text" class="form-control deductamt" name="deductamount[]"  value=""></div><div class="form-group col-md-1"> <i class="icon icon-trash removededuct" style="font-size:33px"></i></div></div>'
        );
    });
});
$(document).on("click", ".Remove", function () {
    $(this).closest(".abc").remove();
    var quantity = $(this).val();
    var rat = $(this).closest(".abc").find(".rat").val();
    var total = parseInt(quantity) * parseInt(rat);
    var amount = $(this).closest(".abc").find(".amount").val(total);
    var sum = 0;
    $(".amount").each(function () {
        sum += parseInt(this.value);
    });
    $(".subtotal").val(sum);
    var subtotal = $(".subtotal").val();
    var totaldeduct = $(".totaldeduct").val();
    var total = subtotal - totaldeduct;
    $(".total").val(total);
});

$(document).on("click", ".removededuct", function () {
    $(this).closest(".invoicestruct").remove();
    var deduct = $(this).closest(".invoicestruct").find(".deductamt").val();
    var totaldeduct = $(".totaldeduct").val();
    var totaldeduction = totaldeduct - deduct;
    $(".totaldeduct").val(totaldeduction);
    var subtotal = $(".subtotal").val();
    var total = subtotal - totaldeduction;
    $(".total").val(total);

});

$(document).on("keyup", ".quantity", function () {
    var quantity = $(this).val();
    var rat = $(this).closest(".abc").find(".rat").val();
    var total = parseInt(quantity) * parseInt(rat);
    var amount = $(this).closest(".abc").find(".amount").val(total);
    var sum = 0;
    $(".amount").each(function () {
        sum += parseInt(this.value);
    });
    $(".subtotal").val(sum);
});

$(document).on("keyup", ".rat", function () {
    var rate = $(this).val();
    var quantity = $(this).closest(".abc").find(".quantity").val();
    var total = parseInt(rate) * parseInt(quantity);
    var amount = $(this).closest(".abc").find(".amount").val(total);
    var sum = 0;
    $(".amount").each(function () {
        sum += parseInt(this.value);
    });
    $(".subtotal").val(sum);
    var subtotal = $(".subtotal").val();
    var totaldeduct = $(".totaldeduct").val();
    var total = subtotal - totaldeduct;
    $(".total").val(total);
});

$(document).on("keyup", ".deductamt", function () {
    var value = $('.pervalue').val();
    var deduction  = $(this).val();
    var subtotal = $(".subtotal").val();
    if(value == "$"){
        var sum = 0;
        $(".deductamt").each(function () {
            sum += parseInt(this.value);
        });
    }
    else{
        var sum = 0;
        sum = (subtotal * deduction) / 100;
    }
    
   
    $(".totaldeduct").val(sum);
    var subtotal = $(".subtotal").val();
    var totaldeduct = $(".totaldeduct").val();
    var total = subtotal - totaldeduct;
    $(".total").val(total);
});

$(document).on("change", ".pervalue", function () {
    var value = $(this).val();
    var deduction  = $('.deductamt').val();
    var subtotal = $(".subtotal").val();
    if(value == "$"){
        var sum = 0;
        $(".deductamt").each(function () {
            sum += parseInt(this.value);
        });
    }
    else{
        var sum = 0;
        sum = (subtotal * deduction) / 100;
    }
    
   
    $(".totaldeduct").val(sum);
    var subtotal = $(".subtotal").val();
    var totaldeduct = $(".totaldeduct").val();
    var total = subtotal - totaldeduct;
    $(".total").val(total);
});

$(document).on("click", ".updateinvoice", function () {
    var property_id = $(".proprtyid").val();
    var formData = $("#invoiceupdateform").serialize();
    $.ajax({
        method: "POST",
        url: "/manager/invoice/invoice-updatedata",
        data: formData,
        dataType: "json",
        success: function (responce) {
            window.location.href = "manager/invoice/index?property_id=" + property_id;
        },
        error: function (e) {
            toastr.error("error");
        },
    });
});
