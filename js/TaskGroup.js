import TaskItem from "./TaskItem";

export default class TaskGroup {
    constructor(title = "", tasks = []) {
        this.title = title;
        this.tasks = tasks;
        this.tasks.push(new TaskItem());
    }

    isEmpty() {
        return this.title.trim() == "" && this.tasks.filter(t => !t.isEmpty()).length == 0;
    }
}
