$("#deleteModal").on("show.bs.modal", function(event) {
    const $button = $(event.relatedTarget); // Button that triggered the modal
    const templateId = $button.closest("tr").data("template-id");

    var modal = $(this);
    deleteButton = modal.find("button.do-delete");
    deleteButton.click(() => {
        deleteButton.find(".spinner-border").removeClass("invisible");
        $.ajax({
            method: "DELETE",
            url: "/manager/notification/template/api/delete-notification-template/" + templateId,
            success: () => {
                console.log("Delete success");
                window.location.reload();
            },
            error: () => {
                console.log("Delete error");
                window.location.reload();
            },
        });
    });
});
