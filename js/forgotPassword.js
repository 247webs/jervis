import { exists } from "fs";

$("#forgotPassword").submit(function(event) {
    event.preventDefault(); //prevent default action
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission
    $.ajax({
        url: post_url,
        type: request_method,
        data: form_data,
        dataType: "json",
    }).done(function(response) {
        $(".alert-danger").hide();
        $(".alert-success").hide();
        if (true === response.error) {
            $(".alert-danger").html(response.description);
            $(".alert-danger").show();
        } else {
            $(".alert-success").html(response.description);
            $(".alert-success").show();
            $("#forgotPassword").hide();
        }
    });
});
