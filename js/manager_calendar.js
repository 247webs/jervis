
document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: ["dayGrid", "timeGrid", "interaction"],    
    header: {
      left: "addNew",
      center: "dayGridMonth,timeGridWeek,timeGridDay",
      right: "prev,title,next"
    },
    displayEventTime: false,
    navLinks: true,
    editable: true,
    allDay: true,
	eventRender: function(info) {
	  $(info.el).tooltip({
		title: info.event.extendedProps.description,
		placement: 'top',
		trigger: 'click',
		container: 'body',
		html: true
	  });      
	},
	events: "/manager/reservation/calendar-data?property_id=" + propertyId,
  });

  // render calendar
  calendar.render();  
});