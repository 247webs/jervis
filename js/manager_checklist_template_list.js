function deleteChecklistTemplate(arg0) {
    console.log("deleteChecklistTemplate", arg0);
}

$("#deleteTemplateModal").on("show.bs.modal", function(event) {
    const $button = $(event.relatedTarget); // Button that triggered the modal
    const templateId = $button.closest("tr").data("template-id");
    console.log(templateId);

    // var recipient = button.data('whatever'); // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find("button.do-delete").click(() => {
        $.ajax({
            method: "DELETE",
            url: "/manager/checklist/template/api/delete-template/" + templateId,
            success: () => {
                console.log("Success");
                window.location.reload();
            },
            error: () => {
                console.log("Error");
                window.location.reload();
            },
        });
    });
    // modal.find('.modal-title').text('New message to ' + recipient)
    // modal.find('.modal-body input').val(recipient)
});
