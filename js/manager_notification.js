$(document).ready(function() {
    if ($("#summernote").length > 0) {
        $("#summernote").summernote();
    }

    $("#send_via").change(function() {
        if ($("option:selected", this).val() == "SMS") {
            $("#emailContent").attr("hidden", "hidden");
            $("#smsContent").removeAttr("hidden");
        } else {
            $("#smsContent").attr("hidden", "hidden");
            $("#emailContent").removeAttr("hidden");
        }
    });
});
$("#notification").submit(function(event) {
    event.preventDefault(); //prevent default action
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serializeArray(); //Encode form elements for submission
    if ($("#summernote").length > 0) {
        form_data.push({ name: "content", value: cleanPastedHTML($("#summernote").summernote("code")) });
    }
    $.ajax({
        url: post_url,
        type: request_method,
        data: form_data,
    }).done(function(response) {
        var propertyId = $("#propertyId").val();
        window.location.href = "/manager/notification/custom-notification?property_id=" + propertyId;
    });
});
function cleanPastedHTML(input) {
    input = input.trim();
    var output = input.toString().replace(/<+[^>]+(class=("|')MsoNormal)+[^>]+>/g, " ");
    output = output.trim();

    output = output.toString().replace(/<\/p>$/g, " ");
    output = output.toString().replace(/((\s+)?rs_id=("|')?[0-9.a-zA-Z]+("|')?(\s+)?)/g, " ");

    var stringStripper = /(\n|\r| class=(")?Mso[a-zA-Z]+(")?)/g;
    output = output.replace(stringStripper, " ");

    var commentSripper = new RegExp("<!--(.*?)-->", "g");
    var output = output.replace(commentSripper, "");

    var tagStripper = new RegExp("<(/)*(meta|link|\\?xml:|st1:|o:|font)(.*?)>", "gi");
    output = output.replace(tagStripper, "");

    var badTags = ["script", "applet", "embed", "noframes", "noscript"];

    for (var i = 0; i < badTags.length; i++) {
        tagStripper = new RegExp("<" + badTags[i] + ".*?" + badTags[i] + "(.*?)>", "gi");
        output = output.replace(tagStripper, "");
    }

    var badAttributes = ["start"];
    for (var i = 0; i < badAttributes.length; i++) {
        var attributeStripper = new RegExp(" " + badAttributes[i] + '="(.*?)"', "gi");
        output = output.replace(attributeStripper, "");
    }

    return output;
}
