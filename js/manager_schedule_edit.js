$(document).on("click", ".addservice", function(event) {
    event.preventDefault();
    var formData = $("#add_service").serialize();
    $.ajax({
        url: "manager/schedule/add-services",
        type: "POST",
        data: formData,
        dataType: "json",
        success: function(response) {
            location.reload();
            toastr.success("Data Save Successfully");
        },
        error: function(e) {
            toastr.error("error");
        },
    });
});

$(document).on("click", ".editservice", function(event) {
    event.preventDefault();
    var servicename = $(this).data("service");
    var servicekey = $(this).data("servicekey");
    $("#servicekey").val(servicekey);
    $("#servicename").val(servicename);
});

$(document).on("click", ".editservicebtn", function(event) {
    event.preventDefault();
    var formData = $("#edit_service").serialize();
    $.ajax({
        url: "manager/schedule/edit-services",
        type: "POST",
        data: formData,
        dataType: "json",
        success: function(response) {
            location.reload();
            toastr.success("Data Edit Successfully");
        },
        error: function(e) {
            toastr.error("error");
        },
    });
});

$(document).on("click", ".deleteservice", function(event) {
    event.preventDefault();
    var service = $(this).data("service");
    $.ajax({
        url: "manager/schedule/destroy-service",
        type: "POST",
        data: {
            service: service,
        },
        dataType: "json",
        success: function(response) {
            location.reload();
            toastr.success("Data Delete Successfully");
        },
        error: function(e) {
            toastr.error("error");
        },
    });
});

$(document).on("click", ".Monday, .Tuesday, .Wednesday, .Thursday, .Friday, .Saturday, .Sunday", function(event) {
    event.preventDefault();
    var day = $(this).data("break");
    var indexkey =
        $("." + day + "Slot")
            .children()
            .last()
            .data("index") + 1;

    if (isNaN(indexkey)) {
        indexkey = 0;
    }

    var $el = $("#hiddendrop tbody")
        .eq(0)
        .clone();
    $el.attr("data-index", indexkey);
    $("." + day + "Slot").append($el);
    $select = $el.find("input.timepicker");
    $select.each(function(index, item) {
        var slot = "start";
        if (index) {
            var slot = "end";
        }
        $(this).attr("name", "");
        $(this).attr("name", day + "[" + indexkey + "][" + slot + "]");

        $(this).attr("id", "");
        $(this).attr("id", day + "[" + indexkey + "][" + slot + "]");
        $(".timepicker").timepicki();
    });
});

$(document).on("click", ".deletetimeslot", function() {
    $(this)
        .closest("tr")
        .remove();
});

$(".addtimeoff").on("click", function(event) {
    event.preventDefault();
    $(".drpoff").toggle("slow", function() {
        if ($(".drpoff").is(":visible")) {
            $(".drpoff").removeAttr("disabled");
        } else {
            $(".drpoff").attr("disabled", "disabled");
        }
    });
    $(".plus").toggle();
    $(".flaticon2-checkmark").toggle();
});

$(document).on("click", ".wrkbtn", function(event) {
    event.preventDefault();
    var btn = $('.wrkspin');
    btn.addClass("spinner-border spinner-border-sm").attr("disabled", true);
    var formData = $("#formwrkhrs").serialize();
    $.ajax({
        url: "/manager/schedule/update-workingHours",
        type: "POST",
        data: formData,
        dataType: "json",
        success: function(response) {
            setTimeout(function() {
                btn.removeClass("spinner-border spinner-border-sm").attr(
                    "disabled",
                    false
                );
            }, 300);
            toastr.success("Data Save Successfully");
        },
        error: function(e) {
            setTimeout(function() {
                btn.removeClass("spinner-border spinner-border-sm").attr(
                    "disabled",
                    false
                );
            }, 300);
        },
    });
});

$(document).on("click", ".addbrkbtn", function(event) {
    event.preventDefault();
    var btn = $('.brkspin');
    btn.addClass("spinner-border spinner-border-sm").attr("disabled", true);
    var formData = $("#formaddbrk").serialize();
    $.ajax({
        url: "/manager/schedule/update-addBreak",
        type: "POST",
        data: formData,
        dataType: "json",
        success: function(response) {
            setTimeout(function() {
                btn.removeClass("spinner-border spinner-border-sm").attr(
                    "disabled",
                    false
                );
            }, 300);
            toastr.success("Data Save Successfully");
        },
        error: function(e) {
            setTimeout(function() {
                btn.removeClass("spinner-border spinner-border-sm").attr(
                    "disabled",
                    false
                );
            }, 300);
            toastr.error("error");
        },
    });
});

$(document).on("click", ".dayoffbtn", function(event) {
    //addbrk
    var btn = $(this);
    btn.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", true);
    event.preventDefault();
    var formData = $("#formdayoff").serialize();
    $.ajax({
        url: "/manager/schedule/add-dayOff",
        type: "POST",
        data: formData,
        dataType: "json",
        success: function(response) {
            setTimeout(function() {
                btn.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr(
                    "disabled",
                    false
                );
            }, 300);
            $("#myModal").modal("hide");
            window.location.reload();
        },
        error: function(e) {
            setTimeout(function() {
                btn.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr(
                    "disabled",
                    false
                );
            }, 300);
        },
    });
});

$(document).on("click", ".deletetimeoffbtn", function(event) {
    //addbrk
    var timeoffid = $(this).data("id");
    $.ajax({
        url: "/manager/schedule/destroy-timeoff",
        type: "POST",
        data: {
            timeoffid: timeoffid,
        },
        dataType: "json",
        success: function(response) {
            $(".timeoff" + timeoffid).remove();
            window.location.reload();
        },
        error: function(e) {
            toastr.error("error");
        },
    });
});
