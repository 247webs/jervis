import 'bootstrap4-toggle/js/bootstrap4-toggle.min.js';

var $originalForm;
$("#confirmSubmission").on("show.bs.modal", function (event) {
    const modal = $(this);

    const $button = $(event.relatedTarget); // Button that triggered the modal
    $originalForm = $button.closest("form");

    const untickedCheckboxes = $originalForm.find('input[name^="group"]:not(:checked)').length;

    const untickedBoxesMessage = modal.find(".unticked-boxes-message");
    if (untickedCheckboxes) {
        modal.find(".unticked-boxes-number").text(untickedCheckboxes);
        untickedBoxesMessage.show();
    } else {
        untickedBoxesMessage.hide();
    }

    const $modalForm = modal.find("form");
    const $spinner = $modalForm.find(".modal-footer .spinner-border");

    const returnUrl = $originalForm.data("returnurl");

    $modalForm.submit(evt => {
        evt.preventDefault();

        const url = $originalForm.attr("action");
        const data = $originalForm.serialize();
        console.log("Submit form", url, data);
        $spinner.removeClass("invisible");
        $.ajax({
            method: "POST",
            url,
            data,
            success: (data, status, jqXhr) => {
                $spinner.addClass("invisible");
                window.location.href = returnUrl;
            },
            error: (jqXHR, textStatus, errorThrown) => {
                $spinner.addClass("invisible");
            },
            dataType: "json",
        });
    });
});

$(document).on("click", ".save", function (event) {
    event.preventDefault();
    $(".valuesave").removeAttr('disabled');
    var url = $('#fill').attr("action");
    const data = $('#fill').serialize();
    const returnUrl = $('#fill').data("returnurl");
    $.ajax({
        method: "POST",
        url,
        data,
        dataType: "json",
        success: function (response) {
            toastr.success("Data Save Successfully");
            window.location.href = returnUrl;
        },
        error: function (e) {
            toastr.error("error");
        },

    });
});

$(document).on("change", ".taskFiles", function (event) {

    var eventID = event.currentTarget.id;
    var eventIDArray = eventID.split('-');
    var requestId = eventIDArray[2];
    var groupId = eventIDArray[3];
    var taskId = eventIDArray[4];
    var form_data = new FormData();
    var bar = $('.file-progress-bar-' + groupId + '-' + taskId);

    for (var i = 0; i < event.target.files.length; i++) {
        form_data.append("file" + i, event.target.files[i]);
    }
    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = ((evt.loaded / evt.total) * 100);
                    bar.width(percentComplete + '%');
                    bar.html(percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        url: 'manager/checklist/api/uploadFiles/?request_id=' + requestId,
        method: "POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            bar.parent().show();
            bar.width('0%');
        },
        success: function (data) {
            var data = JSON.parse(data);
            var result = data.result;
            var previousFilesCount = document.getElementById('filesUploaded-' + groupId + '-' + taskId).innerHTML.match(/\d+/);
            var previousFiles = document.getElementById('file-' + groupId + '-' + taskId).value;

            // Show uploaded file count
            document.getElementById('filesUploaded-' + groupId + '-' + taskId).innerHTML = (Number(previousFilesCount) + Number(result.length)) + ' File(s) uploaded';

            // Store uploaded file path in an hidden variable
            if (previousFiles != '') {
                previousFiles = previousFiles + ',' + result;
                document.getElementById('file-' + groupId + '-' + taskId).value = previousFiles;
            } else {
                document.getElementById('file-' + groupId + '-' + taskId).value = result;
            }
        }
    });
});

$(document).on("change", ".commonFiles", function (event) {
    var eventID = event.currentTarget.id;
    var eventIDArray = eventID.split('-');
    var requestId = eventIDArray[1];
    var form_data = new FormData();
    var bar = $('.file-progress-bar-common');

    for (var i = 0; i < event.target.files.length; i++) {
        form_data.append("file" + i, event.target.files[i]);
    }
    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = ((evt.loaded / evt.total) * 100);
                    bar.width(percentComplete + '%');
                    bar.html(percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        url: 'manager/checklist/api/uploadFiles/?request_id=' + requestId,
        method: "POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            bar.parent().show();
            bar.width('0%');
        },
        success: function (data) {
            var data = JSON.parse(data);
            var result = data.result;
            var previousFilesCount = document.getElementById('countCommonFiles').innerHTML.match(/\d+/);
            var previousFiles = document.getElementById('commonFiles').value;

            // Show uploaded file count
            document.getElementById('countCommonFiles').innerHTML = (Number(previousFilesCount) + Number(result.length)) + ' File(s) uploaded';

            // Store uploaded file path in an hidden variable
            if (previousFiles != '') {
                previousFiles = previousFiles + ',' + result;
                document.getElementById('commonFiles').value = previousFiles;
            } else {
                document.getElementById('commonFiles').value = result;
            }
        }
    });
});

$(document).on("click", ".removeFile", function (event) {
    event.preventDefault();
    $("#deleteConfirmationBox").modal();
    var eventID = event.currentTarget.id;
    var eventIDArray = eventID.split('-');
    var indexId = eventIDArray[1];
    var submissionId = eventIDArray[2];
    var taskId = (eventIDArray[3]) ? eventIDArray[3] : '';
    var groupId = (eventIDArray[4]) ? eventIDArray[4] : '';
    $("#indexId").val(indexId);
    $("#submissionId").val(submissionId);
    $("#taskId").val(taskId);
    $("#groupId").val(groupId);
});

$(document).on('click', '.confirmDeleteFile', function () {
    $.ajax({
        method: "GET",
        url: 'manager/checklist/api/removeFile/?index_id=' + $("#indexId").val() + '&submission_id=' + $("#submissionId").val() + '&task_id=' + $("#taskId").val() + '&group_id=' + $("#groupId").val(),
        success: function (response) {
            $('.file-deleted').html("File Deleted!");
            setTimeout(function () {
                location.reload(true);
            }, 2000);
        },
        error: function (e) {},
    });
});