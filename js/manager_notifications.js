import Vue from "vue";

const datastore = {
    drag: false,
    SMS: "",
    email: "",
    desktop: "",
    error: "",
    isSaving: false,
};

function getPostValues() {
    return {
        SMS: datastore.SMS,
        email: datastore.email,
        desktop: datastore.desktop,
    };
}

const el = "#edit-notifications";

const existingNotifications = $(el).data("notifications");
const existingAccess = $(el).data("access");
if (existingNotifications) {
    datastore.isExistingDevice = true;
    datastore.SMS = existingNotifications.SMS;
    datastore.email = existingNotifications.email;
    datastore.desktop = existingNotifications.desktop;
}

let deleteAccessCallback = null;

const vm = new Vue({
    el,
    data: datastore,
    methods: {
        save: (evt) => {
            evt.preventDefault();
            const url = $(el).attr("action");
            datastore.isSaving = true;
            $.ajax({
                method: "POST",
                url,
                data: { ...getPostValues() },
                success: (data, status, jqXhr) => {
                    console.log({ data });
                    window.location.href = "/manager/profile/notifications";
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                    datastore.isSaving = false;
                    if (jqXHR.responseJSON) {
                        datastore.error = jqXHR.responseJSON.errMsg;
                    }
                },
                dataType: "json",
            });
        },
    },
});
