import { exists } from "fs";

$("#loginForm").submit(function (event) {
    event.preventDefault(); //prevent default action
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission
    $.ajax({
        url: post_url,
        type: request_method,
        data: form_data,
        dataType: "json",
    }).done(function (response) {
        $(".alert-danger").hide();
        if (true === response.error) {
            $(".alert-danger").html(response.description);
            $(".alert-danger").show();
        } else {
            if (response.mfa == true){
                $("#login_wrap").attr("hidden", "hidden");
                $("#mfa").removeAttr("hidden");
            }else{
                window.location.href = "/manager/";
            }
        }
    });
});

$("#auth").submit(function (event) {
    event.preventDefault(); //prevent default action
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission
    $.ajax({
        url: post_url,
        type: request_method,
        data: form_data,
        dataType: "json",
    }).done(function (response) {
        $(".alert-danger").hide();
        if (response.error === true) {
            $(".alert-danger").html(response.description);
            $(".alert-danger").show();
        } else {
            window.location.href = "/manager/";
        }
    });
});
