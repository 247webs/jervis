$("#edit-role").submit(function(event) {
    event.preventDefault(); //prevent default action
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission

    $.ajax({
        url: post_url,
        type: request_method,
        data: form_data,
    }).done(function(response) {
        var propertyId = $("#propertyId").val();
        window.location.href = "/manager/role/index?property_id=" + propertyId;
    });
});

$(".permission-group").on("change", function() {
    $(this)
        .siblings("div")
        .find("input[type='checkbox']")
        .prop("checked", this.checked);
});

function parentChecked() {
    $(".permission-group").each(function() {
        var allChecked = true;
        $(this)
            .siblings("div")
            .find("input[type='checkbox']")
            .each(function() {
                if (!this.checked) allChecked = false;
            });
        $(this).prop("checked", allChecked);
    });
}

parentChecked();

$(".the-permission").on("change", function() {
    parentChecked();
});
