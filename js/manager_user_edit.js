$(".deleteuser").on("click", function() {
    var userId = $(this).data("userid");
    $.ajax({
        method: "POST",
        url: "/manager/user/deleteUser",
        data: {
            userId: userId,
        },
        success: () => {
            console.log("Delete success");
            window.location.reload();
        },
        error: () => {
            console.log("Delete error");
            window.location.reload();
        },
    });
});

$(document).on("click", ".updateroll", function() {
    var property_id = $(".proprtyid").val();
    var formData = $("#rollupdateform").serialize();
    $.ajax({
        method: "POST",
        url: "/manager/user/roll-update",
        data: formData,
        dataType: "json",
        success: function(responce) {
            window.location.href = "manager/user/index?property_id=" + property_id;
        },
        error: function(e) {
            toastr.error("error");
        },
    });
});

$(document).on("click", ".editrole", function() {
    var userId = $(this).data("id");
    var roleid = $(this).data("roleid");
    var count = roleid.length;
    if (count != undefined) {
        var result = roleid.split(",");
    } else {
        var result = $.makeArray(roleid).toString();
    }
    $(".userid").val(userId);
    $(".multiselect-container li").each(function(index, item) {
        var inputCtrl = $(item).find("input[type='checkbox']");
        if (result.indexOf(inputCtrl.val()) >= 0) {
            $(item).addClass("active");
            $("#role_name option[value='" + inputCtrl.val().toString() + "']").attr("selected", "selected");
        }
    });
    $("button.multiselect").text("");
    $(".multiselect-container li").each(function(index, item) {
        if ($(item).hasClass("active")) {
            var options = $(".selectpicker option");
            var buttonTex = $(options[index]).text();
            if ($("button.multiselect").text().length > 0) {
                var val = $("button.multiselect").text();
                $("button.multiselect").text(val + ", " + buttonTex);
            } else {
                $("button.multiselect").text(buttonTex);
            }
        }
    });
});
