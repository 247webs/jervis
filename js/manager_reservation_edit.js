import Vue from "vue";

const datastore = {
    drag: false,
    booking_source: "",
    source_reservation_id: "",
    check_in_date: "",
    check_out_date: "",
    no_of_guest: "",
    no_of_infants: "",
    no_of_toddlers: "",
    email: "",
    phone: "",
    first_name: "",
    last_name: "",
    business_name: "",
    country: "",
    state: "",
    city: "",
    address: "",
    zipcode: "",
    notes: "",
    guest_id: "",
    error: "",
    isSaving: false,
};

function getPostValues() {
    return {
        bookingSource: datastore.booking_source,
        sourceReservationId: datastore.source_reservation_id,
        checkInDate: $("#checkInDate")
            .find("input")
            .val(),
        checkOutDate: $("#checkOutDate")
            .find("input")
            .val(),
        noOfGuest: datastore.no_of_guest,
        noOfInfants: datastore.no_of_infants,
        noOfToddlers: datastore.no_of_toddlers,
        email: datastore.email,
        phone: $("#phone").val(),
        firstName: datastore.first_name,
        lastName: datastore.last_name,
        businessName: datastore.business_name,
        country: $("#countryId").val(),
        state: datastore.state,
        city: datastore.city,
        address: datastore.address,
        zipcode: datastore.zipcode,
        notes: datastore.notes,
        propertyId: $("#reservationPropertyId").val(),
        guestId: datastore.guest_id,
    };
}

const el = "#edit-reservation";

const existingReservation = $(el).data("reservation");
const existingAccess = $(el).data("access");
if (existingReservation) {
    datastore.isExistingReservation = true;
    datastore.booking_source = existingReservation.booking_source;
    datastore.source_reservation_id = existingReservation.source_reservation_id;
    datastore.check_in_date = existingReservation.check_in_date;
    datastore.check_out_date = existingReservation.check_out_date;
    datastore.no_of_guest = existingReservation.no_of_guest;
    datastore.no_of_infants = existingReservation.no_of_infants;
    datastore.no_of_toddlers = existingReservation.no_of_toddlers;
    datastore.email = existingReservation.email;
    datastore.phone = existingReservation.phone;
    datastore.first_name = existingReservation.first_name;
    datastore.last_name = existingReservation.last_name;
    datastore.business_name = existingReservation.business_name;
    datastore.country = existingReservation.country;
    datastore.state = existingReservation.state;
    datastore.city = existingReservation.city;
    datastore.address = existingReservation.address;
    datastore.zipcode = existingReservation.zipcode;
    datastore.notes = existingReservation.notes;
    datastore.guest_id = existingReservation.guest_id;
}

let deleteAccessCallback = null;

const vm = new Vue({
    el,
    data: datastore,
    methods: {
        save: (evt) => {
            evt.preventDefault();
            const url = $(el).attr("action");
            datastore.isSaving = true;
            $.ajax({
                method: "POST",
                url,
                data: { ...getPostValues() },
                success: (data, status, jqXhr) => {
                    console.log({ data });
                    if (!data.success) {
                        $("#limitNotification")
                            .find(".modal-body .form-group")
                            .html("");
                        $("#limitNotification")
                            .find(".modal-body .form-group")
                            .html(data.message);
                        $("#limitNotification").modal("show");
                    } else {
                        var reservationPropertyId = $("#reservationPropertyId").val();
                        window.location.href = "/manager/reservation/index?property_id=" + reservationPropertyId;
                    }
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                    datastore.isSaving = false;
                    if (jqXHR.responseJSON) {
                        datastore.error = jqXHR.responseJSON.errMsg;
                    }
                },
                dataType: "json",
            });
        },
        verifyEmail: function(event) {
            event.preventDefault();
            $.ajax({
                method: "POST",
                url: "manager/reservation/api/verify-email",
                dataType: "json",
                data: { email: vm.email },
                success: (data, status, jqXhr) => {
                    if (data.success) {
                        this.guest_id = data.success.id;
                        this.phone = data.success.phone;
                        this.first_name = data.success.first_name;
                        this.last_name = data.success.last_name;
                        this.business_name = data.success.business_name;
                        this.address = data.success.address;
                        this.country = data.success.country;
                        //this.state.bfhstates({ country: data.success.country, state: data.success.state });
                        this.zipcode = data.success.zipcode;
                        this.city = data.success.city;
                    } else {
                        this.guest_id = "";
                    }
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                },
            });
        },
        verifyPhone: function(event) {
            event.preventDefault();
            $.ajax({
                method: "POST",
                url: "manager/reservation/api/verify-phone",
                dataType: "json",
                data: { phone: vm.phone },
                success: (data, status, jqXhr) => {
                    if (data.success) {
                        this.guest_id = data.success.id;
                        this.email = data.success.email;
                        this.first_name = data.success.first_name;
                        this.last_name = data.success.last_name;
                        this.business_name = data.success.business_name;
                        this.address = data.success.address;
                        this.country = data.success.country;
                        //this.state.bfhstates({ country: data.success.country, state: data.success.state });
                        this.zipcode = data.success.zipcode;
                        this.city = data.success.city;
                    } else {
                        this.guest_id = "";
                    }
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                },
            });
        },
    },
});

// Delete role modal
$("#deleteModal").on("show.bs.modal", function(event) {
    const modal = $(this);
    modal.find("button.do-delete").click((evt) => {
        evt.preventDefault();
        $("#deleteModal").modal("hide");
    });
});
$(document).ready(function() {
    /* $("#checkInDate").datetimepicker({
        format: "yyyy-mm-dd hh:ii:00",
        pick12HourFormat: false,
        setDate: $("#checkInDate input").val(),
        todayHighlight: true,
        pickerPosition: "bottom-left",
        autoclose: true,
        startDate: new Date(),
    });
    $("#checkOutDate").datetimepicker({
        format: "yyyy-mm-dd hh:ii:00",
        pick12HourFormat: false,
        setDate: $("#checkOutDate input").val(),
        todayHighlight: true,
        pickerPosition: "bottom-left",
        autoclose: true,
        startDate: new Date(),
    });*/
});
