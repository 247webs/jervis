import {
    exists
} from "fs";

$("#signup").submit(function (event) {

    event.preventDefault(); //prevent default action
    var btn = $(".sbmt_btn");

    var current = $('a.active').parent('li').index();
    var next = current + 1;
    var second_next = current + 2;

    if (next != 5) {
        $("ul#myTab li:nth-child(" + next + ") a").removeClass("active");
        $("ul#myTab li:nth-child(" + next + ")").addClass("complete-step");
        $("ul#myTab li:nth-child(" + second_next + ")  a").addClass("active");
        $(".step" + current).attr("hidden", "hidden");
        $(".step" + next).removeAttr("hidden");
    }

    if (next == 1) {
        $("#countryId").attr('required', true);
        $("#stateId").attr('required', true);
        $("#cityId").attr('required', true);
        $("#zipcode").attr('required', true);
    } else if (next == 2) {
        $("#password").attr('required', true);
        $("#confirmPassword").attr('required', true);
    } else if (next == 3) {
        $("#role").attr('required', true);
        $("#companyName").attr('required', true);
    } else if (next == 4) {
        $("#otp").attr('required', true);
        $(".sbmt_btn").html("Finish");
    }

    if (next == 5) {
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = $(this).serialize(); //Encode form elements for submission
        $.ajax({
            url: post_url,
            type: request_method,
            data: form_data,
            dataType: "json",
        }).done(function (response) {
            if (response.invalid_otp != undefined && response.invalid_otp == true) {
                $("#invalidOtp").removeAttr("hidden");
                document.getElementById('invalidOtp').innerHTML = response.message;
            } else {
                $("ul#myTab li:nth-child(" + next + ") a").removeClass("active");
                $("ul#myTab li:nth-child(" + next + ")").addClass("complete-step");
                $("ul#myTab li:nth-child(" + second_next + ")  a").addClass("active");
                $(".step" + current).attr("hidden", "hidden");

                if (true === response.error) {
                    $("#error h3.success-message").html(response.description);
                    $("#signup").parent('div.step-form').hide();
                    $("#error").removeAttr("hidden");
                } else {
                    $("#signup").parent('div.step-form').hide();
                    $("#success").removeAttr("hidden");
                }
            }
        });
    }
});

$('#mfa').click(function (event) {
    if (event.target.checked == true) {
        $("#otp").attr('required', true);
        $("#otp").removeAttr('readonly');

    } else {
        $("#otp").removeAttr('required');
        $("#otp").attr('readonly', true);
    }
});