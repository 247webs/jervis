import Vue from "vue";
const el = "#invoice";
const ul = "#invoiceform";
var property_id = $(".proprtyid").val();
let vm = new Vue({
    el,
    delimiters: ["[[", "]]"],
    data: {
        inputs: [
            {
                name: "",
                quantity: 1,
                rate: 1,
                answer: 1,
            },
        ],
        deductinputs:[
            {
                deductitemname: "",
                deductamount:"",
            }
        ],
    },
    methods: {
        save: (evt) => {
            evt.preventDefault();
            $.ajax({
                method: "POST",
                url: "/manager/invoice/save-invoice?property_id=" + property_id,
                data: $(ul).serializeArray(),
                dataType: "json",
                success: (response, statresponseus, jqXhr) => {
                    window.location.href = "manager/invoice/index?property_id=" + property_id;
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                },
            });
        },
        add(index) {
            this.inputs.push({
                name: "",
                quantity: 0,
                rate: 0,
                answer: 0,
            });
        },
        adddeduct(index) {
            this.deductinputs.push({
                deductitemname: "",
                deductamount:"",
            });
        },
        remove(index) {
            this.inputs.splice(index, 1);
        },
        removes(index) {
            this.deductinputs.splice(index, 1);
        },
        totaldeduction:function () {
            return this.deductinputs.reduce(function (totaldeduct, item) {
               var value = $('.pervalue').val();
               var subtotal = $('.subtotal').val();
               if(value == "$"){
                   return totaldeduct + item.deductamount;
               }
               else{
                return (subtotal * item.deductamount) / 100;
               }
               
            }, 0);
        },
        subtotal: function () {
            return  this.inputs.reduce(function (total, item) {
                var final =  total + item.answer;
                return final;
            }, 0);
        },
        total: function () {
            var total_deduction = $('.totaldeduct').val();
            var subtotal = $('.subtotal').val();
            var total = subtotal - total_deduction;
            return total;
        },
    },

});

$('.pervalue').on("change", function(){
    $('.deductamt').val("");
    $('.total').val("");
    $('.totaldeduct').val("");
});

