$("#edit-plan").submit(function(event) {
    event.preventDefault(); //prevent default action
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission

    $.ajax({
        url: post_url,
        type: request_method,
        data: form_data,
    }).done(function(response) {
        window.location.href = "/admin";
    });
});

$("#deleteModal").on("show.bs.modal", function(event) {
    const $button = $(event.relatedTarget); // Button that triggered the modal
    const managerId = $button.closest("tr").data("manager-id");

    var modal = $(this);
    deleteButton = modal.find("button.do-delete");
    deleteButton.click(() => {
        deleteButton.find(".spinner-border").removeClass("invisible");
        $.ajax({
            method: "DELETE",
            url: "/admin/user/api/delete-user/" + managerId,
            success: () => {
                console.log("Delete success");
                window.location.reload();
            },
            error: () => {
                console.log("Delete error");
                window.location.reload();
            },
        });
    });
});
