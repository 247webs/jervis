$(document).ready(function () {
    if (typeof currentProperty  !== 'undefined') {
        getDonutChartData();
        getSalesavgChartYearData();
    }
});

//SalesavgChart

$(".salesWeek").on("click", function (event) {
    if (typeof currentProperty  !== 'undefined') {
        $('.salesMonth').removeClass('active');
        $('.salesYear').removeClass('active');
        $('.salesWeek').addClass('active');
        getSalesavgChartWeekData();
    }
});
$(".salesMonth").on("click", function (event) {
    if (typeof currentProperty  !== 'undefined') {
        $('.salesYear').removeClass('active');
        $('.salesWeek').removeClass('active');
        $('.salesMonth').addClass('active');    
        getSalesavgChartMonthData();
    }
});
$(".salesYear").on("click", function (event) {
    if (typeof currentProperty  !== 'undefined') {
        $('.salesWeek').removeClass('active');
        $('.salesMonth').removeClass('active');
        $('.salesYear').addClass('active');
        getSalesavgChartYearData();
    }
});
function getSalesavgChartWeekData() {
    var weekArr = ["1", "2", "3", "4", "5", "6", "7"];
    let Propertyid = currentProperty;
    var label_data = [];
    var week_data = [];
    $.ajax({
        method: "POST",
        url: "/manager/getAllSalesavgChartByPropertyId",
        data: {
            Propertyid: Propertyid,
            str: "week",
        },
        dataType: "json",
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                week = response[i]["week"];
                data1 = parseInt(response[i]["total"]);
                week_data.push(week);
                label_data.push(data1);
            }

            createSalesavgChart(weekArr, label_data);
        },
    });
}

function getSalesavgChartMonthData() {
    var monthArr = [];
    for (var i = 1; i <= 31; i++) {
        monthArr.push(i.toString());
    }

    let Propertyid = currentProperty;
    var label_data = [];
    var month_data = [];
    $.ajax({
        method: "POST",
        url: "/manager/getAllSalesavgChartByPropertyId",
        data: {
            Propertyid: Propertyid,
            str: "month",
        },
        dataType: "json",
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                month = response[i]["month"];
                data1 = parseInt(response[i]["total"]);
                month_data.push(month);
                label_data.push(data1);
            }

            createSalesavgChart(monthArr, label_data);
        },
    });
}
function getSalesavgChartYearData() {
    var yearArr = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", "SEP", "OCT", "NOV", "DEC"];
    let Propertyid = currentProperty;
    var label_data = [];
    $.ajax({
        method: "POST",
        url: "/manager/getAllSalesavgChartByPropertyId",
        data: {
            Propertyid: Propertyid,
            str: "year",
        },
        dataType: "json",
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                data1 = parseInt(response[i]["total"]);
                label_data.push(data1);
            }

            createSalesavgChart(yearArr, label_data);
        },
    });
}
function createSalesavgChart(categories, data) {
    document.getElementById("sales-line-chart").innerHTML = "";
    var $primary = "#03b8b8";
    var $danger = "#EA5455";
    var $warning = "#FF9F43";
    var $info = "#00cfe8";
    var $success = "#00db89";
    var $primary_light = "#9c8cfc";
    var $warning_light = "#FFC085";
    var $danger_light = "#f29292";
    var $info_light = "#1edec5";
    var $strok_color = "#b9c3cd";
    var $label_color = "#e7eef7";
    var $purple = "#03b8b8";
    var $white = "#fff";

    // Sales  Chart
    // -----------------------------

    var salesavgChartoptions = {
        chart: {
            height: 270,
            toolbar: { show: false },
            type: "line",
            dropShadow: {
                enabled: true,
                top: 20,
                left: 2,
                blur: 6,
                color: $primary,
                opacity: 0.2,
            },
        },
        stroke: {
            curve: "smooth",
            width: 4,
        },
        grid: {
            borderColor: $label_color,
        },
        legend: {
            show: false,
        },
        colors: [$purple],
        fill: {
            type: "gradient",
            gradient: {
                shade: "light",
                inverseColors: false,
                gradientToColors: [$primary],
                shadeIntensity: 1,
                type: "horizontal",
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100, 100, 100],
            },
        },
        markers: {
            size: 0,
            hover: {
                size: 5,
            },
        },
        xaxis: {
            labels: {
                style: {
                    colors: $strok_color,
                },
            },
            axisTicks: {
                show: false,
            },
            categories: categories,
            axisBorder: {
                show: false,
            },
            tickPlacement: "on",
        },
        yaxis: {
            tickAmount: 5,
            labels: {
                style: {
                    color: $strok_color,
                },
                formatter: function (val) {
                    return val > 999 ? (val / 1000).toFixed(1) + "k" : val;
                },
            },
        },
        tooltip: {
            x: { show: false },
        },
        series: [
            {
                name: "&nbsp;",
                data: data,
            },
        ],
    };

    var salesavgChart = new ApexCharts(document.querySelector("#sales-line-chart"), salesavgChartoptions);

    salesavgChart.render();
}

// Donut Chart
function donutChart_Display(label_data) {
    var $primary = "#17cccc",
        $success = "#fd9625",
        $danger = "#65f3f3",
        $warning = "#FF9F43",
        $info = "#00cfe8",
        $label_color_light = "#dae1e7";

    var themeColors = [$primary, $success, $danger, $warning, $info];

    // RTL Support
    var yaxis_opposite = false;
    if ($("html").data("textdirection") == "rtl") {
        yaxis_opposite = true;
    }

    var donutChartOptions = {
        chart: {
            type: "donut",
            // height: 320,
        },
        colors: themeColors,
        series: label_data,
        labels: ["Completed:", "Upcoming:"],
        legend: {
            itemMargin: {
                horizontal: 2,
            },
        },
        responsive: [
            {
                breakpoint: 480,
                options: {
                    chart: {
                        // width: 320
                    },
                    legend: {
                        position: "bottom",
                    },
                },
            },
        ],
    };
    var donutChart = new ApexCharts(document.querySelector("#donut-chart"), donutChartOptions);

    donutChart.render();
}

function getDonutChartData() {
    let Propertyid = currentProperty;
    var label_data = [];
    $.ajax({
        method: "POST",
        url: "/manager/getAllReservationsByPropertyId",
        data: {
            Propertyid: Propertyid,
        },
        dataType: "json",
        success: function (response) {
            if (response.complate > 0) {
                label_data[0] = (parseInt(response.complate) * 100) / parseInt(response.total);
            } else {
                label_data[0] = 0;
            }

            if (response.upcoming > 0) {
                label_data[1] = (parseInt(response.upcoming) * 100) / parseInt(response.total);
            } else {
                label_data[1] = 0;
            }
            donutChart_Display(label_data);
        },
    });
}


$('.month').on("click",function(){
    if (typeof currentProperty  !== 'undefined') {
        let Propertyid = currentProperty;
        $.ajax({
            method: "POST",
            url: "/manager/getMonthlyReservationsByPropertyId",
            data: {
                Propertyid: Propertyid,
            },
            dataType: "json",
            success: function (response) {
            $('.upcomingdata').empty();
            $('.upcomingdata').html(response);

            },
        });
    }
});
$('.year').on("click",function(){
    if (typeof currentProperty  !== 'undefined') {
        let Propertyid = currentProperty;
        $.ajax({
            method: "POST",
            url: "/manager/getYearlyReservationsByPropertyId",
            data: {
                Propertyid: Propertyid,
            },
            dataType: "json",
            success: function (response) {
            $('.upcomingdata').empty();
            $('.upcomingdata').html(response);

            },
        });
    }
});
$('.completed').on("click", function(){
    $('.incomplatetable').hide();
    $('.complatetable').show();
   
});
$('.incomplete').on("click", function(){ 
    $('.complatetable').hide();
    $('.incomplatetable').show();
});
$('.Upcomingdateslider').on("click", function(){ 
    $('.Upcomingdateslider').removeClass("current-date");
    $(this).addClass("current-date");
    if (typeof currentProperty  !== 'undefined') {
    let Propertyid = currentProperty;
        var upcomingsliderDate = $(this).attr("data-upcomingsliderDate");
        $.ajax({
            method: "POST",
            url: "/manager/getUpcomingdatesliderData",
            data: {
                upcomingsliderDate:upcomingsliderDate,
                Propertyid: Propertyid,
            },
            dataType: "json",
            success: function (response) {
            $('.upcomingdata').empty();
            $('.upcomingdata').html(response);
            },
        });
    }
});


