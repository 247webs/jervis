$("#deleteModal").on("show.bs.modal", function(event) {
    const $button = $(event.relatedTarget); // Button that triggered the modal
    const propertyId = $button.closest("tr").data("property-id");

    var modal = $(this);
    deleteButton = modal.find("button.do-delete");
    deleteButton.click(() => {
        deleteButton.find(".spinner-border").removeClass("invisible");
        $.ajax({
            method: "DELETE",
            url: "/manager/property/api/delete-property/" + propertyId,
            success: () => {
                console.log("Delete success");
                window.location.reload();
            },
            error: () => {
                console.log("Delete error");
                window.location.reload();
            },
        });
    });
});
