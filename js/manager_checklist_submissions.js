// Original code from old times
function deleteSubmission(id) {
    if (confirm("Are you sure to delete?") == false) return false;

    $("#" + id).animate(
        {
            backgroundColor: "#ffbcbc",
        },
        2000
    );
    $.get("manager/checklist/api/delete-submission/" + id, function(data, textStatus) {
        var r = $.parseJSON(data);
        if (r.ok) {
            location.reload();
        } else {
            alert("Something went wrong");
        }
    });
}

const $fillRequestModal = $("#fill-request");
const $fillRequestForm = $fillRequestModal.find("form");

const $submitButton = $fillRequestForm.find("button[type=submit]");
const $spinner = $submitButton.find(".spinner-border");

// Validation and form submission
$fillRequestForm.on("submit", evt => {
    evt.preventDefault();

    $spinner.removeClass("invisible");

    $.ajax({
        method: "POST",
        url: $fillRequestForm.attr("action"),
        data: $fillRequestForm.serialize(),
        success: (data, textStatus, jqXHR) => {
            console.log("Success", textStatus, data);

            // Close modal
            $fillRequestModal.modal("hide");
            window.location.reload();
            $spinner.addClass("invisible");
        },
        error: (jqXHR, textStatus, errorThrown) => {
            console.log("Error", textStatus);
            $spinner.addClass("invisible");
        },
    });
});
$(document).ready(function() {
   
});