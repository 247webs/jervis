$(".id").on("click", function () {
    var invoiceId = $(this).data("invoiceid");
    $.ajax({
        method: "POST",
        url: "/manager/invoice/deleteInvoice",
        data: {
            invoiceId: invoiceId,
        },
        success: () => {
            console.log("Delete success");
            window.location.reload();
        },
        error: () => {
            console.log("Delete error");
            window.location.reload();
        },
    });
});
