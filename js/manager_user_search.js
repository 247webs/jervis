import Vue from "vue";

const el = "#search_form";

const vm = new Vue({
    el,
    searchData: [],
    methods: {
        search: (evt) => {
            evt.preventDefault();
            $("#user_list_table").removeAttr("hidden");
            $("#user_list_table").find("tbody").html("");
            const url = $(el).attr("action");
            $.ajax({
                method: "POST",
                url,
                data: $(el).serializeArray(),
                success: (response, status, jqXhr) => {
                    if (response.length) {
                        $.each(response, function (index, value) {
                            $("#user_list_table")
                                .find("tbody")
                                .append(
                                    "<tr><td>" +
                                        value.first_name +
                                        "</td><td>" +
                                        value.last_name +
                                        "</td><td>" +
                                        value.email +
                                        '</td><td><button data-firstname="' +
                                        value.first_name +
                                        '" data-lastname="' +
                                        value.last_name +
                                        '" data-email="' +
                                        value.email +
                                        '" class="btn btn-primary inviteRoll">Invite</button></td></tr>'
                                );
                        });
                    } else {
                        $("#user_list_table").find("tbody").append('<tr><td colspan="4">No Record Found</td></tr>');
                    }
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                },
                dataType: "json",
            });
        },
    },
});

$(document).on("click", ".inviteRoll", function () {
    var firstname = $(this).data("firstname");
    var lastname = $(this).data("lastname");
    var email = $(this).data("email");
    $("#firstname").val(firstname);
    $("#lastname").val(lastname);
    $("#email").val(email);
    $("#inviteRole").modal("show");
});
window.VM = vm;
{
    const $inviteRoleModal = $("#inviteRole");
    const $inviteRoleForm = $inviteRoleModal.find("form");
    const $propertyIdField = $inviteRoleForm.find("input[name=propertyid]");
    const $emailField = $inviteRoleForm.find("input[name=email]");
    const $roleIdField = $inviteRoleForm.find("select[name=role_id]");
    const $submitButton = $inviteRoleForm.find("button[type=submit]");
    const $spinner = $submitButton.find(".spinner-border");

    // When the modal dialog shows up, focus on the input field.
    $inviteRoleModal.on("shown.bs.modal", () => $emailField.focus());

    // Utility function to verify email
    const isEmailValid = () => {
        const email = $emailField.val();
        return email.length >= 5 && email.indexOf("@") >= 0 && email.indexOf(".") >= 0;
    };

    // Validation and form submission
    $inviteRoleForm.on("submit", (evt) => {
        evt.preventDefault();

        // Show error if email is invalid
        if (!isEmailValid()) {
            $emailField.addClass("is-invalid");
            $emailField.removeClass("is-valid");
            return;
        }
        // If the email is valid, remove the fields
        $emailField.addClass("is-valid");
        $emailField.removeClass("is-invalid");

        const email = $emailField.val();
        const role_id = $roleIdField.val();
        console.log("Invite", email);
        $spinner.removeClass("invisible");
        const propertyId = $propertyIdField.val();

        $.ajax({
            method: "POST",
            url: `/manager/property/api/invite-to-role/${propertyId}`,
            data: { email: email, role_id: role_id },
            success: (data, textStatus, jqXHR) => {
                console.log("Success", textStatus, data);

                // Close modal and reset
                $emailField.val("");
                $inviteRoleModal.modal("hide");
                $spinner.addClass("invisible");
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log("Error", textStatus);
                $spinner.addClass("invisible");
            },
        });
    });
}
window.VM = vm;
{
    const $inviteRoleModal = $("#inviteRoleUser");
    const $inviteRoleForm = $inviteRoleModal.find("form");
    const $propertyIdField = $inviteRoleForm.find("input[name=propertyid]");
    const $emailField = $inviteRoleForm.find("input[name=email]");
    const $roleIdField = $inviteRoleForm.find("select[name=role_id]");
    const $submitButton = $inviteRoleForm.find("button[type=submit]");
    const $spinner = $submitButton.find(".spinner-border");

    // When the modal dialog shows up, focus on the input field.
    $inviteRoleModal.on("shown.bs.modal", () => $emailField.focus());

    // Utility function to verify email
    const isEmailValid = () => {
        const email = $emailField.val();
        return email.length >= 5 && email.indexOf("@") >= 0 && email.indexOf(".") >= 0;
    };

    // Validation and form submission
    $inviteRoleForm.on("submit", (evt) => {
        evt.preventDefault();

        // Show error if email is invalid
        if (!isEmailValid()) {
            $emailField.addClass("is-invalid");
            $emailField.removeClass("is-valid");
            return;
        }
        // If the email is valid, remove the fields
        $emailField.addClass("is-valid");
        $emailField.removeClass("is-invalid");

        const email = $emailField.val();
        const role_id = $roleIdField.val();
        console.log("Invite", email);
        $spinner.removeClass("invisible");
        const propertyId = $propertyIdField.val();

        $.ajax({
            method: "POST",
            url: `/manager/property/api/invite-to-role/${propertyId}`,
            data: { email: email, role_id: role_id },
            success: (data, textStatus, jqXHR) => {
                console.log("Success", textStatus, data);

                // Close modal and reset
                $emailField.val("");
                $inviteRoleModal.modal("hide");
                $spinner.addClass("invisible");
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log("Error", textStatus);
                $spinner.addClass("invisible");
            },
        });
    });
}
