$("#deleteModal").on("show.bs.modal", function(event) {
    const $button = $(event.relatedTarget); // Button that triggered the modal
    const roleId = $button.closest("tr").data("role-id");

    var modal = $(this);
    deleteButton = modal.find("button.do-delete");
    deleteButton.click(() => {
        deleteButton.find(".spinner-border").removeClass("invisible");
        $.ajax({
            method: "DELETE",
            url: "/manager/role/api/delete-role/" + roleId,
            success: () => {
                console.log("Delete success");
                window.location.reload();
            },
            error: () => {
                console.log("Delete error");
                window.location.reload();
            },
        });
    });
});

$(".viewModal").on("click", function(event) {
    var roleId = $(this).attr("data-id");
    $(".view-modal-body").load("/manager/role/view-role?id=" + roleId, function() {
        $("#viewModal").modal({ show: true });
    });
});

$(document).on("click", ".invite", function() {
    var roleid = $(this).data("roleid");
    $(".inviterole").val(roleid);
    $("[name=role_id]").val(roleid); //To select Blue
});
