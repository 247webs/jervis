var cardNumber = "";
var stripe = "";
function createStripeClient(publishable_key) {
    // Create a Stripe client
    stripe = Stripe(publishable_key);
    // Create an instance of Elements
    var elements = stripe.elements();
    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: "#32325d",
            lineHeight: "18px",
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
                color: "#aab7c4",
            },
        },
        invalid: {
            color: "#fa755a",
            iconColor: "#fa755a",
        },
    };
    
    // Floating labels
    var inputs = document.querySelectorAll('#card_form .input');
    Array.prototype.forEach.call(inputs, function (input) {
        input.addEventListener('focus', function () {
            input.classList.add('focused');
        });
        input.addEventListener('blur', function () {
            input.classList.remove('focused');
        });
        input.addEventListener('keyup', function () {
            if (input.value.length === 0) {
                input.classList.add('empty');
            } else {
                input.classList.remove('empty');
            }
        });
    });
    
    var elementClasses = {
        focus: 'form-control',
        empty: 'form-control',
        invalid: 'form-control',
        complete: 'form-control',
      };
  
    // Create an instance of the card Element
    //card = elements.create("card", { hidePostalCode: true }, { style: style });
    // Add an instance of the card Element into the `card-element` <div>
   // card.mount("#card-element");
   
    cardNumber = elements.create('cardNumber', {
        style: style,
        classes: elementClasses,
    });
    cardNumber.mount('#card_number');

    var cardExpiry = elements.create('cardExpiry', {
        style: style,
        classes: elementClasses,
    });
    cardExpiry.mount('#card_expiry');

    var cardCvc = elements.create('cardCvc', {
        style: style,
        classes: elementClasses,
    });
    cardCvc.mount('#card_cvc');
   
    // Handle real-time validation errors from the card Element.
    cardNumber.addEventListener("change", function(event) {
        var displayError = document.getElementById("card-errors");
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = "";
        }
    });
    cardExpiry.addEventListener("change", function(event) {
        var displayError = document.getElementById("card-errors");
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = "";
        }
    });
    cardCvc.addEventListener("change", function(event) {
        var displayError = document.getElementById("card-errors");
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = "";
        }
    });
}
jQuery(document).on("click", "#card_submit", function() {
    handleFormSubmission();
});
function handleFormSubmission() {
    // Handle form submission
    var form = document.getElementById("card_form");

    //form.addEventListener('submit', function(event) {
    $("#card_form").submit(function(event) {
        
        event.preventDefault();
        $("#card_submit").attr("disabled", "disabled");
        stripe.createToken(cardNumber).then(function(result) {            
            if (result.error) {
                $("#card_submit").removeAttr("disabled");
                // Inform the user if there was an error
                var errorElement = document.getElementById("card-errors");
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server
                stripeTokenHandler(result.token);
            }
        });
        event.preventDefault();
        //        event.unbind();
    });
}

function stripeTokenHandler(token) {
    $.ajax({
        type: "POST",
        url: $("#card_form").attr("action"),
        dataType: "json",
        async: false,
        data: {
            stripeToken: token.id,
            planId: $("#plan_id").length ? $("#plan_id").val() : null,
        },
        success: function(response) {
            $("#card_submit").removeAttr("disabled");
            var error_body = $("form").find(".error");
            if (response.success.length > 0) {
                showErrorMsg(error_body, "success", response.success);
                setTimeout(function() {
                    window.open("manager/profile/billing", "_self");
                }, 500);
            } else {
                showErrorMsg(error_body, "danger", response.error);
            }
            $("#card_submit").removeAttr("disabled");
        },
        error: function(x, t, m) {
            $("#card_submit").removeAttr("disabled");
        },
    });
}
jQuery(document).on("click", "#update_card_btn", function() {
    $("#update_card_btn_block").attr("hidden", "hidden");
    $(".update_card_form").removeAttr("hidden");
});

jQuery(document).ready(function() {
    createStripeClient(publishable_key);
});
function showErrorMsg(form, type, msg) {
    var alert = $(
        '<div class="alert alert-' +
            type +
            ' alert-dismissible" role="alert">\
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times"></i></button>\
                    <span></span>\
            </div>'
    );

    form.find(".alert").remove();
    alert.prependTo(form);
    //    alert.animateClass('fadeIn animated');
    alert.find("span").html(msg);
}
