// Setup the calendar with the current date
$(document).ready(function() {
    var date = new Date();
    var today = date.getDate();
    // Set click handlers for DOM elements
    $(".right-button").click({ date: date }, next_year);
    $(".left-button").click({ date: date }, prev_year);
    $(".month").click({ date: date }, month_click);
    $("#add-button").click({ date: date }, new_event);
    // Set current month as active
    $(".months-row")
        .children()
        .eq(date.getMonth())
        .addClass("active-month");

    init_calendar(date);
    var events = check_events(today, date.getMonth() + 1, date.getFullYear());
    show_events(events, months[date.getMonth()], today);
});

// Initialize the calendar by appending the HTML dates
function init_calendar(date) {
    $(".tbody").empty();
    $(".events-container").empty();
    var calendar_days = $(".tbody");
    var month = date.getMonth();
    var year = date.getFullYear();
    var day_count = days_in_month(month, year);
    var row = $("<tr class='table-row'></tr>");
    var today = date.getDate();
    // Set date to 1 to find the first day of the month
    date.setDate(1);
    var first_day = date.getDay();
    var wrkdayoffcondition = "";
    var wrkdaycondition = "(day >= currentday && month == monthcurr)";
    var ab = "||( day >= 1 && month == monthcurr+1)";
    var wrkdaycondition1 = "";
    var i;
    if (wrkday != null) {
        for (i = 0; i < wrkday.length; i++) {
            wrkdaycondition1 += ' && (dayname!="' + wrkday[i] + '")';
        }
    }
    if (datatimeoff != "null") {
        var datatimeoff1 = JSON.parse(datatimeoff);
        var count = datatimeoff1[0];
        var obj = datatimeoff1[0];
        for (var i = 0; i < count.length; i++) {
            var mydates = Number(new Date(obj[i].startdate));
            var mydates1 = Number(new Date(obj[i].enddate));
            wrkdayoffcondition += '&& (myInt > "' + mydates1 + '" || myInt < "' + mydates + '")';
        }
    }
    // alert(wrkdayoffcondition);
    wrkdaycondition += wrkdayoffcondition += wrkdaycondition1 += ab += wrkdayoffcondition += wrkdaycondition1;

    // 35+firstDay is the number of date elements to be added to the dates table
    // 35 is from (7 days in a week) * (up to 5 rows of dates in a month)
    for (var i = 0; i < 35 + first_day; i++) {
        // Since some of the elements will be blank,
        // need to calculate actual date from index
        var day = i - first_day + 1;
        // If it is a sunday, make a new row
        if (i % 7 === 0) {
            calendar_days.append(row);
            row = $("<tr class='table-row'></tr>");
        }

        // if current index isn't a day in this month, make it blank
        if (i < first_day || day > day_count) {
            var curr_date = $("<td class='table-date nil'>" + "</td>");
            row.append(curr_date);
        } else {
            var curr_date = $("<td class='table-date'>" + day + "</td>");
            var events = check_events(day, month + 1, year);
            if (today === day && $(".active-date").length === 0) {
                curr_date.addClass("active-date");
                show_events(events, year, months[month], day);
            }

            // If this date has any events, style it with .event-date
            if (events.length !== 0) {
                curr_date.addClass("event-date");
            }
            var newdate = new Date();
            var currentday = newdate.getDate();
            var monthcurr = newdate.getMonth();

            if ((day >= currentday && month >= monthcurr) || (day >= 1 && month == monthcurr + 1)) {
                var todaydate = month + 1 + "/" + day + "/" + year;
                var myInt = Number(new Date(todaydate));
                //alert(timeoffdate);
                // var myInt12 = Number(new Date('1/8/2020'));
                // alert(myInt12);
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var a = new Date(todaydate);
                var dayname = weekday[a.getDay()];
                var daymonth = months[month] + " " + day;
                curr_date.click(
                    {
                        events: events,
                        year: year,
                        dayname: dayname,
                        month: months[month],
                        daymonth: daymonth,
                        myInt: myInt,
                        day: day,
                    },
                    date_click
                );
            }
            row.append(curr_date);

            if (eval(wrkdaycondition)) {
                curr_date.addClass("act-future");
                show_events(events, year, months[month], day);
            }
        }
    }
    // Append the last row and set the current year
    calendar_days.append(row);

    $(".year").text(months[date.getMonth()] + "  " + year);

    $(".slotyear").text(year);
    $(".timeblogyear").text(year);
    $(".timeblogyear").val(year);
}

// Get the number of days in a given month/year
function days_in_month(month, year) {
    var monthStart = new Date(year, month, 1);
    var monthEnd = new Date(year, month + 1, 1);
    return (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
}

// Event handler for when a date is clicked
function date_click(event) {
    $(".active-date").removeClass("active-date");
    $(this).addClass("active-date");
    var date = event.data.month + " " + event.data.day;
    var year = event.data.year;
    var todayday = event.data.dayname;
    //alert();
    var todaydate =
        new Date(Date.parse(event.data.month + " " + event.data.day + ", " + event.data.year)).getMonth() +
        1 +
        "/" +
        event.data.day +
        "/" +
        event.data.year;
    var myInt = Number(new Date(todaydate));
    sessionStorage.setItem("dataabc", todayday);

    if ($(this).hasClass("act-future")) {
        $(".timeblogdate").text(date);
        $(".timeblogdayname").text(todayday);
        $(".timeblogdate").val(date);
        $(".timeblogdayname").val(todayday);
        $(".calenderdetail").hide();
        $(".timeblog").show();
        $(".loader").show();
        $.ajax({
            url: "/manager/schedule/timeslot-data",
            type: "POST",
            data: {
                todayday: todayday,
                date: date,
                myInt: myInt,
                year: year,
                userid: userid,
                propertyId: propertyId,
            },
            dataType: "json",
            success: function(responce) {
                $(".loader").hide();
                $(".timeblog").html("");
                $(".timeblog").html(responce);
            },
            error: function(e) {
                //alert("error");
            },
        });
    } else {
        $(".notbook").show();
        setTimeout(function() {
            $("div.alert").remove();
        }, 3000); // 5 secs
    }
}

// Event handler for when a month is clicked
function month_click(event) {
    $(".events-container").show(250);
    $("#dialog").hide(250);
    var date = event.data.date;
    $(".active-month").removeClass("active-month");
    $(this).addClass("active-month");
    var new_month = $(".month").index(this);
    date.setMonth(new_month);
    init_calendar(date);
}

// Event handler for when the year right-button is clicked
function next_year(event) {
    $("#dialog").hide(250);
    var date = event.data.date;
    var new_year = date.getMonth() + 1;
    $("year").html(new_year);
    date.setMonth(new_year);
    init_calendar(date);
}

// Event handler for when the year left-button is clicked
function prev_year(event) {
    $("#dialog").hide(250);
    var date = event.data.date;
    var new_year = date.getMonth() - 1;
    $("month").html(new_year);
    date.setMonth(new_year);
    init_calendar(date);
}

// Event handler for clicking the new event button
function new_event(event) {
    // if a date isn't selected then do nothing
    if ($(".active-date").length === 0) return;
    // remove red error input on click
    $("input").click(function() {
        $(this).removeClass("error-input");
    });
    // empty inputs and hide events
    $("#dialog input[type=text]").val("");
    $("#dialog input[type=number]").val("");
    $(".events-container").hide(250);
    $("#dialog").show(250);
    // Event handler for cancel button
    $("#cancel-button").click(function() {
        $("#name").removeClass("error-input");
        $("#count").removeClass("error-input");
        $("#dialog").hide(250);
        $(".events-container").show(250);
    });
    // Event handler for ok button
    $("#ok-button")
        .unbind()
        .click({ date: event.data.date }, function() {
            var date = event.data.date;
            var name = $("#name")
                .val()
                .trim();
            var count = parseInt(
                $("#count")
                    .val()
                    .trim()
            );
            var day = parseInt($(".active-date").html());
            // Basic
            validation;
            if (name.length === 0) {
                $("#name").addClass("error-input");
            } else if (isNaN(count)) {
                $("#count").addClass("error-input");
            } else {
                $("#dialog").hide(250);
                console.log("new event");
                new_event_json(name, count, date, day);
                date.setDate(day);
                init_calendar(date);
            }
        });
}

// Adds a json event to event_data
function new_event_json(name, count, date, day, date) {
    var event = {
        occasion: name,
        invited_count: count,
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: day,
    };
    event_data["events"].push(event);
}

// Display all events of the selected date in card views
function show_events(events, month, day) {
    // Clear the dates container
    $(".events-container").empty();
    $(".events-container").show(250);
    console.log(event_data["events"]);
    // If there are no events for this date, notify the user
    if (events.length === 0) {
        var event_card = $("<div class='event-card'></div>");
        var event_name = $("<div class='event-name'>There are no events planned for " + month + " " + day + ".</div>");
        $(event_card).css({ "border-left": "10px solid #FF1744" });
        $(event_card).append(event_name);
        $(".events-container").append(event_card);
    } else {
        // Go through and add each event as a card to the events container
        for (var i = 0; i < events.length; i++) {
            var event_card = $("<div class='event-card'></div>");
            var event_name = $("<div class='event-name'>" + events[i]["occasion"] + ":</div>");
            var event_count = $("<div class='event-count'>" + events[i]["invited_count"] + " Invited</div>");
            if (events[i]["cancelled"] === true) {
                $(event_card).css({
                    "border-left": "10px solid #FF1744",
                });
                event_count = $("<div class='event-cancelled'>Cancelled</div>");
            }
            $(event_card)
                .append(event_name)
                .append(event_count);
            $(".events-container").append(event_card);
        }
    }
}

// Checks if a specific date has any events
function check_events(day, month, year) {
    var events = [];
    for (var i = 0; i < event_data["events"].length; i++) {
        var event = event_data["events"][i];
        if (event["day"] === day && event["month"] === month && event["year"] === year) {
            events.push(event);
        }
    }
    return events;
}

// Given data for events in JSON format
var event_data = {
    events: [],
};

const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
];
