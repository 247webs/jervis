$(document).on("click", ".requestbtn", function (event) {
    var btn = $(this);
    btn.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", true);
    event.preventDefault();
    var property_id = $(".propertyid").val();
    var formData = $("#notificationdata").serialize();
    $.ajax({
        method: "POST",
        url: "/manager/schedule/add-notification",
        data: formData,
        dataType: "json",
        success: () => {
            $("#appoinmentbooking").modal("hide");
            setTimeout(function () {
                btn.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr(
                    "disabled",
                    false
                );
            }, 300);
         
            window.location.href = "/manager/schedule/appointment-notification?property_id=" + property_id;
        },
        error: () => {
            setTimeout(function () {
                btn.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr(
                    "disabled",
                    false
                );
            }, 300);
          
        },
    });
});

$(".deleteappoinment").on("click", function () {
    var appoinmentId = $(this).data("appoinmentid");
    $.ajax({
        method: "POST",
        url: "/manager/schedule/delete-appoinment-data",
        data: {
            appoinmentId: appoinmentId,
        },
        success: () => {
            console.log("Delete success");
            window.location.reload();
        },
        error: () => {
            console.log("Delete error");
            window.location.reload();
        },
    });
});

$(document).on("click", ".acceptbtn", function (event) {
    event.preventDefault();
    var id = $(this).data("id");
    var email = $(this).data("email");
    var date = $(this).data("date");
    var time = $(this).data("time");
    $("#id").val(id);
    $("#email").val(email);
    $("#date").val(date);
    $("#time").val(time);
});

$(document).on("click", ".otherdetailsbtn", function (event) {
    event.preventDefault();
    var service = $(this).data("service");
    var name = $(this).data("name");
    var propertytitle = $(this).data("propertytitle");
    var propertylocation = $(this).data("propertylocation");
	var propertytype = $(this).data("propertytype");
	var zipcode = $(this).data("zipcode");
    var propertysqft = $(this).data("propertysqft");
    var bedroomsqft = $(this).data("bedroomsqft");
    var bathroomsqft = $(this).data("bathroomsqft");
    var cleaningreason = $(this).data("cleaningreason");
    var cleaningfrequency = $(this).data("cleaningfrequency");
    var estimate = $(this).data("estimate");
    $("#service").text(service);
    $("#name").text(name);
    $("#propertytitle").text(propertytitle);
    $("#propertylocation").text(propertylocation);
	$("#propertytype").text(propertytype);
	$("#zipcode").text(zipcode);
    $("#propertysqft").text(propertysqft);
    $("#bedroomsqft").text(bedroomsqft);
    $("#bathroomsqft").text(bathroomsqft);
    $("#cleaningreason").text(cleaningreason);
    $("#cleaningfrequency").text(cleaningfrequency);
    $("#estimate").text(estimate);
});

$(document).on("click", ".confirmappoinmentbtn", function (event) {
    var btn = $(this);
    btn.addClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr("disabled", true);
    event.preventDefault();
    var formData = $("#confirmappoinmentdata").serialize();
    $.ajax({
        method: "POST",
        url: "/manager/schedule/confirm-appoinment",
        data: formData,
        dataType: "json",
        success: () => {
            $("#confirmappoinment").modal("hide");
            setTimeout(function () {
                btn.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr(
                    "disabled",
                    false
                );
            }, 300);           
            window.location.reload();
        },
        error: () => {
            setTimeout(function () {
                btn.removeClass("kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light").attr(
                    "disabled",
                    false
                );
            }, 300);            
        },
    });
});
