import Vue from "vue";

const datastore = {
    drag: false,
    first_name: "",
    last_name: "",
    email: "",
    mobile: "",
    user_time_zone: "",
    error: "",
    isSaving: false,
};

function getPostValues() {
    return {
        first_name: datastore.first_name,
        last_name: datastore.last_name,
        email: datastore.email,
        mobile: datastore.mobile,
        user_time_zone: datastore.user_time_zone,
    };
}

const el = "#edit-profile";

const existingManager = $(el).data("manager");
const existingAccess = $(el).data("access");
if (existingManager) {
    datastore.isExistingDevice = true;
    datastore.first_name = existingManager.first_name;
    datastore.last_name = existingManager.last_name;
    datastore.email = existingManager.email;
    datastore.mobile = existingManager.mobile;
    datastore.user_time_zone = existingManager.user_time_zone;
}

let deleteAccessCallback = null;

const vm = new Vue({
    el,
    data: datastore,
    methods: {
        save: (evt) => {
            evt.preventDefault();
            const url = $(el).attr("action");
            datastore.isSaving = true;
            $.ajax({
                method: "POST",
                url,
                data: { ...getPostValues() },
                success: (data, status, jqXhr) => {
                    console.log({ data });
                    window.location.href = "/manager/profile/index";
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                    datastore.isSaving = false;
                    if (jqXHR.responseJSON) {
                        datastore.error = jqXHR.responseJSON.errMsg;
                    }
                },
                dataType: "json",
            });
        },
    },
});
