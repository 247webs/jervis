import Vue from "vue";

const datastore = {
    drag: false,
    name: "",
    address: "",
    email: "",
    phone: "",
    error: "",
    isSaving: false,
};

function getPostValues() {
    return {
        name: datastore.name,
        address: datastore.address,
        email: datastore.email,
        phone: datastore.phone,
    };
}

const el = "#edit-company";

const existingCompany = $(el).data("company");
const existingAccess = $(el).data("access");
if (existingCompany) {
    datastore.isExistingDevice = true;
    datastore.name = existingCompany.name;
    datastore.address = existingCompany.address;
    datastore.email = existingCompany.email;
    datastore.phone = existingCompany.phone;
}

let deleteAccessCallback = null;

const vm = new Vue({
    el,
    data: datastore,
    methods: {
        save: (evt) => {
            evt.preventDefault();
            const url = $(el).attr("action");
            datastore.isSaving = true;
            $.ajax({
                method: "POST",
                url,
                data: { ...getPostValues() },
                success: (data, status, jqXhr) => {
                    console.log({ data });
                    window.location.href = "/manager/profile/company";
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                    datastore.isSaving = false;
                    if (jqXHR.responseJSON) {
                        datastore.error = jqXHR.responseJSON.errMsg;
                    }
                },
                dataType: "json",
            });
        },
    },
});
