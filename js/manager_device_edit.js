import Vue from "vue";

const datastore = {
    drag: false,
    title: "",
    description: "",
    type: "",
    error: "",
    isSaving: false,
};

function getPostValues() {
    return {
        title: datastore.title,
        description: datastore.description,
        type: datastore.type,
        propertyId: $("#devicePropertyId").val(),
    };
}

const el = "#edit-device";

const existingDevice = $(el).data("device");
const existingAccess = $(el).data("access");
if (existingDevice) {
    datastore.isExistingDevice = true;
    datastore.title = existingDevice.title;
    datastore.description = existingDevice.description;
    datastore.type = existingDevice.type;
}

let deleteAccessCallback = null;

const vm = new Vue({
    el,
    data: datastore,
    methods: {
        save: evt => {
            evt.preventDefault();
            const url = $(el).attr("action");
            datastore.isSaving = true;
            $.ajax({
                method: "POST",
                url,
                data: { ...getPostValues() },
                success: (data, status, jqXhr) => {
                    var devicePropertyId = $("#devicePropertyId").val();
                    window.location.href = "/manager/device/index?property_id=" + devicePropertyId;
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                    datastore.isSaving = false;
                    if (jqXHR.responseJSON) {
                        datastore.error = jqXHR.responseJSON.errMsg;
                    }
                },
                dataType: "json",
            });
        },
    },
});

// Delete role modal
$("#deleteModal").on("show.bs.modal", function (event) {
    const modal = $(this);
    modal.find("button.do-delete").click(evt => {
        evt.preventDefault();
        $("#deleteModal").modal("hide");
    });
});
