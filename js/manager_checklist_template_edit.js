import Vue from "vue";
import draggable from "vuedraggable";

import TaskItem from "./TaskItem";
import TaskGroup from "./TaskGroup";

const datastore = {
    drag: false,
    title: "",
    code: "",
    notes: "",
    error: "",
    groups: [],
    recurring: "",
    to_whome: "",
    duration: "",
    before_after: "",
    event: "",
    subject: "",
};

function hasEmptyLastGroup() {
    if (datastore.groups.length == 0) {
        console.log("hasEmptyLastGroup(a) => false");
        return false;
    }
    const group = datastore.groups[datastore.groups.length - 1];
    return group.isEmpty();
}

function ensureEmptyLastGroup() {
    if (hasEmptyLastGroup()) {
        return;
    }
    datastore.groups.push(new TaskGroup());
}

function ensureEmptyTask(group) {
    for (let i = group.tasks.length - 1; i >= 1; i--) {
        if (group.tasks[i].isEmpty()) {
            group.tasks.pop();
        } else {
            break;
        }
    }
    group.tasks.push(new TaskItem());
}

function getPostValues() {
    return {
        title: datastore.title,
        code: datastore.code,
        notes: datastore.notes,
        groups: datastore.groups
            .filter((group) => !group.isEmpty())
            .map((group) => {
                return {
                    title: group.title,
                    tasks: group.tasks.filter((task) => !task.isEmpty()).map((task) => task.title),
                };
            }),
        recurring: datastore.recurring,
        to_whome: datastore.to_whome,
        duration: datastore.duration,
        before_after: datastore.before_after,
        event: datastore.event,
        subject: datastore.subject,
    };
}

const el = "#edit-checklist-template";

const existingData = $(el).data("template");
if (existingData) {
    console.log(existingData);
    datastore.title = existingData.title;
    datastore.code = existingData.code;
    datastore.notes = existingData.notes;

    datastore.recurring = existingData.recurring;
    datastore.to_whome = existingData.to_whome;
    datastore.duration = existingData.duration;
    datastore.before_after = existingData.before_after;
    datastore.event = existingData.event;
    datastore.subject = existingData.subject;

    // Create TaskGroups and TaskItems from data
    existingData.groups.map((group) => {
        datastore.groups.push(
            new TaskGroup(
                group.title,
                group.tasks.map((task) => new TaskItem(task))
            )
        );
    });
}

ensureEmptyLastGroup();

const vm = new Vue({
    el,
    data: datastore,
    methods: {
        updateItems: (group) => {
            // Trim empty trailing tasks in group
            ensureEmptyTask(group);
        },
        updateGroups: () => {
            // Trim trailing groups with no title and no tasks
            while (hasEmptyLastGroup()) {
                datastore.groups.pop();
            }
            ensureEmptyLastGroup();
        },
        deleteGroup: (groupIdx) => {
            datastore.groups.splice(groupIdx, 1);
            ensureEmptyLastGroup();
        },
        sortGroup: (group) => {
            ensureEmptyTask(group);
        },
        sortGroups: () => {
            ensureEmptyLastGroup();
        },
        save: (evt) => {
            evt.preventDefault();
            const url = $(el).attr("action");
            $.ajax({
                method: "POST",
                url,
                data: { template: JSON.stringify(getPostValues()) },
                success: (data, status, jqXhr) => {
                    console.log({ data });
                    window.location.href = `/manager/checklist/template?property_id=${data.template.property_id}`;
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    if (jqXHR.responseJSON) {
                        datastore.error = jqXHR.responseJSON.errMsg;
                    } else {
                        console.log(jqXHR.responseText);
                    }
                },
                dataType: "json",
            });
        },
    },
    components: {
        draggable,
    },
});

$(document).ready(function() {
    if ($("#recurring").is(":checked")) {
        $("#scheduleInfo").removeClass("invisible");
        $("#scheduleInfo").removeClass("collapse");
    } else {
        $("#scheduleInfo").addClass("invisible");
        $("#scheduleInfo").addClass("collapse");
    }
});

$("#recurring").change(function() {
    if ($(this).is(":checked")) {
        $("#scheduleInfo").removeClass("invisible");
        $("#scheduleInfo").removeClass("collapse");
    } else {
        $("#scheduleInfo").addClass("invisible");
        $("#scheduleInfo").addClass("collapse");
    }
});

$("#duration").change(function() {
    var when = $(this).val();
    var index = jQuery.inArray(when, ["Weekly", "Monthly", "Quarterly", "Yearly"]);
    if (index > -1) {
        $("#before_after, #event")
            .parent()
            .hide();
    } else {
        $("#before_after, #event")
            .parent()
            .show();
    }
});