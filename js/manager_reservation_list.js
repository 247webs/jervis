var deleteReservationId ="";
$(document).on("click", ".deleteBtn", function (event) {
  event.preventDefault();
   deleteReservationId = $(this).attr("id").replace("delete-", "");
  $("#deleteModal").modal("show");
});

$(".do-delete").on("click", function () {   
    $(".spinner-border").removeClass("invisible");
    $("#deleteModal").modal("hide");
    $.ajax({
        method: "DELETE",
        url: "/manager/reservation/api/delete-reservation/" + deleteReservationId,
        success: function success() {
            console.log("Delete success");
            window.location.reload();
        },
        error: function error() {
            console.log("Delete error");
            window.location.reload();
        }
    });
});

var reservationId = "";
$(document).on("click", ".notificationBtn", function(event) {
    event.preventDefault();
    reservationId = $(this).attr("id").replace("notification-", "");    
    $("#notification > tbody").html("");
    getReservationNotification(reservationId);
    $('.tooltip ').removeClass('show');
    $('.tooltip ').addClass('hide');
    $("#notificationModal").modal("show");
});

function getReservationNotification(reservationId) {
    $.ajax({
        method: "POST",
        url: "/manager/reservation/api/get-reservation-notification/" + reservationId,
        dataType: "json",
        success: (response) => {
            console.log("success");
            $("#notification > tbody").html("");
            var $tableBody = "";

            $.each(response.success, function(key, val) {
                $tableBody +=
                    '<tr data-notification-id="' + val.id + '"><td>' + val.title + "</td><td>" + val.status + "</td>";
                $tableBody += "</tr>";
            });

            $("#notification > tbody").html($tableBody);
        },
        error: () => {
            console.log("error");
        },
    });
}
