import Vue from "vue";

const el = "#add_review";

const vm = new Vue({
    el,
    reviewData: [],
    methods: {
        save: (evt) => {
            evt.preventDefault();
            const url = $(el).attr("action");
            $.ajax({
                method: "POST",
                url,
                data: $(el).serializeArray(),
                success: (response, status, jqXhr) => {
                    toastr.success("Thank you for Review");
                    window.location.href = "/manager/";
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log("error", textStatus, jqXHR.responseText);
                },
                dataType: "json",
            });
        },
    },
});
