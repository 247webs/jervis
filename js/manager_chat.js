var is_edit_chat_message = false;
var edit_chat_message_id = "";
var is_edit_group_chat_message = false;
var edit_group_chat_message_id = "";

$(document).ready(function() {
    $(".chatbox").each(function() {
        if ($(this).data("id") == latestUserId) {
            $(this).trigger("click");
        }
    });
});
$(document).on("keyup", "#search", function() {
    var value = $(this).val();
    $(".chatbox").each(function() {
        if (
            $(this)
                .data("name")
                .search(value) > -1
        ) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
});
$(".usertitle").text("Start Chat");
$(document).on("click", ".chatbox", function() {
    $(".fa-user-plus").attr("data-target", "#addperson");
    var id = $(this).data("id");
    var name = $(this).data("name");
    $(".usertitle").text(name);
    $.ajax({
        method: "POST",
        url: "/manager/chat/getChatMessage",
        data: { id: id },
        dataType: "json",
        success: function(response) {
            $(".chatnotification" + id).remove();
            $(".to_group_id").val("");
            $(".leatest_chat_id").val(response.leatest_id.chat_message_id);
            $(".msgbox").html("");
            $(".msgbox").html(response.output);
            $(".to_user_id").val(response.to_user_id);
            $(".ScrollStyle").animate({ scrollTop: $(".ScrollStyle")[0].scrollHeight }, "slow");

            init_tooltips();

            return false;
        },
        error: function(e) {
            console.log("error");
        },
    });

    refreshChatMessage();
});

$(document).on("click", ".edit_chat_message", function() {
    $('[data-toggle="tooltip"]').tooltip("hide");
    $("body")
        .filter(".tooltip")
        .hide();
    edit_chat_message_id = $(this).attr("id");
    var message = $(".text" + edit_chat_message_id + " .chat_text").text();
    $(".message")
        .focus()
        .val(message);
    is_edit_chat_message = true;
});

$(document).on("click", ".edit_group_chat_message", function() {
    $('[data-toggle="tooltip"]').tooltip("hide");
    $("body")
        .filter(".tooltip")
        .hide();
    edit_group_chat_message_id = $(this).attr("id");
    var message = $(".text" + edit_group_chat_message_id + " .chat_text").text();
    $(".message")
        .focus()
        .val(message);
    is_edit_group_chat_message = true;
});

$(document).on("click", ".groupchatbox", function() {
    $(".fa-user-plus").attr("data-target", "#editperson");
    var groupid = $(this).data("id");
    $(".usertitle").text("group");
    $.ajax({
        method: "POST",
        url: "/manager/chat/getGroupChatMessage",
        data: {
            groupid: groupid,
        },
        dataType: "json",
        success: function(response) {
            $(".chatnotification" + groupid).remove();
            $(".msgbox").html("");
            $(".msgbox").html(response.chatData);
            $(".newpersondata").html(response.NonGroupPerson);
            $(".leatest_chat_id").val(response.leatest_id.id);
            $(".to_user_id").val("");
            $(".to_group_id").val(groupid);
            $(".ScrollStyle").animate({ scrollTop: $(".ScrollStyle")[0].scrollHeight }, "slow");

            init_tooltips();

            return false;
        },
        error: function(e) {
            console.log("error");
        },
    });
    //    setInterval(function () {
    refreshChatMessage();
    //    }, 3000);
});

$(".message").keypress(function(event) {
    var keycode = event.keyCode ? event.keyCode : event.which;
    if (keycode == "13" && !event.shiftKey) {
        event.preventDefault();
        saveChatMessage();
        return false;
    }
});

function saveChatMessage() {
    var url = "";
    var groupid = "";
    var message = $(".message").val();
    var chatDivId = makeid(5);
    var id = $(".to_user_id").val();
    var currentdate = new Date();
    var datetime =
        currentdate.getFullYear() +
        "-" +
        (currentdate.getMonth() + 1) +
        "-" +
        currentdate.getDate() +
        " " +
        currentdate.getHours() +
        ":" +
        currentdate.getMinutes();
    var chatClass = "";
    if (is_edit_chat_message) {
        id = edit_chat_message_id;
        url = "/manager/chat/api/updateChatMessage";
    } else if (is_edit_group_chat_message) {
        id = edit_group_chat_message_id;
        url = "/manager/chat/api/updateGroupChatMessage";
    } else {
        if (id == "") {
            chatClass = "groupchatdiv";
            groupid = $(".to_group_id").val();
            url = "/manager/chat/api/saveGroupChatMessage";
            $(".groupchatdiv").append(
                '<div class="chat chat-right chat__message"  id="' +
                    chatDivId +
                    '"><div class="chat-body"><div class="chat-content edit_chat_message"><p><strong>You</strong></p><p class="chat_text">' +
                    message +
                    '</p><p class="chat-time text-right chat__datetime">' +
                    datetime +
                    '</p></div></div></div> <i style="display:block" class="fa flaticon-delete deletegroupchat"></i>'
            );
        } else {
            chatClass = "chatdiv";
            url = "/manager/chat/api/saveChatMessage";
            $(".chatdiv").append(
                '<div class="chat chat-right chat__message"  id="' +
                    chatDivId +
                    '"><div class="chat-body"><div class="chat-content edit_chat_message"><p><strong>You</strong></p><p class="chat_text">' +
                    message +
                    '</p><p class="chat-time text-right chat__datetime">' +
                    datetime +
                    '</p></div></div></div> <i style="display:block" class="fa flaticon-delete deletechat"></i>'
            );
        }
    }
    $(".message").text("");
    $.ajax({
        method: "POST",
        url: url,
        data: {
            id: id,
            to_user_id: $(".to_user_id").val(),
            groupid: groupid,
            message: message,
        },
        dataType: "json",
        success: function(response) {
            console.log(response);
            if (is_edit_chat_message) {
                edit_chat_message_id = "";
                is_edit_chat_message = false;
                $(".chatdiv > .text" + id + " > .chat_text").html();
                $(".chatdiv > .text" + id + " > .chat_text").html(message);
                $(".chatdiv > .text" + id + " > .kt-chat__user > .chat__datetime").html(
                    $.trim(
                        $(".chatdiv > .text" + id + " > .kt-chat__user > .chat__datetime")
                            .html()
                            .replace(/<([^ >]+)[^>]*>.*?<\/\1>|<[^\/]+\/>/gi, "")
                    )
                );
                $(".chatdiv > .text" + id + " > .kt-chat__user > .chat__datetime").html(
                    $(".chatdiv > .text" + id + " .kt-chat__user > .chat__datetime").html() + " <b>Edited</b>"
                );
            } else if (is_edit_group_chat_message) {
                edit_group_chat_message_id = "";
                is_edit_group_chat_message = false;
                $(".groupchatdiv > .text" + id + " > .chat_text").html();
                $(".groupchatdiv > .text" + id + " > .chat_text").html(message);
                $(".groupchatdiv > .text" + id + " > .kt-chat__user > .chat__datetime").html(
                    $.trim(
                        $(".groupchatdiv > .text" + id + " > .kt-chat__user > .chat__datetime")
                            .html()
                            .replace(/<([^ >]+)[^>]*>.*?<\/\1>|<[^\/]+\/>/gi, "")
                    )
                );
                $(".groupchatdiv > .text" + id + " > .kt-chat__user > .chat__datetime").html(
                    $(".groupchatdiv > .text" + id + " .kt-chat__user > .chat__datetime").html() + " <b>Edited</b>"
                );
            } else {
                init_tooltips();
                $("#" + chatDivId).attr("data-chatid", response);
                $("#" + chatDivId).addClass("text" + response);
                if (chatClass == "chatdiv") {
                    $("#" + chatDivId + " > .deletechat").attr("data-id", response);
                    $("#" + chatDivId + " >  .chat_text").attr(
                        "data-original-title",
                        "<span class='edit_chat_message' id='" + response + "'>Edit</span>"
                    );
                } else {
                    $("#" + chatDivId + " > .deletegroupchat").attr("data-id", response);
                    $("#" + chatDivId + " >  .chat_text").attr(
                        "data-original-title",
                        "<span class='edit_group_chat_message' id='" + response + "'>Edit</span>"
                    );
                }
            }
            $(".message").val("");
            $(".ScrollStyle").animate({ scrollTop: $(".ScrollStyle")[0].scrollHeight }, "slow");
            return false;
        },
        error: function(e) {
            console.log("error");
        },
    });
}

function makeid(length) {
    var result = "";
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

$(document).on("click", ".sendchat", function() {
    saveChatMessage();
});

$(document).on("click", ".deletechat", function() {
    var id = $(this).data("id");
    $.ajax({
        method: "POST",
        url: "/manager/chat/api/deleteChat",
        data: {
            id: id,
            to_user_id: $(".to_user_id").val(),
        },
        dataType: "json",
        success: function(response) {
            $(".text" + id)
                .find(".chat_text")
                .html('<small><i class="fa fa-ban" aria-hidden="true"></i> You deleted this message</small>');
            $(".text" + id + " > .chat_text").removeAttr("data-toggle title data-original-title aria-describedby");
            $(".text" + id + " > .kt-chat__user > .chat__datetime").html(
                $.trim(
                    $(".text" + id + " > .kt-chat__user > .chat__datetime")
                        .html()
                        .replace(/<([^ >]+)[^>]*>.*?<\/\1>|<[^\/]+\/>/gi, "")
                )
            );
            $(".text" + id)
                .find(".deletechat")
                .remove();
        },
        error: function(e) {
            console.log("error");
        },
    });
});

$(document).on("click", ".deletegroupchat", function() {
    var id = $(this).data("id");
    $.ajax({
        method: "POST",
        url: "/manager/chat/api/deleteGroupChat",
        data: {
            id: id,
            groupid: $(".to_group_id").val(),
        },
        dataType: "json",
        success: function(response) {
            //            $(".text" + id).remove();
            $(".text" + id)
                .find(".chat_text")
                .html('<small><i class="fa fa-ban" aria-hidden="true"></i> You deleted this message</small>');
            $(".text" + id + " > .chat_text").removeAttr("data-toggle title data-original-title aria-describedby");
            $(".text" + id + " > .kt-chat__user > .chat__datetime").html(
                $.trim(
                    $(".text" + id + " > .kt-chat__user > .chat__datetime")
                        .html()
                        .replace(/<([^ >]+)[^>]*>.*?<\/\1>|<[^\/]+\/>/gi, "")
                )
            );
            $(".text" + id)
                .find(".deletegroupchat")
                .remove();
        },
        error: function(e) {
            console.log("error");
        },
    });
});

$(document).on("click", ".addgroupperson", function(event) {
    event.preventDefault();
    var formData = $("#add_group_person").serialize();
    $.ajax({
        method: "POST",
        url: "/manager/chat/api/addGroup",
        data: formData,
        dataType: "json",
        success: function(response) {
            location.reload();
        },
        error: function(e) {
            console.log("error");
        },
    });
});

$(document).on("click", ".editgroupperson", function(event) {
    event.preventDefault();
    var formData = $("#edit_group_person").serialize();
    $.ajax({
        method: "POST",
        url: "/manager/chat/api/editGroup",
        data: formData,
        dataType: "json",
        success: function(response) {
            location.reload();
        },
        error: function(e) {
            console.log("error");
        },
    });
});

function refreshChatMessage() {
    var num = $(".chat__message")
        .map(function() {
            return $(this).data("chatid");
        })
        .get();

    if (num.length > 0) {
        var leatest_chat_id = Math.max.apply(Math, num);
    } else {
        var leatest_chat_id = 0;
    }

    var id = $(".to_user_id").val();
    if (id != "") {
        $.ajax({
            method: "POST",
            url: "/manager/chat/refreshChatMessage",
            data: {
                id: id,
                leatest_chat_id: leatest_chat_id,
            },
            dataType: "json",
            success: function(response) {
                $(".leatest_chat_id").val(response.leatest_id.chat_message_id);
                var output = jQuery.parseJSON(JSON.stringify(response.output));
                $.each(output, function(key, value) {
                    if ($(".chat__message").hasClass("text" + key)) {
                        modify_chat_message(key, value, "chatdiv");
                    } else {
                        $(".chatdiv").append(value.new_message);
                        $(".ScrollStyle").animate({ scrollTop: $(".ScrollStyle")[0].scrollHeight }, "slow");
                        init_tooltips();
                    }
                });

                setTimeout(function() {
                    refreshChatMessage();
                }, 3000);
            },
        });
    } else {
        var groupid = $(".to_group_id").val();
        $.ajax({
            method: "POST",
            url: "/manager/chat/refreshGroupChatMessage",
            data: {
                groupid: groupid,
                leatest_chat_id: leatest_chat_id,
            },
            dataType: "json",
            success: function(response) {
                $(".leatest_chat_id").val(response.leatest_id.chat_message_id);

                var output = jQuery.parseJSON(JSON.stringify(response.chatData));
                $.each(output, function(key, value) {
                    if ($(".chat__message").hasClass("text" + key)) {
                        modify_chat_message(key, value, "groupchatdiv");
                    } else {
                        $(".groupchatdiv").append(value.new_message);
                        $(".ScrollStyle").animate({ scrollTop: $(".ScrollStyle")[0].scrollHeight }, "slow");
                        init_tooltips();
                    }
                });
                setTimeout(function() {
                    refreshChatMessage();
                }, 3000);
            },
        });
    }
}

function modify_chat_message(chat_id, chat_value, chat_div) {
    //  $("."+chat_div+" > .text" + key).html($(value).filter(".text" + key).html());
    $("." + chat_div + " > .text" + chat_id + " > .chat_text").html();
    $("." + chat_div + " > .text" + chat_id + " > .chat_text").html(chat_value.message);
    if (chat_value.status == 1) {
        $("." + chat_div + " > .text" + chat_id)
            .find(".deletegroupchat")
            .remove();
        $("." + chat_div + " > .text" + chat_id + " > .chat_text").removeAttr(
            "data-toggle title data-original-title aria-describedby"
        );
        $("." + chat_div + " > .text" + chat_id + " > .kt-chat__user > .chat__datetime").html(
            $.trim(
                $("." + chat_div + " > .text" + chat_id + " > .kt-chat__user > .chat__datetime")
                    .html()
                    .replace(/<([^ >]+)[^>]*>.*?<\/\1>|<[^\/]+\/>/gi, "")
            )
        );
    }
    if (chat_value.status == 2) {
        $("." + chat_div + " > .text" + chat_id + " > .kt-chat__user > .chat__datetime").html(
            $.trim(
                $("." + chat_div + " > .text" + chat_id + " > .kt-chat__user > .chat__datetime")
                    .html()
                    .replace(/<([^ >]+)[^>]*>.*?<\/\1>|<[^\/]+\/>/gi, "")
            )
        );
        $("." + chat_div + " > .text" + chat_id + " > .kt-chat__user > .chat__datetime").html(
            $("." + chat_div + " > .text" + chat_id + " > .kt-chat__user > .chat__datetime").html() + " <b>Edited</b>"
        );
    }
}

function init_tooltips() {
    $('[data-toggle="tooltip"]').tooltip({
        html: true,
        placement: "top",
        trigger: "click",
        container: "body",
        animation: true,
    });
    $('[data-toggle="tooltip"]').click(function(event) {
        event.stopPropagation();
    });
    $("body").click(function() {
        $('[data-toggle="tooltip"]').tooltip("hide");
    });
}
