export default class TaskItem {
    constructor(title = "") {
        this.title = title;
    }

    isEmpty() {
        return this.title.trim() == "";
    }
}
