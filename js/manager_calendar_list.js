$("#deleteModal").on("show.bs.modal", function(event) {
    const $button = $(event.relatedTarget); // Button that triggered the modal
    const calendarId = $button.closest("tr").data("calendar-id");

    var modal = $(this);
    deleteButton = modal.find("button.do-delete");
    deleteButton.click(() => {
        deleteButton.find(".spinner-border").removeClass("invisible");
        $.ajax({
            method: "DELETE",
            url: "/manager/calendar/api/delete-calendar/" + calendarId,
            success: () => {
                console.log("Delete success");
                window.location.reload();
            },
            error: () => {
                console.log("Delete error");
                window.location.reload();
            },
        });
    });
});
