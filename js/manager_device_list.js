$(".property_select").on("change", function (event) {
  var select = $(event.target);
  var deviceId = select.closest("tr").data("device-id");
  var propertyId = $(this).val();
  $.ajax({
    method: "UPDATE",
    url: "/manager/device/api/assign-device/" + deviceId + "/" + propertyId,
    success: function success(d) {
      console.log("Assign success");
      window.location = "/manager/device/index?successMsg=Device Assigned";
    },
    error: function error() {
      console.log("Assign error");
      window.location = "/manager/device/index?errorMsg=You don't have access to this property";
    }
  });
});
$(".action_select").on("change", function (event) {
  var url = $(this).val();
  if (url) {
    window.location = url;
  }
  return false;
});
$(".state_select").on("change", function (event) {
  var url = $(this).val();
  if (url) {
    window.location = url;
  }
  return false;
});

$(function () {
  setTimeout(function () {
    $(".alert").hide(500)
  }, 5000);
});