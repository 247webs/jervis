import Vue from "vue";
import { listenerCount } from "cluster";
import VueCropper from "vue-cropperjs";
Vue.component(VueCropper);

let logoData = null;

const datastore = {
    drag: false,
    title: "",
    address: "",
    logo: null,
    type: "",
    country: "",
    state: "",
    city: "",
    zipcode: "",
    property_square_footage: "",
    no_of_bedroom: "",
    no_of_bathroom: "",
    reason_for_cleaning: "",
    frequency_of_cleanings: "",
    current_rental_cleaning_fee: "",
    cleaning_estimate: "",
    error: "",
    access: [],
    isSaving: false,
    isExistingProperty: false,
};

function getPostValues() {
    return {
        title: datastore.title,
        address: datastore.address,
        logo: datastore.logo,
        type: datastore.type,
        country: datastore.country,
        state: datastore.state,
        city: datastore.city,
        zipcode: datastore.zipcode,
        property_square_footage: datastore.property_square_footage,
        no_of_bedroom: datastore.no_of_bedroom,
        no_of_bathroom: datastore.no_of_bathroom,
        reason_for_cleaning: datastore.reason_for_cleaning,
        frequency_of_cleanings: datastore.frequency_of_cleanings,
        current_rental_cleaning_fee: datastore.current_rental_cleaning_fee,
        cleaning_estimate: datastore.cleaning_estimate,
        access: JSON.stringify(
            datastore.access.map((a) => ({
                user_id: a.user_id,
                access: a.access,
            }))
        ),
    };
}

const el = "#edit-property";

const existingProperty = $(el).data("property");
const existingAccess = $(el).data("access");
const existingRoles = $(el).data("roles");
if (existingProperty) {
    datastore.isExistingProperty = true;
    datastore.title = existingProperty.title;
    datastore.address = existingProperty.address;
    datastore.logo = existingProperty.logo;
    datastore.type = existingProperty.type;
    datastore.country = existingProperty.country;
    datastore.state = existingProperty.state;
    datastore.city = existingProperty.city;
    (datastore.zipcode = existingProperty.zipcode),
        (datastore.property_square_footage = existingProperty.property_square_footage),
        (datastore.no_of_bedroom = existingProperty.no_of_bedroom),
        (datastore.no_of_bathroom = existingProperty.no_of_bathroom),
        (datastore.reason_for_cleaning = existingProperty.reason_for_cleaning),
        (datastore.frequency_of_cleanings = existingProperty.frequency_of_cleanings),
        (datastore.current_rental_cleaning_fee = existingProperty.current_rental_cleaning_fee),
        (datastore.cleaning_estimate = existingProperty.cleaning_estimate),
        (datastore.access = existingAccess);
    datastore.roles = existingRoles;
}

let deleteAccessCallback = null;

const vm = new Vue({
    el,
    components: {
        VueCropper,
    },
    data: datastore,
    methods: {
        save: (evt) => {
            evt.preventDefault();

            var current = $("a.active")
                .parent("li")
                .index();
            var next = current + 1;
            var second_next = current + 2;

            if (next != 4) {
                if (next == 0) {
                    $("#propertyAddress").prop("required", false);
                    $("#zipcode").prop("required", false);
                    $("#countryId").prop("required", false);
                    $("#stateId").prop("required", false);
                    $("#cityId").prop("required", false);
                    $("#propertySquareFootage").prop("required", false);
                    $("#propertyBedroom").prop("required", false);
                    $("#propertyBathroom").prop("required", false);
                    $("#propertyReasonForCleaning").prop("required", false);
                    $("#propertyFrequencyOfCleanings").prop("required", false);
                    $("#propertyCurrentRentalCleaningFee").prop("required", false);
                    $("#propertyCleaningEstimate").prop("required", false);
                } else if (next == 1) {
                    $("#propertyAddress").prop("required", true);
                    $("#zipcode").prop("required", true);
                    $("#countryId").prop("required", true);
                    $("#stateId").prop("required", true);
                    $("#cityId").prop("required", true);
                } else if (next == 2) {
                    $("#propertySquareFootage").prop("required", true);
                    $("#propertyBedroom").prop("required", true);
                    $("#propertyBathroom").prop("required", true);
                } else if (next == 3) {
                    $("#propertyReasonForCleaning").prop("required", true);
                    $("#propertyFrequencyOfCleanings").prop("required", true);
                    $("#propertyCurrentRentalCleaningFee").prop("required", true);
                    $("#propertyCleaningEstimate").prop("required", true);
                }

                $("ul#myTab li:nth-child(" + next + ") a").removeClass("active");
                $("ul#myTab li:nth-child(" + next + ")").addClass("complete-step");
                $("ul#myTab li:nth-child(" + second_next + ")  a").addClass("active");
                $(".step" + current).attr("hidden", "hidden");
                $(".step" + next).removeAttr("hidden");
            }
            if (next == 3) {
                $(".sbmt_btn").html("Finish");
            }
            if (next == 4) {
                const url = $(el).attr("action");
                datastore.isSaving = true;
                let dataSet = getPostValues();
                let formData = new FormData();
                formData.append("title", dataSet.title);
                formData.append("address", dataSet.address);
                formData.append("type", dataSet.type);
                formData.append("logo", logoData);
                formData.append("country", dataSet.country);
                formData.append("state", dataSet.state);
                formData.append("city", dataSet.city);
                formData.append("zipcode", dataSet.zipcode);
                formData.append("property_square_footage", dataSet.property_square_footage);
                formData.append("no_of_bedroom", dataSet.no_of_bedroom);
                formData.append("no_of_bathroom", dataSet.no_of_bathroom);
                formData.append("reason_for_cleaning", dataSet.reason_for_cleaning);
                formData.append("frequency_of_cleanings", dataSet.frequency_of_cleanings);
                formData.append("current_rental_cleaning_fee", dataSet.current_rental_cleaning_fee);
                formData.append("cleaning_estimate", dataSet.cleaning_estimate);
                formData.append("access", dataSet.access);

                $.ajax({
                    method: "POST",
                    url,
                    data: formData,
                    processData: false,
                    cache: false,
                    contentType: false,
                    success: (data, status, jqXhr) => {
                        console.log({ data });
                        if (!data.success) {
                            $(el)
                                .find(".spinner-border")
                                .addClass("invisible");
                            $("#limitNotification")
                                .find(".modal-body .form-group")
                                .html("");
                            $("#limitNotification")
                                .find(".modal-body .form-group")
                                .html(data.message);
                            $("#limitNotification").modal("show");
                        } else {
                            //                            window.location.href = "/manager/property/index";
                        }
                        //                        window.location.href = "/manager/property/index";
                        $("ul#myTab li:nth-child(" + next + ") a").removeClass("active");
                        $("ul#myTab li:nth-child(" + next + ")").addClass("complete-step");
                        $("ul#myTab li:nth-child(" + second_next + ")  a").addClass("active");
                        $(".step" + current).attr("hidden", "hidden");
                        $(".step" + next).removeAttr("hidden");
                        $(".sbmt_btn").hide();
                    },
                    error: (jqXHR, textStatus, errorThrown) => {
                        console.log("error", textStatus, jqXHR.responseText);
                        datastore.isSaving = false;
                        if (jqXHR.responseJSON) {
                            datastore.error = jqXHR.responseJSON.errMsg;
                        }
                    },
                    dataType: "json",
                });
            }
        },
        deletePerson: (person) => {
            deleteAccessCallback = () => {
                datastore.access.splice(datastore.access.indexOf(person), 1);
            };

            $("#deleteModal")
                .find(".removePersonName")
                .text(`${person.first_name} ${person.last_name}`);
            $("#deleteModal").modal("show");
        },
        setImage(e) {
            const file = e.target.files[0];
            if (file.type.indexOf("image/") === -1) {
                alert("Please select an image file");
                return;
            }
            if (typeof FileReader === "function") {
                const reader = new FileReader();
                reader.onload = (event) => {
                    this.logo = event.target.result;

                    this.$refs.cropper.replace(event.target.result);
                };
                reader.readAsDataURL(file);
            } else {
                alert("Sorry, FileReader API not supported");
            }
        },
        cropImage() {
            this.$refs.cropper
                .getCroppedCanvas({
                    width: 200,
                    height: 200,
                })
                .toBlob((blob) => {
                    logoData = blob;
                });
        },
    },
});

window.VM = vm;

{
    const $inviteRoleModal = $("#inviteRole");
    const $inviteRoleForm = $inviteRoleModal.find("form");
    const $emailField = $inviteRoleForm.find("input[name=email]");
    const $roleIdField = $inviteRoleForm.find("select[name=role_id]");
    const $submitButton = $inviteRoleForm.find("button[type=submit]");
    const $spinner = $submitButton.find(".spinner-border");

    // When the modal dialog shows up, focus on the input field.
    $inviteRoleModal.on("shown.bs.modal", () => $emailField.focus());

    // Utility function to verify email
    const isEmailValid = () => {
        const email = $emailField.val();
        return email.length >= 5 && email.indexOf("@") >= 0 && email.indexOf(".") >= 0;
    };

    // Validation and form submission
    $inviteRoleForm.on("submit", (evt) => {
        evt.preventDefault();

        // Show error if email is invalid
        if (!isEmailValid()) {
            $emailField.addClass("is-invalid");
            $emailField.removeClass("is-valid");
            return;
        }
        // If the email is valid, remove the fields
        $emailField.addClass("is-valid");
        $emailField.removeClass("is-invalid");

        const email = $emailField.val();
        const role_id = $roleIdField.val();
        console.log("Invite", email);
        $spinner.removeClass("invisible");

        $.ajax({
            method: "POST",
            url: `/manager/property/api/invite-to-role/${existingProperty.id}`,
            data: { email: email, role_id: role_id },
            dataType: "json",
            success: (data, textStatus, jqXHR) => {
                console.log("Success", textStatus, data);

                // Close modal and reset
                $emailField.val("");
                $inviteRoleModal.modal("hide");
                $spinner.addClass("invisible");
                if (!data.success) {
                    $("#limitNotification")
                        .find(".modal-body .form-group")
                        .html("");
                    $("#limitNotification")
                        .find(".modal-body .form-group")
                        .html(data.message);
                    $("#limitNotification").modal("show");
                }
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log("Error", textStatus);
                $spinner.addClass("invisible");
            },
        });
    });
    $emailField.on("keyup", () => {
        // While the user is typing an email, as soon as it becomes valid, give it 'valid' status. Only after it's
        // been valid once or submitted do we show an 'invalid' state.
        if (isEmailValid()) {
            $emailField.addClass("is-valid");
            $emailField.removeClass("is-invalid");
        } else if ($emailField.hasClass("is-valid")) {
            $emailField.removeClass("is-valid");
            $emailField.addClass("is-invalid");
        }
    });
}

// Delete role modal
$("#deleteModal").on("show.bs.modal", function(event) {
    const modal = $(this);
    modal.find("button.do-delete").click((evt) => {
        evt.preventDefault();
        if (deleteAccessCallback) {
            deleteAccessCallback();
            deleteAccessCallback = null;
        }
        $("#deleteModal").modal("hide");
    });
});
