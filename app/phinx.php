<?php

require_once __DIR__.'/config/settings.php';
require_once './autoload.php';

preg_match('~dbname=([^;]+)~', DB_DSN, $match);
$dbName = $match[1];

return [
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds',
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'development',
        'development' => [
            'name' => $dbName,
            'connection' => Model::getDb(),
            'encoding' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
        ],
    ],
];

/*
paths:
    migrations: "%%PHINX_CONFIG_DIR%%/db/migrations"
    seeds: "%%PHINX_CONFIG_DIR%%/db/seeds"

environments:
    default_migration_table: phinxlog
    default_database: development
    production:
        adapter: mysql
        host: localhost
        name: production_db
        user: root
        pass: ""
        port: 3306
        charset: utf8

    development:
        adapter: mysql
        host: db
        name: jervis2
        user: root
        pass: "jervispwd123"
        port: 3306
        charset: utf8

    testing:
        adapter: mysql
        host: localhost
        name: testing_db
        user: root
        pass: ""
        port: 3306
        charset: utf8

version_order: creation
*/
