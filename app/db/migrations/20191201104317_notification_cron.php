<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class NotificationCron extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        CREATE TABLE `notification` (
          `id` int(11) NOT NULL,
          `reservation_id` int(11) NOT NULL,
          `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
          `cc_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
          `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
          `schedule_date` datetime DEFAULT NULL,
          `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
          `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
          `send_via` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
          `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

        ALTER TABLE `notification`
          ADD PRIMARY KEY (`id`,`reservation_id`),
          ADD KEY `reservation_id` (`reservation_id`);

        ALTER TABLE `notification`
          MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

        ALTER TABLE `notification`
          ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE;

        ALTER TABLE `notification` CHANGE `phone` `phone` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;

        ALTER TABLE `notification` ADD `title` VARCHAR(100) NOT NULL DEFAULT '' AFTER `schedule_date`;

        ALTER TABLE `notification` ADD `status` VARCHAR(10) NOT NULL DEFAULT '' AFTER `send_via`;
        */
        $this->table('notification', ['signed' => false])
            ->addColumn('reservation_id', 'integer', ['signed' => false])
            ->addColumn('email', 'string', ['limit' => 50])
            ->addColumn('cc_email', 'string', ['limit' => 50, 'null' => true])
            ->addColumn('phone', 'string', ['limit' => 20])
            ->addColumn('schedule_date', 'datetime', ['null' => true])
            ->addColumn('title', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('subject', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('content', 'text', ['limit' => MysqlAdapter::TEXT_MEDIUM])
            ->addColumn('send_via', 'string', ['limit' => 10, 'null' => true])
            ->addColumn('status', 'string', ['limit' => 10, 'default' => ''])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addIndex('reservation_id')
            ->addForeignKey('reservation_id', 'reservation', 'id', ['delete' => 'CASCADE', 'constraint' => 'notification_ibfk_1'])
            ->save();

        $this->execute("UPDATE `metadata_notification_template` SET `duration` = '22 hour', `event` = 'Check-in' WHERE `metadata_notification_template`.`id` = 8");
        /*
        ALTER TABLE `guest` CHANGE `phone` `phone` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
        */
        $this->table('guest')->changeColumn('phone', 'string', ['limit' => 20])->save();
    }
}
