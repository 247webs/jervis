<?php

use Phinx\Migration\AbstractMigration;

class InvoiceNewFields extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('invoice', ['signed' => false])
            ->addColumn('deductu_item_name', 'string', ['after' => 'service_price', 'limit' => 100])
            ->addColumn('sign', 'string', ['after' => 'deductu_item_name', 'limit' => 100])
            ->addColumn('deduct_amount', 'string', ['after' => 'sign', 'limit' => 100])
            ->addColumn('total_deduction', 'string', ['after' => 'deduct_amount', 'limit' => 100])
            ->addColumn('total', 'string', ['after' => 'total_deduction', 'limit' => 100])
            ->save();
    }
}
