<?php

use Phinx\Migration\AbstractMigration;

class Device extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        CREATE TABLE `device` (
          `id` int(11) NOT NULL,
          `owner_id` int(11) UNSIGNED NOT NULL,
          `property_id` int(11) UNSIGNED NOT NULL,
          `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
          `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
          `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
          `passcode` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
          `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        */
        /*
        ALTER TABLE `device`
          ADD PRIMARY KEY (`id`,`property_id`),
          ADD KEY `owner_id` (`owner_id`),
          ADD KEY `property_id` (`property_id`);
        */
        /*

        ALTER TABLE `device`
          MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
        */
        /*

        ALTER TABLE `device`
          ADD CONSTRAINT `device_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE,
          ADD CONSTRAINT `device_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE;
        */
        $this->table('device', ['signed' => false])
            ->addColumn('owner_id', 'integer', ['signed' => false])
            ->addColumn('property_id', 'integer', ['signed' => false])
            ->addColumn('title', 'string', ['limit' => 150])
            ->addColumn('description', 'string', ['limit' => 255, 'default' => ''])
            ->addColumn('type', 'string', ['limit' => 10])
            ->addColumn('passcode', 'string', ['limit' => 32, 'default' => ''])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('modified', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addIndex('owner_id')
            ->addIndex('property_id')
            ->addForeignKey('owner_id', 'manager', 'id', ['delete' => 'CASCADE', 'constraint' => 'device_ibfk_1'])
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE', 'constraint' => 'device_ibfk_2'])
            ->save();
    }
}
