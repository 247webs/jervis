<?php

use Phinx\Migration\AbstractMigration;

class ChecklistFillRequest extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        CREATE TABLE `checklist_fill_request` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `user_id` int(11) unsigned NOT NULL,
          `template_id` int(11) unsigned NOT NULL,
          `due_date` date NOT NULL,
          `filled_id` int(11) unsigned NULL,
          PRIMARY KEY (`id`),
          KEY `user_id` (`user_id`),
          KEY `template_id` (`template_id`),
          CONSTRAINT `checklist_fill_request_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `manager` (`id`),
          CONSTRAINT `checklist_fill_request_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `checklist_template` (`id`),
          CONSTRAINT `checklist_fill_request_ibfk_3` FOREIGN KEY (`filled_id`) REFERENCES `checklist` (`id`) ON DELETE SET NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
        */
        $this->table('checklist_fill_request', ['signed' => false])
            ->addColumn('user_id', 'integer', ['signed' => false])
            ->addColumn('template_id', 'integer', ['signed' => false])
            ->addColumn('due_date', 'date')
            ->addColumn('filled_id', 'integer', ['null' => true, 'signed' => false])
            ->addIndex('user_id')
            ->addIndex('template_id')
            ->addForeignKey('user_id', 'manager', 'id', ['constraint' => 'checklist_fill_request_ibfk_1'])
            ->addForeignKey('template_id', 'checklist_template', 'id', ['constraint' => 'checklist_fill_request_ibfk_2'])
            ->addForeignKey('filled_id', 'checklist', 'id', ['delete' => 'SET_NULL', 'constraint' => 'checklist_fill_request_ibfk_3'])
            ->save();
    }
}
