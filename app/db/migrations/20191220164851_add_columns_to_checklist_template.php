<?php

use Phinx\Migration\AbstractMigration;

class AddColumnsToChecklistTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('checklist_template')
        ->addColumn('recurring', 'boolean', ['default' => 0,'after' => 'json'])
        ->addColumn('to_whome', 'string', ['limit' => 50, 'default' => ''], ['after' => 'recurring'])
        ->addColumn('duration', 'string', ['limit' => 10, 'default' => ''], ['after' => 'to_whome'])
        ->addColumn('before_after', 'string', ['limit' => 6, 'default' => ''], ['after' => 'duration'])
        ->addColumn('event', 'string', ['limit' => 15, 'default' => ''], ['after' => 'before_after'])
        ->addColumn('subject', 'string', ['limit' => 255, 'default' => ''], ['after' => 'event'])
        ->save();
    }
}
