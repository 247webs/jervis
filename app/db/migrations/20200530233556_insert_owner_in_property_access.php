<?php

use Phinx\Migration\AbstractMigration;

class InsertOwnerInPropertyAccess extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("INSERT INTO `property_access`(`user_id`, `property_id`, `access`) SELECT owner_id,property.id, (SELECT CONCAT('{\"',role.id,'\":true}') role_value FROM `role` WHERE `property_id` = property.id AND role_name = 'Property owner') role FROM `property` WHERE owner_id not in (SELECT user_id FROM `property_access`)");
    }
}
