<?php

use Phinx\Migration\AbstractMigration;

class UpdateWelcomeMail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("UPDATE `metadata_notification_template` SET `content` = '<p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">Hi {GUEST_FIRST_NAME},</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">Your reservation for the {PROPERTY_NAME} for the requested dates has been confirmed.</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">You can add the reservation into your Google Calendar using this link.</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">{GOOGLE_CALENDAR_LINK}</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">Your reservation can be retrieved from the link below:</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">{RESERVATION_DETAIL_LINK}</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">If you have any questions or concerns, please let me know.</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">Thank you,</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">{BUSINESS_OWNER_NAME}</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">RealTimeEscapes.com' WHERE `metadata_notification_template`.`title` = 'Welcome Confirmation Email'");
        $this->execute("UPDATE `metadata_notification_template` SET `content` = '<p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">Hi {GUEST_FIRST_NAME},</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">Our properties are corporate rentals that we also offer through the website you booked from.</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">We require that all guests review and agree to our house rules and rental agreement before check-in. This is the same agreement that guests would complete if they were booking directly with us.</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">These rules keep the property clean and the neighborhood safe for our guests and my neighbors.</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">Please read our house rules &amp; rental agreement and sign the acknowledgment if you agree.</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">{AGREEMENT_DETAIL_LINK}</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">You will be directed to your full check-in details once completed. You will also receive reminder text and email messages as it gets closer to your reservation.</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">If you have any questions, please let me know.</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">Thank you,</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\"></p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">{BUSINESS_OWNER_NAME}</p><p style=\"font-family: &quot;Times New Roman&quot;; font-size: medium;\">RealTimeEscapes.com' WHERE `metadata_notification_template`.`title` = 'Welcome Email'");
    }
}
