<?php

use Phinx\Migration\AbstractMigration;

class PropertyAndRolePermissions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        INSERT INTO `permissions` (`perm_group`, `perm_code`, `description`) VALUES
        ('Property', 'Property.editDetails', 'Edit title and address'),
        ('Property', 'Property.assignRole', 'Assign/remove roles from members'),
        ('Property', 'Property.invite', 'Invite members'),
        ('Property', 'Property.expel', 'Expel members');

        INSERT INTO `permissions` (`perm_group`, `perm_code`, `description`) VALUES
        ('Role', 'Role.create', 'Create'),
        ('Role', 'Role.edit', 'Edit'),
        ('Role', 'Role.delete', 'Delete');
        */
        $this->table('permissions')->insert([
            ['perm_group' => 'Property', 'perm_code' => 'Property.editDetails', 'description' => 'Edit title and address'],
            ['perm_group' => 'Property', 'perm_code' => 'Property.assignRole', 'description' => 'Assign/remove roles from members'],
            ['perm_group' => 'Property', 'perm_code' => 'Property.invite', 'description' => 'Invite members'],
            ['perm_group' => 'Property', 'perm_code' => 'Property.expel', 'description' => 'Expel members'],
            ['perm_group' => 'Role', 'perm_code' => 'Role.create', 'description' => 'Create'],
            ['perm_group' => 'Role', 'perm_code' => 'Role.edit', 'description' => 'Edit'],
            ['perm_group' => 'Role', 'perm_code' => 'Role.delete', 'description' => 'Delete'],
        ])->save();
    }
}
