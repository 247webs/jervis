<?php

use Phinx\Migration\AbstractMigration;

class RoleAndPermission extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        
        $this->execute('DELETE FROM permissions');        

        $this->execute('DELETE FROM metadata_role_perm');

        $this->table('permissions')->insert([
            ['id' => 1, 'perm_group' => 'Rental Agreements', 'perm_code' => 'Agreement.create', 'description' => 'Create/assign'],
            ['id' => 2, 'perm_group' => 'Rental Agreements', 'perm_code' => 'Agreement.view', 'description' => 'View'],
            ['id' => 3, 'perm_group' => 'Rental Agreements', 'perm_code' => 'Agreement.edit', 'description' => 'Edit'],
            ['id' => 4, 'perm_group' => 'Rental Agreements', 'perm_code' => 'Agreement.delete', 'description' => 'Delete'],
            ['id' => 5, 'perm_group' => 'Reservations', 'perm_code' => 'Reservation.create', 'description' => 'Create/assign'],
            ['id' => 6, 'perm_group' => 'Reservations', 'perm_code' => 'Reservation.view', 'description' => 'View'],
            ['id' => 7, 'perm_group' => 'Reservations', 'perm_code' => 'Reservation.edit', 'description' => 'Edit'],
            ['id' => 8, 'perm_group' => 'Reservations', 'perm_code' => 'Reservation.delete', 'description' => 'Delete'],
            ['id' => 9, 'perm_group' => 'Reminder Messages', 'perm_code' => 'Reminder.preCheckIn', 'description' => 'Pre-Check-In'],
            ['id' => 10, 'perm_group' => 'Reminder Messages', 'perm_code' => 'Reminder.midStayCheckUp', 'description' => 'Mid-Stay Check Up'],
            ['id' => 11, 'perm_group' => 'Reminder Messages', 'perm_code' => 'Reminder.preCheckOut', 'description' => 'Pre-Check-Out '],
            ['id' => 12, 'perm_group' => 'Reminder Messages', 'perm_code' => 'Reminder.postCheckOut', 'description' => 'Post Check Out'],
            ['id' => 13, 'perm_group' => 'Reminder Messages', 'perm_code' => 'Reminder.reviewRequest', 'description' => 'Review Request'],
            ['id' => 14, 'perm_group' => 'Property Checklist', 'perm_code' => 'PropertyChecklist.create', 'description' => 'Create/assign'],
            ['id' => 15, 'perm_group' => 'Property Checklist', 'perm_code' => 'PropertyChecklist.review', 'description' => 'Review'],
            ['id' => 16, 'perm_group' => 'Property Checklist', 'perm_code' => 'PropertyChecklist.edit', 'description' => 'Edit Completed'],
            ['id' => 17, 'perm_group' => 'Property Checklist', 'perm_code' => 'PropertyChecklist.complete', 'description' => 'Complete Assigned Checklist'],
            ['id' => 18, 'perm_group' => 'Property Checklist', 'perm_code' => 'PropertyChecklist.delete', 'description' => 'Delete'],
            ['id' => 19, 'perm_group' => 'Cleaning Company Checklist', 'perm_code' => 'CleaningCompanyChecklist.create', 'description' => 'Create/assign'],
            ['id' => 20, 'perm_group' => 'Cleaning Company Checklist', 'perm_code' => 'CleaningCompanyChecklist.view', 'description' => 'Review'],
            ['id' => 21, 'perm_group' => 'Cleaning Company Checklist', 'perm_code' => 'CleaningCompanyChecklist.edit', 'description' => 'Edit Completed'],
            ['id' => 22, 'perm_group' => 'Cleaning Company Checklist', 'perm_code' => 'CleaningCompanyChecklist.complete', 'description' => 'Complete Assigned Checklist'],
            ['id' => 23, 'perm_group' => 'Cleaning Company Checklist', 'perm_code' => 'CleaningCompanyChecklist.delete', 'description' => 'Delete'],
            ['id' => 24, 'perm_group' => 'Handyman Checklist', 'perm_code' => 'HandymanChecklist.create', 'description' => 'Create/assign'],
            ['id' => 25, 'perm_group' => 'Handyman Checklist', 'perm_code' => 'HandymanChecklist.view', 'description' => 'Review'],
            ['id' => 26, 'perm_group' => 'Handyman Checklist', 'perm_code' => 'CompletedHandymanChecklist.edit', 'description' => 'Edit Completed'],
            ['id' => 27, 'perm_group' => 'Handyman Checklist', 'perm_code' => 'HandymanChecklist.complete', 'description' => 'Complete Assigned Repair Checklist'],
            ['id' => 28, 'perm_group' => 'Handyman Checklist', 'perm_code' => 'HandymanChecklist.delete', 'description' => 'Delete'],
            ['id' => 29, 'perm_group' => 'Guest Checklist', 'perm_code' => 'GuestChecklist.create', 'description' => 'Create/assign'],
            ['id' => 30, 'perm_group' => 'Guest Checklist', 'perm_code' => 'GuestChecklist.view', 'description' => 'View'],
            ['id' => 31, 'perm_group' => 'Guest Checklist', 'perm_code' => 'GuestChecklist.edit', 'description' => 'Edit'],
            ['id' => 32, 'perm_group' => 'Guest Checklist', 'perm_code' => 'GuestChecklist.complete', 'description' => 'Complete Assigned Checklist'],
            ['id' => 34, 'perm_group' => 'Devices', 'perm_code' => 'Device.create', 'description' => 'Create'],
            ['id' => 35, 'perm_group' => 'Devices', 'perm_code' => 'Device.edit', 'description' => 'Edit'],
            ['id' => 36, 'perm_group' => 'Devices', 'perm_code' => 'Device.delete', 'description' => 'Delete'],
            ['id' => 39, 'perm_group' => 'Property Checklist', 'perm_code' => 'PropertyChecklist.submitToPropertyOwner', 'description' => 'Submit Assigned Checklist to Property Owner'],
            ['id' => 40, 'perm_group' => 'Rental Agreements', 'perm_code' => 'Agreements.receive', 'description' => 'Receive PDF'],
            ['id' => 41, 'perm_group' => 'Cleaning Company Checklist', 'perm_code' => 'CleaningCompanyChecklist.submit', 'description' => 'Submit Assigned Cleaning Checklist'],
            ['id' => 42, 'perm_group' => 'Handyman Checklist', 'perm_code' => 'HandymanChecklist.submit', 'description' => 'Submit Assigned Handyman Repair Checklist'],
            ['id' => 43, 'perm_group' => 'Property', 'perm_code' => 'Property.editDetails', 'description' => 'Edit Property Details'],
            ['id' => 44, 'perm_group' => 'Property', 'perm_code' => 'Property.assignRole', 'description' => 'Assign/Remove Users & Roles'],
            ['id' => 45, 'perm_group' => 'Property', 'perm_code' => 'Property.invite', 'description' => 'Invite Members'],
            ['id' => 46, 'perm_group' => 'Property', 'perm_code' => 'Property.expel', 'description' => 'Expel Members'],
            ['id' => 47, 'perm_group' => 'Role', 'perm_code' => 'Role.create', 'description' => 'Create'],
            ['id' => 48, 'perm_group' => 'Role', 'perm_code' => 'Role.edit', 'description' => 'Edit'],
            ['id' => 49, 'perm_group' => 'Role', 'perm_code' => 'Role.delete', 'description' => 'Delete'],
            ['id' => 50, 'perm_group' => 'Notification Templates', 'perm_code' => 'NotificationTemplate.edit', 'description' => 'Edit'],
            ['id' => 51, 'perm_group' => 'Rental Agreements', 'perm_code' => 'Agreement.sign', 'description' => 'Sign'],
            ['id' => 53, 'perm_group' => 'Notification Templates', 'perm_code' => 'NotificationTemplate.create', 'description' => 'Create'],
            ['id' => 54, 'perm_group' => 'Notification Templates', 'perm_code' => 'NotificationTemplate.send', 'description' => 'Send'],
            ['id' => 55, 'perm_group' => 'Notification Templates', 'perm_code' => 'NotificationTemplate.delete', 'description' => 'Delete'],
            ['id' => 56, 'perm_group' => 'Third-Party Calendar', 'perm_code' => 'ThirdPartyCalendar.create', 'description' => 'Create'],
            ['id' => 57, 'perm_group' => 'Third-Party Calendar', 'perm_code' => 'ThirdPartyCalendar.edit', 'description' => 'Edit'],
            ['id' => 58, 'perm_group' => 'Third-Party Calendar', 'perm_code' => 'ThirdPartyCalendar.delete', 'description' => 'Delete'],
            ['id' => 59, 'perm_group' => 'Invoice', 'perm_code' => 'Invoice.create', 'description' => 'Create'],
            ['id' => 60, 'perm_group' => 'Invoice', 'perm_code' => 'Invoice.confirm', 'description' => 'Confirm'],
            ['id' => 61, 'perm_group' => 'Invoice', 'perm_code' => 'Invoice.pay', 'description' => 'Pay'],
            ['id' => 62, 'perm_group' => 'Appointment', 'perm_code' => 'Appointment.request', 'description' => 'Request'],
            ['id' => 63, 'perm_group' => 'Appointment', 'perm_code' => 'Appointment.approve', 'description' => 'Approve'],
        ])->save();                   

        $this->table('metadata_role_perm')->insert([
            ['role_id' => 1, 'perm_id' => 1],
            ['role_id' => 1, 'perm_id' => 2],
            ['role_id' => 1, 'perm_id' => 3],
            ['role_id' => 1, 'perm_id' => 4],
            ['role_id' => 1, 'perm_id' => 40],
            ['role_id' => 1, 'perm_id' => 5],
            ['role_id' => 1, 'perm_id' => 6],
            ['role_id' => 1, 'perm_id' => 7],
            ['role_id' => 1, 'perm_id' => 8],
            ['role_id' => 1, 'perm_id' => 9],
            ['role_id' => 1, 'perm_id' => 10],
            ['role_id' => 1, 'perm_id' => 11],
            ['role_id' => 1, 'perm_id' => 12],
            ['role_id' => 1, 'perm_id' => 13],
            ['role_id' => 1, 'perm_id' => 14],
            ['role_id' => 1, 'perm_id' => 15],
            ['role_id' => 1, 'perm_id' => 16],
            ['role_id' => 1, 'perm_id' => 18],
            ['role_id' => 1, 'perm_id' => 19],
            ['role_id' => 1, 'perm_id' => 20],
            ['role_id' => 1, 'perm_id' => 21],
            ['role_id' => 1, 'perm_id' => 23],
            ['role_id' => 1, 'perm_id' => 24],
            ['role_id' => 1, 'perm_id' => 25],
            ['role_id' => 1, 'perm_id' => 26],
            ['role_id' => 1, 'perm_id' => 28],
            ['role_id' => 1, 'perm_id' => 29],
            ['role_id' => 1, 'perm_id' => 30],
            ['role_id' => 1, 'perm_id' => 31],
            ['role_id' => 1, 'perm_id' => 34],
            ['role_id' => 1, 'perm_id' => 35],
            ['role_id' => 1, 'perm_id' => 36],
            ['role_id' => 1, 'perm_id' => 43],
            ['role_id' => 1, 'perm_id' => 44],
            ['role_id' => 1, 'perm_id' => 45],
            ['role_id' => 1, 'perm_id' => 46],
            ['role_id' => 1, 'perm_id' => 47],
            ['role_id' => 1, 'perm_id' => 48],
            ['role_id' => 1, 'perm_id' => 49],
            ['role_id' => 1, 'perm_id' => 50],
            ['role_id' => 1, 'perm_id' => 53],
            ['role_id' => 1, 'perm_id' => 54],
            ['role_id' => 1, 'perm_id' => 56],
            ['role_id' => 1, 'perm_id' => 57],
            ['role_id' => 1, 'perm_id' => 58],
            ['role_id' => 1, 'perm_id' => 60],
            ['role_id' => 1, 'perm_id' => 61],
            ['role_id' => 1, 'perm_id' => 62],
            ['role_id' => 2, 'perm_id' => 1],
            ['role_id' => 2, 'perm_id' => 2],
            ['role_id' => 2, 'perm_id' => 3],
            ['role_id' => 2, 'perm_id' => 40],
            ['role_id' => 2, 'perm_id' => 5],
            ['role_id' => 2, 'perm_id' => 6],
            ['role_id' => 2, 'perm_id' => 7],
            ['role_id' => 2, 'perm_id' => 8],
            ['role_id' => 2, 'perm_id' => 9],
            ['role_id' => 2, 'perm_id' => 10],
            ['role_id' => 2, 'perm_id' => 11],
            ['role_id' => 2, 'perm_id' => 12],
            ['role_id' => 2, 'perm_id' => 13],
            ['role_id' => 2, 'perm_id' => 14],
            ['role_id' => 2, 'perm_id' => 15],
            ['role_id' => 2, 'perm_id' => 16],
            ['role_id' => 2, 'perm_id' => 17],
            ['role_id' => 2, 'perm_id' => 39],
            ['role_id' => 2, 'perm_id' => 19],
            ['role_id' => 2, 'perm_id' => 20],
            ['role_id' => 2, 'perm_id' => 24],
            ['role_id' => 2, 'perm_id' => 25],
            ['role_id' => 2, 'perm_id' => 29],
            ['role_id' => 2, 'perm_id' => 30],
            ['role_id' => 2, 'perm_id' => 35],
            ['role_id' => 2, 'perm_id' => 43],
            ['role_id' => 2, 'perm_id' => 44],
            ['role_id' => 2, 'perm_id' => 45],
            ['role_id' => 2, 'perm_id' => 48],
            ['role_id' => 2, 'perm_id' => 50],
            ['role_id' => 2, 'perm_id' => 53],
            ['role_id' => 2, 'perm_id' => 54],
            ['role_id' => 2, 'perm_id' => 56],
            ['role_id' => 2, 'perm_id' => 57],
            ['role_id' => 2, 'perm_id' => 60],
            ['role_id' => 2, 'perm_id' => 61],
            ['role_id' => 2, 'perm_id' => 62],
            ['role_id' => 3, 'perm_id' => 2],
            ['role_id' => 3, 'perm_id' => 40],
            ['role_id' => 3, 'perm_id' => 51],
            ['role_id' => 3, 'perm_id' => 6],
            ['role_id' => 3, 'perm_id' => 9],
            ['role_id' => 3, 'perm_id' => 10],
            ['role_id' => 3, 'perm_id' => 11],
            ['role_id' => 3, 'perm_id' => 12],
            ['role_id' => 3, 'perm_id' => 13],
            ['role_id' => 3, 'perm_id' => 32],
            ['role_id' => 4, 'perm_id' => 22],
            ['role_id' => 4, 'perm_id' => 41],
            ['role_id' => 4, 'perm_id' => 54],
            ['role_id' => 4, 'perm_id' => 59],
            ['role_id' => 4, 'perm_id' => 63],
            ['role_id' => 5, 'perm_id' => 27],
            ['role_id' => 5, 'perm_id' => 42],
            ['role_id' => 5, 'perm_id' => 54],
            ['role_id' => 5, 'perm_id' => 59],
            ['role_id' => 5, 'perm_id' => 63],
        ])->save();    

    }
}
