<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Yonomi extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('yonomi', ['signed' => false])
        ->addColumn('manager_id', 'integer', ['signed' => false])
        ->addColumn('user', 'string', ['limit' => 100])
        ->addColumn('password', 'string', ['limit' => 100])
        ->addColumn('accessToken', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR])
        ->addColumn('refreshToken', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR])
        ->addIndex('manager_id')
        ->addForeignKey('manager_id', 'manager', 'id', ['delete' => 'CASCADE', 'constraint' => 'yonomi_ibfk_1'])
        ->save();
    }
}
