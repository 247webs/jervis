<?php

use Phinx\Migration\AbstractMigration;

class PropertyAccessInvited extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('property_access_invited', ['signed' => false])
            ->addColumn('user_id', 'integer', ['signed' => false])
            ->addColumn('property_id', 'integer', ['signed' => false])
            ->addColumn('first_name', 'string', ['limit' => 30])
            ->addColumn('last_name', 'string', ['limit' => 30, 'default' => ''])
            ->addColumn('email', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE', 'constraint' => 'property_access_invited_ibfk_1'])
            ->addForeignKey('user_id', 'manager', 'id', ['delete' => 'CASCADE', 'constraint' => 'property_access_invited_ibfk_2'])
            ->save();
    }
}
