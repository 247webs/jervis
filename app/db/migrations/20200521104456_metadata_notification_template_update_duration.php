<?php

use Phinx\Migration\AbstractMigration;

class MetadataNotificationTemplateUpdateDuration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("UPDATE metadata_notification_template SET duration = '1 Day' WHERE title = 'Check-in Email' AND event = 'Check-in'");

        $this->execute("UPDATE metadata_notification_template SET duration = '1 Day' WHERE title = 'Check-in SMS 1' AND event = 'Check-in'");

        $this->execute("UPDATE metadata_notification_template SET duration = '1 Day' WHERE title = 'Check-out Email' AND event = 'Check-out'");

        $this->execute("UPDATE metadata_notification_template SET duration = '1 Day' WHERE title = 'Check-out SMS' AND event = 'Check-out'");
    }
}
