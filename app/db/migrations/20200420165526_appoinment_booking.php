<?php

use Phinx\Migration\AbstractMigration;

class AppoinmentBooking extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('appoinment_booking', ['signed' => false])
            ->addColumn('userid', 'integer', ['signed' => false])
            ->addColumn('comapny_name', 'string', ['limit' => 100])
            ->addColumn('email', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('mobile', 'string', ['limit' => 15, 'default' => ''])
            ->addColumn('date', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('time', 'string', ['limit' => 70, 'default' => ''])
            ->addColumn('status', 'boolean',['default' => '0'])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('userid', 'manager', 'id', ['delete' => 'CASCADE'])
            ->save();
    }
}
