<?php

use Phinx\Migration\AbstractMigration;

class Subscriptions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('subscriptions', ['signed' => false])
            ->addColumn('user_id', 'integer', ['signed' => false])
            ->addColumn('plan_id', 'integer', ['signed' => false])
            ->addColumn('subscription_id', 'string', ['limit' => 255, 'default' => ''])
            ->addColumn('stripe_id', 'string', ['limit' => 255, 'default' => ''])
            ->addColumn('quantity', 'integer', ['signed' => false, 'default' => 1])
            ->addColumn('trial_ends_at', 'datetime', ['null' => true])
            ->addColumn('ends_at', 'datetime', ['null' => true])
            ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addForeignKey('user_id', 'manager', 'id', ['delete' => 'CASCADE'])
            ->save();
    }
}
