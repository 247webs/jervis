<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Property extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        CREATE TABLE `property` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `title` varchar(150) NOT NULL,
          `owner_id` int(11) unsigned NOT NULL,
          `address` text NOT NULL,
          PRIMARY KEY (`id`),
          FOREIGN KEY (owner_id) REFERENCES manager(id) ON DELETE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;
        */
        $this->table('property', ['signed' => false])
            ->addColumn('title', 'string', ['limit' => 150])
            ->addColumn('owner_id', 'integer', ['signed' => false])
            ->addColumn('address', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR])
            ->addForeignKey('owner_id', 'manager', 'id', ['delete' => 'CASCADE'])
            ->save();
        /*
        CREATE TABLE `property_access` (
          `user_id` int(11) unsigned NOT NULL,
          `property_id` int(11) unsigned NOT NULL,
          `access` BLOB NOT NULL,
          PRIMARY KEY (`user_id`, `property_id`),
          FOREIGN KEY (user_id) REFERENCES manager(id) ON DELETE CASCADE,
          FOREIGN KEY (property_id) REFERENCES property(id) ON DELETE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;
        */
        $this->table('property_access', ['id' => false, 'primary_key' => ['user_id', 'property_id']])
            ->addColumn('user_id', 'integer', ['signed' => false])
            ->addColumn('property_id', 'integer', ['signed' => false])
            ->addColumn('access', 'blob', ['limit' => MysqlAdapter::BLOB_REGULAR])
            ->addForeignKey('user_id', 'manager', 'id', ['delete' => 'CASCADE'])
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE'])
            ->save();

        /*
        ALTER TABLE `checklist_template` ADD COLUMN property_id int(11) unsigned NOT NULL AFTER id;
        */
        $this->table('checklist_template')
            ->addColumn('property_id', 'integer', ['after' => 'id', 'signed' => false])
            ->save();
        /*
        ALTER TABLE `checklist` ADD COLUMN property_id int(11) unsigned NOT NULL AFTER id;
        */
        $this->table('checklist')
            ->addColumn('property_id', 'integer', ['after' => 'id', 'signed' => false])
            ->save();
        /*

        NOTE: If you have data in the `checklist_template` table, you now need to set the `property_id` to a valid entry in `property`.

        For example:

        Assuming there is a manager with ID = 1 we create a property for him/her.
        INSERT INTO `property` (`id`, `title`, `owner_id`, `address`) VALUES (1, 'Romantic Bungalow at Sea', 1, '1, Infinite Loop, Cupertino');
        UPDATE checklist_template SET property_id = 1;
        UPDATE checklist SET property_id = 1;

        */
    }
}
