<?php

use Phinx\Migration\AbstractMigration;

class MoreForeignKeysDropSubmitter extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        ALTER TABLE `checklist_template`
          ADD FOREIGN KEY (property_id) REFERENCES property(id) ON DELETE CASCADE,
          DROP COLUMN manager_id;
        */
        $this->table('checklist_template')
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE'])
            ->removeColumn('manager_id')
            ->save();
        /*
        ALTER TABLE `checklist`
          ADD FOREIGN KEY (property_id) REFERENCES property(id) ON DELETE CASCADE,
          DROP COLUMN manager_id;
        */
        $this->table('checklist')
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE'])
            ->removeColumn('manager_id')
            ->save();
        /*
        DROP TABLE `checklist_submitter`;
        */
        $this->table('checklist_submitter')->drop()->save();
    }
}
