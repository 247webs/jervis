<?php

use Phinx\Migration\AbstractMigration;

class Calendar extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('calendar', ['signed' => false])
            ->addColumn('property_id', 'integer', ['signed' => false])
            ->addColumn('booking_source', 'string', ['limit' => 50, 'default' => ''])
            ->addColumn('url', 'string', ['limit' => 255, 'default' => ''])
            ->addColumn('color', 'string', ['limit' => 10])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('modified', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addIndex('property_id')
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE', 'constraint' => 'calendar_ibfk_1'])
            ->save();
    }
}
