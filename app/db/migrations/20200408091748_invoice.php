<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Invoice extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('invoice', ['signed' => false])
            ->addColumn('property_id', 'integer', ['signed' => false])            
            ->addColumn('user_id', 'integer', ['signed' => false])
            ->addColumn('quantity', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR, 'default' => ''])
            ->addColumn('rate', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR, 'default' => ''])
            ->addColumn('total_amount', 'string', ['limit' => 10])
            ->addColumn('service_name', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR, 'default' => ''])
            ->addColumn('service_price', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR, 'default' => ''])
            ->addColumn('status', 'string', ['limit' => 15,'default' => '1'])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->save();
    }
}
