<?php

use Phinx\Migration\AbstractMigration;

class GuestAndReservation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        CREATE TABLE `guest` (
          `id` int(11) NOT NULL,
          `first_name` varchar(20) NOT NULL,
          `last_name` varchar(20) NOT NULL,
          `business_name` varchar(50) DEFAULT NULL,
          `email` varchar(50) NOT NULL,
          `phone` varchar(15) NOT NULL,
          `address` varchar(100) DEFAULT NULL,
          `country` varchar(50) NOT NULL,
          `state` varchar(50) DEFAULT NULL,
          `zipcode` varchar(10) DEFAULT NULL,
          `city` varchar(50) DEFAULT NULL,
          `notes` varchar(255) DEFAULT NULL,
          `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `modified` datetime DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


        ALTER TABLE `guest`
          ADD PRIMARY KEY (`id`);


        ALTER TABLE `guest`
          MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
        */
        $this->table('guest', ['signed' => false])
            ->addColumn('first_name', 'string', ['limit' => 20])
            ->addColumn('last_name', 'string', ['limit' => 20])
            ->addColumn('business_name', 'string', ['limit' => 50, 'null' => true])
            ->addColumn('email', 'string', ['limit' => 50])
            ->addColumn('phone', 'string', ['limit' => 15])
            ->addColumn('address', 'string', ['limit' => 100, 'null' => true])
            ->addColumn('country', 'string', ['limit' => 50])
            ->addColumn('state', 'string', ['limit' => 50, 'null' => true])
            ->addColumn('zipcode', 'string', ['limit' => 10, 'null' => true])
            ->addColumn('city', 'string', ['limit' => 50, 'null' => true])
            ->addColumn('notes', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('modified', 'datetime', ['null' => true])
            ->save();
        /*
        CREATE TABLE `reservation` (
          `id` int(11) NOT NULL,
          `owner_id` int(11) UNSIGNED NOT NULL,
          `property_id` int(11) UNSIGNED NOT NULL,
          `guest_id` int(11) NOT NULL,
          `booking_source` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
          `source_reservation_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
          `check_in_date` datetime NOT NULL,
          `check_out_date` datetime NOT NULL,
          `no_of_guest` int(2) NOT NULL,
          `no_of_infants` int(2) NOT NULL,
          `no_of_toddlers` int(2) NOT NULL,
          `code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
          `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `modified` datetime DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


        ALTER TABLE `reservation`
          ADD PRIMARY KEY (`id`,`property_id`,`guest_id`,`owner_id`),
          ADD KEY `owner_id` (`owner_id`),
          ADD KEY `property_id` (`property_id`),
          ADD KEY `guest_id` (`guest_id`);


        ALTER TABLE `reservation`
          MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

        ALTER TABLE `reservation`
          ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE,
          ADD CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE,
          ADD CONSTRAINT `reservation_ibfk_3` FOREIGN KEY (`guest_id`) REFERENCES `guest` (`id`) ON DELETE CASCADE;
        */

        $this->table('reservation', ['signed' => false])
            ->addColumn('owner_id', 'integer', ['signed' => false])
            ->addColumn('property_id', 'integer', ['signed' => false])
            ->addColumn('guest_id', 'integer', ['signed' => false])
            ->addColumn('booking_source', 'string', ['limit' => 50])
            ->addColumn('source_reservation_id', 'string', ['limit' => 20])
            ->addColumn('check_in_date', 'datetime')
            ->addColumn('check_out_date', 'datetime')
            ->addColumn('no_of_guest', 'integer', ['limit' => 2, 'signed' => false])
            ->addColumn('no_of_infants', 'integer', ['limit' => 2, 'signed' => false])
            ->addColumn('no_of_toddlers', 'integer', ['limit' => 2, 'signed' => false])
            ->addColumn('code', 'string', ['limit' => 100, 'null' => true])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('modified', 'datetime', ['null' => true])
            ->addIndex('owner_id')
            ->addIndex('property_id')
            ->addIndex('guest_id')
            ->addForeignKey('owner_id', 'manager', 'id', ['delete' => 'CASCADE', 'constraint' => 'reservation_ibfk_1'])
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE', 'constraint' => 'reservation_ibfk_2'])
            ->addForeignKey('guest_id', 'guest', 'id', ['delete' => 'CASCADE', 'constraint' => 'reservation_ibfk_3'])
            ->save();
    }
}
