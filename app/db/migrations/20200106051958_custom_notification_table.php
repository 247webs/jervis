<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CustomNotificationTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
       $this->table('custom_notification', ['signed' => false])
            ->addColumn('property_id', 'integer', ['signed' => false])
            ->addColumn('user_id', 'integer', ['signed' => false])
            ->addColumn('email', 'string', ['limit' => 50])
            ->addColumn('cc_email', 'string', ['limit' => 50, 'null' => true])
            ->addColumn('phone', 'string', ['limit' => 20])
            ->addColumn('subject', 'string', ['limit' => 255,'null' => true])
            ->addColumn('content', 'text', ['limit' => MysqlAdapter::TEXT_MEDIUM])
            ->addColumn('send_via', 'string', ['limit' => 10])
            ->addColumn('status', 'string', ['limit' => 10])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addIndex('user_id')
            ->addIndex('property_id')
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE', 'constraint' => 'custom_notification_ibfk_1'])
            ->save();
    }
}
