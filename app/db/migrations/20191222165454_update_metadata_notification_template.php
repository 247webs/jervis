<?php

use Phinx\Migration\AbstractMigration;

class UpdateMetadataNotificationTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("UPDATE `metadata_notification_template` SET `duration` = '2 Day', `beforeAfter` = 'Before', `event` = 'Check-out', `subject` = 'Cleaning scheduled at {PROPERTY_ADDRESS} for {CHECK_OUT_TIME} in 2 DAYS' WHERE `metadata_notification_template`.`title` = 'Cleaning Request Email'");
        $this->execute("UPDATE `metadata_notification_template` SET `duration` = '2 Day', `beforeAfter` = 'Before', `event` = 'Check-out', `subject` = 'Cleaning scheduled at {PROPERTY_ADDRESS} for {CHECK_OUT_TIME} in 2 DAYS. Please confirm!' WHERE `metadata_notification_template`.`title` = 'Cleaning Request SMS'");
    }
}
