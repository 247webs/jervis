<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class ScheduleMeta extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('schedule_meta', ['signed' => false])
            ->addColumn('working_hours', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR, 'default' => ''])
            ->addColumn('add_break', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR, 'default' => ''])
            ->save();

        $this->table('schedule_meta')->insert([
            ['id' => 1, 'working_hours' => '[{"wrkdayname":"Monday","status":"on","starttime":"9:00 AM","endtime":"7:00 PM"},{"wrkdayname":"Tuesday","status":"on","starttime":"9:00 AM","endtime":"7:00 PM"},{"wrkdayname":"Wednesday","status":"on","starttime":"9:00 AM","endtime":"7:00 PM"},{"wrkdayname":"Thursday","status":"on","starttime":"9:00 AM","endtime":"7:00 PM"},{"wrkdayname":"Friday","status":"on","starttime":"9:00 AM","endtime":"7:00 PM"},{"wrkdayname":"Saturday","status":"off","starttime":"9:00 AM","endtime":"7:00 PM"},{"wrkdayname":"Sunday","status":"off","starttime":"9:00 AM","endtime":"7:00 PM"}]', 'add_break' => '{"Monday":[{"start":"1:00 PM","end":"2:00 PM"}],"Tuesday":[{"start":"1:00 PM","end":"2:00 PM"}],"Wednesday":[{"start":"1:00 PM","end":"2:00 PM"}],"Thursday":[{"start":"1:00 PM","end":"2:00 PM"}],"Friday":[{"start":"1:00 PM","end":"2:00 PM"}],"Saturday":[{"start":"1:00 PM","end":"2:00 PM"}],"Sunday":[{"start":"1:00 PM","end":"2:00 PM"}]}'],
        ])->save();            
    }
}
