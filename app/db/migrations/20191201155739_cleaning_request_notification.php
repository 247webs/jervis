<?php

use Phinx\Migration\AbstractMigration;

class CleaningRequestNotification extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        INSERT INTO `metadata_notification_template` (`title`, `duration`, `beforeAfter`, `event`, `subject`, `content`, `send_via`) VALUES
        ('Cleaning Request Email', '', '', '', 'Cleaning Request', '<div>Hello,<br></div><div><br></div><div>We have a reservation at the house with the address below:</div><div>{PROPERTY_ADDRESS}</div><div><br></div><div>The guests will be at the home for the following days:</div><div>Check-in: {CHECK_IN_TIME}</div><div>Check-out: {CHECK_OUT_TIME}</div><div><br></div><div>Can you please schedule for someone to clean the house at 11 AM on {CHECK_OUT_DATE}? Please send us an email to confirm that this has been scheduled.</div><div><br></div><div>Please do not forget to complete the cleaning checklist during cleaning. This is very important for us!</div><div><br></div><div>Thank you,</div><div><br></div><div>{BUSINESS_OWNER_NAME}</div><div>RealTimeEscapes.com</div>', 'Email'),
        ('Cleaning Request SMS', '', '', '', '', 'Jervis - Cleaning request received.  Please confirm!', 'SMS');
        */
        $this->table('metadata_notification_template')->insert([
            [
                'id' => 18,
                'title' => 'Cleaning Request Email',
                'duration' => '',
                'beforeAfter' => '',
                'event' => '',
                'subject' => 'Cleaning Request',
                'content' => '<div>Hello,<br></div><div><br></div><div>We have a reservation at the house with the address below:</div><div>{PROPERTY_ADDRESS}</div><div><br></div><div>The guests will be at the home for the following days:</div><div>Check-in: {CHECK_IN_TIME}</div><div>Check-out: {CHECK_OUT_TIME}</div><div><br></div><div>Can you please schedule for someone to clean the house at 11 AM on {CHECK_OUT_DATE}? Please send us an email to confirm that this has been scheduled.</div><div><br></div><div>Please do not forget to complete the cleaning checklist during cleaning. This is very important for us!</div><div><br></div><div>Thank you,</div><div><br></div><div>{BUSINESS_OWNER_NAME}</div><div>RealTimeEscapes.com</div>',
                'send_via' => 'Email',
            ],
            [
                'id' => 19,
                'title' => 'Cleaning Request SMS',
                'duration' => '',
                'beforeAfter' => '',
                'event' => '',
                'subject' => '',
                'content' => 'Jervis - Cleaning request received.  Please confirm!',
                'send_via' => 'SMS',
            ],
        ])->save();
    }
}
