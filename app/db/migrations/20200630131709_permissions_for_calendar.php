<?php

use Phinx\Migration\AbstractMigration;

class PermissionsForCalendar extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('permissions')->insert([
            ['id' => '64', 'perm_group' => 'Calendar', 'perm_code' => 'Calendar.create', 'description' => 'Create'],
            ['id' => '65', 'perm_group' => 'Calendar', 'perm_code' => 'Calendar.edit', 'description' => 'Edit'],
            ['id' => '66', 'perm_group' => 'Calendar', 'perm_code' => 'Calendar.delete', 'description' => 'Delete'],
        ])->save();

    }
}
