<?php

use Phinx\Migration\AbstractMigration;

class AddSuperUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('manager')->insert([
            ['first_name' => 'Bobby', 'last_name' => 'Varghese',  'email' => 'bobby@securemysanity.com', 'password' => 'd87ed4adf1b51a3e3e2602ed169db592957226de0741b89fb6dad31ce1dcb6dec1b0921a5d36d4a8a66b7193f6a8a297ceb1f230d31a78287be2d95e1f29b16e', 'access_admin' => '1', 'role' => '{"Owner":true}'],
        ])->save();
    }
}
