<?php

use Phinx\Migration\AbstractMigration;

class PropertyAddNewFieldsForIssue50 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('property')
            ->addColumn('type', 'string', ['limit' => 50, 'default' => ''])
            ->addColumn('country', 'string', ['limit' => 50, 'default' => ''])
            ->addColumn('state', 'string', ['limit' => 50, 'default' => ''])
            ->addColumn('city', 'string', ['limit' => 50, 'default' => ''])
            ->addColumn('no_of_bedroom', 'integer', ['limit' => 10, 'default' => 0])
            ->addColumn('no_of_bathroom', 'integer', ['limit' => 10, 'default' => 0])
            ->addColumn('reason_for_cleaning', 'string', ['limit' => 255, 'default' => ''])
            ->addColumn('frequency_of_cleanings', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('current_rental_cleaning_fee', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('cleaning_estimate', 'string', ['limit' => 100, 'default' => ''])
            ->save();
    }
}
