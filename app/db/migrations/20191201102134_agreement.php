<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Agreement extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        CREATE TABLE `agreement` (
          `id` int(11) NOT NULL,
          `owner_id` int(11) UNSIGNED NOT NULL,
          `property_id` int(11) UNSIGNED NOT NULL,
          `guest_id` int(11) NOT NULL,
          `reservation_id` int(11) NOT NULL,
          `ip_address` varchar(15) NOT NULL,
          `document` blob NOT NULL,
          `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `modified` datetime DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

        ALTER TABLE `agreement`
          ADD PRIMARY KEY (`id`,`owner_id`,`property_id`,`guest_id`,`reservation_id`),
          ADD KEY `owner_id` (`owner_id`),
          ADD KEY `property_id` (`property_id`),
          ADD KEY `guest_id` (`guest_id`),
          ADD KEY `reservation_id` (`reservation_id`);

        ALTER TABLE `agreement`
          MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

        ALTER TABLE `agreement`
          ADD CONSTRAINT `agreement_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE,
          ADD CONSTRAINT `agreement_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE,
          ADD CONSTRAINT `agreement_ibfk_3` FOREIGN KEY (`guest_id`) REFERENCES `guest` (`id`) ON DELETE CASCADE,
          ADD CONSTRAINT `agreement_ibfk_4` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE;
        */
        $this->table('agreement', ['signed' => false])
            ->addColumn('owner_id', 'integer', ['signed' => false])
            ->addColumn('property_id', 'integer', ['signed' => false])
            ->addColumn('guest_id', 'integer', ['signed' => false])
            ->addColumn('reservation_id', 'integer', ['signed' => false])
            ->addColumn('ip_address', 'string', ['limit' => 15])
            ->addColumn('document', 'blob', ['limit' => MysqlAdapter::BLOB_REGULAR])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('modified', 'datetime', ['null' => true])
            ->addIndex('owner_id')
            ->addIndex('property_id')
            ->addIndex('guest_id')
            ->addIndex('reservation_id')
            ->addForeignKey('owner_id', 'manager', 'id', ['delete' => 'CASCADE', 'constraint' => 'agreement_ibfk_1'])
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE', 'constraint' => 'agreement_ibfk_2'])
            ->addForeignKey('guest_id', 'guest', 'id', ['delete' => 'CASCADE', 'constraint' => 'agreement_ibfk_3'])
            ->addForeignKey('reservation_id', 'reservation', 'id', ['delete' => 'CASCADE', 'constraint' => 'agreement_ibfk_4'])
            ->save();
    }
}
