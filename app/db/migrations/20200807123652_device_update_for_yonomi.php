<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class DeviceUpdateForYonomi extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
            OLD DB
            +-------------+------------------+------+-----+---------------------+----------------+
            | Field       | Type             | Null | Key | Default             | Extra          |
            +-------------+------------------+------+-----+---------------------+----------------+
            | id          | int(11) unsigned | NO   | PRI | NULL                | auto_increment |
            | property_id | int(11) unsigned | NO   | MUL | NULL                |                |
            | title       | varchar(150)     | NO   |     | NULL                |                |
            | description | varchar(255)     | NO   |     |                     |                |
            | type        | varchar(10)      | NO   |     | NULL                |                |
            | created     | datetime         | NO   |     | current_timestamp() |                |
            | modified    | datetime         | NO   |     | current_timestamp() |                |
            +-------------+------------------+------+-----+---------------------+----------------+
        */

        $this->table('device')->drop()->save();
        $this->table('device', ['signed' => false])
        ->addColumn('manager_id', 'integer', ['signed' => false])
        ->addColumn('property_id', 'integer', ['signed' => false, 'default'=>null])
        ->addColumn('device_id', 'string', ['signed' => false,'limit' => 25])
        ->addColumn('name', 'string', ['limit' => 255])
        ->addColumn('vendor_name', 'string', ['limit' => 255, 'default' => ''])
        ->addColumn('type', 'string', ['limit' => 100])
        ->addColumn('subtype', 'string', ['limit' => 100, 'default' => ''])
        ->addColumn('description', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR])
        ->addColumn('actions', 'string', ['limit' => 32, 'default' => ''])
        ->addIndex('manager_id')
        ->addIndex('property_id')
        ->addIndex(['device_id'], ['unique' => true])
        ->addForeignKey('manager_id', 'manager', 'id', ['delete' => 'CASCADE', 'constraint' => 'device_ibfk_1'])
        ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE', 'constraint' => 'device_ibfk_2'])
        ->save();
    }
}
