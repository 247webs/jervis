<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class ChecklistImprovements extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        TRUNCATE TABLE checklist;


        ALTER TABLE `checklist`
          ADD FOREIGN KEY (template_id) REFERENCES checklist_template(id) ON DELETE CASCADE;

        ALTER TABLE `checklist`
          ADD COLUMN `notes` longtext DEFAULT '' NOT NULL;

        ALTER TABLE `checklist`
          ADD COLUMN `first_name` varchar(100) NOT NULL AFTER `submitter_id`,
          ADD COLUMN `last_name` varchar(100) NOT NULL AFTER `first_name`;

        -- Must allow NULL because a checklist can be submitted by someone who is not logged in
        ALTER TABLE `checklist` CHANGE `submitter_id` `submitter_id` INT(11) UNSIGNED NULL;

        ALTER TABLE `checklist`
          ADD FOREIGN KEY (submitter_id) REFERENCES manager(id) ON DELETE SET NULL;

        ALTER TABLE `checklist` CHANGE `submit_date` `created` DATETIME NOT NULL;

        ALTER TABLE `checklist`
          ADD COLUMN `modified` DATETIME NULL AFTER `created`;
        */
        $this->table('checklist')
            // ->truncate() // does not exist
            ->addForeignKey('template_id', 'checklist_template', 'id', ['delete' => 'CASCADE'])
            ->addColumn('notes', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'default' => ''])
            ->addColumn('first_name', 'string', ['limit' => 100, 'after' => 'submitter_id'])
            ->addColumn('last_name', 'string', ['limit' => 100, 'after' => 'first_name'])
            ->changeColumn('submitter_id', 'integer', ['null' => true, 'signed' => false])
            ->addForeignKey('submitter_id', 'manager', 'id', ['delete' => 'SET_NULL'])
            ->renameColumn('submit_date', 'created', 'datetime')
            ->addColumn('modified', 'datetime', ['after' => 'created', 'null' => true])
            ->save();
    }
}
