<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Plans extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('plans', ['signed' => false])
            ->addColumn('name', 'string', ['limit' => 255, 'default' => ''])
            ->addColumn('description', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR])
            ->addColumn('features', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR])
            ->addColumn('plan_id', 'string', ['limit' => 255, 'default' => ''])
            ->addColumn('role_id', 'integer', ['signed' => false])
            ->addColumn('price', 'string', ['limit' => 10, 'default' => ''])
            ->addColumn('trial_days', 'integer', ['signed' => false])
            ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'datetime', ['null' => true])            
            ->save();

        $this->table('plans')->insert([
            ['id' => 1, 'name' => 'Monthly', 'description' => '$5.00 USD / per month', 'features' => '', 'plan_id' => 'plan_GnCfbxVQZmxeF3', 'role_id' => '2', 'price' => '5', 'trial_days' => '7'],
            ['id' => 2, 'name' => 'Yearly', 'description' => '$50.00 USD / per year', 'features' => '', 'plan_id' => 'plan_GnCgChz8kRyq9t', 'role_id' => '2', 'price' => '50', 'trial_days' => '7'],
        ])->save();            
    }
}
