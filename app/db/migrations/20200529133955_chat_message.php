<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class ChatMessage extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('chat_message', ['signed' => false])
            ->addColumn('to_user_id', 'integer', ['signed' => false])
            ->addColumn('from_user_id', 'integer', ['signed' => false])
            ->addColumn('chat_message', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR, 'default' => ''])
            ->addColumn('timestamp', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('status', 'boolean',['default' => 0])
            ->save();

            

        $sql = "ALTER TABLE chat_message DROP id";
        $this->execute($sql);            
         

        $sql = "ALTER TABLE chat_message ADD chat_message_id INT PRIMARY KEY AUTO_INCREMENT FIRST";
        $this->execute($sql);
    }
}
