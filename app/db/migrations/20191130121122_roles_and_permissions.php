<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class RolesAndPermissions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        CREATE TABLE `metadata_role` (
          `id` int(11) UNSIGNED NOT NULL,
          `role_name` varchar(50) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        ALTER TABLE `metadata_role`
          ADD PRIMARY KEY (`id`);

        ALTER TABLE `metadata_role`
          MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
        */
        $this->table('metadata_role', ['signed' => false])
            ->addColumn('role_name', 'string', ['limit' => 50])
            ->save();

        $this->table('metadata_role')->insert([
            ['id' => 1, 'role_name' => 'Property owner'],
            ['id' => 2, 'role_name' => 'Property manager'],
            ['id' => 3, 'role_name' => 'Property guest'],
            ['id' => 4, 'role_name' => 'Property cleaning company'],
            ['id' => 5, 'role_name' => 'Property handyman'],
        ])->save();

        /*
        CREATE TABLE `permissions` (
          `id` int(11) UNSIGNED NOT NULL,
          `perm_group` varchar(100) NOT NULL,
          `perm_code` varchar(50) NOT NULL,
          `description` varchar(100) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        ALTER TABLE `permissions`
          ADD PRIMARY KEY (`id`);

        ALTER TABLE `permissions`
          MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
        */
        $this->table('permissions', ['signed' => false])
            ->addColumn('perm_group', 'string', ['limit' => 100])
            ->addColumn('perm_code', 'string', ['limit' => 50])
            ->addColumn('description', 'string', ['limit' => 100])
            ->save();

        $this->table('permissions')->insert([
            ['id' => 1, 'perm_group' => 'Rental agreements', 'perm_code' => 'Agreement.create', 'description' => 'Create/assign'],
            ['id' => 2, 'perm_group' => 'Rental agreements', 'perm_code' => 'Agreement.view', 'description' => 'Read & sign rental agreement'],
            ['id' => 3, 'perm_group' => 'Rental agreements', 'perm_code' => 'Agreement.edit', 'description' => 'Edit'],
            ['id' => 4, 'perm_group' => 'Rental agreements', 'perm_code' => 'Agreement.delete', 'description' => 'Delete'],
            ['id' => 5, 'perm_group' => 'Reservations', 'perm_code' => 'Reservation.create', 'description' => 'Create/assign'],
            ['id' => 6, 'perm_group' => 'Reservations', 'perm_code' => 'Reservation.view', 'description' => 'View'],
            ['id' => 7, 'perm_group' => 'Reservations', 'perm_code' => 'Reservation.edit', 'description' => 'Edit'],
            ['id' => 8, 'perm_group' => 'Reservations', 'perm_code' => 'Reservation.delete', 'description' => 'Delete'],
            ['id' => 9, 'perm_group' => 'Reminder messages', 'perm_code' => 'Reminder.preCheckIn', 'description' => 'Pre check-in'],
            ['id' => 10, 'perm_group' => 'Reminder messages', 'perm_code' => 'Reminder.midStayCheckUp', 'description' => 'Mid-stay check-up'],
            ['id' => 11, 'perm_group' => 'Reminder messages', 'perm_code' => 'Reminder.preCheckOut', 'description' => 'Pre check-out'],
            ['id' => 12, 'perm_group' => 'Reminder messages', 'perm_code' => 'Reminder.postCheckOut', 'description' => 'Post check-out'],
            ['id' => 13, 'perm_group' => 'Reminder messages', 'perm_code' => 'Reminder.reviewRequest', 'description' => 'Review request'],
            ['id' => 14, 'perm_group' => 'Property checklist', 'perm_code' => 'PropertyChecklist.create', 'description' => 'Create/assign'],
            ['id' => 15, 'perm_group' => 'Property checklist', 'perm_code' => 'PropertyChecklist.review', 'description' => 'Review'],
            ['id' => 16, 'perm_group' => 'Property checklist', 'perm_code' => 'PropertyChecklist.edit', 'description' => 'Edit Completed'],
            ['id' => 17, 'perm_group' => 'Property checklist', 'perm_code' => 'PropertyChecklist.complete', 'description' => 'Complete assigned checklist'],
            ['id' => 18, 'perm_group' => 'Property checklist', 'perm_code' => 'PropertyChecklist.delete', 'description' => 'Delete'],
            ['id' => 19, 'perm_group' => 'Cleaning company checklist', 'perm_code' => 'CleaningCompanyChecklist.create', 'description' => 'Create/assign'],
            ['id' => 20, 'perm_group' => 'Cleaning company checklist', 'perm_code' => 'CleaningCompanyChecklist.view', 'description' => 'Review'],
            ['id' => 21, 'perm_group' => 'Cleaning company checklist', 'perm_code' => 'CleaningCompanyChecklist.edit', 'description' => 'Edit Completed'],
            ['id' => 22, 'perm_group' => 'Cleaning company checklist', 'perm_code' => 'CleaningCompanyChecklist.complete', 'description' => 'Complete assigned checklist'],
            ['id' => 23, 'perm_group' => 'Cleaning company checklist', 'perm_code' => 'CleaningCompanyChecklist.delete', 'description' => 'Delete'],
            ['id' => 24, 'perm_group' => 'Handyman checklist', 'perm_code' => 'HandymanChecklist.create', 'description' => 'Create/assign'],
            ['id' => 25, 'perm_group' => 'Handyman checklist', 'perm_code' => 'HandymanChecklist.view', 'description' => 'Review'],
            ['id' => 26, 'perm_group' => 'Handyman checklist', 'perm_code' => 'CompletedHandymanChecklist.edit', 'description' => 'Edit Completed'],
            ['id' => 27, 'perm_group' => 'Handyman checklist', 'perm_code' => 'HandymanChecklist.complete', 'description' => 'Complete assigned repair checklist'],
            ['id' => 28, 'perm_group' => 'Handyman checklist', 'perm_code' => 'HandymanChecklist.delete', 'description' => 'Delete'],
            ['id' => 29, 'perm_group' => 'Guest checklist', 'perm_code' => 'GuestChecklist.create', 'description' => 'Create/assign'],
            ['id' => 30, 'perm_group' => 'Guest checklist', 'perm_code' => 'GuestChecklist.view', 'description' => 'View'],
            ['id' => 31, 'perm_group' => 'Guest checklist', 'perm_code' => 'GuestChecklist.edit', 'description' => 'Edit'],
            ['id' => 32, 'perm_group' => 'Guest checklist', 'perm_code' => 'GuestChecklist.complete', 'description' => 'Complete assigned checklist'],
            ['id' => 34, 'perm_group' => 'Devices', 'perm_code' => 'Device.create', 'description' => 'Create'],
            ['id' => 35, 'perm_group' => 'Devices', 'perm_code' => 'Device.edit', 'description' => 'Edit'],
            ['id' => 36, 'perm_group' => 'Devices', 'perm_code' => 'Device.delete', 'description' => 'Delete'],
            ['id' => 39, 'perm_group' => 'Property checklist', 'perm_code' => 'PropertyChecklist.submitToPropertyOwner', 'description' => 'Submit assigned checklist to property owner'],
            ['id' => 40, 'perm_group' => 'Rental agreements', 'perm_code' => 'Agreements.receiveEmailForReview', 'description' => 'Receive a completed PDF rental agreement via email for review'],
            ['id' => 41, 'perm_group' => 'Cleaning company checklist', 'perm_code' => 'CleaningCompanyChecklist.submit', 'description' => 'Submit assigned cleaning checklist'],
            ['id' => 42, 'perm_group' => 'Handyman checklist', 'perm_code' => 'HandymanChecklist.submit', 'description' => 'Submit assigned handyman repair checklist'],
        ])->save();

        /*
        CREATE TABLE `metadata_role_perm` (
          `role_id` int(11) UNSIGNED NOT NULL,
          `perm_id` int(11) UNSIGNED NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        ALTER TABLE `metadata_role_perm`
          ADD KEY `role_id` (`role_id`),
          ADD KEY `perm_id` (`perm_id`);

        ALTER TABLE `metadata_role_perm`
          ADD CONSTRAINT `metadata_role_perm_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `metadata_role` (`id`) ON DELETE CASCADE,
          ADD CONSTRAINT `metadata_role_perm_ibfk_2` FOREIGN KEY (`perm_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;
        */
        $this->table('metadata_role_perm', ['id' => false, 'signed' => false])
            ->addColumn('role_id', 'integer', ['signed' => false])
            ->addColumn('perm_id', 'integer', ['signed' => false])
            ->addIndex('role_id')
            ->addIndex('perm_id')
            ->addForeignKey('role_id', 'metadata_role', 'id', ['delete' => 'CASCADE', 'constraint' => 'metadata_role_perm_ibfk_1'])
            ->addForeignKey('perm_id', 'permissions', 'id', ['delete' => 'CASCADE', 'constraint' => 'metadata_role_perm_ibfk_2'])
            ->save();

        $this->table('metadata_role_perm')->insert([
            ['role_id' => 2, 'perm_id' => 40],
            ['role_id' => 2, 'perm_id' => 17],
            ['role_id' => 2, 'perm_id' => 39],
            ['role_id' => 3, 'perm_id' => 2],
            ['role_id' => 3, 'perm_id' => 32],
            ['role_id' => 4, 'perm_id' => 22],
            ['role_id' => 4, 'perm_id' => 41],
            ['role_id' => 5, 'perm_id' => 27],
            ['role_id' => 5, 'perm_id' => 42],
            ['role_id' => 1, 'perm_id' => 1],
            ['role_id' => 1, 'perm_id' => 3],
            ['role_id' => 1, 'perm_id' => 4],
            ['role_id' => 1, 'perm_id' => 40],
            ['role_id' => 1, 'perm_id' => 5],
            ['role_id' => 1, 'perm_id' => 6],
            ['role_id' => 1, 'perm_id' => 7],
            ['role_id' => 1, 'perm_id' => 8],
            ['role_id' => 1, 'perm_id' => 9],
            ['role_id' => 1, 'perm_id' => 10],
            ['role_id' => 1, 'perm_id' => 11],
            ['role_id' => 1, 'perm_id' => 12],
            ['role_id' => 1, 'perm_id' => 13],
            ['role_id' => 1, 'perm_id' => 14],
            ['role_id' => 1, 'perm_id' => 15],
            ['role_id' => 1, 'perm_id' => 16],
            ['role_id' => 1, 'perm_id' => 18],
            ['role_id' => 1, 'perm_id' => 19],
            ['role_id' => 1, 'perm_id' => 20],
            ['role_id' => 1, 'perm_id' => 21],
            ['role_id' => 1, 'perm_id' => 23],
            ['role_id' => 1, 'perm_id' => 24],
            ['role_id' => 1, 'perm_id' => 25],
            ['role_id' => 1, 'perm_id' => 26],
            ['role_id' => 1, 'perm_id' => 28],
            ['role_id' => 1, 'perm_id' => 29],
            ['role_id' => 1, 'perm_id' => 30],
            ['role_id' => 1, 'perm_id' => 31],
            ['role_id' => 1, 'perm_id' => 34],
            ['role_id' => 1, 'perm_id' => 35],
            ['role_id' => 1, 'perm_id' => 36],
        ])->save();
        /*
        CREATE TABLE `role` (
          `id` int(11) NOT NULL,
          `role_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
          `property_id` int(11) UNSIGNED NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

        ALTER TABLE `role`
          ADD PRIMARY KEY (`id`,`property_id`),
          ADD KEY `property_id` (`property_id`),
          ADD KEY `id` (`id`);

        ALTER TABLE `role`
          MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

        ALTER TABLE `role`
          ADD CONSTRAINT `role_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE;
        */
        $this->table('role', ['signed' => false])
            ->addColumn('role_name', 'string', ['limit' => 50])
            ->addColumn('property_id', 'integer', ['signed' => false])
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE', 'constraint' => 'role_ibfk_1'])
            ->save();
        /*
        CREATE TABLE `role_perm` (
          `role_id` int(11) UNSIGNED NOT NULL,
          `perm_id` int(11) UNSIGNED NOT NULL,
          `property_id` int(11) UNSIGNED NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        ALTER TABLE `role_perm`
          ADD KEY `perm_id` (`perm_id`),
          ADD KEY `role_id` (`role_id`),
          ADD KEY `property_id` (`property_id`);

        ALTER TABLE `role_perm`
          ADD CONSTRAINT `role_perm_ibfk_1` FOREIGN KEY (`perm_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
          ADD CONSTRAINT `role_perm_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE;
        */
        $this->table('role_perm', ['id' => false, 'signed' => false])
            ->addColumn('role_id', 'integer', ['signed' => false])
            ->addColumn('perm_id', 'integer', ['signed' => false])
            ->addColumn('property_id', 'integer', ['signed' => false])
            ->addForeignKey('perm_id', 'permissions', 'id', ['delete' => 'CASCADE', 'constraint' => 'role_perm_ibfk_1'])
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE', 'constraint' => 'role_perm_ibfk_2'])
            ->save();
        /*
        ALTER TABLE `manager` ADD `code` VARCHAR(100) NULL AFTER `access_rci`;
        ALTER TABLE `manager` ADD `role` BLOB NOT NULL AFTER `code`;

        ALTER TABLE `manager` CHANGE `access` `access` TEXT DEFAULT '{}' NOT NULL;
        */
        $this->table('manager')
            ->addColumn('code', 'string', ['limit' => 100, 'null' => true, 'after' => 'access_rci'])
            ->addColumn('role', 'blob', ['limit' => MysqlAdapter::BLOB_REGULAR, 'after' => 'code'])
            ->changeColumn('access', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR, 'default' => '{}'])
            ->save();
    }
}
