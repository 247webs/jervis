<?php

use Phinx\Migration\AbstractMigration;

class CalendarData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('calendar_data', ['signed' => false])
            ->addColumn('property_id', 'integer', ['signed' => false])
            ->addColumn('calendar_id', 'integer', ['signed' => false])
            ->addColumn('start_date', 'datetime')
            ->addColumn('end_date', 'datetime')
            ->addColumn('stamp_date', 'datetime')
            ->addColumn('uid', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('summary', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('modified', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addIndex('property_id')
            ->addIndex('calendar_id')
            ->addForeignKey('property_id', 'property', 'id', ['delete' => 'CASCADE', 'constraint' => 'calendar_data_ibfk_1'])
            ->addForeignKey('calendar_id', 'calendar', 'id', ['delete' => 'CASCADE', 'constraint' => 'calendar_data_ibfk_2'])
            ->save();
    }
}
