<?php

use Phinx\Migration\AbstractMigration;

class MinorChanges extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        ALTER TABLE device DROP FOREIGN KEY device_ibfk_1;
        */
        $this->table('device')->dropForeignKey('owner_id')->save();
        /*
        ALTER TABLE device DROP owner_id;
        */
        $this->table('device')->removeColumn('owner_id')->save();
        /*
        ALTER TABLE reservation DROP FOREIGN KEY reservation_ibfk_1;
        */
        $this->table('reservation')->dropForeignKey('owner_id')->save();
        /*
        ALTER TABLE reservation DROP owner_id;
        */
        $this->table('reservation')->removeColumn('owner_id')->save();
        /*
        ALTER TABLE agreement DROP FOREIGN KEY agreement_ibfk_1;
        */
        $this->table('agreement')->dropForeignKey('owner_id')->save();
        /*
        ALTER TABLE agreement DROP owner_id;
        */
        $this->table('agreement')->removeColumn('owner_id')->save();
    }
}
