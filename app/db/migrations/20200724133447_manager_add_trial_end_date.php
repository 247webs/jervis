<?php

use Phinx\Migration\AbstractMigration;

class ManagerAddTrialEndDate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $this->table('manager')
            ->addColumn('trial_end_date', 'date', ['after' => 'profile'])
            ->save();

        $this->execute("UPDATE manager SET trial_end_date = created + INTERVAL 14 DAY");            
        $this->execute("UPDATE manager SET trial_end_date = STR_TO_DATE(\"2020-12-31\", \"%Y-%m-%d\") where email = 'bobby@loudounescape.com'");

        
    }
}
