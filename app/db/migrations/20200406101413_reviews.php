<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Reviews extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('reviews', ['signed' => false])
            ->addColumn('user_id', 'integer', ['signed' => false])
            ->addColumn('reviewer_id', 'integer', ['signed' => false])
            ->addColumn('name', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('review_text', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR, 'default' => ''])
            ->addColumn('rating', 'integer', ['limit' => 2, 'signed' => false])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addIndex('user_id')
            ->addForeignKey('user_id', 'manager', 'id', ['delete' => 'CASCADE'])
            ->save();
    }
}
