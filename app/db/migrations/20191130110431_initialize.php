<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Initialize extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        CREATE TABLE `checklist` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `manager_id` int(11) unsigned NOT NULL,
          `template_id` int(11) unsigned NOT NULL,
          `submitter_id` int(11) unsigned NOT NULL,
          `data` longtext NOT NULL,
          `submit_date` datetime NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        */
        $this->table('checklist', ['signed' => false])
            ->addColumn('manager_id', 'integer', ['signed' => false])
            ->addColumn('template_id', 'integer', ['signed' => false])
            ->addColumn('submitter_id', 'integer', ['signed' => false])
            ->addColumn('data', 'text', ['limit' => MysqlAdapter::TEXT_LONG])
            ->addColumn('submit_date', 'datetime')
            ->save();

        /*
        CREATE TABLE `checklist_template` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `manager_id` int(11) unsigned NOT NULL,
          `code` varchar(10) NOT NULL DEFAULT '',
          `title` varchar(100) NOT NULL DEFAULT '',
          `type` varchar(10) NOT NULL DEFAULT '',
          `json` text NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        */

        $this->table('checklist_template', ['signed' => false])
            ->addColumn('manager_id', 'integer', ['signed' => false])
            ->addColumn('code', 'string', ['limit' => 10, 'default' => ''])
            ->addColumn('title', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('type', 'string', ['limit' => 10, 'default' => ''])
            ->addColumn('json', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR])
            ->save();

        /*
        CREATE TABLE `manager` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `first_name` varchar(30) NOT NULL DEFAULT '',
          `last_name` varchar(30) NOT NULL DEFAULT '',
          `email` varchar(100) NOT NULL DEFAULT '',
          `password` varchar(128) NOT NULL DEFAULT '',
          `mobile` varchar(20) NOT NULL DEFAULT '',
          `access` text NOT NULL,
          `access_admin` int(1) NOT NULL DEFAULT 0,
          `access_rci` int(1) NOT NULL DEFAULT 0,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        */
        $this->table('manager', ['signed' => false])
            ->addColumn('first_name', 'string', ['limit' => 30])
            ->addColumn('last_name', 'string', ['limit' => 30, 'default' => ''])
            ->addColumn('email', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('password', 'string', ['limit' => 128, 'default' => ''])
            ->addColumn('mobile', 'string', ['limit' => 20, 'default' => ''])
            ->addColumn('access', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR])
            ->addColumn('access_admin', 'integer', ['limit' => 1, 'default' => 0])
            ->addColumn('access_rci', 'integer', ['limit' => 1, 'default' => 0])
            ->save();

        /*
        CREATE TABLE `rci_account` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `manager_id` int(11) unsigned NOT NULL,
          `user_name` varchar(100) NOT NULL DEFAULT '',
          `password` varchar(100) NOT NULL DEFAULT '',
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        */
        $this->table('rci_account', ['signed' => false])
            ->addColumn('manager_id', 'integer', ['signed' => false])
            ->addColumn('user_name', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('password', 'string', ['limit' => 100, 'default' => ''])
            ->save();
        /*
        CREATE TABLE `rci_recipient` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `manager_id` int(11) NOT NULL,
          `first_name` varchar(100) NOT NULL DEFAULT '',
          `last_name` varchar(100) NOT NULL DEFAULT '',
          `email` varchar(100) NOT NULL DEFAULT '',
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        */
        $this->table('rci_recipient', ['signed' => false])
            ->addColumn('manager_id', 'integer', ['signed' => false])
            ->addColumn('first_name', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('last_name', 'string', ['limit' => 100, 'default' => ''])
            ->addColumn('email', 'string', ['limit' => 100, 'default' => ''])
            ->save();
    }
}
