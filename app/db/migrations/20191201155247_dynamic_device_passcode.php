<?php

use Phinx\Migration\AbstractMigration;

class DynamicDevicePasscode extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        /*
        CREATE TABLE `device_passcode` (
          `id` int(11) NOT NULL,
          `reservation_id` int(11) NOT NULL,
          `device_id` int(11) NOT NULL,
          `passcode` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
          `created` datetime NOT NULL DEFAULT current_timestamp(),
          `modified` datetime NULL DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


        ALTER TABLE `device_passcode`
          ADD PRIMARY KEY (`id`),
          ADD KEY `reservation_id` (`reservation_id`);

        ALTER TABLE `device_passcode`
          MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

        ALTER TABLE `device_passcode`
          ADD CONSTRAINT `device_passcode_ibfk_1` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE;
        */
        $this->table('device_passcode', ['signed' => false])
            ->addColumn('reservation_id', 'integer', ['signed' => false])
            ->addColumn('device_id', 'integer', ['signed' => false])
            ->addColumn('passcode', 'string', ['limit' => 32, 'null' => true])
            ->addColumn('created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('modified', 'datetime', ['null' => true])
            ->addIndex('reservation_id')
            ->addForeignKey('reservation_id', 'reservation', 'id', ['delete' => 'CASCADE', 'constraint' => 'device_passcode_ibfk_1'])
            ->save();
        // ALTER TABLE `device` DROP `passcode`;
        $this->table('device')
            ->removeColumn('passcode')
            ->save();
    }
}
