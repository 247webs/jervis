<?php

use Phinx\Migration\AbstractMigration;

class NotificationTemplateUpdate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("UPDATE `notification_template` SET `subject` = REPLACE(REPLACE(REPLACE(`subject`,'RealTime Escapes Reservation','Jervis Reservation'),'RealTime Escapes®','Jervis'),'RealTime Escapes','Jervis'),`content` = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(`content`,'RealTime Escapes (RTE)','Jervis'),'Welcome to RTE','Welcome to Jervis'),'info@rte-llc.org','info@jervis-systems.com'),'RealTimeEscapes.com','jervis-systems.com'),'RealTime Escapes®','Jervis'),'RealTime Escapes','Jervis')");
    }
}
