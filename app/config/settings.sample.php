<?php

// Do not modify this file directly. Make a copy of it and remove the '.sample' for use with jervis.

// Filesystem directories
define('ROOT_DIR', dirname(__FILE__).'/../..');
define('APP_DIR', ROOT_DIR.'/app');
define('VDIR', ROOT_DIR.'/app/mvc/views');

// URLs
define('URL', 'http://localhost:83');
// The base href for the site and prefix for URLs
define('WEB_DIR', '/');

// Suppress SSL, use this for local setups
define('SUPPRESS_SSL', 1);
define('DEBUG', 1);

// Database setup
define('DB_DSN', 'mysql:host=db;dbname=jervis');
define('DB_USR', 'root');
define('DB_PWD', 'jervispwd123');
define('DB_SALT', 'make_this_unique');

// Session object prefix
define('SESSION_NAME', 'logged_in_user_prefix');

define('INVITE_TOKEN_TIMEOUT', 86400 * 3);

// Email
define('MAIL_ADMIN', 'info@jervis-systems.com');


// Specify main and backup SMTP servers (separated by ';')
define('MAIL_SMTP', true);
define('MAIL_SMTP_SERVERS', 'localhost');
define('MAIL_SMTP_USERNAME', '');
define('MAIL_SMTP_PASSWORD', '');
define('MAIL_SMTP_PORT', 587);
define('MAIL_FROM_EMAIL', 'noreply@jervis.com');
define('MAIL_FROM_NAME', 'Jervis Property Manager');

define('SMS_KEY', 'AKIAQBSIXRUL7MV3YJEU');
define('SMS_SECRET', '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');
define('SMS_APPLICATION_ID', '80ab7810c3e44dda8289dbf639cd7f2b');
define('SMS_ORIGINATION_NUMBER', '+12056513092');

//Stripe
define('SECRET_KEY', 'sk_test_VtGd8K16H0iWw8c94yijnZPp0097TNUSTO');
define('PUBLISHABLE_KEY', 'pk_test_TfmFxE22KJoNcDMIqrFVvMcJ00DWkBjBod');
define('STRIPE_CLIENT_ID', 'ca_GPlpfTQfub7ZPr3nLG58yGvP5iZAph93');

// trial days
define('TRIAL_DAY', '14 days');

// Checklists Attachments Directory
define('CHECKLIST_ATTACHMENTS_DIR', '/uploads/checklist/');

//PLAN
define('PLAN', [
    [
        'property' => '1-4',
        'unit' => 10,
        'offer' => '0% off',
        'features' => [
            'Free 10-day free trial',
            'No long-term commitments',
            'Premium support 24/7',
        ]
    ],
    [
        'property' => '5-9',
        'unit' => 9.5,
        'offer' => '5% off',
        'features' => [
            'Free 10-day free trial',
            'No long-term commitments',
            'Premium support 24/7',
        ]
    ],
    [
        'property' => '10-14',
        'unit' => 9,
        'offer' => '10% off',
        'features' => [
            'Free 10-day free trial',
            'No long-term commitments',
            'Premium support 24/7',
        ]
    ],
    [
        'property' => '15-19',
        'unit' => 8.50,
        'offer' => '15% off',
        'features' => [
            'Free 10-day free trial',
            'No long-term commitments',
            'Premium support 24/7',
        ]
   ],
   [
        'property' => '20-49',
        'unit' => 8,
        'offer' => '20% off',
        'features' => [
            'Free 10-day free trial',
            'No long-term commitments',
            'Premium support 24/7',
        ]
    ],
    [
        'property' => '50-99',
        'unit' => 7,
        'offer' => '25% off',
        'features' => [
            'Free 10-day free trial',
            'No long-term commitments',
            'Premium support 24/7',
        ]
   ],
   [
        'property' => '100',
        'unit' => 'Contact US',
        'offer' => '',
        'features' => [
            'Free 10-day free trial',
            'No long-term commitments',
            'Premium support 24/7',
        ]
   ],
]);
