<?php

if (stripos($_SERVER['HTTP_HOST'], '.well-known/acme-challenge') === false) {
    $domains = [
        'dev.jervis.io' => 'https://dev.jervissystems.com',
        'www.jervis.io' => 'https://www.jervis-systems.com',
        'jervis.io' => 'https://www.jervis-systems.com',
        'jervissystems.com' => 'https://www.jervis-systems.com',
        'www.jervissystems.com' => 'https://www.jervis-systems.com',
    ];
    if (in_array($_SERVER['HTTP_HOST'], array_keys($domains))) {
        header('HTTP/1.1 301 Moved Permanently');
        header('location:'.$domains[$_SERVER['HTTP_HOST']]);
        exit;
    }
    if ((!isset($_SERVER['HTTPS']) or ($_SERVER['HTTPS'] != 'on')) && (!defined('SUPPRESS_SSL') || !SUPPRESS_SSL)) {
        header('HTTP/1.1 301 Moved Permanently');
        header('location:'.URL);
        exit;
    }
}

$routes = [
    'anasayfa' => 'IndexController:index',
];
