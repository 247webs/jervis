<?php

$autoloadCache = null;

function ensureAutoloadCache()
{
    global $autoloadCache;
    if ($autoloadCache) {
        return;
    }
    $fsItems = rglob(APP_DIR.'/*');
    $autoloadCache = [];
    foreach ($fsItems as $item) {
        if (strlen($item) > 4 && strtolower(substr($item, -4)) == '.php') {
            $autoloadCache[substr(basename($item), 0, -4)] = $item;
        }
    }
}

function __autoload($className)
{
    global $autoloadCache;
    ensureAutoloadCache();
    if (isset($autoloadCache[$className])) {
        require_once $autoloadCache[$className];

        return;
    } else {
        http_response_code(404);
        //echo "Could not find class with name $className<br>";
        exit;
    }
}

spl_autoload_register('__autoload');
function rglob($pattern, $flags = 0)
{
    $files = glob($pattern, $flags);
    foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
        if (strpos($dir, '/app/core/3rdParty') !== false) {
            // Third-party code contains classes with the same name as ours, we have to ignore them and use their
            // namespaced autoloader instead.
            continue;
        }
        $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
    }

    return $files;
}

require_once APP_DIR.'/core/3rdParty/vendor/autoload.php';
