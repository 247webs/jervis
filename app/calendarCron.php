<?php

require_once dirname(__FILE__).'/config/settings.php';
require_once APP_DIR.'/core/3rdParty/vendor/autoload.php';
require_once APP_DIR.'/mvc/commands/SyncCalendarCommand.php';

use Symfony\Component\Console\Application;

$app = new Application();
$app->add(new SyncCalendarCommand());
$app->run();
