<?php

require_once APP_DIR.'/core/Model.php';
require_once APP_DIR.'/core/helpers/Helpers.php';

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendNotificationCommand extends Command
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = new Model();
    }

    protected function configure()
    {
        $this->setName('send:notification');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $notifications = $this->m->fetchAll("SELECT * FROM notification WHERE CONVERT(DATE_FORMAT(schedule_date,'%Y-%m-%d-%H:%i:00'),DATETIME) <= CONVERT(DATE_FORMAT(now(),'%Y-%m-%d-%H:%i:00'),DATETIME) AND status = ''");

        foreach ($notifications as $key => $notification) {
            if ($notification['send_via'] == 'Email') {
                //send Email
                $this->sendEmailNotification($notification);
            } elseif ($notification['send_via'] == 'SMS') {
                //send SMS
                $this->sendSMSNotification($notification);
            }
        }
    }

    protected function sendEmailNotification($notification)
    {
        if (!empty($notification['email'])) {
            $mail = Helpers::createEmail();
            $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
            $mail->addAddress($notification['email']);
            //$mail->AddCC($notification['cc_email']);
            $mail->isHTML(true);
            $mail->Subject = $notification['subject'];
            $mail->Body = $notification['content'];
            if (!$mail->send()) {
                $this->m->update('notification', ['status' => 'faild'], $notification['id']);
            } else {
                $this->m->update('notification', ['status' => 'sent'], $notification['id']);
            }
        }
    }

    protected function sendSMSNotification($notification)
    {
        $sms = (string) $notification['content'];
        $to = $notification['phone'];

        if (!empty($to)) {
            $result = Helpers::sendSMSNotification($sms, $to);
        }

        $this->m->update('notification', ['status' => 'sent'], $notification['id']);
    }
}
