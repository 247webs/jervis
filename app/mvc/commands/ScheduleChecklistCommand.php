<?php

require_once APP_DIR.'/core/Model.php';
require_once APP_DIR.'/mvc/models/manager/checklist/ManagerChecklistSubmission.php';
require_once APP_DIR.'/mvc/models/manager/checklist/ManagerChecklistTemplate.php';
require_once APP_DIR.'/mvc/models/manager/Manager.php';
require_once APP_DIR.'/mvc/models/manager/role/ManagerRole.php';
require_once APP_DIR.'/mvc/models/manager/reservation/ManagerReservation.php';
require_once APP_DIR.'/mvc/models/manager/property/ManagerProperty.php';
require_once APP_DIR.'/core/helpers/Helpers.php';
require_once APP_DIR.'/core/Controller.php';
require_once APP_DIR.'/mvc/controllers/api/ApiController.php';
require_once APP_DIR.'/mvc/controllers/manager/checklist/api/ManagerChecklistApiController.php';

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScheduleChecklistCommand extends Command
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = new Model();
    }

    protected function configure()
    {
        $this->setName('schedule:checklist');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $model = new Model();

        $checklistTemplates = $this->m->fetchAll('SELECT * FROM checklist_template WHERE `recurring` = 1');

        foreach ($checklistTemplates as $key => $template) {
            $propertyId = $template['property_id'];
            $duration = $template['duration'];
            $before_after = $template['before_after'];
            $event = $template['event'];
            $subject = $template['subject'];

            $users = $this->getUser($template);

            if (count($users)) {
                /*check schedule duration type */
                if (in_array($duration, ['Weekly', 'Monthly', 'Quarterly', 'Yearly'])) {
                    if ($this->verifyScheduleTypeFlag($duration)) {
                        $scheduleDate = date('Y-m-d H:i:s', strtotime('+ 3 Day', strtotime(date('Y-m-d H:i:s'))));

                        $_SERVER['REQUEST_METHOD'] = 'POST';
                        $_POST['template_id'] = $template['id'];
                        $_POST['recipient_id'] = $users['id'];
                        $_POST['due_date'] = $scheduleDate;

                        $this->fillChecklistRequest($propertyId, $_POST);
                    }
                } else {
                    $reservationModel = new ManagerReservation();
                    $reservations = $reservationModel->getReservationsByPropertyId($propertyId);

                    foreach ($reservations as $reservation) {
                        if ($event == 'Check-in') {
                            $date = $reservation['check_in_date'];
                        } elseif ($event == 'Check-out') {
                            $date = $reservation['check_out_date'];
                        } else {
                            $date = $reservation['reservation_date'];
                        }

                        if ($before_after == 'Before') {
                            $scheduleDate = date('Y-m-d H:i:s', strtotime('-'.$template['duration'], strtotime($date)));
                        } else {
                            $scheduleDate = date('Y-m-d H:i:s', strtotime('+'.$template['duration'], strtotime($date)));
                        }
                        $dateDiff = round((time() - strtotime($scheduleDate)) / 60);

                        if ($dateDiff <= 30 && $dateDiff > 0) {
                            $_SERVER['REQUEST_METHOD'] = 'POST';
                            $_POST['template_id'] = $template['id'];
                            $_POST['recipient_id'] = $users['id'];
                            $_POST['due_date'] = $scheduleDate;

                            $this->fillChecklistRequest($reservation['property_id'], $_POST);
                        }
                    }
                }
            }
        }
    }

    protected function getUser($template)
    {
        $users = [];

        $propertyAccesss = $this->m->fetchAll('SELECT * FROM `property_access` WHERE `property_id` = :property_id ', [
            ':property_id' => $template['property_id'],
        ]);

        foreach ($propertyAccesss as $propertyAccesssKey => $propertyAccesssKeyValue) {
            $decodedJson = json_decode($propertyAccesssKeyValue['access'], true);
            foreach ($decodedJson as $roleId => $access) {
                if ($access) {
                    $roleModel = new ManagerRole();
                    $role = $roleModel->getRoleById($roleId);
                    if ($role['role_name'] == $template['to_whome']) {
                        $users = $this->m->fetch('SELECT * FROM `manager` WHERE `id` = :id ', [
                            ':id' => $propertyAccesssKeyValue['user_id'],
                        ]);
                        break;
                    }
                }
            }
        }

        return $users;
    }

    protected function fillChecklistRequest($propertyId, $formData)
    {
        $propertyModel = new ManagerProperty();
        $property = $propertyModel->getPropertyById($propertyId);

        Model::sessionSet('managerId', $property['owner_id']);
        $ManagerChecklistApi = new ManagerChecklistApiController();
        $ManagerChecklistApi->fillRequest($formData);
    }

    protected function verifyScheduleTypeFlag($duration)
    {
        $scheduleTypeFlag = $this->m->fetch("SELECT 
            CASE WHEN DATE(NOW() + INTERVAL (6 - WEEKDAY(NOW())) DAY) = CURDATE() THEN 1 ELSE 0 END Weekly,
            CASE WHEN CAST(DATE_FORMAT(NOW() ,'%Y-%m-01') as DATE) = CURDATE() THEN 1 ELSE 0 END Monthly,
            CASE WHEN CAST(DATE_FORMAT(NOW() ,'%Y-01-01') as DATE) = CURDATE() THEN 1 ELSE 0 END Yearly,
            CASE WHEN CAST(MAKEDATE(YEAR(CURDATE()), 1) + INTERVAL QUARTER(CURDATE()) QUARTER - INTERVAL 1 QUARTER as DATE) = CURDATE() THEN 1 ELSE 0 END Quarterly");

        if ($scheduleTypeFlag[$duration]) {
            return true;
        }

        return false;
    }
}
