<?php

require_once APP_DIR.'/core/Model.php';
require_once APP_DIR.'/mvc/models/manager/Manager.php';
require_once APP_DIR.'/mvc/models/manager/property/ManagerProperty.php';
require_once APP_DIR.'/mvc/models/manager/user/ManagerUser.php';
require_once APP_DIR.'/core/helpers/Helpers.php';

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncCalendarCommand extends Command
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = new Model();
    }

    protected function configure()
    {
        $this->setName('sync:calendar');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->exportCalendar();
        $this->importCalendar();
    }

    protected function exportCalendar()
    {
        print_r("\r\n Export calendar process started **********");
        $properties = $this->m->fetchAll('SELECT * FROM `property`');

        foreach ($properties as $key => $property) {
            print_r("\r\n Process started for property id ".$property['id']);
            $reservations = $this->m->fetchAll('SELECT * FROM `reservation` WHERE `property_id` = :property_id ', [
                ':property_id' => $property['id'],
            ]);

            $calFileName = md5($property['id']);
            $vcal_header = 'BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//jervis.com 0.1//EN
CALSCALE:GREGORIAN';
            $vcal_footer = 'END:VCALENDAR';
            $vcal_events = array();

            foreach ($reservations as $key => $reservation) {
                $guest = $this->m->fetch('SELECT * FROM `guest` WHERE `id` = :guest_id ', [
                    ':guest_id' => $reservation['guest_id'],
                ]);

                $reservation['check_in_date'] = date('Ymd\THis\Z', strtotime($reservation['check_in_date']));
                $reservation['check_out_date'] = date('Ymd\THis\Z', strtotime($reservation['check_out_date']));
                $ts = date('Ymd\THis\Z', time());
                $vcal_events[] = "BEGIN:VEVENT
DTEND;VALUE=DATE:{$reservation['check_out_date']}
DTSTART;VALUE=DATE:{$reservation['check_in_date']}
DTSTAMP:{$ts}
UID:{$reservation['id']}@jervis.com
SUMMARY:{$guest['first_name']} {$guest['last_name']} ({$reservation['id']})
END:VEVENT";
            }
            $vcal_data = $vcal_header."\n".
            implode("\n", $vcal_events)."\n".
            $vcal_footer;

            $dirPath = getcwd().'/ics';
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0755, true);
            }

            file_put_contents('ics/'.$calFileName.'.ics', $vcal_data);
            print_r("\r\n Process completed for property id ".$property['id']);
        }
        print_r("\r\n Export calendar process completed **********");
    }

    protected function importCalendar()
    {
        print_r("\r\n Import calendar process completed **********");
        $calendars = $this->m->fetchAll('SELECT * FROM `calendar`');

        foreach ($calendars as $key => $calendar) {
            print_r("\r\n Process started for property id ".$calendar['property_id'].' source file is '.$calendar['booking_source']);
            $context = stream_context_create(
                array(
                    'http' => array(
                        'header' => 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
                    ),
                )
            );
            $ical = file_get_contents($calendar['url'], false, $context);
            preg_match_all('/DTSTART;VALUE=DATE:([\d]{8})/sU', $ical, $checkins);
            preg_match_all('/DTEND;VALUE=DATE:([\d]{8})/sU', $ical, $checkouts);
            preg_match_all('/DTSTAMP:([\d]{8})/sU', $ical, $stamps);
            preg_match_all('/UID:([^\r\n]+)/', $ical, $uids);
            preg_match_all('/SUMMARY:([^\r\n]+)/', $ical, $summaries);
            for ($i = 0; $i < count($checkins[0]); ++$i) {
                //check if uid is exist
                print_r("\r\n".$this->add_minus_to_ymd($checkins[1][$i]).' - '.$this->add_minus_to_ymd($checkouts[1][$i]).' - '.$summaries[1][$i]);
                $calendar_data = $this->m->fetch('SELECT * FROM calendar_data WHERE uid = :uid', [':uid' => $uids[1][$i]]);
                if ($calendar_data) {
                    $this->m->update('calendar_data', [
                        'start_date' => $this->add_minus_to_ymd($checkins[1][$i]),
                        'end_date' => $this->add_minus_to_ymd($checkouts[1][$i]),
                        'summary' => $summaries[1][$i],
                        'modified' => Helpers::mysqlDatetime(time()),
                    ], $uids[1][$i], 'uid');
                } else {
                    //create calendar data
                    $this->m->insert('calendar_data', [
                        'property_id' => $calendar['property_id'],
                        'calendar_id' => $calendar['id'],
                        'start_date' => $this->add_minus_to_ymd($checkins[1][$i]),
                        'end_date' => $this->add_minus_to_ymd($checkouts[1][$i]),
                        'uid' => $uids[1][$i],
                        'summary' => $summaries[1][$i],
                    ]);

                    $propertyModel = new ManagerProperty();
                    $property = $propertyModel->getPropertyById($calendar['property_id']);

                    $userModel = new ManagerUser();
                    $userModel->saveNotificationByUserId($property['owner_id'], $message = '', $link = '', $type = 'reservation');
                }
            }
            print_r("\r\n Process completed for property id ".$calendar['property_id']);
        }
        print_r("\r\n Import calendar process completed **********");
    }

    protected function add_minus_to_ymd($ymd)
    {
        return preg_replace('/([\d]{4})([\d]{2})([\d]{2})/s', '$1-$2-$3', $ymd);
    }
}
