<?php

use Respect\Rest\Routable;

class RestController implements Routable
{
    public $userId;

    public function __construct()
    {
        error_reporting(0);
        try {
            $accessToken = $this->getBearerToken();
            $ApiOauthAccessTokenController = new ApiOauthAccessTokenController();
            $response = json_decode($ApiOauthAccessTokenController->get($accessToken));
            if (isset($response->token->userId)) {
                $this->userId = $response->token->userId;
                Model::sessionSet('managerId', $this->userId);
            }
        } catch (Exception $ex) {
        }
    }

    public function currentUserHasPermission(array $property, $permissionName)
    {
        if ($this->userId == $property['owner_id']) {
            return true;
        }
        $managerRoleModel = new ManagerRole();
        $permissions = $managerRoleModel->getCachedUserPropertyPermissions($this->userId, $property['id']);

        return $permissions[$permissionName];
    }

    public function currentUserHasAnyPermission(array $property, array $permissionNames)
    {
        if ($this->userId == $property['owner_id']) {
            return true;
        }
        $managerRoleModel = new ManagerRole();
        $permissions = $managerRoleModel->getCachedUserPropertyPermissions($this->userId, $property['id']);
        foreach ($permissionNames as $permissionName) {
            if ($permissions[$permissionName]) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get header Authorization.
     * */
    public function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER['Authorization']);
        } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER['HTTP_AUTHORIZATION']);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }

        return $headers;
    }

    /**
     * get access token from header.
     * */
    public function getBearerToken()
    {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s((.*)\.(.*)\.(.*))/', $headers, $matches)) {
                return $matches[1];
            }
        }

        return null;
    }

    /**
     * Response.
     *
     * Takes pure data and optionally a status code, then creates the response.
     * Set $continue to TRUE to flush the response to the client and continue running the script.
     *
     * @param array    $data
     * @param int|null $http_code
     * @param bool     $continue
     */
    public function response($data = null, $http_code = null, $continue = false)
    {
        // If data is NULL and not code provide, error and bail
        if ($data === null && $http_code === null) {
            $http_code = 404;

            // create the output variable here in the case of $this->response(array());
            $output = null;
        }
        // If data is NULL but http code provided, keep the output empty
        elseif ($data === null && is_numeric($http_code)) {
            $output = null;
        }
        // Otherwise (if no data but 200 provided) or some data, carry on camping!
        else {
            is_numeric($http_code) || $http_code = 200;

            // Set the correct format header
            header('Content-Type: application/json; charset='.strtolower('UTF-8'));
            $output = $data;
        }

        $this->set_status_header($http_code);

        if ($continue) {
            Model::sessionUnset('managerId');

            return json_encode($output);
            exit;
//            ob_end_flush();
//            ob_flush();
//            flush();
        } else {
            if ($http_code >= 200 && $http_code < 300) {
                if (is_array($data)) {
                    $finalOutput['message'] = 'Success';
                    $finalOutput['data'] = $data;
                } else {
                    $finalOutput['message'] = $data;
                }

                exit(json_encode($finalOutput));
            } else {
                if (is_array($data['message'])) {
                    $messages = $data['message'];
                    $response = array();
                    foreach ($messages as $key => $value) {
                        $res['type'] = $key;
                        $res['message'] = $value;
                        $response[] = $res;
                    }
                } else {
                    $res['type'] = 'error';
                    $res['message'] = $data['message'];
                    $response[] = $res;
                }

                $finalOutput['message'] = '';
                $finalOutput['errors'] = $response;
                exit(json_encode($finalOutput));
            }

            $finalOutput['data'] = $output;
            exit(json_encode($finalOutput));
        }
    }

    /**
     * Set HTTP Status Header.
     *
     * @param	int	the status code
     * @param	string
     */
    public function set_status_header($code = 200, $text = '')
    {
        if (empty($code) or !is_numeric($code)) {
            show_error('Status codes must be numeric', 500);
        }

        if (empty($text)) {
            is_int($code) or $code = (int) $code;
            $stati = array(
                    100 => 'Continue',
                    101 => 'Switching Protocols',

                    200 => 'OK',
                    201 => 'Created',
                    202 => 'Accepted',
                    203 => 'Non-Authoritative Information',
                    204 => 'No Content',
                    205 => 'Reset Content',
                    206 => 'Partial Content',

                    300 => 'Multiple Choices',
                    301 => 'Moved Permanently',
                    302 => 'Found',
                    303 => 'See Other',
                    304 => 'Not Modified',
                    305 => 'Use Proxy',
                    307 => 'Temporary Redirect',

                    400 => 'Bad Request',
                    401 => 'Unauthorized',
                    402 => 'Payment Required',
                    403 => 'Forbidden',
                    404 => 'Not Found',
                    405 => 'Method Not Allowed',
                    406 => 'Not Acceptable',
                    407 => 'Proxy Authentication Required',
                    408 => 'Request Timeout',
                    409 => 'Conflict',
                    410 => 'Gone',
                    411 => 'Length Required',
                    412 => 'Precondition Failed',
                    413 => 'Request Entity Too Large',
                    414 => 'Request-URI Too Long',
                    415 => 'Unsupported Media Type',
                    416 => 'Requested Range Not Satisfiable',
                    417 => 'Expectation Failed',
                    422 => 'Unprocessable Entity',
                    426 => 'Upgrade Required',
                    428 => 'Precondition Required',
                    429 => 'Too Many Requests',
                    431 => 'Request Header Fields Too Large',

                    500 => 'Internal Server Error',
                    501 => 'Not Implemented',
                    502 => 'Bad Gateway',
                    503 => 'Service Unavailable',
                    504 => 'Gateway Timeout',
                    505 => 'HTTP Version Not Supported',
                    511 => 'Network Authentication Required',
                );

            if (isset($stati[$code])) {
                $text = $stati[$code];
            } else {
                return 'No status text available. Please check your status code number or supply your own message text.';
            }
        }

        $server_protocol = (isset($_SERVER['SERVER_PROTOCOL']) && in_array($_SERVER['SERVER_PROTOCOL'], array('HTTP/1.0', 'HTTP/1.1', 'HTTP/2'), true))
                ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1';
        header($server_protocol.' '.$code.' '.$text, true, $code);
    }
}
