<?php

class IndexController extends ViewController
{
    public function index()
    {
        self::viewTwig('index.html', []);
    }
}
