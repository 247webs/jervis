<?php

class LoginController extends ViewController
{
    protected $m;
    protected $authenticator;
    protected $secret;

    public function __construct()
    {
        $this->m = self::model('Login');
        $this->authenticator = new PHPGangsta_GoogleAuthenticator();
        $this->secret = $this->authenticator->createSecret();
    }

    public function index()
    {
        $data['title'] = 'Jervis Systems - Manager Login';
        self::viewTwig('login/index.html', $data);
    }

    public function doLogin()
    {
        $email = (isset($_POST['email']) and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) ? $_POST['email'] : '';
        $password = (isset($_POST['password']) and !empty($_POST['password'])) ? $_POST['password'] : '';
        if ($this->m->doLogin($email, $password, $this->secret)) {
            $returnUrl = Model::sessionGet('returnUrl');
            if ($returnUrl) {
                Model::sessionUnset('returnUrl');
                Helpers::redirectRelative($returnUrl);
            } else {
                $response['error'] = false;
                $response['description'] = 'Login Successful';
                if (Model::sessionGet('managerMfa') == 1) {
                    $response['mfa'] = true;
                } else {
                    $response['mfa'] = false;
                }
                exit(json_encode($response));
            }
        } else {
            Helpers::redirect('login');
        }
    }

    public function emailVerification()
    {
        $token = $_GET['token'];
        $splitToken = explode('_', $token);
        if (count($splitToken) != 2) {
            return false;
        }
        list($_, $userId) = $splitToken;
        if (sha1(DB_SALT . $userId) . '_' . $userId != $token) {
            echo 'not valid';
        } else {
            $this->m->verifyAccout($userId);
            $url = URL . '/login';
            echo "Your account is verified. <a href=\"{$url}\">Click here for login</a><br>";
        }
    }

    public function varifyCode()
    {
        $data['title'] = 'Jervis Systems - Manager Login';
        $success = $this->authenticator->verifyCode(Model::sessionGet('managerGoogleAuthCode'), $_POST['otp'], 0);
        if ($success) {
            $response['error'] = false;
            $response['description'] = 'Login Successful';
            exit(json_encode($response));
        } else {
            $response['error'] = true;
            $response['description'] = 'Invalid Code !';
            exit(json_encode($response));
        }
    }
}
