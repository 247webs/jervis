<?php

class SignupController extends ViewController
{
    protected $m;
    protected $authenticator;

    public function __construct()
    {
        $this->m = self::model('Signup');
        $this->authenticator = new PHPGangsta_GoogleAuthenticator();
    }

    public function index()
    {
        $secret = $this->authenticator->createSecret();
        $qrCodeUrl = $this->authenticator->getQRCodeGoogleUrl('jervis', $secret, URL);
        $data['title'] = 'Jervis Systems - Manager Signup';
        $data['plans'] = $this->m->getStripePlans();
        $timezone = Helpers::timezone();
        $data['timezone'] = $timezone;
        $data['qrCodeUrl'] = $qrCodeUrl;
        $data['secret'] = $secret;
        self::viewTwig('signup/index.html', $data);
    }

    public function doSignup()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }
        $response = array();
        $email = (isset($_POST['email']) and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) ? $_POST['email'] : '';
        if ($this->m->userExists($email)) {
            $response['description'] = 'User already exists';
            $response['error'] = true;
        } else {
            if (isset($_POST['otp']) && isset($_POST['secret']) && $_POST['otp'] != '' && $_POST['secret'] != '') {
                if ($this->authenticator->verifyCode($_POST['secret'], $_POST['otp'], 0)) {
                    $_POST['mfa'] = '1';
                } else {
                    echo json_encode(['invalid_otp' => true, 'message' => 'Invalid OTP !']);
                    exit;
                }
            }
            $response = $this->m->saveUser($_POST);
        }
        echo json_encode($response);
    }
}
