<?php

class ManagerChatApiController extends ManagerController
{
    public function saveChatMessage()
    {
        $ChatModel = self::model('ManagerChat');
        $notificationData = $ChatModel->saveChatNotification($_POST);
        $userData = $ChatModel->saveChatMessage($_POST);
        echo json_encode($userData);
    }

    public function updateChatMessage()
    {
        $ChatModel = self::model('ManagerChat');
        $ChatModel->updateChatMessage($_POST);
        echo json_encode('');
    }

    public function saveGroupChatMessage()
    {
        $groupChatModel = self::model('ManagerGroupChat');
        $ChatModel = self::model('ManagerChat');
        $notificationData = $groupChatModel->saveGroupChatNotification($_POST);
        $userData = $ChatModel->saveChatMessage($_POST);
        echo json_encode($userData);
    }

    public function updateGroupChatMessage()
    {
        $groupChatModel = self::model('ManagerGroupChat');
        $groupChatModel->updateGroupChatMessage($_POST);
        echo json_encode('');
    }

    public function deleteChat()
    {
        $ChatModel = self::model('ManagerChat');
        $deleteChat = $ChatModel->deleteChat($_POST);
        echo json_encode('');
    }

    public function deleteGroupChat()
    {
        $groupChatModel = self::model('ManagerGroupChat');
        $deleteChat = $groupChatModel->deleteGroupChat($_POST);
        echo json_encode('');
    }

    public function addGroup()
    {
        $groupChatModel = self::model('ManagerGroupChat');
        $addGroup = $groupChatModel->addGroup($_POST);
        echo json_encode('');
    }

    public function editgroup()
    {
        $groupChatModel = self::model('ManagerGroupChat');
        $addGroup = $groupChatModel->editgroup($_POST);
        echo json_encode('');
    }
}
