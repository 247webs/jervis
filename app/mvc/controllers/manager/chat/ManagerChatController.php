<?php

class ManagerChatController extends ManagerController
{
    public function index()
    {
        $ChatModel = self::model('ManagerChat');
        $groupChatModel = self::model('ManagerGroupChat');
        $userData = $ChatModel->getChatUsers();
        $chatNotification = $ChatModel->getChatNotification();
        $groupChatNotification = $groupChatModel->getGroupChatNotification();
        $group = $groupChatModel->getGroupDetails();
        $latestUserId = $ChatModel->getLatestChatFromUserId();

        self::viewTwig('manager/chat/index.html', [
            'userData' => $userData,
            'chatNotification' => $chatNotification,
            'groupChatNotification' => $groupChatNotification,
            'group' => $group,
            'latestUserId' => $latestUserId,
        ]);
    }

    public function getChatMessage()
    {
        $ChatModel = self::model('ManagerChat');
        $deleteChatNotification = $ChatModel->deleteChatNotification($_POST);
        $chatData = $ChatModel->getChatMessage($_POST);

        echo json_encode($chatData);
    }

    public function refreshChatMessage()
    {
        $ChatModel = self::model('ManagerChat');
        $chatData = $ChatModel->getRefreshChatMessage($_POST);
        echo json_encode($chatData);
    }

    public function refreshGroupChatMessage()
    {
        $groupChatModel = self::model('ManagerGroupChat');
        $chatData = $groupChatModel->getRefreshGroupChatMessage($_POST);
        $leatest_id = $groupChatModel->getGroupLeatestChatId($_POST);
        $output = array('chatData' => $chatData, 'leatest_id' => $leatest_id);
        echo json_encode($output);
    }

    public function getGroupChatMessage()
    {
        $groupChatModel = self::model('ManagerGroupChat');
        $deleteChatNotification = $groupChatModel->deleteGroupChatNotification($_POST);
        $NonGroupPerson = $groupChatModel->getNonGroupPerson($_POST);
        $chatData = $groupChatModel->getGroupChatMessage($_POST);
        $leatest_id = $groupChatModel->getGroupLeatestChatId($_POST);
        $returnOutput = array('chatData' => $chatData, 'NonGroupPerson' => $NonGroupPerson, 'leatest_id' => $leatest_id);
        echo json_encode($returnOutput);
    }
}
