<?php

class ManagerUserController extends ManagerController
{
    public function index()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        $roleId = !empty($_GET['roleid']) ? intval($_GET['roleid']) : null;

        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }
        /*
        if (!self::model('ManagerRole')->currentUserHasAnyPermission(
            $property,
            []
        )) {
            self::forbidden('You have insufficient permissions to manage Users.');
        }
        */
        $propertyModel = self::model('ManagerProperty');
        $propertyData = $propertyModel->getAccessByPropertyId($propertyId);
        $managerUserModel = self::model('ManagerUser');
        $roleName = $managerUserModel->getRoleName($propertyData);

        $roles = $managerUserModel->getRolesByPropertyId($propertyId);
        $invitedUserData = $managerUserModel->getInvitedUserData($propertyId);

        self::viewTwig('manager/user/index.html', [
            'propertyData' => $propertyData,
            'invitedUserData' => $invitedUserData,
            'propertyId' => $propertyId,
            'roleName' => $roleName,
            'roles' => $roles,
            'roleId' => $roleId,
        ]);
    }

    public function addUser()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;

        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }
        /*
        if (!self::model('ManagerRole')->currentUserHasAnyPermission(
            $property,
            []
        )) {
            self::forbidden('You have insufficient permissions to manage Users.');
        }
        */
        $roleModel = self::model('ManagerRole');
        $roles = $roleModel->getRolesByPropertyId($propertyId);

        self::viewTwig('manager/user/adduser.html', [
            'propertyId' => $propertyId,
            'roles' => $roles,
        ]);
    }

    public function searchUser($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }
        $propertyId = (int) $params[0];
        $managerUserModel = self::model('ManagerUser');
        $searchData = $managerUserModel->searchUsers($_POST);
        exit(json_encode($searchData));
    }

    public function deleteUser()
    {
        $userId = $_POST['userId'];
        $ManagerUserModal = self::model('ManagerUser');
        $userId = $ManagerUserModal->deleteUser($userId);
        exit(json_encode(''));
    }

    public function rollUpdate()
    {
        $ManagerUserModal = self::model('ManagerUser');
        $updateroleId = $ManagerUserModal->updateRole($_POST);
        exit(json_encode(''));
    }
}
