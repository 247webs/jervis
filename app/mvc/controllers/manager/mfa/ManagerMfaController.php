<?php

class ManagerMfaController extends ManagerController
{
    protected $m;
    protected $authenticator;

    public function __construct()
    {
        $this->m = self::model('Manager');
        $this->authenticator = new PHPGangsta_GoogleAuthenticator();
    }

    public function index()
    {
        $data = [];
        $data['mfa'] = true;
        if (Model::sessionGet('managerMfa') == 0) {
            $secret = $this->authenticator->createSecret();
            $qrCodeUrl = $this->authenticator->getQRCodeGoogleUrl('jervis', $secret, URL);
            $data['secret'] = $secret;
            $data['qrCodeUrl'] = $qrCodeUrl;
            $data['mfa'] = false;
        }
        self::viewTwig('manager/mfa/index.html', $data);
    }

    public function changeMfaStatus()
    {
        if (isset($_POST['otp']) && isset($_POST['secret']) && $_POST['otp'] != '' && $_POST['secret'] != '') {
            if ($this->authenticator->verifyCode($_POST['secret'], $_POST['otp'], 0)) {
                $this->m->changeMfaStatus($_POST['secret'], 1, Model::sessionGet('managerId'));
                echo json_encode(['success' => true]);
            } else {
                $response['error'] = true;
                $response['message'] = 'Invalid Code !';
                echo json_encode($response);
            }
        } else {
            $this->m->changeMfaStatus('', 0, Model::sessionGet('managerId'));
            echo json_encode(['success' => true]);
        }
    }
}
