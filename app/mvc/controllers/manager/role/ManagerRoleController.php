<?php

class ManagerRoleController extends ManagerController
{
    public function index()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }

        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($propertyId);
        if (!$property) {
            self::badRequest('Missing property.');
        }

        $roleModel = self::model('ManagerRole');

        if (!$roleModel->currentUserHasAnyPermission(
            $property,
            [
                'Role.create',
                'Role.edit',
                'Role.delete',
            ]
        )) {
            self::forbidden('You have insufficient permissions to manage roles.');
        }

        $roles = $roleModel->getRolesByPropertyId($propertyId);

        self::viewTwig('manager/role/index.html', [
            'roles' => $roles,
            'property_id' => $propertyId,
            'property' => $property,
        ]);
    }

    public function addRole()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }

        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($propertyId);
        if (!$property) {
            self::badRequest('Missing property.');
        }

        $roleModel = self::model('ManagerRole');

        if (!$roleModel->currentUserHasPermission(
            $property,
            'Role.create'
        )) {
            self::forbidden('You have insufficient permissions to create roles.');
        }

        $permissions = $roleModel->getPermissions();

        $role = [];
        self::viewTwig('manager/role/role_edit.html', [
            'role' => null,
            'permissions' => $permissions,
            'assignedPermissions' => null,
            'access' => [],
            'propertyId' => $propertyId,
        ]);
    }

    public function editRole()
    {
        $roleId = !empty($_GET['id']) ? intval($_GET['id']) : null;
        if (!$roleId) {
            self::notFound("Missing parameter 'id'");
        }

        $roleModel = self::model('ManagerRole');
        $role = $roleModel->getRoleById($roleId);

        if (!$role) {
            self::notFound("Could not find role with id '$roleId");
        }

        $propertyId = $role['property_id'];
        if (!$propertyId) {
            self::notFound("Missing parameter 'property_id'");
        }

        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($propertyId);
        if (!$property) {
            self::badRequest('Missing property.');
        }

        if (!$roleModel->currentUserHasPermission(
            $property,
            'Role.edit'
        )) {
            self::forbidden('You have insufficient permissions to edit roles.');
        }

        $assignedPermissions = $roleModel->getAssignedPermissions($roleId);

        $permissions = $roleModel->getPermissions();

        self::viewTwig('manager/role/role_edit.html', [
            'role' => $role,
            'permissions' => $permissions,
            'assignedPermissions' => $assignedPermissions,
            'access' => [],
            'propertyId' => $propertyId,
            'property' => $property,
        ]);
    }
}
