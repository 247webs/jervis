<?php

class ManagerRoleApiController extends ManagerController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = self::model('ManagerRole');
    }

    public function saveRole($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }
        if (empty($params[0])) {
            $propertyId = !empty($_POST['propertyId']) ? $_POST['propertyId'] : 0;
            if (!$propertyId) {
                self::badRequest('Missing propertyId.');
            }
            $propertyModel = self::model('ManagerProperty');
            $property = $propertyModel->getPropertyById($propertyId);
            if (!$property) {
                self::notFound('Missing property.');
            }
            $roleModel = self::model('ManagerRole');

            if (!$roleModel->currentUserHasPermission(
                $property,
                'Role.create'
            )) {
                self::forbidden('You have insufficient permissions to create roles.');
            }
            // Creating new role
            $this->m->createRole($_POST);
        } else {
            // Editing existing role
            $roleId = intval($params[0]);
            // TODO: Consider which users can edit this
            $role = $this->m->getRoleById($roleId);
            if (!$role) {
                self::notFound('Role cannot be found.');
            }
            $propertyModel = self::model('ManagerProperty');
            $property = $propertyModel->getPropertyById($role['property_id']);
            if (!$property) {
                self::notFound('Missing property.');
            }
            if (!$this->m->currentUserHasPermission(
                $property,
                'Role.edit'
            )) {
                self::forbidden('You have insufficient permissions to edit roles.');
            }
            $this->m->updateRole($roleId, $_POST);
        }
        exit(json_encode(['success' => true]));
    }

    public function deleteRole($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Delete requires the DELETE HTTP method.';
            exit;
        }
        if (isset($params[0]) and !empty($params[0])) {
            $roleId = intval($params[0]);
            // TODO: Consider which users can edit this
            $role = $this->m->getRoleById($roleId);
            if (!$role) {
                self::notFound('Role cannot be found.');
            }

            $propertyModel = self::model('ManagerProperty');
            $property = $propertyModel->getPropertyById($role['property_id']);
            if (!$property) {
                self::notFound('Missing property.');
            }

            if (!$this->m->currentUserHasPermission(
                $property,
                'Role.delete'
            )) {
                self::forbidden('You have insufficient permissions to delete roles.');
            }

            exit(json_encode([
                'success' => $this->m->deleteRole(intval($params[0])),
            ]));
        } else {
            self::badRequest("Missing 'id' for role");
        }
    }

    public function setRole($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }
        if (isset($params[0]) and !empty($params[0])) {
            if (!in_array($params[0], ['Owner', 'Manager', 'Cleaning', 'Handyman'])) {
                self::notFound('Role cannot be found.');
            } else {
                $roleName[$params[0]] = true;
                Model::sessionSet('managerRole', json_decode(json_encode($roleName)));
            }
        } else {
            self::notFound('Role cannot be found.');
        }
    }
}
