<?php

class ManagerSettingsController extends ManagerController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        if (Model::sessionGet('managerAccess')->rci !== true) {
            Helpers::redirect('manager/');
        }
    }

    public function index()
    {
        $data['title'] = 'Jervis Systems - Settings';
        self::view('manager/settings/index', $data, 'manager');
    }
}
