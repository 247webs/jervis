<?php

class ManagerController extends ViewController
{
    protected $m;

    public function __construct()
    {
        $this->m = self::model('Manager');

        $freeTrialEnd = Model::sessionGet('freeTrialEnd');
        if ($freeTrialEnd) {
            $profileModel = self::model('ManagerProfile');

            $subscriptions = $profileModel->getSubscriptionsByOwnerId(Model::sessionGet('managerId'));

            $card = $profileModel->retrieveStripeCustomer($subscriptions);

            $billingPlans = $profileModel->getBillingPlans();
            Model::sessionUnset('freeTrialEnd');
            self::viewTwig('manager/profile/billing.html', [
                'card' => $card,
                'PUBLISHABLE_KEY' => PUBLISHABLE_KEY,
                'billingPlans' => $billingPlans,
            ]);
            exit;
        }

        if (!$this->m->isLogged()) {
            Model::sessionSet('returnUrl', Helpers::getRequestUrl());
            Helpers::redirect('login');
        }
    }

    public function logout()
    {
        $this->m->logout();
        Helpers::redirect();
    }

    public function index()
    {
        $userRole = Model::sessionGet('managerRole');

        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;

        $propertyModel = self::model('ManagerProperty');
        $properties = $propertyModel->getPropertiesRelevantForCurrentUser();

        if (!$propertyId) {
            if ($properties) {
                $propertyId = $properties[0]['id'];
            }
        }

        $fillRequestModel = self::model('ManagerChecklistFillRequest');
        $requests = $fillRequestModel->getRequestsByPropertyId($propertyId);

        $manager = self::model('Manager')->getManagerById(Model::sessionGet('managerId'));
        $managerPlan = json_decode($manager['plan']);
        $devices = '';
        $chatData = '';
        $usersCount = '';
        $reservationsCount = '';
        $UpcomingSliderMenu = '';
        if (isset($userRole->Owner) && $userRole->Owner == 1) {
            $deviceModel = self::model('ManagerDevice');
            $devices = $deviceModel->getDevicesByPropertyId($propertyId);

            $chatModel = self::model('ManagerChat');
            $chatData = $chatModel->getRecentChat();

            $usersCount = $propertyModel->getUsersRelevantForCurrentUser();
            $reservationsCount = self::model('ManagerReservation')->getReservationsRelevantForCurrentUser();
            $UpcomingSliderMenu = self::model('Manager')->getUpcomingreservationSliderMenu();
            self::viewTwig('manager/owner.html', [
                'upcomingReservations' => $this->m->getUpcomingReservationsByPropertyId($propertyId),
                'completedReservations' => $this->m->getCompletedReservationsByPropertyId($propertyId),
                'todayreservations' => $this->m->getTodayUpcomingReservationsByPropertyId($propertyId),
                'requests' => $requests,
                'properties' => $properties,
                'devices' => $devices,
                'managerPlan' => $managerPlan,
                'usersCount' => $usersCount,
                'reservationsCount' => $reservationsCount,
                'chatData' => $chatData,
                'UpcomingSliderMenu' => $UpcomingSliderMenu,
            ]);
        } elseif (isset($userRole->Manager) && $userRole->Manager == 1) {
            $deviceModel = self::model('ManagerDevice');
            $devices = $deviceModel->getDevicesByPropertyId($propertyId);

            $chatModel = self::model('ManagerChat');
            $chatData = $chatModel->getRecentChat($propertyId);

            $usersCount = $propertyModel->getUsersRelevantForCurrentUser();
            $reservationsCount = self::model('ManagerReservation')->getReservationsRelevantForCurrentUser();
            $UpcomingSliderMenu = self::model('Manager')->getUpcomingreservationSliderMenu();
            self::viewTwig('manager/manager.html', [
                'upcomingReservations' => $this->m->getUpcomingReservationsByPropertyId($propertyId),
                'completedReservations' => $this->m->getCompletedReservationsByPropertyId($propertyId),
                'todayreservations' => $this->m->getTodayUpcomingReservationsByPropertyId($propertyId),
                'requests' => $requests,
                'properties' => $properties,
                'devices' => $devices,
                'managerPlan' => $managerPlan,
                'usersCount' => $usersCount,
                'reservationsCount' => $reservationsCount,
                'chatData' => $chatData,
                'UpcomingSliderMenu' => $UpcomingSliderMenu,
            ]);
        } elseif (isset($userRole->Cleaning) && $userRole->Cleaning == 1) {
            self::viewTwig('manager/cleaning.html', [
                'upcomingReservations' => $this->m->getUpcomingReservationsByPropertyId($propertyId),
                'completedReservations' => $this->m->getCompletedReservationsByPropertyId($propertyId),
                'todayreservations' => $this->m->getTodayUpcomingReservationsByPropertyId($propertyId),
                'requests' => $requests,
                'properties' => $properties,
                'devices' => $devices,
                'managerPlan' => $managerPlan,
                'usersCount' => $usersCount,
                'reservationsCount' => $reservationsCount,
                'chatData' => $chatData,
                'UpcomingSliderMenu' => $UpcomingSliderMenu,
            ]);
        } elseif (isset($userRole->Handyman) && $userRole->Handyman == 1) {
            $deviceModel = self::model('ManagerDevice');
            $devices = $deviceModel->getDevicesByPropertyId($propertyId);

            self::viewTwig('manager/handyman.html', [
                'upcomingReservations' => $this->m->getUpcomingReservationsByPropertyId($propertyId),
                'completedReservations' => $this->m->getCompletedReservationsByPropertyId($propertyId),
                'todayreservations' => $this->m->getTodayUpcomingReservationsByPropertyId($propertyId),
                'requests' => $requests,
                'properties' => $properties,
                'devices' => $devices,
                'managerPlan' => $managerPlan,
                'usersCount' => $usersCount,
                'reservationsCount' => $reservationsCount,
                'chatData' => $chatData,
                'UpcomingSliderMenu' => $UpcomingSliderMenu,
            ]);
        } elseif (isset($userRole->Guest) && $userRole->Guest == 1) {
            $url = '/manager/chat/index?property_id='.$propertyId;
            header('Location: '.$url);
            exit;
        }
    }

    public function notFound($error)
    {
        http_response_code(404);
        echo '<h1>Not found</h1>';
        exit(is_array($error) ? implode("<br>\r\n", $error) : $error);
    }

    public function badRequest($error)
    {
        http_response_code(400);
        echo '<h1>Bad request</h1>';
        exit(is_array($error) ? implode("<br>\r\n", $error) : $error);
    }

    public function forbidden($error)
    {
        http_response_code(403);
        echo '<h1>Forbidden</h1>';
        exit(is_array($error) ? implode("<br>\r\n", $error) : $error);
    }

    public function methodNotAllowed($error)
    {
        http_response_code(405);
        echo '<h1>Method not allowed</h1>';
        exit(is_array($error) ? implode("<br>\r\n", $error) : $error);
    }

    public function getUpcomingdatesliderData()
    {
        $data = $this->m->getUpcomingdatesliderData($_POST);
        exit(json_encode($data));
    }

    public function getAllReservationsByPropertyId()
    {
        $propertyId = $_POST['Propertyid'];
        $data = $this->m->getAllReservationsByPropertyId($propertyId);
        exit(json_encode($data));
    }

    public function getAllSalesavgChartByPropertyId()
    {
        $propertyId = $_POST['Propertyid'];
        $str = $_POST['str'];
        $data = $this->m->getSalesavgChartByPropertyId($propertyId, $str);
        exit(json_encode($data));
    }

    public function getMonthlyReservationsByPropertyId()
    {
        $propertyId = $_POST['Propertyid'];
        $data = $this->m->getMonthlyReservationsByPropertyId($propertyId);
        echo json_encode($data);
    }

    public function getYearlyReservationsByPropertyId()
    {
        $propertyId = $_POST['Propertyid'];
        $data = $this->m->getYearlyReservationsByPropertyId($propertyId);
        echo json_encode($data);
    }
}
