<?php

class ManagerNotificationTemplateApiController extends ApiController
{
    protected $m;

    public function __construct()
    {
        // parent::__construct();
        $this->m = self::model('ManagerNotificationTemplate');
    }

    public function saveTemplate()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            self::methodNotAllowed('Save requires POST HTTP method.');
            exit;
        }

        $templateModel = $this->m;
        $propertyModel = self::model('ManagerProperty');

        $templateId = null;
        $propertyId = null;

        if (!empty($_GET['id'])) {
            $templateId = intval($_GET['id']);
            $template = $templateModel->getTemplateById($templateId);
            if (!$template) {
                self::notFound('Template could not be found.');
            }
            $propertyId = $template['property_id'];
        } elseif (!empty($_GET['property_id'])) {
            $propertyId = intval($_GET['property_id']);
        }

        $property = $propertyModel->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Property could not be found.');
        }

        $errors = [];
        $template = $templateModel->saveTemplate($property['id'], $templateId, $_POST, $errors);
        if ($template) {
            exit(json_encode(['template' => $template]));
        } else {
            self::badRequest($errors);
        }
    }

    public function saveNotification()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            self::methodNotAllowed('Save requires POST HTTP method.');
            exit;
        }

        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($propertyId);

        if (!$property) {
            self::notFound('Property could not be found.');
        }

        $ManagerNotificationModel = self::model('ManagerNotification');
        $notification = $ManagerNotificationModel->sendNotification($property, $_POST);
        if ($notification) {
            exit(json_encode(['notification' => $notification]));
        } else {
            self::badRequest([]);
        }
    }

    public function deleteNotificationTemplate($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Delete requires the DELETE HTTP method.';
            exit;
        }
        $template = $this->m->getTemplateById($params[0]);
        if (!$template) {
            self::notFound('Template cannot be found.');
        }
        $propertyId = $template['property_id'];
        $property = self::model('ManagerProperty')->getPropertyById($propertyId, false);
        if (!$property) {
            self::notFound('Property could not be found.');
        }

        if (isset($params[0]) and !empty($params[0])) {
            exit(json_encode([
                'success' => $this->m->deleteNotificationTemplate(intval($params[0])),
            ]));
        } else {
            self::badRequest("Missing 'id' for Template");
        }
    }
}
