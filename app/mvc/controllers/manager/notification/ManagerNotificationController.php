<?php

class ManagerNotificationController extends ManagerController
{
    public function index()
    {
    }

    public function template()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($propertyId);
        if (!$property) {
            self::forbidden('You have insufficient permissions to access this property.');
        }

        if (!self::model('ManagerRole')->currentUserHasPermission($property, 'NotificationTemplate.edit')) {
            self::forbidden('You have insufficient permissions to manage notification templates.');
        }

        $templateModel = self::model('ManagerNotificationTemplate');
        $templates = $templateModel->getTemplatesByPropertyId($propertyId);

        self::viewTwig('manager/notification/template.html', [
            'templates' => $templates,
            'property' => $property,
        ]);
    }

    public function editTemplate()
    {
        $templateId = !empty($_GET['id']) ? intval($_GET['id']) : null;
        if (!$templateId) {
            self::notFound("Missing parameter 'id'");
        }

        $templateModel = self::model('ManagerNotificationTemplate');
        $template = $templateModel->getTemplateById($templateId);
        if (!$template) {
            self::notFound("Could not find template with id '$templateId");
        }

        if ($template['send_via'] == 'Email') {
            $template['content'] = str_replace(array("\r", "\n"), '', $template['content']);
            $template['content'] = str_replace("'", "\'", $template['content']);
        }

        $durations = $templateModel->getDurations();

        self::viewTwig('manager/notification/template_edit.html', [
            'template' => $template,
            'propertyId' => $template['property_id'],
            'durations' => $durations,
        ]);
    }

    public function addTemplate()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Property missing.');
        }

        $durations = self::model('ManagerNotificationTemplate')->getDurations();

        self::viewTwig('manager/notification/template_add.html', [
            'template' => null,
            'propertyId' => $propertyId,
            'durations' => $durations,
        ]);
    }

    public function customNotification()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }

        $notificationModel = self::model('ManagerNotification');
        $notifications = $notificationModel->getCustomNotificationsByPropertyId($propertyId);

        self::viewTwig('manager/notification/index.html', [
            'propertyId' => $propertyId,
            'notifications' => $notifications,
        ]);
    }

    public function addNotification()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }

        $templateModel = self::model('ManagerNotificationTemplate');
        $durations = $templateModel->getDurations();
        $roleModel = self::model('ManagerRole');
        $roles = $roleModel->getRolesByPropertyId($propertyId);
        self::viewTwig('manager/notification/notification.html', [
            'propertyId' => $propertyId,
            'durations' => $durations,
            'roles' => $roles,
        ]);
    }
}
