<?php

class ManagerRciController extends ManagerController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        if (Model::sessionGet('managerAccess')->rci !== true) {
            Helpers::redirect('manager/');
        }
    }

    public function show()
    {
        $data['title'] = 'Jervis Systems - RCI Results';
        self::view('manager/rci/index', $data, 'manager');
    }

    public function account()
    {
        $data['title'] = 'Jervis Systems - RCI Accounts';
        self::view('manager/rci/account', $data, 'manager');
    }

    public function recipient()
    {
        $data['title'] = 'Jervis Systems - RCI Recipients';
        self::view('manager/rci/recipient', $data, 'manager');
    }
}
