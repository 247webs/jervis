<?php

class ManagerRciRecipientApiController extends ManagerRciController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = self::model('ManagerRciRecipient');
    }

    public function show()
    {
        $apiResponse = $this->m->getAll();
        View::apiRender($apiResponse);
    }

    public function set()
    {
        $apiResponse = $this->m->set($_POST);
        View::apiRender($apiResponse);
    }

    public function del($params)
    {
        if (isset($params[0]) and !empty($params[0])) {
            $apiResponse = $this->m->del(intval($params[0]));
        }
        View::apiRender($apiResponse);
    }
}
