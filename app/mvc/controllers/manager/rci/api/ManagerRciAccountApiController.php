<?php

class ManagerRciAccountApiController extends ManagerRciController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = self::model('ManagerRciAccount');
    }

    public function show()
    {
        $apiResponse = $this->m->getAll();
        View::apiRender($apiResponse);
    }

    public function set()
    {
        $apiResponse = $this->m->set($_POST);
        View::apiRender($apiResponse);
    }

    public function get($params)
    {
        if (isset($params[0]) and !is_null($params[0])) {
            $apiResponse = $this->m->get(intval($params[0]));
        }
        View::apiRender($apiResponse);
    }

    public function del($params)
    {
        if (isset($params[0]) and !empty($params[0])) {
            $apiResponse = $this->m->del(intval($params[0]));
        }
        View::apiRender($apiResponse);
    }
}
