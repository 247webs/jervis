<?php

class ManagerPropertyController extends ManagerController
{
    public function index()
    {
        $propertyModel = self::model('ManagerProperty');
        $properties = $propertyModel->getPropertiesRelevantForCurrentUser();

        $propertyIds = array_column($properties, 'property_id');
        $access = $propertyModel->getAccessByPropertyIds($propertyIds);

        foreach ($properties as &$property) {
            $property['managers'] = [];
            if (!empty($access[$property['property_id']])) {
                $property['managers'] = array_map(
                    function ($v) {
                        return $v['first_name'].' '.$v['last_name'];
                    },
                    $access[$property['property_id']]
                );
            }
        }
        unset($property);

        self::viewTwig('manager/property/index.html', [
            'properties' => $properties,
        ]);
    }

    public function addProperty()
    {
        self::viewTwig('manager/property/property_edit.html', ['property' => [], 'access' => [], 'roles' => []]);
    }

    public function editProperty()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::notFound("Missing parameter 'property_id'");
        }
        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($propertyId);

        if (!$property) {
            self::notFound("Could not find property with id '$propertyId");
        }
        if (!self::model('ManagerRole')->currentUserHasAnyPermission(
            $property,
            [
                'Property.editDetails',
                'Property.assignRole',
                'Property.invite',
                'Property.expel',
            ]
        )) {
            self::forbidden('You have insufficient permissions to edit this property.');
        }

        $access = $propertyModel->getAccessByPropertyId($propertyId);
        $roleModel = self::model('ManagerRole');
        $roles = $roleModel->getRolesByPropertyId($propertyId);

        self::viewTwig('manager/property/property_edit.html', [
            'property' => $property,
            'access' => $access,
            'roles' => $roles,
        ]);
    }

    public function acceptInvitation()
    {
        // token
        $token = empty($_GET['token']) ? null : $_GET['token'];
        if (!$token) {
            self::notFound("Missing parameter 'token'");
        }
        $managerEmail = Model::sessionGet('managerEmail');
        /**
         * @var ManagerProperty propertyModel;
         */
        $propertyModel = self::model('ManagerProperty');
        if (!$propertyModel->isValidToken($token, $managerEmail, $propertyId, $roleId)) {
            self::badRequest('Token is expired or sent to an email different from the one connected to this account.');
        }
        $property = $propertyModel->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Property does not exist');
        }

        $role = self::model('ManagerRole')->getRoleById($roleId);

        if (!$role) {
            self::notFound("Could not find role with id $roleId");
        }

        if ($role['property_id'] != $propertyId) {
            self::forbidden('Invitation was for a role that does not belong to this property.');
        }

        if ($this->isPost()) {
            $managerUserModel = self::model('ManagerUser');

            $roles = $managerUserModel->getRoleByPropertyIdAndRoleId(ManagerProperty::$mId, $propertyId);

            if (!empty($roles)) {
                $decodedJson = json_decode($roles['access'], true);
                $decodedJson[$roleId] = true;
                $access = (object) $decodedJson;
            } else {
                $access = (object) [$roleId => true];
            }

            $propertyModel->setPropertyAccess($propertyId, ManagerProperty::$mId, $access);

            $deleteInvitedUsers = $managerUserModel->deleteInvitedUsers(ManagerProperty::$mId);
            $propertyOwner = $this->m->fetch(
                'SELECT * FROM manager WHERE id = :id',
                [':id' => $property['owner_id']]
            );
            if (!$propertyOwner) {
                self::notFound('Property owner does not exist');
            }
            // TODO: Show message that you will first get access to the property when the owner has assigned a role

            $mail = Helpers::createEmail();
            $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
            $mail->addAddress($propertyOwner['email']);
            $mail->isHTML(true);
            $newUserName = "{$this->m->sessionGet('managerFirstName')} {$this->m->sessionGet('managerLastName')}";

            $modelAdminNotification = new AdminNotification();
            $emailTemplate = $modelAdminNotification->getTemplateByTitle('Property - New user added confirmation');

            $subject = str_replace('{USER_NAME}', $newUserName, $emailTemplate['subject']);
            $body = str_replace('{USER_NAME}', $newUserName, $emailTemplate['body']);
            $body = str_replace('{PROPERTY_NAME}', $property['title'], $body);

            $mail->Subject = $subject;
            $mail->Body = $body;
            $mail->send();
            $url = '/manager/user/index?property_id='.$propertyId;
            $message = "{$newUserName} added to property";

            $managerUserModel->saveNotificationByUserId($propertyOwner['id'], $message, $url);

            self::viewTwig(
                'manager/message.html',
                [
                    'header' => $property['title'],
                    'message' => "You have succesfully been added to the property {$property['title']}.",
                ]
            );
        } else {
            self::viewTwig('manager/property/property_invite_show.html', [
                'property' => $property,
                'token' => $token,
            ]);
        }
    }
}
