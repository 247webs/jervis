<?php

class ManagerPropertyApiController extends ManagerController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = self::model('ManagerProperty');
    }

    public function saveProperty($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }
        if (empty($params[0])) {
            /*check user property plan */
            // $managerModel = new Manager();
            // $manager = $managerModel->getManagerById(Model::sessionGet('managerId'));
            // $managerPlan = json_decode($manager['plan']);

            $properties = $this->m->getPropertiesRelevantForCurrentUser();

            // if (isset($managerPlan->property) && count($properties) >= $managerPlan->property) {
            //     exit(json_encode(['success' => false, 'message' => 'You have insufficient limits to create property.']));
            // }

            // Creating new property
            $this->m->createProperty($_POST);

        /* Email notification – This can be at specific thresholds.*/
            // $this->m->sendEmailNotificationForPropertyLimit($properties, $manager);
        } else {
            // Editing existing property
            $propertyId = intval($params[0]);
            $property = $this->m->getPropertyById($propertyId);
            if (!$property) {
                self::notFound('Property cannot be found.');
            }
            $roleModel = self::model('ManagerRole');
            if (!$roleModel->currentUserHasAnyPermission($property, [
                'Property.editDetails',
                'Property.assignRole',
                'Property.expel',
            ])) {
                self::forbidden('You are not authorized to edit this property.');
            }
            Model::getDb()->beginTransaction();
            if ($roleModel->currentUserHasPermission($property, 'Property.editDetails')) {
                $this->m->updatePropertyDetails($propertyId, $_POST);
            }
            if ($roleModel->currentUserHasPermission($property, 'Property.assignRole')) {
                // We can only expel if we are also doing inserts after, otherwise we'll remove all members.
                // Down the line we may want to just remove the ones that aren't in the payload and allow for
                // separation of `assignRole` and `expel` but it does not seem important.
                if ($roleModel->currentUserHasPermission($property, 'Property.expel')) {
                    $this->m->deletePropertyPermissions($propertyId);
                }

                $this->m->insertPropertyPermissions($propertyId, $_POST);
            }
            Model::getDb()->commit();
        }
        exit(json_encode(['success' => true]));
    }

    public function deleteProperty($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Delete requires the DELETE HTTP method.';
            exit;
        }
        // Must be owner
        $propertyId = intval($params[0]);
        $property = $this->m->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Property cannot be found.');
        }
        if ($property['owner_id'] != Model::sessionGet('managerId')) {
            self::forbidden('Only the property owner can delete a property.');
        }

        if (isset($params[0]) and !empty($params[0])) {
            exit(json_encode([
                'success' => $this->m->deleteProperty(intval($params[0])),
            ]));
        } else {
            self::badRequest("Missing 'id' for property");
        }
    }

    public function inviteToRole($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Invite to Role requires the POST HTTP method.';
            exit;
        }
        $propertyId = !empty($params[0]) ? $params[0] : 0;
        $property = $this->m->getPropertyById($params[0]);
        if (!$property) {
            self::notFound('Property cannot be found.');
        }
        if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Property.invite')) {
            self::forbidden('You have insufficient permissions to invite users to roles.');
        }

        // /*check user plan */
        // $managerModel = new Manager();
        // $manager = self::model('Manager')->getManagerById(Model::sessionGet('managerId'));
        // $managerPlan = json_decode($manager['plan']);

        $users = $this->m->getUsersRelevantForCurrentUser();

        // /* Email notification – This can be at specific thresholds.*/
        // $this->m->sendEmailNotificationForUserLimit($users, $manager);

        // if (count($users)) {
        //     if (isset($managerPlan->user) && $users['users'] >= $managerPlan->user) {
        //         exit(json_encode(['success' => false, 'message' => 'You have insufficient limits to invite user.']));
        //     }
        // }

        $email = $_POST['email'];
        $roleId = $_POST['role_id'];

        $saveInvitedUser = $this->m->saveInvitedUser($email, $propertyId);

        $invitePerson = self::model('ManagerProperty')->getUsersByEmail($email);
        $role = self::model('ManagerRole')->getRoleById($roleId);

        if (!$role) {
            self::notFound("Could not find role with id $roleId");
        }

        if ($role['property_id'] != $property['id']) {
            self::forbidden('Cannot invite to a role that is not in this property.');
        }

        $token = $this->m->generateInviteToken($property['id'], $roleId, $email);
        // Generate secure link for invitation
        $url = URL.'/manager/property/accept-invitation?token='.$token;
        $message = 'You have been invited to the '.$role['role_name'].' role in '.$property['title'].'.';
        self::model('ManagerUser')->saveNotificationByEmail($email, $message, $url);

        try {
            $output = [];
            $mail = Helpers::createEmail($output);
            $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
            $mail->addAddress($email);
            $mail->isHTML(true);

            $modelAdminNotification = new AdminNotification();
            $emailTemplate = $modelAdminNotification->getTemplateByTitle('Property - Role Invitation');

            $subject = str_replace('{PROPERTY_NAME}', $property['title'], $emailTemplate['subject']);
            $body = str_replace('{USER_NAME}', $invitePerson['first_name'].' '.$invitePerson['last_name'], $emailTemplate['body']);
            $body = str_replace('{ROLE_NAME}', $role['role_name'], $body);
            $body = str_replace('{PROPERTY_NAME}', $property['title'], $body);
            $body = str_replace('{INVITE_URL}', $url, $body);

            $mail->Subject = $subject;
            $mail->Body = $body;
            $mail->send();
            exit(json_encode(['success' => true, 'message' => '']));
        } catch (Exception $e) {
            // Message could not be sent. Mailer Error: {$mail->ErrorInfo}
            http_response_code(500);
            echo json_encode([
                'error' => 'Failed to send email.',
            ]);
            exit;
        }
    }
}
