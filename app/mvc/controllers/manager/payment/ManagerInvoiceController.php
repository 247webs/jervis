<?php

class ManagerInvoiceController extends ManagerController
{
    public function index()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }
        if (!self::model('ManagerRole')->currentUserHasAnyPermission(
            $property,
            [
                'Invoice.create',
                'Invoice.confirm',
                'Invoice.pay',
            ]
        )) {
            self::forbidden('You have insufficient permissions to manage Invoice.');
        }

        $managerInvoiceModel = self::model('ManagerInvoice');
        $invoiceData = $managerInvoiceModel->getInvoicesByPropertyId($propertyId);

        if (!empty($invoiceData)) {
            foreach ($invoiceData as $data) {
                $token[] = sha1(DB_SALT.$data['id']);
            }
        } else {
            $token = '';
        }
        self::viewTwig('manager/payment/invoice_index.html', [
            'propertyId' => $propertyId,
            'invoiceData' => $invoiceData,
            'token' => $token,
        ]);
    }

    public function createInvoice()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }
        if (!self::model('ManagerRole')->currentUserHasAnyPermission(
            $property,
            [
                'Invoice.create',
            ]
        )) {
            self::forbidden('You have insufficient permissions to manage Invoice.');
        }

        $userData = self::model('Manager')->getManagerById($property['owner_id']);

        self::viewTwig('manager/payment/add_invoice.html', [
            'propertyId' => $propertyId,
            'userData' => $userData,
        ]);
    }

    public function saveInvoice()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }
        if (!self::model('ManagerRole')->currentUserHasAnyPermission(
            $property,
            [
                'Invoice.create',
            ]
        )) {
            self::forbidden('You have insufficient permissions to manage Invoice.');
        }

        if (!empty($propertyId)) {
            $managerInvoiceModel = self::model('ManagerInvoice');
            $saveInvoiceData = $managerInvoiceModel->saveInvoice($propertyId, $_POST);
        }
        exit(json_encode(''));
    }

    public function viewInvoice()
    {
        $token = $_GET['token'];
        $splitToken = explode('_', $token);
        if (count($splitToken) != 2) {
            return false;
        }
        list($_, $invoiceId) = $splitToken;
        if (sha1(DB_SALT.$invoiceId).'_'.$invoiceId != $token) {
            echo 'not valid';
        } else {
            $managerInvoiceModel = self::model('ManagerInvoice');
            $billedFromDetails = $managerInvoiceModel->getUserDetails();
            $invoiceDetails = $managerInvoiceModel->viewInvoice($invoiceId);

            $property = self::model('ManagerProperty')->getPropertyById($invoiceDetails['property_id']);

            if (!$property) {
                self::notFound('Could not find property.');
            }
            if (!self::model('ManagerRole')->currentUserHasAnyPermission(
                $property,
                [
                    'Invoice.create',
                    'Invoice.confirm',
                    'Invoice.pay',
                ]
            )) {
                self::forbidden('You have insufficient permissions to manage Invoice.');
            }

            $billedToDetails = self::model('Manager')->getManagerById($property['owner_id']);

            $serviceName = json_decode($invoiceDetails['service_name']);
            $servicePrice = json_decode($invoiceDetails['service_price']);
            $deduct_item = json_decode($invoiceDetails['deductu_item_name']);
            $deduct_amount = json_decode($invoiceDetails['deduct_amount']);
            $sign = json_decode($invoiceDetails['sign']);
            self::viewTwig('manager/payment/invoice.html', [
                'serviceName' => $serviceName,
                'servicePrice' => $servicePrice,
                'deduct_item' => $deduct_item,
                'deduct_amount' => $deduct_amount,
                'sign' => $sign,
                'invoiceDetails' => $invoiceDetails,
                'billedFromDetails' => $billedFromDetails,
                'billedToDetails' => $billedToDetails,
            ]);
        }
    }

    public function updateInvoice()
    {
        $token = $_GET['token'];
        $splitToken = explode('_', $token);
        if (count($splitToken) != 2) {
            return false;
        }
        list($_, $invoiceId) = $splitToken;
        if (sha1(DB_SALT.$invoiceId).'_'.$invoiceId != $token) {
            echo 'not a valid request';
        } else {
            $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
            if (!$propertyId) {
                self::badRequest("Missing parameter 'property_id'");
            }
            $property = self::model('ManagerProperty')->getPropertyById($propertyId);
            if (!$property) {
                self::notFound('Could not find property.');
            }
            if (!self::model('ManagerRole')->currentUserHasAnyPermission(
                $property,
                [
                    'Invoice.create',
                    'Invoice.confirm',
                    'Invoice.pay',
                ]
            )) {
                self::forbidden('You have insufficient permissions to manage Invoice.');
            }

            $userData = self::model('Manager')->getManagerById($property['owner_id']);

            $managerInvoiceModel = self::model('ManagerInvoice');
            $billedFromDetails = $managerInvoiceModel->getUserDetails();
            $invoiceDetails = $managerInvoiceModel->viewInvoice($invoiceId);
            // $billedToDetails = $managerInvoiceModel->getSendUserDetails($invoiceDetails['billed_to_id']);
            $serviceName = json_decode($invoiceDetails['service_name']);
            $servicePrice = json_decode($invoiceDetails['service_price']);
            $quantity = json_decode($invoiceDetails['quantity']);
            $rate = json_decode($invoiceDetails['rate']);
            $deduct_item = json_decode($invoiceDetails['deductu_item_name']);
            $deduct_amount = json_decode($invoiceDetails['deduct_amount']);
            $sign = json_decode($invoiceDetails['sign']);
            self::viewTwig('manager/payment/update_invoice.html', [
                'serviceName' => $serviceName,
                'servicePrice' => $servicePrice,
                'invoiceDetails' => $invoiceDetails,
                'billedFromDetails' => $billedFromDetails,
               // 'billedToDetails' => $billedToDetails,
                'userData' => $userData,
                'quantity' => $quantity,
                'rate' => $rate,
                 'deduct_item' => $deduct_item,
                'deduct_amount' => $deduct_amount,
                'sign' => $sign,
                'propertyId' => $propertyId,
            ]);
        }
    }

    public function invoiceUpdateData()
    {
        //  $billedto = $_POST['billedto'];
        $managerInvoiceModel = self::model('ManagerInvoice');
        $userDetails = $managerInvoiceModel->getUserDetails();
        // $sendUserDetails = $managerInvoiceModel->getSendUserDetails($billedto);////
        $updateInvoiceData = $managerInvoiceModel->updateInvoiceData($_POST, $userDetails);
        exit(json_encode(''));
    }
}
