<?php

class ManagerPaymentController extends ManagerController
{
    public function index()
    {
        header('X-Frame-Options: allow-from https://connect.stripe.com/');
        $managerPaymentModel = self::model('ManagerPayment');
        $stripData = $managerPaymentModel->getStripeData();
        $getStripData = $stripData['stripe_data'];
        if (!$getStripData) {
            $connectUrl = 'https://connect.stripe.com/express/oauth/authorize?redirect_uri='.URL.'/payment/authenticate&client_id='.STRIPE_CLIENT_ID.'&scope=read_write';
        }
        $loginUrl = $managerPaymentModel->getLoginUrl();
        $loginUrl = $loginUrl['stripe_login_url'];
        self::viewTwig('manager/payment/index.html', [
            'getStripData' => $getStripData,
            'loginUrl' => $loginUrl,
            'connectUrl' => $connectUrl,
        ]);
    }

    public function authenticate()
    {
        $code = $_GET['code'];
        $result = $this->getRequestToken($code);
        $managerPaymentModel = self::model('ManagerPayment');
        $managerPaymentModel->updatePayment($result);

        $stripData = $managerPaymentModel->getStripeData();
        $getStripData = $stripData['stripe_data'];
        $decodeResult = json_decode($result, true);
        $accountNo = $decodeResult['stripe_user_id'];
        $loginData = $this->getLoginUrl($accountNo);
        $loginUrlData = $loginData['url'];
        $managerPaymentModel->updateLoginUrl($loginUrlData);
        Helpers::redirect('manager/payment/index');
    }

    public function getLoginUrl($accountNo)
    {
        \Stripe\Stripe::setApiKey(SECRET_KEY);

        $loginData = \Stripe\Account::createLoginLink(
            $accountNo
        );

        return $loginData;
    }

    public function getRequestToken($code)
    {
        \Stripe\Stripe::setApiKey(SECRET_KEY);

        $response = \Stripe\OAuth::token([
          'grant_type' => 'authorization_code',
          'code' => $code,
        ]);

        return json_encode($response);
    }
}
