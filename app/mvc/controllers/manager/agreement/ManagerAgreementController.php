<?php

use Dompdf\Dompdf;
use Dompdf\Options;

class ManagerAgreementController extends ManagerController
{
    protected $m;

    public function __construct()
    {
        $this->m = self::model('ManagerAgreement');
    }

    public function index()
    {
        $code = !empty($_GET['code']) ? $_GET['code'] : null;
        if (!$code) {
            self::badRequest("Missing parameter 'code'");
        }

        $blobPdf = $this->m->getDocumentByCode($code);
        if ($blobPdf) {
            header('Content-type:application/pdf');
            header('Content-Disposition: inline;  filename="Agreement_'.$blobPdf['reservation_id'].'.pdf"');
            echo $blobPdf['document'];
            exit;
        } else {
            $managerReservationModel = new ManagerReservation();
            $reservation = $managerReservationModel->getReservationByCode($code);
            if (!$reservation) {
                self::badRequest('Invalid code');
            }
            $propertyId = $reservation['property_id'];

            $propertyModel = self::model('ManagerProperty');
            $property = $propertyModel->getPropertyById($propertyId);
            $logo = $property['title'];
            if (!empty($property['logo'])) {
                $logo = '<img src="'.$property['logo'].'" width="150" height="150"/>';
            }

            $managerNotificationTemplateModel = self::model('ManagerNotificationTemplate');

            $notification = $managerNotificationTemplateModel->getTemplatesByPropertyIdWithTitle($propertyId, 'Rental Agreement');
            $content = $notification['content'];
            $content = str_replace('{BUSINESS_LOGO}', $logo, $content);
            $content = str_replace('{SIGNATURE_BOX}', '<div class="row" id="signatureBox"><div class="col-md-12"><b>Signature</b> &nbsp;&nbsp;&nbsp;<a id="clear" style="cursor:pointer">[Clear]</a></div><div class="col-md-6"><canvas id="signature-pad" class="signature-pad" style="border:1px solid #ccc;"></canvas></div><input type="hidden" name="signature" id="signature" /></div>', $content);

            self::viewTwig('manager/agreement/index.html', [
            'content' => $content,
            'code' => $code,
        ]);
        }
    }

    public function generatePdf()
    {
        $signeture_url = $_POST['signature_url'];
        $code = $_POST['code'];

        $pdfOptions = new Options();
        $pdfOptions->set('isRemoteEnabled', true);
        $pdfOptions->set('defaultFont', 'Arial');

        $dompdf = new Dompdf($pdfOptions);

        $managerReservationModel = new ManagerReservation();
        $reservation = $managerReservationModel->getReservationByCode($code);
        $reservationId = $reservation['id'];

        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($reservation['property_id']);
        $logo = $property['title'];
        if (!empty($property['logo'])) {
            $logoPath = getcwd().$property['logo'];
            $logo = '<img src="'.$logoPath.'" width="150" height="150"/>';
        }

        $managerNotificationTemplateModel = self::model('ManagerNotificationTemplate');
        $notification = $managerNotificationTemplateModel->getTemplatesByPropertyIdWithTitle($reservation['property_id'], 'Rental Agreement');
        $content = $notification['content'];
        $content = str_replace('{BUSINESS_LOGO}', $logo, $content);
        $content = str_replace('{SIGNATURE_BOX}', '<div class="row" id="signatureBox"><div class="col-md-12"><b>Signature</b> &nbsp;&nbsp;&nbsp;<a id="clear" style="cursor:pointer">[Clear]</a></div><div class="col-md-6"><canvas id="signature-pad" class="signature-pad" style="border:1px solid #ccc;"></canvas></div><input type="hidden" name="signature" id="signature" /></div>', $content);

        $content = preg_replace('/<div+[^>]+(id=("|\')signatureBox)+[^>]+>[^*]+(input)+[^>]+>(\s+)?<\/div>/', '<img src="'.$signeture_url.'" class="img-responsive" width="400" style="padding-top: 5px;"/>', $content);
        $content = preg_replace('/<button+[^>]+(id=("|\')save-pdf)+[^>]+>(.*?)<\/button>/', '', $content);

        // Load HTML to Dompdf
        $dompdf->loadHtml($content);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Store PDF Binary Data
        $output = $dompdf->output();

        /* create agreement */
        $this->m->createAgreement($code, $output);

        /* send house rules signature confirmation email */
        NotificationHelpers::sendEmailNotification($reservationId, 'House Rules Signature Confirmation Email');

        /* send welcome confirmation email */
        NotificationHelpers::sendEmailNotification($reservationId, 'Welcome Confirmation Email');
    }
}
