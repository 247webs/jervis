<?php

class ManagerScheduleController extends ManagerController
{
    public function index()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        //$timestore = Helpers::splitTime('00:00', '23:30', '30');
        $managerScheduleModel = self::model('ManagerSchedule');
        $scheduleData = $managerScheduleModel->getScheduleByUserId();

        $dayoffData = $managerScheduleModel->getDayoffByUserId();
        $workinHours = json_decode($scheduleData['working_hours'], true);

        $addBreak = json_decode($scheduleData['add_break'], true);
        $service = json_decode($scheduleData['service'], true);
        foreach ($dayoffData as $key => $data) {
            $dayoff = json_decode($data['dayoff'], true);
            $dayoff['id'] = $data['id'];
            $dayoffOffDetail[] = $dayoff;
        }
        if (empty($dayoffOffDetail)) {
            $dayoffOffDetail = '';
        }

        self::viewTwig('manager/schedule/index.html', [
            'propertyId' => $propertyId,
            //'timestore' => $timestore,
            'workinHours' => $workinHours,
            'addBreak' => $addBreak,
            'service' => $service,
            'dayoffOffDetail' => $dayoffOffDetail,
        ]);
    }

    public function updateWorkingHours()
    {
        $managerScheduleModel = self::model('ManagerSchedule');
        $updateData = $managerScheduleModel->updateWorkingHours($_POST);
        exit(json_encode(''));
    }

    public function updateAddBreak()
    {
        $managerScheduleModel = self::model('ManagerSchedule');
        $updateData = $managerScheduleModel->updateAddbreak($_POST);
        exit(json_encode(''));
    }

    public function addDayOff()
    {
        $managerScheduleModel = self::model('ManagerSchedule');
        $adddayoff = $managerScheduleModel->addDayOff($_POST);
        exit(json_encode(''));
    }

    public function destroyTimeoff()
    {
        $timeoffId = $_POST['timeoffid'];
        $managerScheduleModel = self::model('ManagerSchedule');
        $deleteTimeoff = $managerScheduleModel->deleteTimeOff($timeoffId);
        exit(json_encode(''));
    }

    public function booking()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        $userid = $_GET['user_id'];

        if (!empty($userid)) {
            $managerScheduleModel = self::model('ManagerSchedule');
            $workingHourData = $managerScheduleModel->getWorkingHours($userid);

            $breakData = $managerScheduleModel->getAddBreak($userid);
            $dayoff = $managerScheduleModel->getDayoff($userid);
            $filter_data_all = $managerScheduleModel->getFilterData($workingHourData, $breakData);
            if (isset($workingHourData['wrkoff'])) {
                $wrkdaytoggle_off = $workingHourData['wrkoff'];
            } else {
                $wrkdaytoggle_off = '';
            }
            if (!empty($dayoff)) {
                $datatimeoff = $dayoff['alldayoff'];
            } else {
                $datatimeoff = null;
            }
            $wrkdaytoggle_off = json_encode($wrkdaytoggle_off);
            $wrkdaytoggle_off = str_replace('"', '', $wrkdaytoggle_off);
            $wrkdaytoggle_off = str_replace('[', '', $wrkdaytoggle_off);
            $wrkdaytoggle_off = str_replace(']', '', $wrkdaytoggle_off);

            self::viewTwig('manager/schedule/calender.html', [
                'filter_data_all' => $filter_data_all,
                'wrkdaytoggle_off' => $wrkdaytoggle_off,
                'datatimeoff' => $datatimeoff,
                'userid' => $userid,
                'propertyId' => $propertyId,
            ]);
        }
    }

    public function timeslotData()
    {
        $userid = $_POST['userid'];
        $propertyId = $_POST['propertyId'];
        $managerScheduleModel = self::model('ManagerSchedule');
        $workingHourData = $managerScheduleModel->getWorkingHours($userid);
        $breakData = $managerScheduleModel->getAddBreak($userid);
        $dayoff = $managerScheduleModel->getDayoff($userid);
        $filter_data_all = $managerScheduleModel->getFilterData($workingHourData, $breakData);
        $timezone = Helpers::timezone();
        $timeslotData = $managerScheduleModel->getTimeslotData($_POST, $filter_data_all, $dayoff, $propertyId, $timezone);
        echo json_encode($timeslotData);
    }

    public function appointmentNotification()
    {
        $profileModel = new ManagerProfile();
        $subscriptions = $profileModel->getSubscriptionsByOwnerId(Model::sessionGet('managerId'));
        $managerSubscriptions = ($subscriptions) ? true : false;
        if ($managerSubscriptions) {
            $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
            $managerScheduleModel = self::model('ManagerSchedule');
            $appoinmentData = $managerScheduleModel->getAppoinmentData($propertyId);
            $managerUserModel = self::model('ManagerUser');
            $managerUserModel->deleteNotification($notificationId = '', $type = 'appoinment');
            self::viewTwig('manager/schedule/appoinment_notification.html', [
                'propertyId' => $propertyId,
                'appoinmentData' => $appoinmentData,
            ]);
        } else {
            Helpers::redirect('manager/profile/billing');
        }
    }

    public function addNotification()
    {
        $managerScheduleModel = self::model('ManagerSchedule');
        $propertyModel = self::model('ManagerProperty');
        $email = $_POST['email'];

        $userModel = self::model('ManagerUser');
        $userModel->saveNotificationByEmail($email, $message = '', $link = '', $type = 'appoinment');
        $timeslotData = $managerScheduleModel->addAppoinmentData($_POST);
        exit(json_encode(''));
    }

    public function deleteAppoinmentData()
    {
        $appoinmentId = $_POST['appoinmentId'];
        $managerScheduleModel = self::model('ManagerSchedule');
        $appoinmentId = $managerScheduleModel->deleteAppoinment($appoinmentId);
        exit(json_encode(''));
    }

    public function confirmAppoinment()
    {
        $managerScheduleModel = self::model('ManagerSchedule');
        $propertyModel = self::model('ManagerProperty');
        $email = $_POST['email'];
        $managerUserModel = self::model('ManagerUser');
        $managerUserModel->saveNotificationByEmail($email, $message = '', $link = '', $type = 'appoinment');
        $timeslotData = $managerScheduleModel->confirmAppoinment($_POST);
        exit(json_encode(''));
    }

    public function addServices()
    {
        $managerScheduleModel = self::model('ManagerSchedule');
        $addService = $managerScheduleModel->addService($_POST);
        exit(json_encode(''));
    }

    public function editServices()
    {
        $managerScheduleModel = self::model('ManagerSchedule');
        $addService = $managerScheduleModel->editService($_POST);
        exit(json_encode(''));
    }

    public function destroyService()
    {
        $managerScheduleModel = self::model('ManagerSchedule');
        $addService = $managerScheduleModel->destroyService($_POST);
        exit(json_encode(''));
    }
}
