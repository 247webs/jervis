<?php

class ManagerReservationApiController extends ManagerController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = self::model('ManagerReservation');
    }

    public function saveReservation($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }
        if (empty($params[0])) {
            $propertyId = $_POST['propertyId'];
            $property = self::model('ManagerProperty')->getPropertyById($propertyId, false);
            if (!$property) {
                self::notFound('Missing property.');
            }
            if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Reservation.create')) {
                self::forbidden('You have insufficient permissions to create reservations.');
            }

            /*check user reservations plan */

            $manager = self::model('Manager')->getManagerById(Model::sessionGet('managerId'));
            $managerPlan = json_decode($manager['plan']);

            $reservations = $this->m->getReservationsRelevantForCurrentUser();

            // if (count($reservations)) {
            //     if (isset($managerPlan->reservation) && $reservations['per_year'] >= $managerPlan->reservation->per_year) {
            //         exit(json_encode(['success' => false, 'message' => 'You have insufficient limits to create reservation per year.']));
            //     }
            //     if (isset($managerPlan->reservation) && $reservations['per_month'] >= $managerPlan->reservation->per_month) {
            //         exit(json_encode(['success' => false, 'message' => 'You have insufficient limits to create reservation per month.']));
            //     }
            // }

            // Creating new reservation
            $this->m->createReservation($_POST);
            exit(json_encode(['success' => true]));
        // $reservations = $this->m->getReservationsRelevantForCurrentUser();

            // /* Email notification – This can be at specific thresholds.*/
            // $this->m->sendEmailNotificationForReservationLimit($reservations, $manager);
        } else {
            // Editing existing reservation
            $reservationId = intval($params[0]);
            // TODO: Consider which users can edit this
            $reservation = $this->m->getReservationById($reservationId);
            if (!$reservation) {
                self::notFound('Reservation cannot be found.');
            }
            $propertyId = $reservation['property_id'];
            $property = self::model('ManagerProperty')->getPropertyById($propertyId, false);
            if (!$property) {
                self::notFound('Missing property.');
            }
            if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Reservation.edit')) {
                self::forbidden('You have insufficient permissions to edit reservations.');
            }
            $this->m->deleteReservationNotification($reservationId, 'Cleaning Request Email');
            $this->m->deleteReservationNotification($reservationId, 'Cleaning Request SMS');
            $this->m->deleteReservationNotification($reservationId, 'Cleaning Request Email - Reminder');
            $this->m->deleteReservationNotification($reservationId, 'Cleaning Request SMS - Reminder');
            $this->m->updateReservation($reservationId, $_POST);
        }
        exit(json_encode(['success' => true]));
    }

    public function deleteReservation($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Delete requires the DELETE HTTP method.';
            exit;
        }
        $reservation = $this->m->getReservationById($params[0]);
        if (!$reservation) {
            self::notFound('Reservation cannot be found.');
        }
        $propertyId = $reservation['property_id'];
        $property = self::model('ManagerProperty')->getPropertyById($propertyId, false);
        if (!$property) {
            self::notFound('Missing property.');
        }
        if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Reservation.delete')) {
            self::forbidden('You have insufficient permissions to delete reservations.');
        }
        if (isset($params[0]) and !empty($params[0])) {
            exit(json_encode([
                'success' => $this->m->deleteReservation(intval($params[0])),
            ]));
        } else {
            self::badRequest("Missing 'id' for Reservation");
        }
    }

    public function verifyEmail()
    {
        $email = $_POST['email'];
        $response = $this->m->getGuestByEmail($email);
        exit(json_encode(['success' => $response]));
    }

    public function verifyPhone()
    {
        $phone = $_POST['phone'];
        $response = $this->m->getGuestByPhone($phone);
        exit(json_encode(['success' => $response]));
    }

    public function getReservationNotification($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }

        if (isset($params[0]) and !empty($params[0])) {
            $managerNotificationModel = new ManagerNotification();
            exit(json_encode([
                'success' => $managerNotificationModel->getNotificationByReservationId(intval($params[0])),
            ]));
        } else {
            self::badRequest("Missing 'id' for Reservation");
        }
    }
}
