<?php

class ManagerReservationChatController extends ManagerController
{
    protected $m;

    public function __construct()
    {
        $this->m = self::model('ManagerReservation');
    }

    public function index()
    {
        $code = !empty($_GET['code']) ? $_GET['code'] : null;
        if (!$code) {
            self::badRequest("Missing parameter 'code'");
        }

        $reservation = $this->m->getReservationByCode($code);
        if (!$reservation) {
            self::badRequest('Invalid code');
        }
        $reservationId = $reservation['id'];
        $propertyId = $reservation['property_id'];
        $guestId = $reservation['guest_id'];

        $guest = $this->m->getGuestById($guestId);
        $guestEmail = $guest['email'];

        $manager = self::model('Manager')->getManagerByEmail($guestEmail);
        if (!$manager) {
            $password = $this->generateRandomString();
            $userId = self::model('Manager')->createManagerByGuest($guest, $password);
            $roleId = $this->getRoleId($propertyId);
            if ($roleId) {
                $access = (object) [$roleId => true];
                self::model('ManagerProperty')->setPropertyAccess($propertyId, $userId, $access);
            }

            $to = $guest['email'];
            $subject = 'Login to jervis system';
            $message = 'Please Login to Jervis <br>E-Mail : '.$guest['email']." <br>Password : $password<br><a href=\"".URL.'/login">Click here for login</a>';
            Helpers::sendEmailNotification($to, $subject, $message);

            echo 'We have sent Login credentail Your Email <br> Please Login';
            exit();
        } else {
            Helpers::redirect('login');
        }
    }

    public function getRoleId($propertyId)
    {
        $roleModel = self::model('ManagerRole');
        $roles = $roleModel->getRolesByPropertyId($propertyId);
        foreach ($roles as $role) {
            if ($role['role_name'] == 'Property guest') {
                return $role['role_id'];
            }
        }

        return 0;
    }

    public function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
