<?php

class ManagerReservationController extends ManagerController
{
    public function index()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }

        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }

        if (!self::model('ManagerRole')->currentUserHasAnyPermission(
            $property,
            [
                'Reservation.view',
                'Reservation.edit',
                'Reservation.delete',
                'Reservation.create',
            ]
        )) {
            self::forbidden('You have insufficient permissions to manage reservations.');
        }

        $reservationModel = self::model('ManagerReservation');
        $reservations = $reservationModel->getReservationsByPropertyId($propertyId);
        $managerUserModel = self::model('ManagerUser');
        $managerUserModel->deleteNotification($notificationId = '', $type = 'reservation');
        self::viewTwig('manager/reservation/index.html', [
            'reservations' => $reservations,
            'property_id' => $propertyId,
            'property' => $property,
        ]);
    }

    public function addReservation()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }

        if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Reservation.create')) {
            self::forbidden('You have insufficient permissions to create reservations.');
        }

        $device = [];
        self::viewTwig('manager/reservation/reservation_edit.html', [
            'reservation' => null,
            'access' => [],
            'propertyId' => $propertyId,
        ]);
    }

    public function editReservation()
    {
        $reservationId = !empty($_GET['id']) ? intval($_GET['id']) : null;
        if (!$reservationId) {
            self::notFound("Missing parameter 'id'");
        }
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::notFound("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }

        if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Reservation.edit')) {
            self::forbidden('You have insufficient permissions to edit reservations.');
        }
        $reservationModel = self::model('ManagerReservation');
        $reservation = $reservationModel->getReservationById($reservationId);

        if (!$reservation) {
            self::notFound("Could not find reservation with id '$reservationId");
        }

        self::viewTwig('manager/reservation/reservation_edit.html', [
            'reservation' => $reservation,
            'access' => [],
            'propertyId' => $propertyId,
        ]);
    }

    public function calendarData($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'GET') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the GET HTTP method.';
            exit;
        }

        $propertyId = intval($_GET['property_id']);
        $property = self::model('ManagerProperty')->getPropertyById($propertyId, false);
        if (!$property) {
            self::notFound('Missing property.');
        }

        $data[] = array();

        $calendarData = self::model('ManagerReservation')->getCalendarDataByPropertyId($propertyId);

        foreach ($calendarData as $key => $calendar) {
            $data[] = [
                'title' => $calendar['summary'],
                'description' => '',
                'start' => date('Y-m-d', strtotime($calendar['start_date'])),
                'end' => date('Y-m-d', strtotime($calendar['end_date'])),
                'backgroundColor' => $calendar['color'],
            ];
        }

        $reservationModel = self::model('ManagerReservation');
        $reservations = $reservationModel->getReservationsByPropertyId($propertyId);

        foreach ($reservations as $key => $reservation) {
            $guest = $this->m->fetch('SELECT * FROM `guest` WHERE `id` = :guest_id ', [
                ':guest_id' => $reservation['guest_id'],
            ]);

            $editReservation = '';
            $deleteReservation = '';
            if (self::model('ManagerRole')->currentUserHasAnyPermission($property, ['Reservation.edit'])) {
                $editReservation = "<a href='/manager/reservation/edit-reservation?id={$reservation['reservation_id']}&property_id={$reservation['property_id']}'>Edit</a><br>";
            }
            if (self::model('ManagerRole')->currentUserHasAnyPermission($property, ['Reservation.delete'])) {
                $deleteReservation = "<a href='#' class='deleteBtn' id='delete-{$reservation['reservation_id']}'>Delete</a><br>";
            }

            $data[] = [
                'title' => "{$guest['first_name']} {$guest['last_name']} ({$reservation['reservation_id']})",
                'description' => $editReservation.$deleteReservation."<a href='#'  class='notificationBtn' id='notification-{$reservation['reservation_id']}'>Notification</a>",
                'start' => date('Y-m-d', strtotime($reservation['check_in_date'])),
                'end' => date('Y-m-d', strtotime($reservation['check_out_date'])),
                'backgroundColor' => '#03bcb6',
            ];
        }

        echo json_encode($data);
        exit;
    }
}
