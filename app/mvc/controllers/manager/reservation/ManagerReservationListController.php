<?php

class ManagerReservationListController extends ManagerController
{
    protected $m;

    public function __construct()
    {
        $this->m = self::model('ManagerReservation');
    }

    public function index()
    {
        $code = !empty($_GET['code']) ? $_GET['code'] : null;
        if (!$code) {
            self::badRequest("Missing parameter 'code'");
        }

        $reservation = $this->m->getReservationByCode($code);
        if (!$reservation) {
            self::badRequest('Invalid code');
        }
        $reservationId = $reservation['id'];
        $guestId = $reservation['guest_id'];

        $guest = $this->m->getGuestById($guestId);

        $reservations = $this->m->getReservationsByguestId($guestId);

        if (!$reservations) {
            self::notFound("Could not find reservation with id '$reservationId");
        }
        $upcomingReservations = [];
        $completedReservations = [];
        foreach ($reservations as $key => $reservation) {
            if (time() < strtotime($reservation['check_in_date'])) {
                $upcomingReservations[] = $reservation;
            }
            if (time() >= strtotime($reservation['check_out_date'])) {
                $completedReservations[] = $reservation;
            }
        }
        self::viewTwig('manager/reservation/reservation_list.html', [
            'guest' => $guest,
            'upcomingReservations' => $upcomingReservations,
            'completedReservations' => $completedReservations,
        ]);
    }
}
