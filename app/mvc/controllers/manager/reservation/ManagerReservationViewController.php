<?php

class ManagerReservationViewController extends ManagerController
{
    protected $m;

    public function __construct()
    {
        $this->m = self::model('ManagerReservation');
    }

    public function index()
    {
        $code = !empty($_GET['code']) ? $_GET['code'] : null;
        if (!$code) {
            self::badRequest("Missing parameter 'code'");
        }

        $reservation = $this->m->getReservationByCode($code);
        if (!$reservation) {
            self::badRequest('Invalid code');
        }
        $reservationId = $reservation['id'];
        $propertyId = $reservation['property_id'];

        $reservation = $this->m->getReservationById($reservationId);
        $reservation['night_stay'] = round((strtotime($reservation['check_out_date']) - strtotime($reservation['check_in_date'])) / 86400);

        $property = $this->m->getPropertyById($propertyId);

        if (!$reservation) {
            self::notFound("Could not find reservation with id '$reservationId");
        }

        $reservationDetailLink = URL.'/manager/reservation/view/index?code='.$reservation['code'];
        $reservation['google_link'] = 'https://calendar.google.com/calendar/r/eventedit?text='.urlencode($property['title']).'&location='.urlencode($property['address']).'&details='.urlencode('<a href="'.$reservationDetailLink.'">Your Reservation Link</a>').'&dates='.date('Ymd', strtotime($reservation['check_in_date'])).'T'.date('His', strtotime($reservation['check_in_date'])).'/'.date('Ymd', strtotime($reservation['check_out_date'])).'T'.date('His', strtotime($reservation['check_out_date'])).'&pli=1';

        //$managerDeviceModel = new ManagerDevice();
        //$devices = $managerDeviceModel->getDevicesByReservationId($reservationId);

        self::viewTwig('manager/reservation/reservation_view.html', [
            'reservation' => $reservation,
            'property' => $property,
            'devices' => null,
        ]);
    }

    public function checkout()
    {
        $code = !empty($_GET['code']) ? $_GET['code'] : null;
        if (!$code) {
            self::badRequest("Missing parameter 'code'");
        }

        $reservation = $this->m->getReservationByCode($code);
        if (!$reservation) {
            self::badRequest('Invalid code');
        }
        $reservationId = $reservation['id'];
        $propertyId = $reservation['property_id'];

        $reservation = $this->m->getReservationById($reservationId);

        $property = $this->m->getPropertyById($propertyId);
        $ownerId = $property['owner_id'];

        $managerModel = new Manager();
        $manager = $managerModel->getManagerById($ownerId);

        $managerNotificationTemplateModel = self::model('ManagerNotificationTemplate');
        $notification = $managerNotificationTemplateModel->getTemplatesByPropertyIdWithTitle($propertyId, 'Check-out Reminder Email');

        $content = NotificationHelpers::replaceEmailToken($notification['content'], $reservation, $manager, [], $property);
        $content = str_replace('TODAY ', '', $content);

        self::viewTwig('manager/reservation/reservation_checkout.html', [
            'content' => $content,
        ]);
    }
}
