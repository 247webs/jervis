<?php

class ManagerProfileController extends ManagerController
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = Model::sessionGet('managerId');
    }

    public function index()
    {
        $managerModel = self::model('Manager');
        $manager = $managerModel->getManagerById(self::$mId);
        $timezone = $this->timezone($manager);
        self::viewTwig('manager/profile/index.html', [
            'manager' => $manager,
            'access' => [],
            'timezone' => $timezone,
        ]);
    }

    public function timezone($manager)
    {
        $zones_array = array();
        $timestamp = time();
        $timezone = '';
        $ip = $_SERVER['REMOTE_ADDR'];
        $timezone_name = $manager['user_time_zone'];
        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            if ($zone == $timezone_name && !empty($timezone_name)) {
                $timezone .= '<option  value='.$zone.' selected>UTC/GMT '.date('P', $timestamp).'-'.$zone.'</option>';
            } else {
                $timezone .= '<option  value='.$zone.'>UTC/GMT '.date('P', $timestamp).'-'.$zone.'</option>';
            }
        }

        return $timezone;
    }

    public function changePassword()
    {
        self::viewTwig('manager/profile/change_password.html', [
            'manager' => null,
            'access' => [],
        ]);
    }

    public function billing()
    {
        $profileModel = self::model('ManagerProfile');
        $subscriptions = $profileModel->getSubscriptionsByOwnerId(self::$mId);

        $card = $profileModel->retrieveStripeCustomer($subscriptions);

        $billingPlans = $profileModel->getBillingPlans();

        self::viewTwig('manager/profile/billing.html', [
            'card' => $card,
            'PUBLISHABLE_KEY' => PUBLISHABLE_KEY,
            'billingPlans' => $billingPlans,
        ]);
    }

    public function company()
    {
        $company = ['name' => '', 'address' => '', 'email' => '', 'phone' => ''];
        $managerModel = self::model('Manager');
        $manager = $managerModel->getManagerById(self::$mId);
        $companyInfo = json_decode($manager['profile'], true);
        if (isset($companyInfo['company'])) {
            $company = $companyInfo['company'];
        }

        self::viewTwig('manager/profile/company.html', [
            'company' => $company,
            'access' => [],
        ]);
    }

    public function notifications()
    {
        $notifications = ['email' => 0, 'SMS' => 0, 'desktop' => 0];
        $managerModel = self::model('Manager');
        $manager = $managerModel->getManagerById(self::$mId);

        $profile = json_decode($manager['profile'], true);
        if (isset($profile['notifications'])) {
            $notifications = $profile['notifications'];
        }

        self::viewTwig('manager/profile/notifications.html', [
            'notifications' => $notifications,
            'access' => [],
        ]);
    }

    public function plan()
    {
        $profileModel = self::model('ManagerProfile');
        $subscriptions = $profileModel->getSubscriptionsByOwnerId(self::$mId);
        $plan = $profileModel->getPlanBySubscriptions($subscriptions);

        $propertyModel = self::model('ManagerProperty');
        $properties = $propertyModel->getPropertiesRelevantForCurrentUser();

        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;

        $managerModel = self::model('Manager');
        $manager = $managerModel->getManagerById(self::$mId);
        $end_trial = false;
        $end_date = date('F, d, Y', strtotime($manager['created'].' + 10 days'));

        $diff = strtotime(date('Y-m-d', strtotime($manager['created'].' + 10 days'))) - strtotime(date('Y-m-d'));
        $days_remaining = abs(round($diff / 86400));

        if (strtotime(date('Y-m-d', strtotime($manager['created'].' + 10 days'))) <= strtotime(date('Y-m-d'))) {
            $end_trial = true;
        }

        self::viewTwig('manager/profile/plan.html', [
            'subscriptions' => $subscriptions,
            'properties' => $properties,
            'end_trial' => $end_trial,
            'end_date' => $end_date,
            'days_remaining' => $days_remaining,
            'plan' => $plan,
            'propertyId' => $propertyId,
        ]);
    }
}
