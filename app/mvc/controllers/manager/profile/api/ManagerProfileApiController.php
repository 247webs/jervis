<?php

class ManagerProfileApiController extends ManagerController
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = Model::sessionGet('managerId');
        $this->m = self::model('ManagerProfile');
    }

    public function saveProfile()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }

        $this->m->updateProfile($_POST);
        exit(json_encode(['success' => true]));
    }

    public function savePassword()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }
        $response = $this->m->updatePassword($_POST);

        exit(json_encode($response));
    }

    public function saveCard()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }

        $response = $this->m->updateCard($_POST);

        exit(json_encode($response));
    }

    public function addCard()
    {
        $response = array('success' => '', 'error' => '');
        try {
            if ($_SERVER['REQUEST_METHOD'] != 'POST') {
                http_response_code(405);
                echo '<h1>Method not allowed</h1>';
                echo 'Set requires the POST HTTP method.';
                exit;
            }

            $stripeToken = $_POST['stripeToken'];
            $planId = $_POST['planId'];

            if (empty($stripeToken) || empty($planId)) {
                self::notFound('Token OR planId missing.');
            }

            $profileModel = self::model('ManagerProfile');
            $plan = $profileModel->getPlansById($planId);
            if (!$plan) {
                self::notFound('Plan cannot be found.');
            }

            $managerModel = self::model('Manager');
            $manager = $managerModel->getManagerById(self::$mId);
            if (!$manager) {
                self::notFound('Manager cannot be found.');
            }

            $response = $profileModel->createStripeSubscription($plan, $manager, $stripeToken);
        } catch (Exception $ex) {
            $response['error'] = $ex->getMessage().' Please use another credit card or contact your bank.';
        }
        exit(json_encode($response));
    }

    public function saveCompany()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }

        $this->m->updateCompany($_POST);

        exit(json_encode(['success' => true]));
    }

    public function saveNotifications()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }

        $this->m->updateNotifications($_POST);

        exit(json_encode(['success' => true]));
    }
}
