<?php

class ManagerChecklistTemplateApiController extends ApiController
{
    protected $m;

    public function __construct()
    {
        // parent::__construct();
        $this->m = self::model('ManagerChecklistTemplate');
    }

    // public function show()
    // {
    //     $apiResponse = $this->m->getAll();
    //     View::apiRender($apiResponse);
    // }

    // public function get($params)
    // {
    //     if (isset($params[0]) and !is_null($params[0])) {
    //         $apiResponse = $this->m->get(intval($params[0]));
    //     }
    //     View::apiRender($apiResponse);
    // }

    public function saveTemplate()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            self::methodNotAllowed('Save requires POST HTTP method.');
            exit;
        }

        $templateModel = $this->m;
        $propertyModel = self::model('ManagerProperty');

        $templateId = !empty($_GET['id']) ? intval($_GET['id']) : null;

        if (empty($templateId)) {
            // Create new
            $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;

            $property = $propertyModel->getPropertyById($propertyId);
        } else {
            // Update
            $template = $templateModel->getTemplateById($templateId);

            if (!$template) {
                self::notFound('Template could not be found.');
            }

            $property = $propertyModel->getPropertyById($template['property_id']);
        }
        if (!$property) {
            self::notFound('Property could not be found.');
        }
        if (!$propertyModel->propertyIsRelevant($property, Model::sessionGet('managerId'))) {
            self::forbidden('You have insufficient permissions to save templates.');
        }

        if (empty($_POST['template'])) {
            self::badRequest("Request is missing 'template' value.");
        }

        $values = json_decode($_POST['template'], JSON_OBJECT_AS_ARRAY);

        if (empty($values)) {
            self::badRequest('Invalid JSON.');
        }

        $errors = [];
        $template = $templateModel->saveTemplate($property['id'], $templateId, $values, $errors);
        if ($template) {
            exit(json_encode(['template' => $template]));
        } else {
            self::badRequest($errors);
        }
    }

    public function deleteTemplate($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
            self::methodNotAllowed('Delete requires the DELETE HTTP method.');
        }
        if (empty($params[0])) {
            self::badRequest('Missing id');
        }
        $templateModel = $this->m;
        $propertyModel = self::model('ManagerProperty');

        $template = $templateModel->getTemplateById($params[0]);
        if (!$template) {
            self::notFound('Template could not be found.');
        }
        $property = $propertyModel->getPropertyById($template['property_id']);
        if (!$property) {
            self::notFound('Property could not be found.');
        }
        if (!$propertyModel->propertyIsRelevant($property, Model::sessionGet('managerId'))) {
            self::forbidden('You have insufficient permissions to delete templates');
        }

        $succ = $this->m->deleteTemplate(intval($params[0]));
        exit(json_encode(['result' => $succ]));
    }
}
