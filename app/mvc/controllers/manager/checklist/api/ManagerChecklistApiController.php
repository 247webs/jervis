<?php

class ManagerChecklistApiController extends ApiController
{
    protected $m;

    public function __construct()
    {
        $this->m = self::model('ManagerChecklistSubmission');
    }

    public function deleteSubmission($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
            self::methodNotAllowed('Delete requires the DELETE HTTP method.');
        }
        if (empty($params[0])) {
            self::badRequest('Missing id');
        }
        $submissionModel = $this->m;
        $propertyModel = self::model('ManagerProperty');

        $submission = $submissionModel->getSubmissionById($params[0]);
        if (!$submission) {
            self::notFound('Checklist submission could not be found.');
        }
        $property = $propertyModel->getPropertyById($submission['property_id']);
        if (!$property) {
            self::notFound('Property could not be found.');
        }
        if (!$propertyModel->propertyIsRelevant($property, Model::sessionGet('managerId'))) {
            self::forbidden('You have insufficient permissions to delete checklist submissions.');
        }

        $success = $submissionModel->deleteSubmission($params[0]);
        exit(json_encode(['result' => $success]));
    }

    public function fill($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            self::methodNotAllowed('Fill requires the POST HTTP method.');
        }
        if (empty($params[0])) {
            self::badRequest('Missing id');
        }
        $submissionId = !empty($params[1]) ? intval($params[1]) : 0;

        $requestId = !empty($_GET['request_id']) ? intval($_GET['request_id']) : null;

        $submissionModel = $this->m;
        $propertyModel = self::model('ManagerProperty');
        $templateModel = self::model('ManagerChecklistTemplate');

        $fillRequestModel = self::model('ManagerChecklistFillRequest');
        $request = $fillRequestModel->getRequestById($requestId);

        $templateId = intval($params[0]);

        $template = $templateModel->getTemplateById($templateId);
        if (!$template) {
            self::notFound('Checklist template could not be found.');
        }
        $property = $propertyModel->getPropertyById($template['property_id']);
        if (!$property) {
            self::notFound('Property could not be found.');
        }
        $currentUserId = Model::sessionGet('managerId');
        if ($request) {
            if ($request['user_id'] != $currentUserId) {
                self::forbidden('You have insufficient permissions to fill this checklist request.');
            }
        } else {
            if (!$propertyModel->propertyIsRelevant($property, $currentUserId)) {
                self::forbidden('You have insufficient permissions to fill checklists.');
            }
        }
        if ($request && $request['filled_id']) {
            self::forbidden('This request is already filled out.');
        }

        if ($submissionId) {
            $submission = $submissionModel->getSubmissionById($submissionId);
            if (!$submission) {
                self::notFound('Checklist submission could not be found.');
            }
        } else {
            $submission = null;
        }

        // Actually validate the thing
        //count notes
        foreach ($_POST['note'] as $note) {
            $filled_array = array_filter($note); // will return only filled values
            $count[] = count($filled_array);
        }
        $notesCount = array_sum($count);

        $success = $submissionModel->fillSubmission($template, $_POST, $submission, $request, $notesCount);
        $date = date('Y/m/d h:i:s A');
        if ($success && $property['owner_id'] != $currentUserId) {
            $managerModel = self::model('Manager');
            $user = $managerModel->getManagerById($property['owner_id']);
            $managerUserModel = self::model('ManagerUser');
            $managerUserModel->saveNotificationByUserId($property['owner_id'], $message = '', $link = '', $type = 'checklist');

            $subject = 'Checklist submitted';
            $message = 'Hello '.$user['first_name'].', <br/><br/> '.Model::sessionGet('managerFirstName').' has submitted the '.$template['title'].' on '.$date.' in '.$property['title'].'.<br/>You can review the submitted checklist by <a href = "'.URL.'/manager/checklist/edit-submission/?submission_id='.$requestId.'">clicking here.</a>';

            $url = URL.'/manager/checklist/edit-submission/?submission_id='.$requestId;

            $modelAdminNotification = new AdminNotification();
            $emailTemplate = $modelAdminNotification->getTemplateByTitle('Checklist - submitted notification');

            $body = str_replace('{BUSINESS_OWNER_NAME}', $user['first_name'], $emailTemplate['body']);
            $body = str_replace('{SERVICE_PROVIDER_USER_NAME}', Model::sessionGet('managerFirstName'), $body);
            $body = str_replace('{CHECKLIST_TITLE}', $template['title'], $body);
            $body = str_replace('{CHECKLIST_SUBMIT_TIME}', $date, $body);
            $body = str_replace('{PROPERTY_NAME}', $property['title'], $body);
            $body = str_replace('{CHECKLIST_URL}', $url, $body);

            Helpers::sendEmailNotification($user['email'], $emailTemplate['subject'], $body);

            $emailTemplate = $modelAdminNotification->getTemplateByTitle('Checklist - confirmation');

            $loggedInUserEmail = Model::sessionGet('managerEmail');

            $body = str_replace('{CHECKLIST_TITLE}', $template['title'], $emailTemplate['body']);
            $body = str_replace('{CHECKLIST_SUBMIT_TIME}', $date, $body);
            Helpers::sendEmailNotification($loggedInUserEmail, $emailTemplate['subject'], $body);
        }

        exit(json_encode(['result' => $success]));
    }

    public function fillRequest($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            self::methodNotAllowed('Fill requires the POST HTTP method.');
        }

        if (empty($_POST['template_id']) || empty($_POST['recipient_id']) || empty($_POST['due_date'])) {
            self::badRequest('Missing required field.');
        }

        $templateModel = self::model('ManagerChecklistTemplate');
        $propertyModel = self::model('ManagerProperty');
        $submissionModel = self::model('ManagerChecklistSubmission');
        $managerModel = self::model('Manager');

        $template = $templateModel->getTemplateById($_POST['template_id']);

        if (!$template) {
            self::notFound('The selected template is missing.');
        }

        $propertyId = $template['property_id'];
        $property = $propertyModel->getPropertyById($propertyId);

        if (!$property) {
            self::notFound('The property is missing.');
        }

        if (!$propertyModel->propertyIsRelevant($property, Model::sessionGet('managerId'))) {
            self::forbidden('You have insufficient permissions to submit fill requests in this property.');
        }

        $user = $managerModel->fetch(
            'SELECT * FROM manager WHERE id = :id',
            [':id' => $_POST['recipient_id']]
        );
        $managerUserModel = self::model('ManagerUser');
        $managerUserModel->saveNotificationByUserId($user['id'], $message = '', $link = '', $type = 'checklist');

        if (!$user) {
            self::notFound('Recipient could not be found.');
        }

        $success = $submissionModel->createSubmissionFillRequest($template, $user);

        $requestId = $submissionModel->lastInsertId();

        $url = URL.'/manager/checklist/fill/?template_id='.$template['id'].'&request_id='.$requestId;

        // Send email
        if (!empty($user['email'])) {
            $mail = Helpers::createEmail();
            $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
            $mail->addAddress($user['email']);
            $mail->isHTML(true);

            $modelAdminNotification = new AdminNotification();
            $emailTemplate = $modelAdminNotification->getTemplateByTitle('Checklist - Fill Request');

            $mail->Subject = str_replace('{CHECKLIST_TITLE}', $template['title'], $emailTemplate['subject']);
            $mail->Body = str_replace('{CHECKLIST_URL}', $url, $emailTemplate['body']);

            $mail->send();
        }

        // Send SMS

        $url = Helpers::makeShortUrl($url);
        $smsBody = "You are requested to fill the checklist {$url}";
        if (!empty($user['mobile'])) {
            Helpers::sendSMSNotification($smsBody, '+1'.$user['mobile']);
        }

        exit(json_encode(['result' => $success]));
    }

    public function uploadFiles($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            self::methodNotAllowed('Fill requires the POST HTTP method.');
        }
        $requestId = $_GET['request_id'];

        // Create attachment directory if not exist
        $requestAttachmentDir = getcwd().CHECKLIST_ATTACHMENTS_DIR.$requestId;
        if (!file_exists($requestAttachmentDir)) {
            mkdir($requestAttachmentDir, 0755, true);
        }

        // Upload multiple files on backend
        $filesUploaded = [];
        foreach ($_FILES as $file) {
            $fileName = time().'_'.Helpers::randomString(10).'.'.Helpers::getFileExtension($file['name']);
            $targetPath = $requestAttachmentDir.'/'.$fileName;
            move_uploaded_file($file['tmp_name'], $targetPath);
            $filesUploaded[] = CHECKLIST_ATTACHMENTS_DIR.$requestId.'/'.$fileName;
        }

        exit(json_encode(['result' => $filesUploaded]));
    }

    public function removeFile()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'GET') {
            self::methodNotAllowed('Fill requires the POST HTTP method.');
        }

        $indexId = ($_GET['index_id'] - 1); // File index (to be removed)
        $submissionId = $_GET['submission_id'];
        $submissionModel = self::model('ManagerChecklistSubmission');
        $checkList = $submissionModel->getSubmissionById($submissionId);

        // Removing the task file by index
        if (isset($_GET['task_id']) && $_GET['task_id'] !== '' && isset($_GET['group_id']) && $_GET['group_id'] !== '') {
            $taskId = $_GET['task_id'];
            $groupId = $_GET['group_id'];
            $taskFiles = $checkList['data'][$groupId]['tasks'][$taskId]['files'];

            // Remove file from server directory
            unlink(substr($taskFiles[$indexId]['file'], 1));
            unset($taskFiles[$indexId]);

            // Save the updated file object in database
            $checkList['data'][$groupId]['tasks'][$taskId]['files'] = array_values($taskFiles);
            $data = ['data' => json_encode($checkList['data'])];
            $success = $submissionModel->updateChecklist($data, $submissionId);
        }
        // Removing the common file by index
        else {
            $files = json_decode($checkList['files']);

            // Remove file from server directory
            unlink(substr($files[$indexId]->file, 1));
            unset($files[$indexId]);

            // Save the updated file object in database
            $files = array_values($files);
            $data = ['files' => !empty($files) ? json_encode($files) : ''];
            $success = $submissionModel->updateChecklist($data, $submissionId);
        }
        exit(json_encode(['result' => 'success']));
    }
}
