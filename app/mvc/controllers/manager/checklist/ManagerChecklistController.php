<?php

use Dompdf\Dompdf;
use Dompdf\Options;

class ManagerChecklistController extends ManagerController
{
    public function index()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Property could not be found.');
        }
        if (!$propertyModel->propertyIsRelevant($property, Model::sessionGet('managerId'))) {
            self::forbidden('You have insufficient permissions to see checklists for this property.');
        }

        $templateModel = self::model('ManagerChecklistTemplate');
        $templates = $templateModel->getTemplatesByPropertyId($propertyId);

        $submissionModel = self::model('ManagerChecklistSubmission');
        $submissions = $submissionModel->getSubmissionsByPropertyId($propertyId);

        $propertyModel = self::model('ManagerProperty');
        $users = $propertyModel->getAccessByPropertyId($propertyId);
        $ownerId = $submissionModel->sessionGet('managerId');

        $fillRequestModel = self::model('ManagerChecklistFillRequest');
        $requests = $fillRequestModel->getRequestsByPropertyId($property['id']);

        $managerUserModel = self::model('ManagerUser');
        $managerUserModel->deleteNotification($notificationId = '', $type = 'checklist');
        if (!empty($submissions)) {
            $token = sha1(DB_SALT . $submissions[0]['submitter_id']);
        } else {
            $token = '';
        }

        self::viewTwig('manager/checklist/index.html', [
            'users' => $users,
            'property' => $property,
            'templates' => $templates,
            'submissions' => $submissions,
            'requests' => $requests,
            'token' => $token,
            'ownerId' => $ownerId,
        ]);
    }

    public function template()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($propertyId);
        if (!$property) {
            self::forbidden('You have insufficient permissions to access this property.');
        }
        if (!$propertyModel->propertyIsRelevant($property, Model::sessionGet('managerId'))) {
            self::forbidden('You have insufficient permissions to view templates for this property.');
        }

        $templateModel = self::model('ManagerChecklistTemplate');
        $templates = $templateModel->getTemplatesByPropertyId($propertyId);

        self::viewTwig('manager/checklist/template.html', [
            'templates' => $templates,
            'property' => $property,
        ]);
    }

    public function addTemplate()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;

        $roleModel = self::model('ManagerRole');
        $roles = $roleModel->getRolesByPropertyId($propertyId);

        self::viewTwig('manager/checklist/template_edit.html', [
            'template' => null,
            'propertyId' => $propertyId,
            'roles' => $roles,
        ]);
    }

    public function editTemplate()
    {
        $templateId = !empty($_GET['id']) ? intval($_GET['id']) : null;
        if (!$templateId) {
            self::notFound("Missing parameter 'id'");
        }
        $templateModel = self::model('ManagerChecklistTemplate');
        $template = $templateModel->getTemplateById($templateId);
        if (!$template) {
            self::notFound("Could not find template with id '$templateId");
        }

        $roleModel = self::model('ManagerRole');
        $roles = $roleModel->getRolesByPropertyId($template['property_id']);

        self::viewTwig('manager/checklist/template_edit.html', [
            'template' => $template,
            'propertyId' => $template['property_id'],
            'roles' => $roles,
        ]);
    }

    public function fill()
    {
        $currentUserId = Model::sessionGet('managerId');
        $templateId = !empty($_GET['template_id']) ? intval($_GET['template_id']) : null;
        $requestId = !empty($_GET['request_id']) ? intval($_GET['request_id']) : null;
        if (!$templateId) {
            self::badRequest("Missing parameter 'template_id'");
        }

        $templateModel = self::model('ManagerChecklistTemplate');
        $template = $templateModel->getTemplateById($templateId);
        if (!$template) {
            self::notFound("Could not find template with id '$templateId");
        }

        $fillRequestModel = self::model('ManagerChecklistFillRequest');
        $request = $fillRequestModel->getRequestById($requestId);

        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($template['property_id']);

        if ($request) {
            if ($request['user_id'] != $currentUserId) {
                self::forbidden('You do not have permission to fill out this checklist.');
            }
        } else {
            if (!$propertyModel->propertyIsRelevant($property, $currentUserId)) {
                self::forbidden('You do not have permission to fill out this checklist.');
            }
        }

        self::viewTwig('manager/checklist/fill.html', [
            'submission' => null, // For editing later
            'template' => $template,
            'property' => $property,
            'request' => $request,
            'session' => Model::sessionGetAll(),
        ]);
    }

    public function pdfView()
    {
        $submissionId = !empty($_GET['submission_id']) ? intval($_GET['submission_id']) : null;
        if (!$submissionId) {
            self::badRequest("Missing parameter 'submission_id'");
        }
        $submissionModel = self::model('ManagerChecklistSubmission');
        $submission = $submissionModel->getSubmissionById($submissionId);

        $templateModel = self::model('ManagerChecklistTemplate');
        $template = $templateModel->getTemplateById($submission['template_id']);
        if (!$template) {
            self::notFound('Could not find template for submission.');
        }

        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($submission['property_id']);
        if (!$propertyModel->propertyIsRelevant($property, Model::sessionGet('managerId'))) {
            self::forbidden('You do not have permission to fill out this checklist.');
        }
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('isRemoteEnabled', true);
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);
        if ($submission) {
            $filledby = $submission['first_name'] . ' ' . $submission['last_name'];
        } else {
            $filledby = session('managerFirstName') . ' ' . session('managerLastName');
        }

        if ($submission) {
            $date = $submission['created'];
        } else {
            $date = 'now' | date('F j, Y');
        }

        $modified = date('Y-m-d');
        // Retrieve the HTML generated in our twig file
        $html = '<html>
    <head>

    </head>
  <body>
        <div class="container">
            <h1>
                ' . $template['title'] . '</h1>';
        if ($submission) {
            $html .= '<input type="hidden" name="checklistId" value=""/>';
        }
        $html .= ' <div>
                    Filled by <br> <label>' . $filledby . '</label>
                   </div><br>
                <div>
                    Date <br>  <label>' . $date . '</label>
                </div><br>';
        if ($submission) {
            $html .= '<div>
                        Modified <br> <label>' . $modified . '</label>
                    </div><br>';
        }
        foreach ($template['groups'] as $groupId => $group) {
            $html .= '<h2>' . $group['title'] . '</h2>';
            foreach ($group['tasks'] as $taskId => $task) {
                if (!empty($submission) && $submission['data'][$groupId]['tasks'][$taskId]['checked']) {
                    $check = 'checked';
                } else {
                    $check = '';
                }
                if (!empty($submission) && $submission['data'][$groupId]['tasks'][$taskId]['note']) {
                    $notes = 'show';
                    $display = 'block';
                } else {
                    $notes = '';
                    $display = 'none';
                }
                if (!empty($submission) && $submission['data'][$groupId]['tasks'][$taskId]['note']) {
                    $textarea = $submission['data'][$groupId]['tasks'][$taskId]['note'];
                } else {
                    $textarea = '';
                }
                if (!empty($submission) && $submission['data'][$groupId]['tasks'][$taskId]['files']) {
                    $files = 'show';
                    $displayFiles = 'block';
                } else {
                    $files = '';
                    $displayFiles = 'none';
                }

                $html .= '<div class="form-group">
                            <label>
                                <input type="checkbox" class="toggle" name="' . $group['tasks'][$taskId] . '" ' . $check . ' value="1" data-toggle="toggle" data-on="Yes" data-off="No" data-size="xs"  data-onstyle="success"/>
                                ' . $task . '
                            </label>
                            <div class="collapse ' . $notes . '" style="display:' . $display . '" id="multi-collapse' . $groupId . $taskId . '">
                                 <label style="display:' . $display . '">Note : ' . $textarea . '</label>    <label></label>
                            </div>
                            <div class="collapse '.$files.'" style="display:'.$displayFiles.'" id="multi-collapse'.$groupId.$taskId.'">
                                 <label style="display:'.$displayFiles.'">Attachments: Available on Jervis System</label>    <label></label>

                            </div><br>
                        </div>';
            }
        }
        if ($submission) {
            $noteData = $submission['notes'];
        } else {
            $noteData = '';
        }
        if ($submission['files']) {
            $fileData = 'Attachments: Some Common attachment files are available on the Jervis System';
        } else {
            $fileData = '';
        }
        $html .= ' <div class="form-group">
                    <label for="notes">Notes</label>
                    <textarea name="notes" id="notes" class="form-control" placeholder="Let us know if there is something we should pay attention to about this property.">'.$noteData.'</textarea>
                </div>
                <div class="form-group">
                    <p>'.$fileData.'</p>
                </div>
        </div>
        </body>
</html>';

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (inline view)
        $dompdf->stream('mypdf.pdf', [
            'Attachment' => false,
        ]);
    }

    public function incompleteTasks()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($propertyId);
        if (!$property) {
            self::forbidden('You have insufficient permissions to access this property.');
        }
        if (!$propertyModel->propertyIsRelevant($property, Model::sessionGet('managerId'))) {
            self::forbidden('You are not authorized to see checklists for this property.');
        }

        $submissionModel = self::model('ManagerChecklistSubmission');
        $submissions = $submissionModel->getTasksByPropertyId($propertyId);

        $uncheckedTasks = [];
        if (count($submissions)) {
            foreach ($submissions as $submissionId => $submission) {
                foreach ($submission['data'] as $data) {
                    foreach ($data['tasks'] as $taskId => $task) {
                        if (isset($uncheckedTasks[$data['title']]) && in_array($task['title'], $uncheckedTasks[$data['title']])) {
                            $uncheckedTasks[$data['title']][$task['title']] = $uncheckedTasks[$data['title']][$task['title']] < $submission['created'] ? $submission['created'] : $uncheckedTasks[$data['title']][$task['title']];
                        } else {
                            if ($task['checked']) {
                                $uncheckedTasks[$data['title']][$task['title']] = $submission['created'];
                            } else {
                                $uncheckedTasks[$data['title']][$task['title']] = null;
                            }
                        }
                    }
                }
            }
        }

        self::viewTwig('manager/checklist/incomplete_task.html', [
            'property' => $property,
            'submissions' => $submissions,
            'uncheckedTasks' => $uncheckedTasks,
        ]);
    }

    public function editSubmission()
    {
        $submissionId = !empty($_GET['submission_id']) ? intval($_GET['submission_id']) : null;
        $requestId = !empty($_GET['request_id']) ? intval($_GET['request_id']) : null;
        if (!$submissionId) {
            self::badRequest("Missing parameter 'submission_id'");
        }
        $submissionModel = self::model('ManagerChecklistSubmission');
        $submission = $submissionModel->getSubmissionById($submissionId);

        $fillRequestModel = self::model('ManagerChecklistFillRequest');
        $request = $fillRequestModel->getRequestById($requestId);

        $templateModel = self::model('ManagerChecklistTemplate');
        $template = $templateModel->getTemplateById($submission['template_id']);
        if (!$template) {
            self::notFound('Could not find template for submission.');
        }

        $propertyModel = self::model('ManagerProperty');
        $property = $propertyModel->getPropertyById($submission['property_id']);
        if (!$propertyModel->propertyIsRelevant($property, Model::sessionGet('managerId'))) {
            self::forbidden('You do not have permission to fill out this checklist.');
        }

        self::viewTwig('manager/checklist/fill.html', [
            'submission' => $submission,
            'template' => $template,
            'property' => $property,
            'request' => $request,
            'session' => Model::sessionGetAll(),
        ]);
    }
}
