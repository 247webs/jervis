<?php

class ManagerReviewController extends ManagerController
{
    public function index()
    {
        $token = $_GET['token'];
        $splitToken = explode('_', $token);
        if (count($splitToken) != 2) {
            return false;
        }
        list($_, $userId) = $splitToken;
        if (sha1(DB_SALT.$userId).'_'.$userId != $token) {
            echo 'not valid';
        } else {
            $managerReviewModel = self::model('ManagerReview');
            $reviewerId = $managerReviewModel->getReviewerId();
            self::viewTwig('manager/review/review_form.html', [
                 'userId' => $userId,
                 'reviewerId' => $reviewerId,
            ]);
        }
    }

    public function addReview()
    {
        $managerReviewModel = self::model('ManagerReview');
        $addReview = $managerReviewModel->createReview($_POST, $_GET);
        exit(json_encode(''));
    }

    public function list()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        $managerReviewModel = self::model('ManagerReview');
        //$userId = $managerReviewModel->getUsersIdByPropertyId($propertyId);
        $ratingData = $managerReviewModel->getReviewsByPropertyId($propertyId);
        self::viewTwig('manager/review/index.html', [
            'propertyId' => $propertyId,
            'ratingData' => $ratingData,
       ]);
    }
}
