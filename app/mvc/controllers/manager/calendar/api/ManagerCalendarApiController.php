<?php

class ManagerCalendarApiController extends ManagerController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = self::model('ManagerCalendar');
    }

    public function saveCalendar($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }
        if (empty($params[0])) {
            $propertyId = $_POST['propertyId'];
            $property = self::model('ManagerProperty')->getPropertyById($propertyId, false);
            if (!$property) {
                self::notFound('Missing property.');
            }
			if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Calendar.create')) {
                self::forbidden('You have insufficient permissions to create calendar.');
            }
            $this->m->createCalendar($_POST);
        } else {
            $calendarId = intval($params[0]);
            // TODO: Consider which users can edit this
            $calendar = $this->m->getCalendarById($calendarId);
            if (!$calendar) {
                self::notFound('Calendar cannot be found.');
            }
            $propertyId = $calendar['property_id'];
            $property = self::model('ManagerProperty')->getPropertyById($propertyId, false);
            if (!$property) {
                self::notFound('Missing property.');
            }
			if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Calendar.edit')) {
                self::forbidden('You have insufficient permissions to edit calendar.');
            }
			
            $this->m->updateCalendar($calendarId, $_POST);
        }
        exit(json_encode(['success' => true]));
    }

    public function deleteCalendar($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Delete requires the DELETE HTTP method.';
            exit;
        }
        $calendar = $this->m->getCalendarById($params[0]);
        if (!$calendar) {
            self::notFound('Calendar cannot be found.');
        }
        $propertyId = $calendar['property_id'];
        $property = self::model('ManagerProperty')->getPropertyById($propertyId, false);
        if (!$property) {
            self::notFound('Missing property.');
        }
		if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Calendar.delete')) {
            self::forbidden('You have insufficient permissions to delete calendar.');
        }

        if (isset($params[0]) and !empty($params[0])) {
            exit(json_encode([
                'success' => $this->m->deleteCalendar(intval($params[0])),
            ]));
        } else {
            self::badRequest("Missing 'id' for Calendar");
        }
    }
}
