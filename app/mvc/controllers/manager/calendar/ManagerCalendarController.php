<?php

class ManagerCalendarController extends ManagerController
{
    public function index()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }

        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }

        if (!self::model('ManagerRole')->currentUserHasAnyPermission(
            $property,
            [
                'Calendar.edit',
                'Calendar.delete',
                'Calendar.create',
            ]
        )) {
            self::forbidden('You have insufficient permissions to manage calendar.');
        }

        $calendarModel = self::model('ManagerCalendar');
        $calendars = $calendarModel->getCalendarsByPropertyId($propertyId);

        self::viewTwig('manager/calendar/index.html', [
            'calendars' => $calendars,
            'property' => $property,
        ]);
    }

    public function addCalendar()
    {
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::badRequest("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }

        if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Calendar.create')) {
            self::forbidden('You have insufficient permissions to create calendar.');
        }

        $calendar = [];
        self::viewTwig('manager/calendar/calendar_edit.html', [
            'calendar' => null,
            'access' => [],
            'propertyId' => $propertyId,
        ]);
    }

    public function editCalendar()
    {
        $calendarId = !empty($_GET['id']) ? intval($_GET['id']) : null;
        if (!$calendarId) {
            self::notFound("Missing parameter 'id'");
        }
        $propertyId = !empty($_GET['property_id']) ? intval($_GET['property_id']) : null;
        if (!$propertyId) {
            self::notFound("Missing parameter 'property_id'");
        }
        $property = self::model('ManagerProperty')->getPropertyById($propertyId);
        if (!$property) {
            self::notFound('Could not find property.');
        }

        if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Calendar.edit')) {
            self::forbidden('You have insufficient permissions to edit calendar.');
        }

        $calendarModel = self::model('ManagerCalendar');
        $calendar = $calendarModel->getCalendarById($calendarId);

        if (!$calendar) {
            self::notFound("Could not find calendar with id '$calendarId");
        }

        self::viewTwig('manager/calendar/calendar_edit.html', [
            'calendar' => $calendar,
            'access' => [],
            'propertyId' => $propertyId,
        ]);
    }
}
