<?php

class ManagerDeviceController extends ManagerController
{
    public function __construct()
    {
        $userRole = Model::sessionGet('managerRole');
        if (!(isset($userRole->Owner) && $userRole->Owner == 1)) {
            self::forbidden('You have insufficient permissions to use this function.');
        }
    }
    public function index()
    {
        $yonomi = self::model('ManagerDevice')->setYonomi();
        if ($yonomi->errMsg) {
            if ($_SERVER['REMOTE_ADDR'] == "18.205.219.182") {
                self::forbidden(sprintf("%s<pre>DEBUGINFO:<br />%s</pre>", $yonomi->errMsg, print_r($yonomi->logs, true)));
            } else {
                self::forbidden($yonomi->errMsg);
            }
        }
        
        $devices = self::model('ManagerDevice')->getDevicesByManagerId();
        $property_list = self::model('ManagerProperty')->getPropertiesRelevantForCurrentUser();
        self::viewTwig('manager/device/index.html', [
            'devices' => $devices,
            'property_list' => $property_list,
            'successMsg' => (isset($_GET['successMsg']) ? $_GET['successMsg'] : ""),
            'errorMsg' => (isset($_GET['errorMsg']) ? $_GET['errorMsg'] : "")
        ]);
    }
}
