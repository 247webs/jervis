<?php

class ManagerDeviceApiController extends ManagerController
{
    protected $m;
    protected $yonomi;
    public function __construct()
    {
        parent::__construct();
        $userRole = Model::sessionGet('managerRole');
        if (!(isset($userRole->Owner) && $userRole->Owner == 1)) {
            self::forbidden('You have insufficient permissions to use this function.');
        }
        $this->m = self::model('ManagerDevice');
    }
    public function connect($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'GET') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the GET HTTP method.';
            exit;
        }
        if (!isset($params[0])) {
            self::notFound('Device Type Not Found');
        }
        switch ($params[0]) {
            case "schlage_account":
            case "kasa_account":
            case "august_account":
                $yonomi = $this->m->setYonomi();
                $newDevice =$yonomi->createDevice($params[0]);
                $AuthUri = $yonomi->getAuthUri($params[0], $newDevice->_id);
                Helpers::redirectRelative($AuthUri);
            break;
            default:
            self::notFound('This Device Type Not Implemented Yet');
        }
    }
    public function disconnect($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'GET') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the GET HTTP method.';
            exit;
        }
        if (!isset($params[0])) {
            self::notFound('Device Type Not Found');
        }
        switch ($params[0]) {
            case "schlage_account":
            case "kasa_account":
            case "august_account":
                $yonomi = $this->m->setYonomi();
                $accountDevices = $yonomi->getDevices(["type"=>$params[0]]);
                foreach ($accountDevices as $accountDevice) {
                    foreach ($yonomi->getDevices(["parent_id"=>$accountDevice->_id]) as $device) {
                        $this->m->deleteDevice($device->_id);
                    }
                    $yonomi->deleteDevice($accountDevice->_id);
                }
                Helpers::redirect("manager/device/index");
            break;
            default:
            self::notFound('This Device Type Not Implemented Yet');
        }
    }
    public function auth()
    {
        $this->m->updateYonomiDevices();
        if (isset($_GET['status']) and ($_GET['status']=="success")) {
            Helpers::redirect("manager/device/index?successMsg=Devices Imported");
        } else {
            Helpers::redirect("manager/device/index?errorMsg=Devices Import Failed");
        }
    }
    public function refresh()
    {
        $this->m->updateYonomiDevices();
        Helpers::redirect("manager/device/index?successMsg=Refreshed");
    }
    public function assignDevice($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'UPDATE') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the UPDATE HTTP method.';
            exit;
        }
        if (!isset($params[0])) {
            self::notFound('Device ID Not Found');
        }
        if (!isset($params[1])) {
            self::notFound('Property ID Not Found');
        }
        $deviceId = $params[0];
        $propertyId = $params[1];
        if ($propertyId>0) {
            $property = self::model('ManagerProperty')->getPropertyById($propertyId, false);
            if (!$property) {
                self::notFound('Missing property.');
            }
            if (!self::model('ManagerRole')->currentUserHasPermission($property, 'Device.create')) {
                self::forbidden('You have insufficient permissions to create devices.');
            }
        }
        $this->m->assignDeviceToProperty($deviceId, $propertyId);
    }
    public function actionRequest($params)
    {
        if (!isset($params[0])) {
            self::notFound('Device ID Not Found');
        }
        if (!isset($params[1])) {
            self::notFound('Action ID Not Found');
        }
        $deviceId = $params[0];
        $actionId = $params[1];
        
        $yonomi = $this->m->setYonomi();
        $actionRequest = $yonomi->actionRequest($deviceId, $actionId);
        if (isset($actionRequest->error)) {
            Helpers::redirect("manager/device/index?errorMsg=".$actionRequest->error->message);
        }
        if (!isset($actionRequest->_id)) {
            Helpers::redirect("manager/device/index?errorMsg=Action request error");
        }
        usleep(500);
        do {
            $actionResponse = $yonomi->actionResponse($actionRequest->_id);
            usleep(500);
        } while (!(isset($actionResponse->status) and (($actionResponse->status=="success") or ($actionResponse->status=="expired"))));
        if ($actionResponse->status == "success") {
            Helpers::redirect("manager/device/index?successMsg=Command applied");
        } else {
            Helpers::redirect("manager/device/index?errorMsg=Problem occurred while accessing the device.");
        }
    }
    public function stateRequest($params)
    {
        if (!isset($params[0])) {
            self::notFound('Device ID Not Found');
        }
        if (!isset($params[1])) {
            self::notFound('State ID Not Found');
        }
        $deviceId = $params[0];
        $stateId = $params[1];
        
        $yonomi = $this->m->setYonomi();
        $stateRequest = $yonomi->stateRequest($deviceId, [$stateId]);
        if (isset($stateRequest->error)) {
            Helpers::redirect("manager/device/index?errorMsg=".$stateRequest[0]->error->message);
        }
        if (!isset($stateRequest[0]->_id)) {
            Helpers::redirect("manager/device/index?errorMsg=State request error");
        }
        usleep(500);
        do {
            $stateResponse = $yonomi->stateResponse($stateRequest[0]->_id);
            usleep(500);
        } while (!(isset($stateResponse->status) and (($stateResponse->status=="success") or ($stateResponse->status=="expired"))));

        if ($stateResponse->status == "success") {
            foreach ($stateResponse->results as $resp) {
                if ($resp->devicetype_state_id == $stateId) {
                    Helpers::redirect("manager/device/index?successMsg=Result: ".$resp->value_human_readable);
                }
            }
        } else {
            Helpers::redirect("manager/device/index?errorMsg=Problem occurred while accessing the device.");
        }
        if (isset($stateResponse->results[0])) {
            Helpers::redirect("manager/device/index?successMsg=Result: ".$stateResponse->results[0]->value_human_readable);
        }
    }
}
