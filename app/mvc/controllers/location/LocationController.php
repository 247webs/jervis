<?php

    class LocationController
    {
        public function index()
        {
            try {
                if (!isset($_GET['type']) || empty($_GET['type'])) {
                    throw new exception('Type is not set.');
                }
                $type = $_GET['type'];
                if ($type == 'getCountries') {
                    $data = $this->getCountries();
                }
                if ($type == 'getStates') {
                    if (!isset($_GET['countryId']) || empty($_GET['countryId'])) {
                        throw new exception('Country Id is not set.');
                    }
                    $countryId = $_GET['countryId'];
                    $data = $this->getStates($countryId);
                }
                if ($type == 'getCities') {
                    if (!isset($_GET['stateId']) || empty($_GET['stateId'])) {
                        throw new exception('State Id is not set.');
                    }
                    $stateId = $_GET['stateId'];
                    $data = $this->getCities($stateId);
                }
            } catch (Exception $e) {
                $data = array('status' => 'error', 'tp' => 0, 'msg' => $e->getMessage());
            } finally {
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        }

        public static function getCountries()
        {
            try {
                $geoLocationModel = new GeoLocation();
                $countries = $geoLocationModel->getCountries();
                $res = array();
                foreach ($countries as $country) {
                    $res[$country['id']] = $country['name'];
                }
                $data = array('status' => 'success', 'tp' => 1, 'msg' => 'Countries fetched successfully.', 'result' => $res, 'presel' => false, 'hits' => 1);
            } catch (Exception $e) {
                $data = array('status' => 'error', 'tp' => 0, 'msg' => $e->getMessage());
            } finally {
                return $data;
            }
        }

        public static function getStates($countryId)
        {
            try {
                $geoLocationModel = new GeoLocation();
                $states = $geoLocationModel->getStatesByCountryId($countryId);

                $res = array();
                foreach ($states as $state) {
                    $res[$state['id']] = $state['name'];
                }

                $data = array('status' => 'success', 'tp' => 1, 'msg' => 'States fetched successfully.', 'result' => $res);
            } catch (Exception $e) {
                $data = array('status' => 'error', 'tp' => 0, 'msg' => $e->getMessage());
            } finally {
                return $data;
            }
        }

        public static function getCities($stateId)
        {
            try {
                $geoLocationModel = new GeoLocation();
                $cities = $geoLocationModel->getCitiesByStateId($stateId);
                foreach ($cities as $city) {
                    $res[$city['id']] = $city['name'];
                }
                $data = array('status' => 'success', 'tp' => 1, 'msg' => 'Cities fetched successfully.', 'result' => $res);
            } catch (Exception $e) {
                $data = array('status' => 'error', 'tp' => 0, 'msg' => $e->getMessage());
            } finally {
                return $data;
            }
        }
    }
