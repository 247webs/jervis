<?php

class RequestController extends ViewController
{
    protected $m;

    public function __construct()
    {
    }

    public function index()
    {
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
            $help = $_POST['help'];
            $best = $_POST['best'];
            $name = $_POST['name'];
            $company = $_POST['company'];
            $about = $_POST['about'];

            $mail = Helpers::createEmail();
            $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
            $mail->addAddress('info@jervis-systems.com');
            $mail->isHTML(true);
            $mail->Subject = 'Request Info Submitted';
            $mail->Body = "$name from $company has submitted request info for Jervis System<br><br>Please find all the details as below<br><br>Name : $name<br>Position : $best<br>Company Name : $company<br>Email : $email<br>How did you hear about us? : $about <br> Need Help for : $help <br><br>Yours, <br> The Jervis team";
            if ($mail->send()) {
                $response['description'] = 'Your message has been sent';
                $response['error'] = false;
            } else {
                $response['description'] = DEBUG == 1 ? $mail->ErrorInfo : 'Something went wrong';
                $response['error'] = true;
            }
        } else {
            $response['description'] = 'Please provide email address';
            $response['error'] = true;
        }
        exit(json_encode($response));
    }
}