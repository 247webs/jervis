<?php

class AdminUserController extends AdminController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = self::model('AdminUser');
    }

    public function index()
    {
        $id = !empty($_GET['id']) ? $_GET['id'] : null;
        if (!$id) {
            self::badRequest("Missing parameter 'id'");
        }

        $manager = self::model('Manager')->getManagerById($id);
        if (!$manager) {
            self::notFound('Manager cannot be found.');
        }

        self::viewTwig('admin/user/index.html', [
            'manager' => $manager,
            'plan' => json_decode($manager['plan'], true),
            'trial_end_date' => $manager['trial_end_date'],
        ]);
    }
}
