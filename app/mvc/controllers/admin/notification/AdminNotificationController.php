<?php

class AdminNotificationController extends AdminController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = self::model('AdminNotification');
    }

    public function addTemplate()
    {
        self::viewTwig('admin/notification/template_edit.html', [
            'template' => null,
        ]);
    }

    public function editTemplate()
    {
        $templateId = !empty($_GET['id']) ? intval($_GET['id']) : null;
        if (!$templateId) {
            self::notFound("Missing parameter 'id'");
        }

        $template = $this->m->getTemplateById($templateId);
        if (!$template) {
            self::notFound("Could not find template with id '$templateId");
        }        
        
        $template['body'] = str_replace(array("\r", "\n"), '', $template['body']);
        $template['body'] = str_replace("'", "\'", $template['body']);        
        
        self::viewTwig('admin/notification/template_edit.html', [
            'template' => $template,
        ]);
    }
}