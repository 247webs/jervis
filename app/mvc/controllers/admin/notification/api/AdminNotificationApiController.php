<?php

class AdminNotificationApiController extends AdminController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = self::model('AdminNotification');
    }

    public function saveTemplate()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            self::methodNotAllowed('Save requires POST HTTP method.');
            exit;
        }
        
        $templateId = null;
        
        if (!empty($_GET['id'])) {
            $templateId = intval($_GET['id']);            
            $template = $this->m->getTemplateById($templateId);
            if (!$template) {
                self::notFound("Could not find template with id '$templateId");
            }
        }
        
        $errors = [];
        $template = $this->m->saveTemplate($templateId, $_POST, $errors);
        if ($template) {
            exit(json_encode(['template' => $template]));
        } else {
            self::badRequest($errors);
        }
    }

    public function deleteNotificationTemplate($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Delete requires the DELETE HTTP method.';
            exit;
        }
        $deleteData = $this->m->deleteTemplate($params);
        echo json_encode('');
    }
}