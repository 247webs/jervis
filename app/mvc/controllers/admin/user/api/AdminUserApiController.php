<?php

class AdminUserApiController extends AdminController
{
    protected $m;

    public function __construct()
    {
        parent::__construct();
        $this->m = self::model('AdminUser');
    }

    public function saveUserPlan($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Set requires the POST HTTP method.';
            exit;
        }

        $managerType = Model::sessionGet('managerType');

        $managerId = intval($params[0]);

        $manager = self::model('Manager')->getManagerById($managerId);
        if (!$manager) {
            self::notFound('Manager cannot be found.');
        }

        if (isset($params[0]) and !empty($params[0])) {
            if ($managerType != 3) {
                exit(json_encode([
                    'success' => $this->m->updateUserPlan($managerId, $_POST),
                ]));
            } else {
                self::badRequest('User type cannot modify plan');
            }
        } else {
            self::badRequest("Missing 'id' for Manager");
        }
    }

    public function deleteUser($params)
    {
        if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Delete requires the DELETE HTTP method.';
            exit;
        }

        $managerType = Model::sessionGet('managerType');

        $manager = self::model('Manager')->getManagerById($params[0]);
        if (!$manager) {
            self::notFound('Manager cannot be found.');
        }
        if (isset($params[0]) and !empty($params[0])) {
            if ($managerType != 3) {
                exit(json_encode([
                    'success' => $this->m->deleteUser(intval($params[0])),
                ]));
            } else {
                self::badRequest('User type cannot delete user');
            }
        } else {
            self::badRequest("Missing 'id' for Manager");
        }
    }
}
