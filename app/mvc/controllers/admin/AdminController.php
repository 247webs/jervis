<?php

class AdminController extends ManagerController
{
    protected $m;
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = Model::sessionGet('managerId');
        $this->m = self::model('Admin');
    }

    public function index()
    {
        $userId = self::$mId;

        $manager = self::model('Manager')->getManagerById($userId);

        if ($manager['access_admin']) {
            $managers = $this->m->getManagers();
            self::viewTwig('admin/index.html', [
                'managers' => $managers,
            ]);
        } else {
            self::forbidden('You have insufficient permissions to access admin.');
        }
    }

    public function notification()
    {
        $userId = self::$mId;
        $manager = self::model('Manager')->getManagerById($userId);

        $adminNotificationModel = self::model('AdminNotification');
        $template = $adminNotificationModel->getTemplate();
        if ($manager['access_admin']) {
            self::viewTwig('admin/notification/index.html', [
                'template' => $template,
            ]);
        } else {
            self::forbidden('You have insufficient permissions to access admin.');
        }
    }
}
