<?php
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiChecklistsTemplatesController extends RestController {
    
    public function get($templateId = null) { 
        parent::__construct();        
        
        if (v::numericVal()->validate($templateId)) {
            
            $templateModel = new ManagerChecklistTemplate;
            $template = $templateModel->getTemplateById($templateId);
            if (!$template) {
                $this->response(['error' => TRUE, 'message' => "Could not find template with id '$templateId"], 400);
            }
            
            $propertyModel = new ManagerProperty();
            $property = $propertyModel->getPropertyById($template['property_id']);
            if (!$property) {
                $this->response(['error' => TRUE, 'message' => 'Could not find property.'], 400);
            }

            if ($this->userId != $property['owner_id']) {
                $this->response(['error' => TRUE, 'message' => 'You have insufficient permissions to manage templates.'], 400);
            }

            $this->response($template, 200);
        }
        $this->response(['error' => TRUE, 'message' => 'Missing templatechecklist id'], 400);
    }

    public function delete($templateId = null) {
        error_reporting(0);
        parent::__construct();

        if (v::numericVal()->validate($templateId)) {

            $templateModel = new ManagerChecklistTemplate;
            $template = $templateModel->getTemplateById($templateId);
            if (!$template) {
                $this->response(['error' => TRUE, 'message' => "Could not find template with id '$templateId"], 400);
            }

            $propertyModel = new ManagerProperty();
            $property = $propertyModel->getPropertyById($template['property_id']);
            if (!$property) {
                $this->response(['error' => TRUE, 'message' => 'Could not find property.'], 400);
            }
            
            if (!$propertyModel->propertyIsRelevant($property, $this->userId)) {
                $this->response(['error' => TRUE, 'message' => 'You have insufficient permissions to delete templates'], 400);
            }
            
            $managerChecklistTemplateModel = new ManagerChecklistTemplate();
            $managerChecklistTemplateModel->deleteTemplate(intval($templateId));

            $this->response('Template has been deleted successfully', 200);
        }
        $this->response(['error' => TRUE, 'message' => 'Missing templatechecklist id'], 400);
    }

}