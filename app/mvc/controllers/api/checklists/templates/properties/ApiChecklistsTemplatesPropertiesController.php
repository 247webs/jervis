<?php
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiChecklistsTemplatesPropertiesController extends RestController {
    
    public function __construct() 
    {
//        error_reporting(0);
        parent::__construct();
    }
    
    public function get($propertyId = null) 
    {        
        if (v::numericVal()->validate($propertyId)) {
                        
            $propertyModel = new ManagerProperty();
            $property = $propertyModel->getPropertyById($propertyId);
            if (!$property) {
                $this->response(['error' => TRUE, 'message' => 'Could not find property.'], 400);
            }

            if (!$propertyModel->propertyIsRelevant($property, $this->userId)) {
                $this->response(['error' => TRUE, 'message' => 'You have insufficient permissions to see checklists for this property.'], 400);
            }
            
            $managerChecklistTemplateModel =  new ManagerChecklistTemplate();
            $templates = $managerChecklistTemplateModel->getTemplatesByPropertyId($propertyId);
            
            foreach ($templates as $key => $template) {
                
                foreach ($template['groups'] as $key_group => $group) {
                    
                    unset($templates[$key]['groups'][$key_group]['title']);
                    unset($templates[$key]['groups'][$key_group]['tasks']);
                    $templates[$key]['groups'][$key_group]['id'] = $key_group+1;
                    $templates[$key]['groups'][$key_group]['title'] = $group['title'];
                    
                    foreach ($group['tasks'] as $key_task => $task) {
                        
                        $templates[$key]['groups'][$key_group]['tasks'][$key_task]['id'] = $key_task+1;
                        $templates[$key]['groups'][$key_group]['tasks'][$key_task]['title'] = $task;
                    }       
                }
            }

            $this->response($templates, 200);
        }
        $this->response(['error' => TRUE, 'message' => 'Missing templatechecklist id'], 400);
    }

    public function delete() { }
    
    public function post($propertyId = null) {        
        try {
            $templateId = null;
            if (v::numericVal()->validate($propertyId)) {
                
                //$this->setCreateUserRules($_POST);

                $managerChecklistTemplateModel = new ManagerChecklistTemplate();
                $propertyModel = new ManagerProperty();
                
                $property = $propertyModel->getPropertyById($propertyId);
                if (!$property) {
                    $this->response(['error' => TRUE, 'message' => 'Could not find property.'], 400);
                }

                if (!$propertyModel->propertyIsRelevant($property, $this->userId)) {
                    $this->response(['error' => TRUE, 'message' => 'You have insufficient permissions to see checklists for this property.'], 400);
                }
                
                if (empty($_POST['template'])) {
                    $this->response(['error' => TRUE, 'message' => 'Request is missing template value.'], 400);                    
                }

                $values = json_decode($_POST['template'], JSON_OBJECT_AS_ARRAY);
                if (empty($values)) {
                    $this->response(['error' => TRUE, 'message' => 'Invalid JSON.'], 400);                   
                }

                $errors = [];
                $template = $templateModel->saveTemplate($property['id'], $templateId, $values, $errors);
                if ($template) {
                    $this->response('Template has been created successfully.', 200); 
                } else {
                    $this->response(['error' => TRUE, 'message' => $errors], 400);                  
                }
            }
            $this->response(['error' => TRUE, 'message' => 'Missing templatechecklist id'], 400);
        } catch (Exception $ex) {
            
        }
    }

}