<?php

use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiChecklistsController extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->m = new ManagerChecklistFillRequest();
    }
    
    public function get($id)
    {
        if (v::numericVal()->validate($id)) {

            $request = $this->m->getRequestById($id);
            if (!$request) {                
                $this->response(['error' => TRUE, 'message' => "Could not find checklist with id '$id'"], 400);
            }
            $request['due_date'] = date('Y-m-d', $request['due_date']); 
            unset($request['is_due']);
        
            $this->response($request, 200);
        }
        $this->response(['error' => TRUE, 'message' => 'Missing id'], 400);
    }

    public function delete($id)
    {
    }

    public function post()
    {
        try {
            $this->setAssignChecklistRules($_POST);
            
            $_POST['due_date'] = $_POST['dueDate'];
            unset($_POST['dueDate']);
            
            $templateModel = new ManagerChecklistTemplate();
            $propertyModel = new ManagerProperty();
            $submissionModel = new ManagerChecklistSubmission();
            $managerModel = new Manager();
        
            $template = $templateModel->getTemplateById($_POST['templateId']);
            if (!$template) {                
                $this->response(['error' => true, 'message' => 'Template cannot be found.'], 400);
            }
            
            $property = $propertyModel->getPropertyById($_POST['propertyId']);
            if (!$property) {            
                $this->response(['error' => true, 'message' => 'Property cannot be found.'], 400);
            }
            
            if (!$propertyModel->propertyIsRelevant($property, $this->userId)) {
                $this->response(['error' => true, 'message' => 'You have insufficient permissions to submit fill requests in this property.'], 400);                
            }
            
            if($template['property_id'] != $property['id']) {
                $this->response(['error' => true, 'message' => 'Template property cannot be match.'], 400);
            }
            
            $user = $managerModel->fetch('SELECT * FROM manager WHERE id = :id', [':id' => $_POST['userId']]);
            if (!$user) {                
                $this->response(['error' => true, 'message' => 'Recipient could not be found.'], 400);
            }
            
            $users = $propertyModel->getAccessByPropertyId($_POST['propertyId']);            
            $key = array_search($user['id'], array_column($users, 'id'));
            if($key === FALSE) {
                $this->response('Recipient does not belong to this property.', 400);
            }
            
            $managerUserModel = new ManagerUser();
            $managerUserModel->saveNotificationByUserId($user['id'], '', '', 'checklist');

            $success = $submissionModel->createSubmissionFillRequest($template, $user);
            $requestId = $submissionModel->lastInsertId();

            $adminNotificationModel = new AdminNotification();
            $emailTemplate = $adminNotificationModel->getTemplateByTitle('Checklist - Fill Request');
                        
            // Send email            
            try {
                $url = URL.'/manager/checklist/fill/?template_id='.$template['id'].'&request_id='.$requestId;
                                
                $mail = Helpers::createEmail();
                $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
                $mail->addAddress($user['email']);
                $mail->isHTML(true);
                $mail->Subject = str_replace('{CHECKLIST_TITLE}', $template['title'], $emailTemplate['subject']);
                $mail->Body = str_replace('{CHECKLIST_URL}', $url, $emailTemplate['body']);
                $mail->send();
                
            } catch (Exception $e) {                
                $this->response(['error' => true, 'message' => 'Failed to send email.'], 500);
            }

            // Send SMS
            $url = Helpers::makeShortUrl($url);
            $smsBody = "You are requested to fill the checklist {$url}";
            if (!empty($user['mobile'])) {
                Helpers::sendSMSNotification($smsBody, '+1' . $user['mobile']);
            }

            $this->response('Assign checklist to team member.', 200);
            
        } catch (Exception $ex) {
        }
    }
    
    public function setAssignChecklistRules($data)
    {
        try {
            v::create()
                    ->key('propertyId', v::digit())
                    ->key('templateId', v::digit())
                    ->key('userId', v::digit())
                    ->key('dueDate', v::date())
                    ->key('priority', v::containsAny([1,2,3]))                    
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }
}
