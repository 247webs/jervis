<?php
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiChecklistsPropertiesController extends RestController {
    
    public function get($propertyId = null) { 
        parent::__construct();        
        
        if (v::numericVal()->validate($propertyId)) {
            
            $propertyModel = new ManagerProperty();
            $property = $propertyModel->getPropertyById($propertyId);
            if (!$property) {
                $this->response(['error' => TRUE, 'message' => 'Could not find property.'], 400);
            }
            
            if (!$propertyModel->propertyIsRelevant($property, $this->userId)) {
                $this->response(['error' => TRUE, 'message' => 'You have insufficient permissions to see checklists for this property.'], 400);
            }

            $fillRequestModel = new ManagerChecklistFillRequest();
            $requests = $fillRequestModel->getRequestsByPropertyId($property['id']); 
            
            foreach ($requests as $key => $request) {             
                $requests[$key]['due_date'] = date('Y-m-d', $request['due_date']);
                unset($requests[$key]['first_name']);
                unset($requests[$key]['last_name']);
                unset($requests[$key]['template_title']);
                unset($requests[$key]['is_due']);
            }

            $this->response($requests, 200);
        }
        $this->response(['error' => TRUE, 'message' => 'Missing parameter property_id'], 400);
    }

    public function delete() {}

}