<?php

use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiChecklistsFillController extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->m = new ManagerChecklistFillRequest();
    }
    
    public function get() {}

    public function delete() {}

    public function post()
    {
        try {            
            
            $this->setChecklistsFillRules($_POST);
                        
            $templateModel = new ManagerChecklistTemplate();
            $propertyModel = new ManagerProperty();
            $submissionModel = new ManagerChecklistSubmission();
                        
            $templateId = $_POST['templateId'];
            $requestId = $_POST['requestId'];
            
            $template = $templateModel->getTemplateById($templateId);
            if (!$template) {                
                $this->response(['error' => true, 'message' => 'Checklist template could not be found.'], 400);
            }   
                
            $property = $propertyModel->getPropertyById($template['property_id']);
            if (!$property) {            
                $this->response(['error' => true, 'message' => 'Property cannot be found.'], 400);
            }
            
            $request = $this->m->getRequestById($requestId);
            if ($request) {
                if ($request['user_id'] != $this->userId) {
					$this->response(['error' => true, 'message' => 'You do not have permission to fill out this checklist.'], 400);
                }
            } else {
                if (!$propertyModel->propertyIsRelevant($property, $this->userId)) {
                    $this->response(['error' => true, 'message' => 'You have insufficient permissions to fill checklists.'], 400);
                }
            }
            
            if ($request && $request['filled_id']) {
                $this->response(['error' => true, 'message' => 'This request is already filled out.'], 400);
            }
            
            $submission = null;
            
            $managerModel = new Manager();
            $manager = $managerModel->getManagerById($this->userId);
                        
            // Actually validate the thing
            //count notes
            foreach ($_POST['response']['groups'] as $note) {
                $filled_array = array_filter($note['tasks']); // will return only filled values
                $count[] = count($filled_array);
            }
            $notesCount = array_sum($count);
            
            $success = $submissionModel->fillSubmissionByApi($template, $_POST['response'], $submission, $request, $notesCount, $manager);                        
            $date = date('Y/m/d h:i:s A');
            if ($success && $property['owner_id'] != $this->userId) {
                
                $user = $managerModel->getManagerById($property['owner_id']);
                $managerUserModel = new ManagerUser();
                $managerUserModel->saveNotificationByUserId($property['owner_id'], '', '', 'checklist');

                $subject = 'Checklist submitted';
                $message = 'Hello ' . $user['first_name'] . ', <br/><br/> ' . $manager['first_name'] . ' has submitted the ' . $template['title'] . ' on ' . $date . ' in ' . $property['title'] . '.<br/>You can review the submitted checklist by <a href = "' . URL . '/manager/checklist/edit-submission/?submission_id=' . $requestId . '">clicking here.</a>';

                $url = URL . '/manager/checklist/edit-submission/?submission_id=' . $requestId;

                $modelAdminNotification = new AdminNotification();
                $emailTemplate = $modelAdminNotification->getTemplateByTitle('Checklist - submitted notification');

                $body = str_replace('{BUSINESS_OWNER_NAME}', $user['first_name'], $emailTemplate['body']);
                $body = str_replace('{SERVICE_PROVIDER_USER_NAME}', $manager['first_name'], $body);
                $body = str_replace('{CHECKLIST_TITLE}', $template['title'], $body);
                $body = str_replace('{CHECKLIST_SUBMIT_TIME}', $date, $body);
                $body = str_replace('{PROPERTY_NAME}', $property['title'], $body);
                $body = str_replace('{CHECKLIST_URL}', $url, $body);

                Helpers::sendEmailNotification($user['email'], $emailTemplate['subject'], $body);

                $emailTemplate = $modelAdminNotification->getTemplateByTitle('Checklist - confirmation');

                $loggedInUserEmail = $manager['email'];

                $body = str_replace('{CHECKLIST_TITLE}', $template['title'], $emailTemplate['body']);
                $body = str_replace('{CHECKLIST_SUBMIT_TIME}', $date, $body);
                
                Helpers::sendEmailNotification($loggedInUserEmail, $emailTemplate['subject'], $body);
            }

            $this->response('Checklist has been filled successfully.', 200);
            
        } catch (Exception $ex) {
        }
    }
    
    public function setChecklistsFillRules($data)
    {
        try {
            v::create()
                    ->key('requestId', v::digit())
                    ->key('templateId', v::digit())
                    ->key('response', v::notEmpty())                    
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }
}
