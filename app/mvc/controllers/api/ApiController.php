<?php

class ApiController extends Controller
{
    public function bitbucket()
    {
        $modelApi = self::model('Api');
        if ($modelApi->checkBearer() === true) {
            View::apiRender($modelApi->pullFromBitbucket());
        } else {
            View::apiRender('Not Authorized.');
        }
    }

    public function notFound($error)
    {
        http_response_code(404);
        header('Content-Type: application/json');
        $message = [
            'header' => 'Not found',
            'error' => $error,
        ];
        exit(json_encode($message));
    }

    public function badRequest($error)
    {
        http_response_code(400);
        header('Content-Type: application/json');
        $message = [
            'header' => 'Bad request',
            'error' => $error,
        ];
        exit(json_encode($message));
    }

    public function forbidden($error)
    {
        http_response_code(403);
        header('Content-Type: application/json');
        $message = [
            'header' => 'Forbidden',
            'error' => $error,
        ];
        exit(json_encode($message));
    }

    public function methodNotAllowed($error)
    {
        http_response_code(405);
        $message = [
            'header' => 'Method not allowed',
            'error' => $error,
        ];
        exit(json_encode($message));
    }
}
