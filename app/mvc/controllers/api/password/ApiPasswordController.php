<?php

use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiPasswordController extends RestController
{
    public function __construct() {
        //parent::__construct(); 
    }

    public function get() {}

    public function delete() {}

    public function post()
    {
        try {
            $this->setResetPasswordRules($_POST);

            $email = (isset($_POST['email']) and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) ? $_POST['email'] : '';
            $signupModel = new Signup();
            if ($signupModel->userExists($email)) {
                
                $token = $this->generateToken();
                
                $forgotPasswordModel = new ForgotPassword();                
                $forgotPasswordModel->setToken($email, $token);
                
                $mail = Helpers::createEmail();
                $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
                $mail->addAddress($email);
                $mail->isHTML(true);

                $modelAdminNotification = new AdminNotification();
                $template = $modelAdminNotification->getTemplateByTitle('Password - Reset');

                $url = URL . "/forgotPassword/reset?token=$token";
                $mail->Subject = $template['subject'];

                $body = str_replace('{USER_EMAIL}', $email, $template['body']);
                $body = str_replace('{RESET_PASSWORD_URL}', $url . ' ', $body);

                $mail->Body = $body;
                $mail->send();

                $this->response('An email is on its way to you. Follow the instructions to reset your password.', 200);
            } else {                
                $this->response(['error' => true, 'message' => 'We could not find your email. Try another?'], 400);
            }
        } catch (Exception $ex) {
        }
    }

    private function generateToken()
    {
        $token = random_bytes(32);

        return hash('sha256', $token);
    }
    
    private function setResetPasswordRules($data)
    {
        try {
            v::create()                    
                    ->key('email', v::email())
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }
}
