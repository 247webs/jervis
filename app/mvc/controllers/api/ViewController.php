<?php

class ViewController extends Controller
{
    public function notFound($error)
    {
        http_response_code(404);
        echo '<h1>Not found</h1>';
        exit(is_array($error) ? implode("<br>\r\n", $error) : $error);
    }

    public function badRequest($error)
    {
        http_response_code(400);
        echo '<h1>Bad request</h1>';
        exit(is_array($error) ? implode("<br>\r\n", $error) : $error);
    }

    public function forbidden($error)
    {
        http_response_code(403);
        echo '<h1>Forbidden</h1>';
        exit(is_array($error) ? implode("<br>\r\n", $error) : $error);
    }

    public function methodNotAllowed($error)
    {
        http_response_code(405);
        echo '<h1>Method not allowed</h1>';
        exit(is_array($error) ? implode("<br>\r\n", $error) : $error);
    }
}
