<?php

class ApiStatesController extends RestController
{
    public function __construct() {
       // parent::__construct();
    }

    public function get($stateId = null) {      
        
        if (!empty($stateId)) {
            
            $geoLocationModel = new GeoLocation();
            $cities = $geoLocationModel->getCitiesByStateId($stateId);
            if (!$cities) {
                $this->response(['error' => TRUE, 'message' => 'Could not find cities.'], 400);                
            }
            
            $this->response($cities, 200);
            
        } 
        
        $this->response(['error' => TRUE, 'message' => 'Missing stateId.'], 400);
    }

}