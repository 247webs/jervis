<?php

class ApiCountriesController extends RestController
{
    public function __construct() {
       // parent::__construct();
    }

    public function get($countryId = null) {      
        
        if (empty($countryId)) {
            
            $geoLocationModel = new GeoLocation();
            $countries = $geoLocationModel->getCountries();
            if (!$countries) {
                $this->response(['error' => TRUE, 'message' => 'Could not find countries.'], 400);                
            }
            
            $this->response($countries, 200);
            
        } else {
            
            $geoLocationModel = new GeoLocation();
            $states = $geoLocationModel->getStatesByCountryId($countryId);
            if (!$states) {
                $this->response(['error' => TRUE, 'message' => 'Could not find states.'], 400);
            }

            $this->response($states, 200);
        }
    }

}