<?php
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiRolesController extends RestController {
           
    public function __construct()
    {
        parent::__construct();        
    }
    
    public function get($propertyId = null) {                 
                       
        if (v::numericVal()->validate($propertyId)) {
        
            $propertyModel = new ManagerProperty();
            $property = $propertyModel->getPropertyById($propertyId);
            if (!$property) {                
                $this->response(['error' => False, 'message' => 'Property cannot be found.'], 400);
            }
            
            if (!$this->currentUserHasAnyPermission($property, [] )) {                
                $this->response(['error' => TRUE, 'message' => 'You have insufficient permissions to manage property.'], 400);
            }
            
            $roleModel = new ManagerRole();
            $roles = $roleModel->getRolesByPropertyId($propertyId);
            
            $roles = array_map(function($role) {                
                return array(
                    'id' => $role['role_id'],
                    'name' => $role['role_name']
                );
            }, $roles);

            $this->response($roles, 200);
        }
        
        $this->response(['error' => TRUE, 'message' => 'Missing property_id'], 400);        
    }  
    
    public function delete() {}
    
    public function post() {}    
}