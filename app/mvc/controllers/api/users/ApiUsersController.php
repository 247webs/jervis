<?php

use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiUsersController extends RestController
{
    public function __construct()
    {
    }

    public function get($propertyId = null)
    {
        parent::__construct();

        if ($propertyId == 'me') {
            $managerModel = new Manager();
            $manager = $managerModel->getManagerById($this->userId);

            $user['id'] = $manager['id'];
            $user['first_name'] = $manager['first_name'];
            $user['last_name'] = $manager['last_name'];
            $user['country'] = $manager['country'];
            $user['state'] = $manager['state'];
            $user['city'] = $manager['city'];
            $user['zipcode'] = $manager['zipcode'];
            $user['email'] = $manager['email'];
            $user['mobile'] = $manager['mobile'];

            $profile = json_decode($manager['profile'], true);

            $role = json_decode($manager['role'], true);
            $role = array_keys($role, true);

            $user['companyName'] = $profile['company']['name'];

            $user['role'] = $role[0];

            $this->response($user, 200);
        } elseif (v::numericVal()->validate($propertyId)) {
            $propertyModel = new ManagerProperty();
            $property = $propertyModel->getPropertyById($propertyId);
            if (!$property) {
                $this->response(['error' => true, 'message' => 'Could not find property.'], 400);
            }

            if (!$this->currentUserHasAnyPermission($property, [])) {
                $this->response(['error' => true, 'message' => 'You have insufficient permissions to manage Users.'], 400);
            }

            $users = $propertyModel->getAccessByPropertyId($propertyId);

            foreach ($users as $key => $user) {
                unset($user['property_id']);
                unset($user['user_id']);
                $users[$key] = $user;
            }

            $this->response($users, 200);
        }
        $this->response(['error' => true, 'message' => 'Missing property_id'], 400);
    }

    public function delete($userId = null, $propertyId = null)
    {
        parent::__construct();

        if (v::numericVal()->validate($userId) && v::numericVal()->validate($propertyId)) {
            $propertyModel = new ManagerProperty();
            $property = $propertyModel->getPropertyById(intval($propertyId));
            if (!$property) {
                $this->response(['error' => true, 'message' => 'Property cannot be found.'], 400);
            }

            if ($this->currentUserHasPermission($property, 'Property.assignRole')) {
                // We can only expel if we are also doing inserts after, otherwise we'll remove all members.
                // Down the line we may want to just remove the ones that aren't in the payload and allow for
                // separation of `assignRole` and `expel` but it does not seem important.
                if ($this->currentUserHasPermission($property, 'Property.expel')) {
                    $propertyModel->deletePropertyPermissions($propertyId);

                    $this->response('User Property Permissions has been deleted successfully.', 200);
                } else {
                    $this->response(['error' => true, 'message' => 'You have insufficient permissions to manage property expel.'], 400);
                }
            } else {
                $this->response(['error' => true, 'message' => 'You have insufficient permissions to manage property assignRole.'], 400);
            }
        }

        $this->response(['error' => true, 'message' => 'Invalid userId or propertyId'], 400);
    }

    public function post($me = null, $method = null)
    {
        if (isset($me) && $me == 'me' && isset($method)) {
            parent::__construct();
            if ($method == 'personal') {
                $this->setUserPersonalRules($_POST);
                $managerProfileModel = new ManagerProfile();
                $managerProfileModel->updateProfile($_POST);
                $this->response('Profile Updated', 200);
            } elseif ($method == 'location') {
                $this->setUserLocationRules($_POST);
                $managerProfileModel = new ManagerProfile();
                $managerProfileModel->updateLocation($_POST);
                $this->response('Location Updated', 200);
            } elseif ($method == 'password') {
                $this->setUserPasswordRules($_POST);
                $managerProfileModel = new ManagerProfile();
                $managerProfileModel->updateUserPassword($_POST);
                $this->response('Password Updated', 200);
            } elseif ($method == 'role') {
                $this->setUserRoleRules($_POST);
                $managerProfileModel = new ManagerProfile();
                $managerProfileModel->updateUserRole($_POST);
                $this->response('Role Updated', 200);
            } else {
                $this->response('Not a valid method', 400);
            }
        }

        try {
            $this->setCreateUserRules($_POST);

            $email = (isset($_POST['email']) and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) ? $_POST['email'] : '';
            $signupModel = new Signup();
            if ($signupModel->userExists($email)) {
                $this->response(['error' => true, 'message' => 'User already exists'], 400);
            } else {
                $saveUserResult = $signupModel->saveUser($_POST);
                if ($saveUserResult['error'] === false) {
                    $this->response($saveUserResult['description'], 200);
                } else {
                    $this->response(
                        [
                        'error' => true,
                        'message' => $saveUserResult['description']
                        ],
                        400
                    );
                }
            }
        } catch (Exception $ex) {
        }
    }

    public function setCreateUserRules($data)
    {
        try {
            v::create()
                    ->key('firstName', v::notEmpty())
                    ->key('lastName', v::notEmpty())
                    ->key('country', v::notEmpty())
                    ->key('state', v::notEmpty())
                    ->key('city', v::notEmpty())
                    ->key('zipcode', v::notEmpty())
                    ->key('email', v::email())
                    ->key('mobile', v::digit('-'))
                    ->key('password', v::notEmpty())
                    ->key('role', v::containsAny(['Owner', 'Manager', 'Cleaning', 'Handyman']))
                    ->key('companyName', v::notEmpty())
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }

    public function setUserRoleRules($data)
    {
        try {
            v::create()
                    ->key('role', v::containsAny(['Owner', 'Manager', 'Cleaning', 'Handyman']))
                    ->key('companyName', v::notEmpty())
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }

    public function setUserPasswordRules($data)
    {
        try {
            v::create()
                    ->key('password', v::notEmpty())
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }

    public function setUserLocationRules($data)
    {
        try {
            v::create()
                    ->key('country', v::notEmpty())
                    ->key('state', v::notEmpty())
                    ->key('city', v::notEmpty())
                    ->key('zipcode', v::notEmpty())
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }

    public function setUserPersonalRules($data)
    {
        try {
            v::create()
                    ->key('first_name', v::notEmpty())
                    ->key('last_name', v::notEmpty())
                    ->key('email', v::email())
                    ->key('mobile', v::digit('-'))
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }
}
