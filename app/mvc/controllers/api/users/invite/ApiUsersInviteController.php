<?php

use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiUsersInviteController extends RestController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get() {}

    public function delete() {}

    public function post()
    {
        try {
            $this->setInviteUserRules($_POST);
            
            $propertyId = $_POST['propertyId'];
            $email = $_POST['email'];
            $roleId = $_POST['roleId'];
        
            $propertyModel = new ManagerProperty();
            $property = $propertyModel->getPropertyById($propertyId);
            if (!$property) {                
                $this->response(['error' => False, 'message' => 'Property cannot be found.'], 400);
            }
            
            if (!$this->currentUserHasPermission($property, 'Property.invite' )) {                
                $this->response(['error' => TRUE, 'message' => 'You have insufficient permissions to invite users to roles.'], 400);
            }
            
            $propertyModel->saveInvitedUser($email, $propertyId);
            
            $roleModel = new ManagerRole();
            $role = $roleModel->getRoleById($roleId);
            if (!$role) {
                $this->response(['error' => true, 'message' => "Could not find role with id {$roleId}"], 400);                
            }

            if ($role['property_id'] != $property['id']) {
                $this->response(['error' => true, 'message' => 'Cannot invite to a role that is not in this property.'], 400);
            }
            
            $token = $propertyModel->generateInviteToken($property['id'], $roleId, $email);
            
            // Generate secure link for invitation
            $url = URL.'/manager/property/accept-invitation?token='.$token;
            $message = 'You have been invited to the '.$role['role_name'].' role in '.$property['title'].'.';
            
            $userModel = new ManagerUser();
            $userModel->saveNotificationByEmail($email, $message, $url);
            
            $invitePerson = $propertyModel->getUsersByEmail($email);
            
            $modelAdminNotification = new AdminNotification();
            $emailTemplate = $modelAdminNotification->getTemplateByTitle('Property - Role Invitation');
            
            try {
                $output = [];
                $mail = Helpers::createEmail($output);
                $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
                $mail->addAddress($email);
                $mail->isHTML(true);                

                $subject = str_replace('{PROPERTY_NAME}', $property['title'], $emailTemplate['subject']);
                $body = str_replace('{USER_NAME}', $invitePerson['first_name'] . ' ' . $invitePerson['last_name'], $emailTemplate['body']);
                $body = str_replace('{ROLE_NAME}', $role['role_name'], $body);
                $body = str_replace('{PROPERTY_NAME}', $property['title'], $body);
                $body = str_replace('{INVITE_URL}', $url, $body);

                $mail->Subject = $subject;
                $mail->Body = $body;
                $mail->send();
                
                $this->response('Invite user to team successfully.', 200);
                
            } catch (Exception $e) {
                // Message could not be sent. Mailer Error: {$mail->ErrorInfo}                
                $this->response(['error' => true, 'message' => 'Failed to send email.'], 500);
            }

        } catch (Exception $ex) {
        }
    }

    public function setInviteUserRules($data)
    {
        try {
            v::create()
                    ->key('propertyId', v::digit())
                    ->key('email', v::email())
                    ->key('roleId', v::digit())
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }
}