<?php

use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiPropertiesController extends RestController
{
    public function __construct()
    {
        parent::__construct();
        $this->m = new ManagerProperty();
    }

    public function get($propertyId = null)
    {
        if (empty($propertyId)) {
            $properties = $this->m->getPropertiesRelevantForUser($this->userId);
            foreach ($properties as $key => $value) {
                unset($value['property_id']);
                unset($value['access']);
                $properties[$key] = $value;
            }

            $this->response($properties, 200);
        } else {
            $property = $this->m->getPropertyById($propertyId);
            if (!$property) {
                $this->response(['error' => false, 'message' => 'Property cannot be found.'], 400);
            }

            if (!$this->currentUserHasAnyPermission($property, [])) {
                $this->response(['error' => true, 'message' => 'You have insufficient permissions to manage property.'], 400);
            }
            unset($property['property_id']);
            unset($property['access']);

            $this->response($property, 200);
        }

        $this->response(['error' => true, 'message' => 'Missing property_id'], 400);
    }

    public function delete($propertyId = null)
    {
        if (v::numericVal()->validate($propertyId)) {
            $property = $this->m->getPropertyById($propertyId);
            if (!$property) {
                $this->response(['error' => true, 'message' => 'Could not find property.'], 400);
            }
            if ($property['owner_id'] != $this->userId) {
                $this->response(['error' => true, 'message' => 'Only the property owner can delete a property.'], 400);
            }

            $this->deleteProperty(intval($propertyId));

            $this->response('Property has been deleted successfully.', 200);
        }

        $this->response(['error' => true, 'message' => 'Missing property_id'], 400);
    }

    public function post($propertyId = null)
    {
        if ($propertyId) {
            if (v::numericVal()->validate($propertyId)) {
                $property = $this->m->getPropertyById($propertyId);
                if (!$property) {
                    $this->response(['error' => true, 'message' => 'Could not find property.'], 400);
                }

                $this->setCreatePropertyRules($_POST);

                $this->m->updatePropertyDetails($propertyId, $_POST);

                $this->response('Property has been update successfully.', 200);
            }
        } else {
            $this->setCreatePropertyRules($_POST);

            $this->m->createProperty($_POST, $this->userId);

            $this->response('Property has been created successfully.', 200);
        }
    }

    public function setCreatePropertyRules($data)
    {
        try {
            v::create()
                    ->key('title', v::notEmpty())
                    ->key('address', v::notEmpty())
                    ->key('type', v::containsAny(['Single Family', 'Townhouse', 'Apartment', 'Other']))
                    ->key('country', v::notEmpty())
                    ->key('state', v::notEmpty())
                    ->key('city', v::notEmpty())
                    ->key('zipcode', v::notEmpty())
                    ->key('property_square_footage', v::notEmpty())
                    ->key('no_of_bedroom', v::notEmpty())
                    ->key('no_of_bathroom', v::notEmpty())
                    ->key('reason_for_cleaning', v::notEmpty())
                    ->key('frequency_of_cleanings', v::notEmpty())
                    ->key('current_rental_cleaning_fee', v::notEmpty())
                    ->key('cleaning_estimate', v::notEmpty())
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }

    public function deleteProperty($propertyId)
    {
        /* Delete logo */
        $targetPath = getcwd().'/img/logos'.'/'.$propertyId.'_logo.png';
        if (file_exists($targetPath)) {
            unlink($targetPath);
        }

        $sql = 'DELETE FROM property WHERE id = :id AND owner_id = :manager_id';

        return (bool) $this->m->rowCount($sql, [
            ':id' => $propertyId,
            ':manager_id' => $this->userId,
        ]);
    }
}
