<?php

use Firebase\JWT\JWT;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class ApiOauthAccessTokenController extends RestController
{
    public function __construct()
    {
    }

    public function get($accessToken = null)
    {
        try {
            if ($accessToken) {
                try {
                    $key = DB_SALT;
                    $decoded = JWT::decode($accessToken, $key, array('HS256'));
                    if (isset($decoded->uuid) && $decoded->uuid != $_SERVER['HTTP_UUID']) {
                        $this->response(['error' => true, 'message' => 'Access token is not valid for this device'], 400);
                    }
                } catch (\Firebase\JWT\ExpiredException $ex) {
                    $this->response(['error' => true, 'message' => 'Expired token'], 400);
                }

                return $this->response(['error' => false, 'message' => 'Valid token', 'token' => $decoded], 200, true);
            }
        } catch (Exception $ex) {
            $this->response(['error' => true, 'message' => 'Invalid access_token'], 400);
        }
        $this->response(['error' => true, 'message' => 'Invalid access_token'], 400);
    }

    public function delete($id)
    {
    }

    public function post()
    {
        try {
            $this->setCreateAccessTokenRules($_POST);
            $email = (isset($_POST['email']) and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) ? $_POST['email'] : '';
            $password = $_POST['password'];
            $signupModel = new Signup();
            if ($user = $signupModel->userExistsByEmailAndPassword($email, $password)) {
                if ($user['status'] == 0) {
                    $this->response(['error' => true, 'message' => 'Your account is not verified, please check your email for verification email.'], 400);
                    exit;
                }
                $key = DB_SALT;
                $payload = array(
                    'email' => $_POST['email'],
                    'userId' => $user['id'],
                    'iat' => time(),
                    'exp' => time() + 86400 * 30 * 12,
                    'uuid' => $_SERVER['HTTP_UUID'],
                );
                $accessToken = JWT::encode($payload, $key);

                $this->response(['access_token' => $accessToken], 200);
            } else {
                $this->response(['error' => true, 'message' => 'Your username and password do not match. Please try again.'], 400);
            }
        } catch (Exception $ex) {
        }
    }

    public function setCreateAccessTokenRules($data)
    {
        try {
            v::create()
                    ->key('email', v::email())
                    ->key('password', v::notBlank())
                ->assert($data);
        } catch (NestedValidationException $exception) {
            $this->response(['error' => true, 'message' => $exception->getMessages()], 400);
        }
    }
}
