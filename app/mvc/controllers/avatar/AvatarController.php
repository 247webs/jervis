<?php

class AvatarController extends ViewController
{
    public function __construct()
    {
        $name = urldecode($_GET['name']);
        $initials = preg_replace('/[^a-zA-Z- ]/', '', $name);
        $str = preg_match_all('/\b\w/', $initials, $matches);
        $text = strtoupper(substr(implode('', $matches[0]), 0, 2));
        $path = getcwd();
        $font = $path.'/fonts/font-r.ttf';
        $im = imagecreatetruecolor(200, 200);
        $textColour = imagecolorallocate($im, 92, 192, 174);
        $size = 55;
        $box = imagettfbbox($size, 0, $font, $text);
        $text_width = abs($box[2]) - abs($box[0]);
        $text_height = abs($box[5]) - abs($box[3]);
        $image_width = imagesx($im);
        $image_height = imagesy($im);
        $x = ($image_width - $text_width) / 2;
        $y = ($image_height + $text_height) / 2;
        $bcolor = imagecolorallocate($im, 255, 255, 255);
        imagefilledrectangle($im, 0, 0, 200, 200, $bcolor);
        imagettftext($im, $size, 0, $x, $y, $textColour, $font, $text);
        header('Content-Type: image/png');
        imagepng($im);
        imagedestroy($im);
    }
}
