<?php

class ContactController extends ViewController
{
    public function index()
    {
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
            $name = $_POST['name'];
            $phone = $_POST['phone'];
            $message = $_POST['message'];
            $company = $_POST['company'];

            $mail = Helpers::createEmail();
            $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
            $mail->addAddress('info@jervis-systems.com');
            $mail->isHTML(true);
            $mail->Subject = 'Contact Form Submitted';
            $mail->Body = "$name has submitted contact form on Jervis System<br><br>Please find all the details as below<br><br>Name : $name<br>Phone : $phone<br>Email : $email<br>Message : $message <br><br>Yours, <br> The Jervis team";
            if ($mail->send()) {
                $response['description'] = 'Your message has been sent';
                $response['error'] = false;
            } else {
                $response['description'] = DEBUG == 1 ? $mail->ErrorInfo : 'Something went wrong';
                $response['error'] = true;
            }
        } else {
            $response['description'] = 'Please provide email address';
            $response['error'] = true;
        }
        //exit(json_encode($response));
        self::viewTwig('contact.html', []);
    }
}
