<?php

class ForgotPasswordController extends ViewController
{
    protected $m;

    public function __construct()
    {
        $this->m = self::model('ForgotPassword');
    }

    public function index()
    {
        $data['title'] = 'Jervis Systems - Manager Reset Password';
        self::viewTwig('forgotPassword/index.html', $data);
    }

    public function reset()
    {
        if (isset($_POST['token']) && isset($_POST['password']) && isset($_POST['confirmPassword'])) {
            if ($this->m->tokenExists($_POST['token'])) {
                $this->m->resetPasswordByToken($_POST['token'], $_POST['password']);
                $response['description'] = 'Your password has been reset successfully!';
                $response['error'] = false;
                exit(json_encode($response));
            } else {
                $response['description'] = 'Reset code expired!';
                $response['error'] = true;
                exit(json_encode($response));
            }
        } elseif (isset($_GET['token'])) {
            $token = $_GET['token'];
            $data['title'] = 'Jervis Systems - Manager Reset Password';
            $data['token'] = $token;
            self::viewTwig('forgotPassword/reset.html', $data);
        } else {
            $email = (isset($_POST['email']) and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) ? $_POST['email'] : '';

            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $data = array(
                    'secret' => '6Leqp_oUAAAAAIIMyGoxxRqpfNp3TKvsmYgtIR0Q',
                    'response' => $_POST['recaptcha_response'],
            );
            $options = array(
                    'http' => array(
                            'header' => "Content-Type: application/x-www-form-urlencoded\r\n",
                            'method' => 'POST',
                            'content' => http_build_query($data),
                    ),
            );
            $context = stream_context_create($options);
            $verify = file_get_contents($url, false, $context);
            $captcha_result = json_decode($verify);
            if (!(($captcha_result->success !== false) and ($captcha_result->score >= 0.7))) {
                $response['description'] = 'Please refresh your browser and try again.';
                $response['error'] = true;
                exit(json_encode($response));
            } elseif ($this->m->userExists($email)) {
                $token = $this->generateToken();
                $this->m->setToken($email, $token);
                $mail = Helpers::createEmail();
                $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
                $mail->addAddress($email);
                $mail->isHTML(true);

                $modelAdminNotification = new AdminNotification();
                $template = $modelAdminNotification->getTemplateByTitle('Password - Reset');

                $url = URL."/forgotPassword/reset?token=$token";
                $mail->Subject = $template['subject'];

                $body = str_replace('{USER_EMAIL}', $email, $template['body']);
                $body = str_replace('{RESET_PASSWORD_URL}', $url.' ', $body);

                $mail->Body = $body;
                $mail->send();

                $response['description'] = 'An email is on its way to you. Follow the instructions to reset your password.';
                $response['error'] = false;
                exit(json_encode($response));
            } else {
                $response['description'] = 'We could not find your email. Try another?';
                $response['error'] = true;
                exit(json_encode($response));
            }
        }
    }

    private function generateToken()
    {
        $token = random_bytes(32);

        return hash('sha256', $token);
    }
}
