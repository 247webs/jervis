<?php

class ManagerReview extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function getReviewerId()
    {
        $reviewerId = self::$mId;

        return  $reviewerId;
    }

    public function createReview(array $formData, $Ids)
    {
        $managerModel = new Manager();

        $manager = $managerModel->getManagerById($Ids['reviewerid']);

        return $this->insert('reviews', [
            'user_id' => $Ids['userid'],
            'reviewer_id' => $manager['id'],
            'name' => $manager['first_name'],
            'review_text' => $formData['feedback'],
            'rating' => $formData['rate'],
        ], );
    }

    public function getReviewsByPropertyId($propertyId)
    {
        $sql = 'SELECT r.name, r.review_text, r.rating,  m.first_name FROM `reviews` r
        LEFT JOIN manager m ON m.id = r.user_id
        WHERE `user_id` in (SELECT `user_id` FROM `property_access` WHERE `property_id` = :property_id)';

        return self::fetchAll($sql, [
            ':property_id' => $propertyId,
        ]);
    }
}
