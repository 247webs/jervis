<?php

class ManagerCalendar extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCalendarsByPropertyId($propertyId)
    {
        $sql = 'SELECT calendar.id as calendar_id, calendar.property_id as property_id, booking_source, url, color
                FROM calendar
                WHERE calendar.property_id = :property_id';

        return self::fetchAll($sql, [
            ':property_id' => $propertyId,
        ]);
    }

    public function createCalendar(array $formData)
    {
        $propertyId = $formData['propertyId'];

        $this->insert('calendar', [
            'property_id' => $propertyId,
            'booking_source' => $formData['bookingSource'],
            'url' => $formData['url'],
            'color' => $formData['color'],
        ]);
    }

    public function updateCalendar($calendarId, array $formData)
    {
        self::$db->beginTransaction();

        $this->update('calendar', [
            'property_id' => $formData['propertyId'],
            'booking_source' => $formData['bookingSource'],
            'url' => $formData['url'],
            'color' => $formData['color'],
        ], $calendarId, 'id');

        self::$db->commit();
    }

    public function getCalendarById($calendarId)
    {
        $sql = 'SELECT calendar.id as calendar_id, calendar.property_id as property_id, booking_source, url, color FROM calendar WHERE id = :id';

        return self::fetch($sql, [
            ':id' => $calendarId,
        ]);
    }

    public function deleteCalendar($calendarId)
    {
        return (bool) $this->rowCount('DELETE FROM calendar WHERE id = :id', [
            ':id' => $calendarId,
        ]);
    }
}
