<?php

class ManagerProfile extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function updateProfile(array $formData)
    {
        self::$db->beginTransaction();
        $this->update('manager', [
            'first_name' => $formData['first_name'],
            'last_name' => $formData['last_name'],
            'email' => $formData['email'],
            'mobile' => $formData['mobile'],
            'user_time_zone' => $formData['user_time_zone'],
        ], self::$mId, 'id');
        self::$db->commit();
    }

    public function updateUserRole(array $formData)
    {
        $managerModel = new Manager();
        $manager = $managerModel->getManagerById(self::$mId);
        $profiles = json_decode($manager['profile'], true);

        $profiles['company'] = [
            'name' => $formData['companyName'],
            'address' => $profiles['address'],
            'email' => $profiles['email'],
            'phone' => $profiles['phone'],
        ];

        self::$db->beginTransaction();
        $this->update('manager', [
            'role' => json_encode([$formData['role'] => true]),
            'profile' => json_encode($profiles),
        ], self::$mId, 'id');
        self::$db->commit();
    }

    public function updateUserPassword(array $formData)
    {
        self::$db->beginTransaction();
        $this->update('manager', [
            'password' => Helpers::createHash($formData['password']),
        ], self::$mId, 'id');
        self::$db->commit();
    }

    public function updateLocation(array $formData)
    {
        self::$db->beginTransaction();
        $this->update('manager', [
            'country' => $formData['country'],
            'state' => $formData['state'],
            'city' => $formData['city'],
            'zipcode' => $formData['zipcode'],
        ], self::$mId, 'id');
        self::$db->commit();
    }

    public function updatePassword(array $formData)
    {
        $response = array('success' => '', 'error' => '');
        try {
            $currentPassword = $formData['current_password'];
            $manager = $this->getManagerByPassword($currentPassword);
            if ($manager) {
                self::$db->beginTransaction();
                $this->update('manager', [
                    'password' => Helpers::createHash($formData['new_password']),
                ], self::$mId, 'id');
                self::$db->commit();

                $response['success'] = 'Your password have been changed successfully.';
            } else {
                $response['error'] = "Your current password does't match Please try again.";
            }
        } catch (Exception $ex) {
        }

        return $response;
    }

    public function getManagerByPassword($currentPassword)
    {
        $sql = 'SELECT * FROM manager WHERE id = :owner_id AND password = :password';

        return self::fetch($sql, [
            ':owner_id' => self::$mId,
            ':password' => Helpers::createHash($currentPassword),
        ]);
    }

    public function getSubscriptionsByOwnerId($ownerId)
    {
        $sql = 'SELECT * FROM subscriptions WHERE user_id = :owner_id';

        return self::fetch($sql, [
            ':owner_id' => $ownerId,
        ]);
    }

    public function getPlanBySubscriptions($subscriptions)
    {
        $sql = 'SELECT description FROM plans WHERE id = :id';

        return self::fetch($sql, [
            ':id' => $subscriptions['plan_id'],
        ]);
    }

    public function retrieveStripeCustomer($subscriptions)
    {
        $response = '';

        if (strlen($subscriptions['stripe_id']) > 0) {
            \Stripe\Stripe::setApiKey(SECRET_KEY);

            // Retrieve the customer and expand their default source
            $response = \Stripe\Customer::Retrieve(
                array('id' => $subscriptions['stripe_id'], 'expand' => array('default_source'))
            );
        }

        return $response;
    }

    public function updateCard(array $formData)
    {
        $response = array('success' => '', 'error' => '');
        try {
            $subscriptions = $this->getSubscriptionsByOwnerId(self::$mId);
            $stripeToken = $formData['stripeToken'];

            if ($stripeToken && strlen($subscriptions['stripe_id']) > 0) {
                \Stripe\Stripe::setApiKey(SECRET_KEY);

                try {
                    $cu = \Stripe\Customer::retrieve($subscriptions['stripe_id']); // stored in your application
                    $cu->source = $stripeToken; // obtained with Checkout
                    $cu->save();

                    $response['success'] = 'Your card details have been updated!';
                } catch (\Stripe\Error\Card $e) {
                    // Use the variable $error to save any errors
                    // To be displayed to the customer later in the page
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $response['error'] = $err['message'].' Please use another credit card or contact your bank.';
                }
                // Add additional error handling here as needed
            } else {
                $response['error'] = $ex->getMessage().' Please use another credit card or contact your bank.';
            }
        } catch (Exception $ex) {
        }

        return $response;
    }

    public function updateCompany(array $formData)
    {
        $managerModel = new Manager();
        $manager = $managerModel->getManagerById(self::$mId);
        $profiles = json_decode($manager['profile'], true);

        $profiles['company'] = [
            'name' => $formData['name'],
            'address' => $formData['address'],
            'email' => $formData['email'],
            'phone' => $formData['phone'],
        ];

        self::$db->beginTransaction();
        $this->update('manager', [
            'profile' => json_encode($profiles),
        ], self::$mId, 'id');
        self::$db->commit();
    }

    public function updateNotifications(array $formData)
    {
        $managerModel = new Manager();
        $manager = $managerModel->getManagerById(self::$mId);
        $profiles = json_decode($manager['profile'], true);

        $profiles['notifications'] = [
            'email' => ($formData['email'] == 'false' || $formData['email'] == '0') ? 0 : 1,
            'SMS' => ($formData['SMS'] == 'false' || $formData['SMS'] == '0') ? 0 : 1,
            'desktop' => ($formData['desktop'] == 'false' || $formData['desktop'] == '0') ? 0 : 1,
        ];

        self::$db->beginTransaction();
        $this->update('manager', [
            'profile' => json_encode($profiles),
        ], self::$mId, 'id');
        self::$db->commit();
    }

    public function getBillingPlans()
    {
        return self::fetchAll('SELECT * FROM `plans`');
    }

    public function getPlansById($planId)
    {
        $sql = 'SELECT * FROM plans WHERE id = :id';

        return self::fetch($sql, [
            ':id' => $planId,
        ]);
    }

    public function createStripeSubscription($plan, $manager, $stripeToken)
    {
        $response = array('success' => '', 'error' => '');
        try {
            \Stripe\Stripe::setApiKey(SECRET_KEY);

            $customer = \Stripe\Customer::create(array(
                        'email' => $manager['email'],
                        'source' => $stripeToken,
            ));

            if (isset($customer->id)) {
                /* Setting Stripe Subscription */
                $subscription = \Stripe\Subscription::create([
                            'customer' => $customer->id,
                            'items' => [
                                [
                                    'plan' => $plan['plan_id'],
                                ],
                            ],
                ]);

                /* create subscription */
                $this->createSubscription($customer->id, $subscription->id, $plan);

                $response['success'] = 'Your card details have been added!';
            }
        } catch (\Stripe\Error\Card $e) {
            // Use the variable $error to save any errors
            // To be displayed to the customer later in the page
            $body = $e->getJsonBody();
            $err = $body['error'];
            $response['error'] = $err['message'].' Please use another credit card or contact your bank.';
        }

        return $response;
    }

    public function createSubscription($customerId, $subscriptionId, $plan)
    {
        $ownerId = self::$mId;

        $this->insert('subscriptions', [
                    'user_id' => $ownerId,
                    'plan_id' => $plan['id'],
                    'subscription_id' => $subscriptionId,
                    'stripe_id' => $customerId,
                ]);
    }
}
