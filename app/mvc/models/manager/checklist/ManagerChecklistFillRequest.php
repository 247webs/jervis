<?php

class ManagerChecklistFillRequest extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function getRequestsByPropertyId($propertyId)
    {
        return $this->prepareRequests($this->fetchAll(
            'SELECT r.*, m.first_name, m.last_name, t.title template_title FROM `checklist_fill_request` r
            INNER JOIN manager m ON (m.id = r.user_id)
            INNER JOIN checklist_template t ON (t.id = r.template_id)
            WHERE `property_id` = :property_id',
            [':property_id' => $propertyId]
        ));
    }

    public function getRequestById($requestId)
    {
        return $this->prepareRequest($this->fetch('SELECT * FROM checklist_fill_request WHERE id = :request_id', [
            ':request_id' => $requestId,
        ]));
    }

    public function prepareRequests(array $requests)
    {
        return array_map([$this, 'prepareRequest'], $requests);
    }

    public function prepareRequest($request)
    {
        if (!$request) {
            return;
        }
        $dueDateSplit = explode('-', $request['due_date']);
        $request['due_date'] = mktime(0, 0, 0, $dueDateSplit[1], $dueDateSplit[2], $dueDateSplit[0]);
        $request['is_due'] = $request['due_date'] < time();

        return $request;
    }

    public function setFilled($requestId, $submissionId, $status)
    {
        $this->update(
            'checklist_fill_request',
            [
				'filled_id' => $submissionId,
				'status' => $status
			],
            $requestId
        );
    }
}
