<?php

class ManagerChecklistSubmission extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function getSubmissionsByPropertyId($propertyId)
    {
        $submissions = self::fetchAll(
            'SELECT chk.*, chk.id as checklist_id, WEEK(created, 3) as submit_week, tpl.code,r.id as request_id,r.due_date,tpl.title as template_title,r.status,r.priority
            FROM checklist chk
            INNER JOIN checklist_template tpl ON tpl.id = chk.template_id
            INNER JOIN checklist_fill_request r ON (r.template_id = chk.template_id)
            WHERE chk.property_id = :property_id
             ORDER BY created DESC',
            [':property_id' => $propertyId]
        );

        $submissions = array_map([$this, 'prepareSubmission'], $submissions);

        return $submissions;
    }

    public function getTasksByPropertyId($propertyId)
    {
        $submissions = self::fetchAll(
            'SELECT `property_id`, `data`, date(`created`) created FROM `checklist` where `property_id` = :property_id',
            [':property_id' => $propertyId]
        );

        $submissions = array_map([$this, 'prepareSubmission'], $submissions);

        return $submissions;
    }

    public function getSubmissionById($submissionId)
    {
        return $this->prepareSubmission(self::fetch(
            'SELECT chk.*, chk.id as checklist_id, WEEK(created, 3) as submit_week, tpl.code
             FROM checklist chk
             INNER JOIN checklist_template tpl ON tpl.id = chk.template_id
             WHERE chk.id = :id',
            [':id' => $submissionId]
        ));
    }

    protected function prepareSubmission($submission)
    {
        if (!$submission) {
            return $submission;
        }

        $submission['data'] = json_decode($submission['data'], true);

        return $submission;
    }

    public function deleteSubmission($submissionId)
    {
        return $this->delete('checklist', ['id' => $submissionId]);
    }

    public function fillSubmission(array $template, array $postParameters, $existingSubmission = null, $fillRequest = null, $notesCount)
    {
        if (isset($postParameters['save'])) {
            $status = 2;
        } else {
            if (isset($existingSubmission['submitter_id']) && $existingSubmission['submitter_id'] != self::$mId) {
                $status = 1;
            } else {
                $status = 3;
            }
        }
        // We make a full copy of the template groups because the template might change since the user submitted it.
        $uploadedAt = date('F j, Y H:i:s');
        $uploadedBy = Model::sessionGet('managerEmail');
        $filledGroups = $template['groups'];
        foreach ($template['groups'] as $groupIndex => $group) {
            $filledGroups[$groupIndex]['tasks'] = [];
            foreach ($group['tasks'] as $taskIndex => $task) {
                $fileDetails = [];
                if ($postParameters['fileName'][$groupIndex][$taskIndex] != '') {
                    $file_array = explode(',', $postParameters['fileName'][$groupIndex][$taskIndex]);
                    foreach ($file_array as $key => $value) {
                        $fileDetails[$key] = ['file' => $value, 'uploaded_by' => $uploadedBy, 'uploaded_at' => $uploadedAt];
                    }
                }
                $filledGroups[$groupIndex]['tasks'][] = [
                    'title' => $task,
                    'checked' => !empty($postParameters['group'][$groupIndex][$taskIndex]) && boolval($postParameters['group'][$groupIndex][$taskIndex]),
                    'note' => $postParameters['note'][$groupIndex][$taskIndex],
                    'files' => !empty($fileDetails) ? $fileDetails : '',
                ];
            }
        }

        $notes = !empty($postParameters['notes']) ? strval($postParameters['notes']) : '';
        $files = !empty($postParameters['commonFiles']) ? strval($postParameters['commonFiles']) : '';
        $filesDetails = [];

        if ($files != '') {
            $filesArray = explode(',', $files);
            foreach ($filesArray as $key => $value) {
                $filesDetails[$key] = ['file' => $value, 'uploaded_by' => $uploadedBy, 'uploaded_at' => $uploadedAt];
            }
        }

        $tableData = [
            'data' => json_encode($filledGroups),
            'notes' => $postParameters['notes'],
            'files' => !empty($filesDetails) ? json_encode($filesDetails) : '',
            'notecount' => $notesCount,
        ];

        if ($existingSubmission) {
            if (!isset($postParameters['save'])) {
                self::$db->beginTransaction();
                $this->update('checklist_fill_request', [
                    'status' => $status,
                ], $existingSubmission['id'], 'filled_id');
                self::$db->commit();
            }
            $tableData += [
                'modified' => Helpers::mysqlDatetime(time()),
            ];

            // Append task files
            $checkList = $this->getSubmissionById($existingSubmission['id']);
            foreach ($template['groups'] as $groupIndex => $group) {
                foreach ($group['tasks'] as $taskIndex => $task) {
                    $newTaskFiles = json_decode($tableData['data'], true);
                    $existingTaskFiles = $checkList['data'][$groupIndex]['tasks'][$taskIndex]['files'];
                    if (!empty($newTaskFiles[$groupIndex]['tasks'][$taskIndex]['files'])) {
                        if (!empty($checkList['data'][$groupIndex]['tasks'][$taskIndex]['files'])) {
                            for ($i = 0; $i < count($newTaskFiles[$groupIndex]['tasks'][$taskIndex]['files']); ++$i) {
                                array_push($existingTaskFiles, $newTaskFiles[$groupIndex]['tasks'][$taskIndex]['files'][$i]);
                            }
                            $newTaskFiles[$groupIndex]['tasks'][$taskIndex]['files'] = $existingTaskFiles;
                            $tableData['data'] = json_encode($newTaskFiles);
                        }
                    } else {
                        $newTaskFiles[$groupIndex]['tasks'][$taskIndex]['files'] = $checkList['data'][$groupIndex]['tasks'][$taskIndex]['files'];
                        $tableData['data'] = json_encode($newTaskFiles);
                    }
                }
            }

            // Append common task files
            $newCommonFiles = json_decode($tableData['files'], true);
            $existingCommonFiles = json_decode($checkList['files'], true);
            if (!empty($newCommonFiles)) {
                if (!empty($existingCommonFiles)) {
                    for ($i = 0; $i < count($newCommonFiles); ++$i) {
                        array_push($existingCommonFiles, $newCommonFiles[$i]);
                    }
                    $newCommonFiles = $existingCommonFiles;
                    $tableData['files'] = json_encode($newCommonFiles);
                }
            } else {
                $newCommonFiles = $checkList['files'];
                $tableData['files'] = $newCommonFiles;
            }

            return $this->update('checklist', $tableData, $existingSubmission['id']);
        } else {
            $tableData += [
                'property_id' => $template['property_id'],
                'template_id' => $template['id'],
                'created' => Helpers::mysqlDatetime(time()),
                'first_name' => Model::sessionGet('managerFirstName'),
                'last_name' => Model::sessionGet('managerLastName'),
                // TODO: Guests don't have an account but managers/handymen/cleaning do
                'submitter_id' => Model::sessionGet('managerId'),
            ];

            $succ = $this->insert('checklist', $tableData);

            $id = $this->lastInsertId();

            if ($fillRequest) {
                $fillRequestModel = new ManagerChecklistFillRequest();
                $fillRequestModel->setFilled($fillRequest['id'], $id, $status);
            }

            return $succ;
        }
    }

    public function createSubmissionFillRequest(array $template, array $user)
    {
        return $this->insert('checklist_fill_request', [
            'template_id' => $template['id'],
            'user_id' => $user['id'],
            'due_date' => $_POST['due_date'],
            'priority' => $_POST['priority'],
        ]);
    }

    public function fillSubmissionByApi(array $template, array $postParameters, $existingSubmission = null, $fillRequest = null, $notesCount, $manager)
    {
        $status = 1;

        // We make a full copy of the template groups because the template might change since the user submitted it.
        $filledGroups = $template['groups'];
        foreach ($template['groups'] as $groupIndex => $group) {
            $filledGroups[$groupIndex]['tasks'] = [];

            foreach ($group['tasks'] as $taskIndex => $task) {
                $filledGroups[$groupIndex]['tasks'][] = [
                    'title' => $task,
                    'checked' => !empty($postParameters['groups'][$groupIndex]['tasks'][$taskIndex]['value']) && boolval($postParameters['groups'][$groupIndex]['tasks'][$taskIndex]['value']),
                    'note' => $postParameters['groups'][$groupIndex]['tasks'][$taskIndex]['note'],
                ];
            }
        }

        $tableData = [
            'data' => json_encode($filledGroups),
            'notes' => isset($postParameters['notes']) ? $postParameters['notes'] : '',
            'notecount' => $notesCount,
            'property_id' => $template['property_id'],
            'template_id' => $template['id'],
            'created' => Helpers::mysqlDatetime(time()),
            'first_name' => $manager['first_name'],
            'last_name' => $manager['last_name'],
            // TODO: Guests don't have an account but managers/handymen/cleaning do
            'submitter_id' => $manager['id'],
        ];

        $succ = $this->insert('checklist', $tableData);
        $id = $this->lastInsertId();
        if ($fillRequest) {
            $fillRequestModel = new ManagerChecklistFillRequest();
            $fillRequestModel->setFilled($fillRequest['id'], $id, $status);
        }

        return $succ;
    }

    public function updateChecklist(array $data, $submissionId)
    {
        return $this->update('checklist', $data, $submissionId);
    }
}
