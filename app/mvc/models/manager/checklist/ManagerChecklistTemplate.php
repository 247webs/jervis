<?php

class ManagerChecklistTemplate extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function deleteTemplate($templateId)
    {
        $sql = 'DELETE FROM checklist_template WHERE id = :id';

        return (bool) $this->rowCount($sql, [
            ':id' => $templateId,
        ]);
    }

    /**
     * Save or update template. Returns template ID on success, false on failure.
     *
     * @param int       Property ID
     * @param int       optional template ID, depending on update/insert
     * @param array     Values from a possible form, of the format:
     *  [
     *      'title':    string,
     *      'code':     string,
     *      'notes':    string,
     *      'groups':   Group[],
     *  ]
     *
     * Group is of the format:
     *  [
     *      'title':    string,
     *      'tasks':    string[],
     *  ]
     * @param array&    Array of errors
     *
     * @return int | null
     */
    public function saveTemplate($propertyId, $templateId, array $formValues, array &$outErrors = [])
    {
        list($errors, $template) = $this->getValidChecklistTemplateFromInput($formValues);
        if ($errors) {
            $outErrors = $errors;

            return false;
        }

        list('title' => $title, 'code' => $code, 'notes' => $notes, 'groups' => $groups, 'recurring' => $recurring, 'to_whome' => $to_whome, 'duration' => $duration, 'before_after' => $before_after, 'event' => $event, 'subject' => $subject) = $template;

        if (in_array($duration, ['Weekly', 'Monthly', 'Quarterly', 'Yearly'])) {
            $before_after = '';
            $event = '';
        }

        $tableValues = [
            'title' => $title,
            'code' => $code,
            'json' => json_encode([
                'notes' => $notes,
                'groups' => $groups,
            ]),
            'recurring' => ($recurring) ? 1 : 0,
            'to_whome' => $to_whome,
            'duration' => $duration,
            'before_after' => $before_after,
            'event' => $event,
            'subject' => $subject,
        ];

        if (!$templateId) {
            $tableValues['property_id'] = $propertyId;
            $affectedRows = self::insert('checklist_template', $tableValues);
            $tableValues['template_id'] = self::lastInsertId();

            if ($affectedRows) {
                return $tableValues;
            } else {
                $outErrors[] = 'No affected rows.';

                return false;
            }
        } else {
            $affectedRows = self::update(
                'checklist_template',
                $tableValues,
                $templateId,
                'id'
            );
            $tableValues['property_id'] = $propertyId;
            $tableValues['template_id'] = $templateId;

            return $tableValues;

            // if ($affectedRows) {
            //     return $tableValues;
            // } else {
            //     $outErrors[] = 'No affected rows.';

            //     return false;
            // }
        }
    }

    private function getValidChecklistTemplateFromInput(array $formValues)
    {
        $errors = [];
        // Expected keys
        $errors = array_merge($errors, $this->expectKeys($formValues, ['title', 'code', 'notes', 'groups', 'recurring', 'to_whome', 'duration', 'before_after', 'event', 'subject']));

        if ($errors) {
            return [$errors, null];
        }

        $this->verifyString($formValues, 'title', $errors, 1);
        //$this->verifyString($formValues, 'code', $errors, 1);
        $this->verifyString($formValues, 'notes', $errors);

        $this->verifyString($formValues, 'to_whome', $errors);
        $this->verifyString($formValues, 'duration', $errors);
        $this->verifyString($formValues, 'before_after', $errors);
        $this->verifyString($formValues, 'event', $errors);
        $this->verifyString($formValues, 'subject', $errors);

        if (!is_array($formValues['groups']) || count($formValues['groups']) == 0) {
            $errors[] = 'Missing groups';
        }

        if ($errors) {
            return [$errors, null];
        }

        foreach ($formValues['groups'] as &$group) {
            $errors = array_merge($errors, $this->expectKeys($group, ['title', 'tasks']));
            $this->verifyString($group, 'title', $errors, 1);

            if (!array_key_exists('tasks', $group) || !is_array($group['tasks'])) {
                $errors[] = "Groups must have a 'tasks' array";
                continue;
            }

            $taskIndices = array_keys($group['tasks']);

            foreach ($taskIndices as $index) {
                $this->verifyString($group['tasks'], $index, $errors, 1);
            }
        }

        if ($errors) {
            return [$errors, null];
        }

        return [null, $formValues];
    }

    private function verifyString(array &$arr, $key, array &$errors, $minLength = 0)
    {
        if (!array_key_exists($key, $arr)) {
            $errors[] = "Missing key '$key'";

            return;
        }
        if (!is_string($arr[$key])) {
            $errors[] = "Expected value at key '$key' to be string";

            return;
        }

        $arr[$key] = trim($arr[$key]);

        if (strlen($arr[$key]) < $minLength) {
            $errors[] = "Expected value at key '$key' to be longer than $minLength letters";
        }
    }

    private function expectKeys(array $input, array $expectedKeys)
    {
        $errors = [];
        $keys = array_keys($input);
        if ($unexpectedKeys = array_diff($keys, $expectedKeys)) {
            $errors[] = 'Unexpected array key(s) '.implode(', ', $unexpectedKeys);
        }
        if ($missingKeys = array_diff($expectedKeys, $keys)) {
            $errors[] = 'Missing array key(s) '.implode(', ', $missingKeys);
        }

        return $errors;
    }

    public function getTemplatesByPropertyId($propertyId)
    {
        $templates = self::fetchAll(
            'SELECT * FROM checklist_template WHERE property_id = :property_id',
            [':property_id' => $propertyId]
        );

        $templates = array_map([$this, 'prepareTemplate'], $templates);

        return $templates;
    }

    public function getTemplateById($templateId)
    {
        return $this->prepareTemplate(self::fetch(
            'SELECT * FROM checklist_template WHERE id = :id',
            [':id' => $templateId]
        ));
    }

    protected function prepareTemplate($template)
    {
        if (!$template) {
            return $template;
        }
        $decodedJson = $this->migrateOldFormat(json_decode($template['json'], JSON_OBJECT_AS_ARRAY));

        $template['groups'] = $decodedJson['groups'];
        $template['notes'] = $decodedJson['notes'];
        unset($template['json']);

        return $template;
    }

    protected function migrateOldFormat($json)
    {
        if (!array_key_exists('notes', $json) || !array_key_exists('groups', $json)) {
            $keys = array_keys($json);
            $groups = [];
            $notes = '';
            foreach ($keys as $key) {
                if ($key == 'NOTES') {
                    $notes = $json[$key]['description'];
                    continue;
                }
                if (is_array($json[$key])) {
                    $group = [
                        'title' => $key,
                        'tasks' => $json[$key],
                    ];
                    $groups[] = $group;
                }
            }
            if (!is_string($notes)) {
                $notes = '';
            }

            return ['groups' => $groups, 'notes' => $notes];
        }

        return $json;
    }
}
