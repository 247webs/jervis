<?php

class Manager extends Model
{
    public function logout()
    {
        self::sessionSet('managerLogged', false);
    }

    public function isLogged()
    {
        return self::sessionGet('managerLogged');
    }

    public function getManagerById($ownerId)
    {
        $sql = 'SELECT * FROM manager WHERE id = :owner_id';

        return self::fetch($sql, [
            ':owner_id' => $ownerId,
        ]);
    }

    public function getCompletedReservationsByPropertyId($propertyId)
    {
        $sql = "SELECT reservation.id as reservation_id, reservation.property_id as property_id, booking_source, source_reservation_id, check_in_date, check_out_date, property.title, property.address
                FROM reservation
                LEFT JOIN property ON property.id = reservation.property_id
                WHERE reservation.property_id = :property_id AND CONVERT(DATE_FORMAT(check_out_date,'%Y-%m-%d-%H:%i:00'),DATETIME) < CONVERT(DATE_FORMAT(now(),'%Y-%m-%d-%H:%i:00'),DATETIME)";

        return self::fetchAll($sql, [
            'property_id' => $propertyId,
        ]);
    }

    public function getManagerByEmail($email)
    {
        $sql = 'SELECT * FROM manager WHERE email = :email';

        return self::fetch($sql, [
            ':email' => $email,
        ]);
    }

    public function createManagerByGuest(array $formData, $password)
    {
        $this->insert('manager', [
            'first_name' => $formData['first_name'],
            'last_name' => $formData['last_name'],
            'country' => $formData['country'],
            'state' => $formData['state'],
            'city' => $formData['city'],
            'zipcode' => $formData['zipcode'],
            'email' => $formData['email'],
            'mobile' => $formData['phone'],
            'password' => Helpers::createHash($password),
            'role' => json_encode(['Guest' => true]),
            'status' => '1',
            'user_time_zone' => Helpers::getUserTimeZonebyIpApi(),
            'trial_end_date' => date('Y-m-d', strtotime('+' . TRIAL_DAY)),
        ]);

        return $this->lastInsertId();
    }

    public function getAllReservationsByPropertyId($propertyId)
    {
        $data = self::fetch("select
            (select count(*) complate from reservation where  CONVERT(DATE_FORMAT(check_out_date,'%Y-%m-%d-%H:%i:00'),DATETIME) < CONVERT(DATE_FORMAT(now(),'%Y-%m-%d-%H:%i:00'),DATETIME)) complate,
            (select count(*) upcoming from reservation where CONVERT(DATE_FORMAT(check_out_date,'%Y-%m-%d-%H:%i:00'),DATETIME) > CONVERT(DATE_FORMAT(now(),'%Y-%m-%d-%H:%i:00'),DATETIME)) upcoming,
            (select count(*) total from reservation) total
            from reservation where `property_id` = $propertyId");

        return $data;
    }

    public function getSalesavgChartByPropertyId($propertyId, $str)
    {
        if ($str == 'week') {
            $data = self::fetchAll("SELECT week, total
                    FROM
                    (
                    SELECT DAYOFMONTH(created) as week, count(1) total FROM `reservation` where `property_id` = '$propertyId' and year(created) = year(now()) GROUP BY 1
                    UNION
                    select 1 week, 0 total union select 2 week, 0 total UNION select 3 week, 0 total UNION select 4 week, 0 total UNION select 5 week, 0 total UNION select 6 week, 0 total UNION select 7 week, 0 total
                    ) a
                    group by 1 ORDER by week");

            return $data;
        }
        if ($str == 'month') {
            $data = self::fetchAll("SELECT month, total
            FROM
            (
            SELECT MONTH(created) as month, count(1) total FROM `reservation` where `property_id` = '$propertyId' and year(created) = year(now()) GROUP BY 1
            UNION
            select 1 month, 0 total union select 2 month, 0 total UNION select 3 month, 0 total
            UNION select 4 month, 0 total UNION select 5 month, 0 total UNION select 6 month, 0 total
            UNION select 7 month, 0 total UNION select 8 month, 0 total UNION select 9 month, 0 total
            UNION select 10 month, 0 total UNION select 11 month, 0 total UNION select 12 month, 0 total
            union select 13 month, 0 total UNION select 14 month, 0 total UNION select 15 month, 0 total
            UNION select 16 month, 0 total UNION select 17 month, 0 total UNION select 18 month, 0 total
            UNION select 19 month, 0 total UNION select 20 month, 0 total UNION select 21 month, 0 total
            UNION select 22 month, 0 total UNION select 23 month, 0 total UNION select 24 month, 0 total
            UNION select 25 month, 0 total UNION select 26 month, 0 total UNION select 27 month, 0 total
            UNION select 28 month, 0 total UNION select 29 month, 0 total UNION select 30 month, 0 total
            UNION select 31 month, 0 total
            ) a
            group by 1 ORDER by month");

            return $data;
        }
        if ($str == 'year') {
            $data = self::fetchAll("SELECT month, total
                FROM
                (
                SELECT MONTH(created) as month, count(1) total FROM `reservation` where `property_id` = '$propertyId' and year(created) = year(now()) GROUP BY 1
                UNION
                select 1 month, 0 total union select 2 month, 0 total UNION select 3 month, 0 total UNION select 4 month, 0 total UNION select 5 month, 0 total UNION select 6 month, 0 total UNION select 7 month, 0 total UNION select 8 month, 0 total UNION select 9 month, 0 total UNION select 10 month, 0 total UNION select 11 month, 0 total UNION select 12 month, 0 total
                ) a
                group by 1 ORDER by month");

            return $data;
        }
    }

    public function getUpcomingdatesliderData(array $formData)
    {
        $slideDetailData = self::fetchAll("SELECT DATE_FORMAT(check_in_date,'%h:%i') AS time , property.title
        FROM reservation
        LEFT JOIN property ON property.id = reservation.property_id
        WHERE reservation.property_id = :property_id AND DATE_FORMAT(check_in_date,'%Y-%m-%d') = :sliderData", [
            ':sliderData' => $formData['upcomingsliderDate'],
            ':property_id' => $formData['Propertyid'],
        ]);
        if (!empty($slideDetailData)) {
            foreach ($slideDetailData as $slideDetailDatas) {
                $stringData[] = "<tr>
            <td>$slideDetailDatas[time]</td>
            <td>$slideDetailDatas[title]</td>
            </tr>";
            }
        } else {
            $stringData = "<td colspan='2'>No Reservation Available</td>";
        }

        return $stringData;
    }

    public function getMonthlyReservationsByPropertyId($propertyId)
    {
        $montData = self::fetchAll("SELECT DATE_FORMAT(check_in_date,'%h:%i') AS time , property.title
        FROM reservation
        LEFT JOIN property ON property.id = reservation.property_id
        WHERE reservation.property_id = :property_id AND CONVERT(DATE_FORMAT(check_in_date,'%Y-%m-%d-%H:%i:00'),DATETIME) > CONVERT(DATE_FORMAT(now(),'%Y-%m-%d-%H:%i:00'),DATETIME) ORDER BY reservation.id DESC LIMIT 30", [
            ':property_id' => $propertyId,
        ]);

        foreach ($montData as $montDatas) {
            $stringData[] = "<tr>
        <td>$montDatas[time]</td>
        <td>$montDatas[title]</td>
        </tr>";
        }

        return $stringData;
    }

    public function getYearlyReservationsByPropertyId($propertyId)
    {
        $montData = self::fetchAll("SELECT DATE_FORMAT(check_in_date,'%h:%i') AS time , property.title
        FROM reservation
        LEFT JOIN property ON property.id = reservation.property_id
        WHERE reservation.property_id = :property_id AND CONVERT(DATE_FORMAT(check_in_date,'%Y-%m-%d-%H:%i:00'),DATETIME) > CONVERT(DATE_FORMAT(now(),'%Y-%m-%d-%H:%i:00'),DATETIME) ORDER BY reservation.id DESC LIMIT 365", [
            ':property_id' => $propertyId,
        ]);

        foreach ($montData as $montDatas) {
            $stringData[] = "<tr>
        <td>$montDatas[time]</td>
        <td>$montDatas[title]</td>
        </tr>";
        }

        return $stringData;
    }

    public function getUpcomingReservationsByPropertyId($propertyId)
    {
        $sql = "SELECT reservation.id as reservation_id, reservation.property_id as property_id, booking_source, source_reservation_id, check_in_date, DATE_FORMAT(check_in_date,'%h:%i') AS time , check_out_date, property.title, property.address
        FROM reservation
        LEFT JOIN property ON property.id = reservation.property_id
        WHERE reservation.property_id = :property_id AND CONVERT(DATE_FORMAT(check_in_date,'%Y-%m-%d-%H:%i:00'),DATETIME) > CONVERT(DATE_FORMAT(now(),'%Y-%m-%d-%H:%i:00'),DATETIME) ORDER BY reservation.id DESC LIMIT 7";

        return self::fetchAll($sql, [
            'property_id' => $propertyId,
        ]);
    }

    public function getTodayUpcomingReservationsByPropertyId($propertyId)
    {
        $sql = "SELECT reservation.id as reservation_id, reservation.property_id as property_id, booking_source, source_reservation_id, check_in_date, DATE_FORMAT(check_in_date,'%h:%i') AS time , check_out_date, property.title, property.address
        FROM reservation
        LEFT JOIN property ON property.id = reservation.property_id
        WHERE reservation.property_id = :property_id AND CONVERT(DATE_FORMAT(check_in_date,'%Y-%m-%d-%H:%i:00'),DATETIME) = CONVERT(DATE_FORMAT(now(),'%Y-%m-%d-%H:%i:00'),DATETIME)";

        return self::fetchAll($sql, [
            'property_id' => $propertyId,
        ]);
    }

    public function getUpcomingreservationSliderMenu()
    {
        $sql = 'SELECT DATE(DATE_ADD(Now(), INTERVAL -1 Day)) DDate, SUBSTR(DAYNAME(DATE_ADD(Now(), INTERVAL -1 Day)),1,3) Day, DAY(DATE_ADD(Now(), INTERVAL -1 Day)) DayNum, DAY(Now())currentDay
        Union All
        SELECT DATE(Now()), SUBSTR(DAYNAME(Now()),1,3), DAY(Now()),DAY(Now())currentDay
        Union All
        SELECT DATE(DATE_ADD(Now(), INTERVAL 1 Day)), SUBSTR(DAYNAME(DATE_ADD(Now(), INTERVAL 1 Day)),1,3), DAY(DATE_ADD(Now(), INTERVAL 1 Day)),DAY(Now())currentDay
        Union All
        SELECT DATE(DATE_ADD(Now(), INTERVAL 2 Day)), SUBSTR(DAYNAME(DATE_ADD(Now(), INTERVAL 2 Day)),1,3), DAY(DATE_ADD(Now(), INTERVAL 2 Day)),DAY(Now())currentDay
        Union All
        SELECT DATE(DATE_ADD(Now(), INTERVAL 3 Day)), SUBSTR(DAYNAME(DATE_ADD(Now(), INTERVAL 3 Day)),1,3), DAY(DATE_ADD(Now(), INTERVAL 3 Day)),DAY(Now())currentDay
        Union All
        SELECT DATE(DATE_ADD(Now(), INTERVAL 4 Day)), SUBSTR(DAYNAME(DATE_ADD(Now(), INTERVAL 4 Day)),1,3), DAY(DATE_ADD(Now(), INTERVAL 4 Day)),DAY(Now())currentDay
        Union All
        SELECT DATE(DATE_ADD(Now(), INTERVAL 5 Day)), SUBSTR(DAYNAME(DATE_ADD(Now(), INTERVAL 5 Day)),1,3), DAY(DATE_ADD(Now(), INTERVAL 5 Day)),DAY(Now())currentDay';

        return self::fetchAll($sql);
    }

    public function changeMfaStatus($secret, $status, $user)
    {
        $this->update('manager', ['google_auth_code' => $secret, 'mfa' => $status], $user);
        self::sessionSet('managerGoogleAuthCode', $secret);
        self::sessionSet('managerMfa', $status);
        return true;
    }
}
