<?php

class ManagerProperty extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function getPropertiesRelevantForCurrentUser()
    {
        return $this->getPropertiesRelevantForUser(self::$mId);
    }

    public function getPropertiesRelevantForUser($userId)
    {
        $sql = 'SELECT p.*, p.id AS property_id, a.access
                FROM property p
                LEFT JOIN property_access a ON (a.property_id = p.id AND a.user_id = :user_id_b)
                WHERE p.owner_id = :user_id_a OR a.access IS NOT NULL
                ORDER BY title ASC';

        $properties = self::fetchAll($sql, [
            ':user_id_a' => $userId,
            ':user_id_b' => $userId,
        ]);

        $properties = $this->prepareProperties($properties);

        $properties = array_filter($properties, function ($property) use ($userId) {
            return $this->propertyIsRelevant($property, $userId);
        });

        return $properties;
    }

    /**
     * Do whatever preparations are necessary to get a useful property. For now this just deserializes the JSON
     * `access` property if one is present.
     *
     * @param array     Array of properties
     *
     * @return array
     */
    public function prepareProperties(array $properties)
    {
        return array_map(function (array $property) {
            return $this->prepareProperty($property);
        }, $properties);
    }

    public function prepareProperty($property)
    {
        if ($property) {
            if (!empty($property['access'])) {
                $property['access'] = json_decode($property['access']);
            }
        }

        return $property;
    }

    /**
     * Determine roughly if property is relevant for user. The property is relevant if the user is owner, cleaner or
     * manager of the property.
     *
     * @param array     The property to determine is relevant
     * @param int       The user ID
     *
     * @return bool
     */
    public function propertyIsRelevant(array $property, $userId)
    {
        return $property['owner_id'] == $userId || array_filter(get_object_vars($property['access']));
    }

    public function getAccessByPropertyId($propertyId)
    {
        $sql = 'SELECT pa.property_id, m.id, m.first_name, m.last_name,m.country,m.state,m.city,m.zipcode,m.email,m.mobile, pa.*
                FROM property_access pa
                INNER JOIN manager m ON (pa.user_id = m.id)
                WHERE pa.property_id = :property_id
                ORDER BY m.first_name ASC, m.last_name';

        $items = self::fetchAll($sql, [$propertyId]);
        $items = array_map(function ($item) {
            $item['access'] = json_decode($item['access']);

            return $item;
        }, $items);

        return $items;
    }

    public function getAccessByPropertyIds(array $propertyIds)
    {
        if (!$propertyIds) {
            return [];
        }
        $q = implode(',', str_split(str_repeat('?', count($propertyIds))));
        $sql = "SELECT pa.property_id, pa.*, m.id, m.first_name, m.last_name,m.country,m.state,m.city,m.zipcode,m.email,m.mobile
                FROM property_access pa
                INNER JOIN manager m ON (pa.user_id = m.id)
                WHERE pa.property_id IN ($q)
                ORDER BY m.first_name ASC, m.last_name";

        $items = self::fetchAll($sql, $propertyIds);
        $items = array_map(function ($item) {
            $item['access'] = json_decode($item['access'], JSON_OBJECT_AS_ARRAY);

            return $item;
        }, $items);

        $grouped = [];
        foreach ($items as $item) {
            if (!array_key_exists($item['property_id'], $grouped)) {
                $grouped[$item['property_id']] = [];
            }
            $grouped[$item['property_id']][] = $item;
        }

        return $grouped;
    }

    public function setPropertyAccess($propertyId, $userId, stdClass $access = null)
    {
        return $this->insert('property_access', [
            'user_id' => $userId,
            'property_id' => $propertyId,
            // We want to ensure that 'access' is an associative array, so we do the '{}'
            'access' => $access ? json_encode($access) : '{}',
        ], 'REPLACE');
    }

    public function updatePropertyDetails($propertyId, array $formData)
    {
        $this->update('property', [
            'title' => $formData['title'],
            'address' => $formData['address'],
            'type' => $formData['type'],
            'country' => $formData['country'],
            'state' => $formData['state'],
            'city' => $formData['city'],
            'zipcode' => $formData['zipcode'],
            'property_square_footage' => $formData['property_square_footage'],
            'no_of_bedroom' => $formData['no_of_bedroom'],
            'no_of_bathroom' => $formData['no_of_bathroom'],
            'reason_for_cleaning' => $formData['reason_for_cleaning'],
            'frequency_of_cleanings' => $formData['frequency_of_cleanings'],
            'current_rental_cleaning_fee' => $formData['current_rental_cleaning_fee'],
            'cleaning_estimate' => $formData['cleaning_estimate'],
        ], $propertyId, 'id');

        /* Upload Logo */
        $this->uploadPropertyLogo($propertyId);
    }

    public function deletePropertyPermissions($propertyId)
    {
        self::query('DELETE FROM property_access WHERE property_id = :property_id', [
            ':property_id' => $propertyId,
        ]);
    }

    public function insertPropertyPermissions($propertyId, array $formData)
    {
        $access = json_decode($formData['access']);
        foreach ($access as $user) {
            $this->setPropertyAccess($propertyId, $user->user_id, $user->access);
        }
    }

    public function updateProperty($propertyId, array $formData)
    {
        self::$db->beginTransaction();
        $this->updatePropertyDetails($propertyId, $formData);
        $this->deletePropertyPermissions($propertyId);
        $this->insertPropertyPermissions($propertyId, $formData);
        self::$db->commit();

        /* Upload Logo */
        $this->uploadPropertyLogo($propertyId);
    }

    public function createProperty(array $formData, $ownerId = null)
    {
        if (empty($ownerId)) {
            $ownerId = self::$mId;
        }
        $this->insert('property', [
            'title' => $formData['title'],
            'address' => $formData['address'],
            'owner_id' => $ownerId,
            'type' => $formData['type'],
            'country' => $formData['country'],
            'state' => $formData['state'],
            'city' => $formData['city'],
            'zipcode' => $formData['zipcode'],
            'property_square_footage' => $formData['property_square_footage'],
            'no_of_bedroom' => $formData['no_of_bedroom'],
            'no_of_bathroom' => $formData['no_of_bathroom'],
            'reason_for_cleaning' => $formData['reason_for_cleaning'],
            'frequency_of_cleanings' => $formData['frequency_of_cleanings'],
            'current_rental_cleaning_fee' => $formData['current_rental_cleaning_fee'],
            'cleaning_estimate' => $formData['cleaning_estimate'],
        ]);

        $property_id = $this->lastInsertId();

        $sql = 'SELECT * FROM `metadata_role`';
        $roles = self::fetchAll($sql);

        foreach ($roles as $role) {
            $this->insert('role', [
                'role_name' => $role['role_name'],
                'property_id' => $property_id,
            ]);
            $role_id = $this->lastInsertId();

            self::query('INSERT INTO role_perm SELECT :role_id_1 AS role_id , perm_id, :property_id AS property_id FROM metadata_role_perm WHERE `role_id` = :role_id_2', [
                ':role_id_1' => $role_id,
                ':role_id_2' => $role['id'],
                ':property_id' => $property_id,
            ]);
        }

        $roles = self::fetch('SELECT id FROM `role` WHERE role_name = :role_name AND property_id = :property_id', [
            'role_name' => 'Property owner',
            'property_id' => $property_id,
            ]);
        $access = (object) [$roles['id'] => true];
        $this->setPropertyAccess($property_id, $ownerId, $access);

        /*create notification template */
        $this->createNotificationTemplate($property_id);

        /*create checklist template */
        $this->createChecklistTemplate($property_id);

        /* upload logo */
        $this->uploadPropertyLogo($property_id);
    }

    public function deleteProperty($propertyId)
    {
        /* Delete logo */
        $targetPath = getcwd().'/img/logos'.'/'.$propertyId.'_logo.png';
        if (file_exists($targetPath)) {
            unlink($targetPath);
        }

        return $this->delRow($propertyId);
    }

    private function delRow($id)
    {
        $sql = 'DELETE FROM property WHERE id = :id AND owner_id = :manager_id';

        return (bool) $this->rowCount($sql, [
            ':id' => $id,
            ':manager_id' => self::$mId,
        ]);
    }

    public function getPropertyById($propertyId)
    {
        $userId = self::$mId;
        $sql = 'SELECT p.*, p.id AS property_id, a.access
        FROM property p
        LEFT JOIN property_access a ON (a.property_id = p.id AND a.user_id = :user_id)
        WHERE p.id = :property_id
        ORDER BY title ASC';

        return $this->prepareProperty(self::fetch($sql, [
            ':user_id' => $userId,
            ':property_id' => $propertyId,
        ]));

        return $property;
    }

    public function generateInviteToken($propertyId, $roleId, $email, $time = null)
    {
        $time = $time ?? time();

        return sha1(DB_SALT.$propertyId.$time.'X'.$roleId.'X'.strtolower($email)).'_'.$time.'_'.$propertyId.'_'.$roleId;
    }

    public function isValidToken($token, $email, &$outPropertyId, &$outRoleId)
    {
        $outPropertyId = null;
        $splitToken = explode('_', $token);
        if (count($splitToken) != 4) {
            return false;
        }
        list($_, $time, $propertyId, $roleId) = $splitToken;

        if ($time < time() - INVITE_TOKEN_TIMEOUT) {
            return false;
        }

        $valid = $this->generateInviteToken($propertyId, $roleId, $email, $time) == $token;
        if ($valid) {
            $outPropertyId = $propertyId;
            $outRoleId = $roleId;
        }

        return $valid;
    }

    public function createNotificationTemplate($property_id)
    {
        $sql = 'SELECT * FROM `metadata_notification_template`';
        $notificationTemplates = self::fetchAll($sql);

        foreach ($notificationTemplates as $notification) {
            $this->insert('notification_template', [
                'property_id' => $property_id,
                'title' => $notification['title'],
                'duration' => $notification['duration'],
                'beforeAfter' => $notification['beforeAfter'],
                'event' => $notification['event'],
                'subject' => $notification['subject'],
                'content' => $notification['content'],
                'send_via' => $notification['send_via'],
            ]);
        }
    }

    public function createChecklistTemplate($property_id)
    {
        $sql = 'SELECT * FROM `metadata_checklist_template`';
        $checklistTemplates = self::fetchAll($sql);

        foreach ($checklistTemplates as $checklist) {
            $this->insert('checklist_template', [
                'property_id' => $property_id,
                'code' => $checklist['code'],
                'title' => $checklist['title'],
                'type' => $checklist['type'],
                'json' => $checklist['json'],
            ]);
        }
    }

    public function uploadPropertyLogo($property_id)
    {
        $dirPath = getcwd().'/img/logos';
        if (!file_exists($dirPath)) {
            mkdir($dirPath, 0755, true);
        }

        if (isset($_FILES['logo']) && $_FILES['logo']['error'] == 0) {
            $extension = 'png';
            $tempFile = $_FILES['logo']['tmp_name'];
            $fileName = $property_id.'_logo.'.$extension;
            $targetPath = getcwd().'/img/logos'.'/'.$fileName;
            /* Check if file already exists */
            if (file_exists($targetPath)) {
                unlink($targetPath);
            }
            move_uploaded_file($tempFile, $targetPath);

            $logo = '/img/logos/'.$fileName;
            $this->update('property', [
                'logo' => $logo,
            ], $property_id, 'id');
        }
    }

    public function sendEmailNotificationForPropertyLimit($properties, $manager)
    {
        $managerPlan = json_decode($manager['plan']);
        if (isset($managerPlan->property)) {
            $propertyCount = count($properties);
            $allowedProperty = $managerPlan->property;
            $percentage = ($propertyCount * 100) / $allowedProperty;

            $subject = 'Jervis plan user limit';
            $message = "<div><span style=\"font-family: Verdana;\">Dear Jervis Customer,</span></div><div><br></div><div><span style=\"font-family: Verdana;\">You are reaching your Jervis plan user limit.&nbsp; Details are below:</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Total allowed property: $allowedProperty</span></div><div><span style=\"font-family: Verdana;\">Property exists on Jervis: $propertyCount</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Please contact us if you have any questions to support@jervis-systems.com.</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Regards,</span></div><div><span style=\"font-family: Verdana;\">Jervis Management</span></div>";

            if ($percentage >= 50 && $percentage < 75) {
                Helpers::sendEmailNotification($manager['email'], $subject, $message);
            } elseif ($percentage >= 75 && $percentage < 90) {
                Helpers::sendEmailNotification($manager['email'], $subject, $message);
            } elseif ($percentage >= 90) {
                Helpers::sendEmailNotification($manager['email'], $subject, $message);
            }
        }
    }

    public function getUsersRelevantForCurrentUser()
    {
        $properties = $this->getPropertiesRelevantForCurrentUser();
        if (count($properties)) {
            $propertyIds = array_column($properties, 'property_id');

            $ids = implode(',', $propertyIds);

            return self::fetch("SELECT count(user_id) AS users FROM property_access WHERE property_id IN ($ids)");
        }

        return [];
    }

    public function getUsersByEmail($email)
    {
        $sql = 'SELECT first_name, last_name 
        FROM manager
        WHERE email = :email';

        return self::fetch($sql, [
            ':email' => $email,
        ]);
    }

    public function saveInvitedUser($email, $propertyId)
    {
        $userData = self::fetch('SELECT id,first_name,last_name FROM manager WHERE email = :email', [
            ':email' => $email,
        ]);
        if (isset($userData)) {
            return $this->insert('property_access_invited', [
                'user_id' => self::$mId,
                'property_id' => $propertyId,
                'first_name' => $userData['first_name'],
                'last_name' => $userData['last_name'],
                'email' => $email,
            ], );
        } else {
            return $this->insert('property_access_invited', [
                'user_id' => self::$mId,
                'property_id' => $propertyId,
                'email' => $email,
            ], );
        }
    }

    public function sendEmailNotificationForUserLimit($users, $manager)
    {
        $managerPlan = json_decode($manager['plan']);
        $totalUser = count($users);
        $allowedUser = $managerPlan->user;

        $subject = 'Jervis plan user limit';
        $message = "<div><span style=\"font-family: Verdana;\">Dear Jervis Customer,</span></div><div><br></div><div><span style=\"font-family: Verdana;\">You are reaching your Jervis plan user limit.&nbsp; Details are below:</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Total allowed user: $allowedUser</span></div><div><span style=\"font-family: Verdana;\">User exists on Jervis: $totalUser</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Please contact us if you have any questions to support@jervis-systems.com.</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Regards,</span></div><div><span style=\"font-family: Verdana;\">Jervis Management</span></div>";
        if (isset($managerPlan->user)) {
            $percentage = ($totalUser = count($users) * 100) / $allowedUser;
            if ($percentage >= 50 && $percentage < 75) {
                Helpers::sendEmailNotification($manager['email'], $subject, $message);
            } elseif ($percentage >= 75 && $percentage < 90) {
                Helpers::sendEmailNotification($manager['email'], $subject, $message);
            } elseif ($percentage >= 90) {
                Helpers::sendEmailNotification($manager['email'], $subject, $message);
            }
        }
    }

    public function getUserRoles($property_id, $user_id)
    {
        $roleAccess = array();
        $sql = 'SELECT `access` FROM `property_access` WHERE `user_id` = :user_id AND `property_id` = :property_id';

        $propertyAccesss = self::fetchAll($sql, [
            ':user_id' => $user_id,
            ':property_id' => $property_id,
        ]);

        foreach ($propertyAccesss as $propertyAccesssKey => $propertyAccesssKeyValue) {
            $decodedJson = json_decode($propertyAccesssKeyValue['access'], true);
            foreach ($decodedJson as $roleId => $access) {
                if ($access) {
                    $roleModel = new ManagerRole();
                    $role = $roleModel->getRoleById($roleId);
                    $roleAccess[$roleId] = $role['role_name'];
                }
            }
        }

        return $roleAccess;
    }
}
