<?php

class ManagerInvoice extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function getInvoiceById($invoiceId)
    {
        $sql = 'SELECT *
                FROM invoice
                WHERE id = :invoice_id';

        return self::fetch($sql, [
            ':invoice_id' => $invoiceId,
        ]);
    }

    public function getInvoicesByPropertyId($propertyId)
    {
        $sql = 'SELECT id, total
                FROM invoice
                WHERE invoice.property_id = :property_id';

        return self::fetchAll($sql, [
            ':property_id' => $propertyId,
        ]);
    }

    public function saveInvoice($propertyId, $formData)
    {
        $serviceName = json_encode($formData['name']);
        $servicePrice = json_encode($formData['amount']);
        $quantity = json_encode($formData['quantity']);
        $rate = json_encode($formData['rate']);
        $deductu_item_name = json_encode($formData['deductitemname']);
        $sign = json_encode($formData['per']);
        $deduct_amount =  json_encode($formData['deductamount']);
        $total_deduction = $formData['totaldeduction'];
        $total = $formData['total'];

        return $this->insert('invoice', [
            'user_id' => self::$mId,
            'property_id' => $propertyId,
            'total_amount' => $formData['subtotal'],
            'total_amount' => $formData['subtotal'],
            'quantity' => $quantity,
            'rate' => $rate,
            'service_name' => $serviceName,
            'service_price' => $servicePrice,
            'deductu_item_name' => $deductu_item_name,
            'sign' => $sign,
            'deduct_amount' => $deduct_amount,
            'total_deduction' => $total_deduction,
            'total' => $total,
        ], );
    }

    public function updateInvoiceData(array $formData, $userDetails)
    {
        $serviceName = json_encode($formData['name']);
        $servicePrice = json_encode($formData['amount']);
        $quantity = json_encode($formData['quantity']);
        $rate = json_encode($formData['rate']);
        if(isset($formData['deductitemname']))
        {
            $deductu_item_name = json_encode($formData['deductitemname']);
            $deduct_amount = json_encode($formData['deductamount']);
            $sign = json_encode($formData['per']);
        }
       else{
            $deductu_item_name = "";
            $deduct_amount = "";
            $sign = "";
       }

        self::$db->beginTransaction();
        $this->update('invoice', [
            'total_amount' => $formData['subtotal'],
            'total_amount' => $formData['subtotal'],
            'quantity' => $quantity,
            'rate' => $rate,
            'service_name' => $serviceName,
            'service_price' => $servicePrice,
            'deductu_item_name' => $deductu_item_name,
            'sign' => $sign,
            'deduct_amount' => $deduct_amount,
            'total_deduction' => $formData['totaldeduction'],
            'total' => $formData['total'],
        ], $formData['id'], 'id');
        self::$db->commit();
    }

    public function viewInvoice($invoiceId)
    {
        $sql = 'SELECT *
                FROM invoice
                WHERE id = :id';

        return self::fetch($sql, [
            ':id' => $invoiceId,
        ]);
    }

    public function deleteInvoice($invoiceId)
    {
        return $this->delete('invoice', ['id' => $invoiceId]);
    }

    public function getUserDetails()
    {
        $sql = 'SELECT id, first_name, last_name, city, state, country, zipcode, email, mobile
                FROM manager
                WHERE id = :id';

        return self::fetch($sql, [
            ':id' => self::$mId,
        ]);
    }

    public function getSendUserDetails($billedto)
    {
        $sql = 'SELECT id, first_name, last_name, city, state, country, zipcode, email, mobile
                FROM manager
                WHERE id = :id';

        return self::fetch($sql, [
            ':id' => $billedto,
        ]);
    }
}
