<?php

class ManagerPayment extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function updatePayment($result)
    {
        $this->update('manager', [
            'stripe_data' => $result,
        ], self::$mId, 'id');
    }

    public function updateLoginUrl($loginUrlData)
    {
        $this->update('manager', [
             'stripe_login_url' => $loginUrlData,
        ], self::$mId, 'id');
    }

    public function getStripeData()
    {
        $sql = 'SELECT	stripe_data
                FROM manager
                WHERE id = :id';

        return self::fetch($sql, [
            ':id' => self::$mId,
        ]);
    }

    public function getLoginUrl()
    {
        $sql = 'SELECT  stripe_login_url
                FROM manager
                WHERE id = :id';

        return self::fetch($sql, [
            ':id' => self::$mId,
        ]);
    }
}
