<?php

class ManagerReservation extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getReservationsByPropertyId($propertyId)
    {
        $sql = 'SELECT reservation.id as reservation_id, reservation.property_id as property_id, booking_source, source_reservation_id, check_in_date, check_out_date, guest_id
                FROM reservation
                WHERE reservation.property_id = :property_id';

        return self::fetchAll($sql, [
            ':property_id' => $propertyId,
        ]);
    }

    public function createReservation(array $formData)
    {
        $propertyId = $formData['propertyId'];

        $guestData = [
            'first_name' => $formData['firstName'],
            'last_name' => $formData['lastName'],
            'business_name' => $formData['businessName'],
            'email' => $formData['email'],
            'phone' => $formData['phone'],
            'address' => $formData['address'],
            'country' => $formData['country'],
            'state' => $formData['state'],
            'zipcode' => $formData['zipcode'],
            'city' => $formData['city'],
            'notes' => $formData['notes'],
        ];

        if ($formData['guestId']) {
            $guestId = $formData['guestId'];
            $this->update('guest', $guestData, $guestId, 'id');
        } else {
            $this->insert('guest', $guestData);
            $guestId = $this->lastInsertId();
        }

        $this->insert('reservation', [
            'property_id' => $propertyId,
            'guest_id' => $guestId,
            'booking_source' => $formData['bookingSource'],
            'source_reservation_id' => $formData['sourceReservationId'],
            'check_in_date' => date('Y-m-d H:i:s', strtotime($formData['checkInDate'])),
            'check_out_date' => date('Y-m-d H:i:s', strtotime($formData['checkOutDate'])),
            'no_of_guest' => intval($formData['noOfGuest']),
            'no_of_infants' => intval($formData['noOfInfants']),
            'no_of_toddlers' => intval($formData['noOfToddlers']),
            'code' => $this->generatePdfToken(),
        ]);
        $reservationId = $this->lastInsertId();

        /* Generate device passcode */

        $managerDeviceModel = new ManagerDevice();
        $devices = $managerDeviceModel->getDevicesByPropertyId($propertyId);

        foreach ($devices as $key => $device) {
            $this->insert('device_passcode', [
                'reservation_id' => $reservationId,
                'device_id' => $device['device_id'],
                'passcode' => Helpers::randomNumber(4),
                'created' => date('Y-m-d h:i:s'),
            ]);
        }

        /* Create notification */
        $managerNotificationModel = new ManagerNotification();
        $managerNotificationModel->createNotification($propertyId, $reservationId);
    }

    public function getReservationById($reservationId)
    {
        $sql = 'SELECT reservation.id as reservation_id, reservation.property_id as property_id, code, 
        guest_id, booking_source, source_reservation_id, DATE_FORMAT(check_in_date, "%Y-%m-%d %H:%i:%s") AS check_in_date, DATE_FORMAT(check_out_date, "%Y-%m-%d %H:%i:%s") AS check_out_date, no_of_guest, no_of_infants, no_of_toddlers, 
        first_name, last_name, business_name, email, phone, address, country, state, zipcode, city, notes, reservation.created AS reservation_date
        FROM reservation
        LEFT JOIN guest g ON g.id = reservation.guest_id
        WHERE reservation.id = :reservation_id';

        return self::fetch($sql, [
            ':reservation_id' => $reservationId,
        ]);
    }

    public function updateReservation($reservationId, array $formData)
    {
        self::$db->beginTransaction();

        $guest = self::fetch(
            'SELECT guest_id FROM reservation WHERE id = :reservation_id',
            [':reservation_id' => $reservationId]
        );

        $this->update('guest', [
            'first_name' => $formData['firstName'],
            'last_name' => $formData['lastName'],
            'business_name' => $formData['businessName'],
            'email' => $formData['email'],
            'phone' => $formData['phone'],
            'address' => $formData['address'],
            'country' => $formData['country'],
            'state' => $formData['state'],
            'zipcode' => $formData['zipcode'],
            'city' => $formData['city'],
            'notes' => $formData['notes'],
            'modified' => date('Y-m-d h:i:s'),
        ], $guest['guest_id'], 'id');

        $this->update('reservation', [
            'property_id' => $formData['propertyId'],
            'booking_source' => $formData['bookingSource'],
            'source_reservation_id' => $formData['sourceReservationId'],
            'check_in_date' => date('Y-m-d H:i:s', strtotime($formData['checkInDate'])),
            'check_out_date' => date('Y-m-d H:i:s', strtotime($formData['checkOutDate'])),
            'no_of_guest' => $formData['noOfGuest'],
            'no_of_infants' => $formData['noOfInfants'],
            'no_of_toddlers' => $formData['noOfToddlers'],
            'modified' => date('Y-m-d h:i:s'),
        ], $reservationId, 'id');

        self::$db->commit();
        $propertyId = $formData['propertyId'];
        $managerNotificationModel = new ManagerNotification();
        $managerNotificationModel->editNotification($propertyId, $reservationId);
    }

    public function deleteReservation($reservationId)
    {
        return $this->delRow($reservationId);
    }

    private function delRow($id)
    {
        $sql = 'DELETE FROM reservation WHERE id = :id';

        return (bool) $this->rowCount($sql, [
            ':id' => $id,
        ]);
    }

    public function getGuestById($guestId)
    {
        $sql = 'SELECT * FROM guest WHERE id = :id';

        return self::fetch($sql, [
            ':id' => $guestId,
        ]);
    }

    public function getGuestByEmail($email)
    {
        if (strlen(trim($email)) > 0) {
            $sql = 'SELECT * FROM guest WHERE email = :email';

            return self::fetch($sql, [
                ':email' => $email,
            ]);
        }

        return false;
    }

    public function getReservationsByguestId($guestId)
    {
        $sql = 'SELECT * 
                FROM reservation 
                LEFT JOIN property ON property.id = reservation.property_id
                WHERE guest_id = :guest_id';

        return self::fetchAll($sql, [
            ':guest_id' => $guestId,
        ]);
    }

    public function getGuestByPhone($phone)
    {
        if (strlen(trim($phone)) > 0) {
            $sql = 'SELECT * FROM guest WHERE phone = :phone';

            return self::fetch($sql, [
                ':phone' => $phone,
            ]);
        }

        return false;
    }

    private function generatePdfToken()
    {
        $token = random_bytes(32);

        return hash('sha256', $token);
    }

    public function getPropertyById($propertyId)
    {
        return self::fetch('SELECT * FROM property WHERE id = :property_id', [
            ':property_id' => $propertyId,
        ]);
    }

    public function getReservationByCode($code)
    {
        $sql = 'SELECT * FROM reservation WHERE code = :code';

        return self::fetch($sql, [
            ':code' => $code,
        ]);
    }

    public function getReservationsRelevantForCurrentUser()
    {
        $propertyModel = new ManagerProperty();
        $properties = $propertyModel->getPropertiesRelevantForCurrentUser();
        if (count($properties)) {
            $propertyIds = array_column($properties, 'property_id');

            $ids = implode(',', $propertyIds);
            $sql = "SELECT
                (SELECT count(id) FROM reservation WHERE property_id IN ($ids) AND  date(created) BETWEEN date(now()) - INTERVAL 1 MONTH AND date(now())) AS per_month,
                (SELECT count(id) FROM reservation WHERE property_id IN ($ids) AND  YEAR(created) = YEAR(CURRENT_DATE())) AS per_year";

            return self::fetch($sql);
        }

        return [];
    }

    public function getCalendarDataByPropertyId($propertyId)
    {
        $sql = 'SELECT calendar_data.*, calendar.color FROM calendar_data
                LEFT JOIN calendar ON calendar.property_id = calendar_data.property_id AND calendar.id = calendar_data.calendar_id
                WHERE calendar_data.property_id = :property_id
                AND ((start_date BETWEEN :start_date AND :end_date) OR (end_date BETWEEN :start_date2 AND :end_date2))';

        return self::fetchAll($sql, [
            ':property_id' => $propertyId,
            ':start_date' => $_GET['start'],
            ':end_date' => $_GET['end'],
            ':start_date2' => $_GET['start'],
            ':end_date2' => $_GET['end'],
        ]);
    }

    public function sendEmailNotificationForReservationLimit($reservations, $manager)
    {
        $managerPlan = json_decode($manager['plan']);

        if (count($reservations)) {
            if (isset($managerPlan->reservation) && isset($managerPlan->reservation->per_year)) {
                $percentage = ($reservations['per_year'] * 100) / $managerPlan->reservation->per_year;

                $allowedReservation = $managerPlan->reservation->per_year;
                $totalReservation = $reservations['per_year'];

                $subject = 'Jervis plan user limit';
                $message = "<div><span style=\"font-family: Verdana;\">Dear Jervis Customer,</span></div><div><br></div><div><span style=\"font-family: Verdana;\">You are reaching your Jervis plan user limit.&nbsp; Details are below:</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Total allowed reservation per year: $allowedReservation</span></div><div><span style=\"font-family: Verdana;\">Reservation exists on Jervis: $totalReservation</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Please contact us if you have any questions to support@jervis-systems.com.</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Regards,</span></div><div><span style=\"font-family: Verdana;\">Jervis Management</span></div>";

                if ($percentage >= 50 && $percentage < 75) {
                    Helpers::sendEmailNotification($manager['email'], $subject, $message);
                } elseif ($percentage >= 75 && $percentage < 90) {
                    Helpers::sendEmailNotification($manager['email'], $subject, $message);
                } elseif ($percentage >= 90) {
                    Helpers::sendEmailNotification($manager['email'], $subject, $message);
                }
            }
            if (isset($managerPlan->reservation) && isset($managerPlan->reservation->per_month)) {
                $percentage = ($reservations['per_month'] * 100) / $managerPlan->reservation->per_month;

                $allowedReservation = $managerPlan->reservation->per_month;
                $totalReservation = $reservations['per_month'];

                $subject = 'Jervis plan user limit';
                $message = "<div><span style=\"font-family: Verdana;\">Dear Jervis Customer,</span></div><div><br></div><div><span style=\"font-family: Verdana;\">You are reaching your Jervis plan user limit.&nbsp; Details are below:</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Total allowed reservation per month: $allowedReservation</span></div><div><span style=\"font-family: Verdana;\">Reservation exists on Jervis: $totalReservation</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Please contact us if you have any questions to support@jervis-systems.com.</span></div><div><br></div><div><span style=\"font-family: Verdana;\">Regards,</span></div><div><span style=\"font-family: Verdana;\">Jervis Management</span></div>";

                if ($percentage >= 50 && $percentage < 75) {
                    Helpers::sendEmailNotification($manager['email'], $subject, $message);
                } elseif ($percentage >= 75 && $percentage < 90) {
                    Helpers::sendEmailNotification($manager['email'], $subject, $message);
                } elseif ($percentage >= 90) {
                    Helpers::sendEmailNotification($manager['email'], $subject, $message);
                }
            }
        }
    }

    public function deleteReservationNotification($reservationId, $title)
    {
        return $this->delete('notification', ['reservation_id' => $reservationId, 'title' => $title]);
    }
}
