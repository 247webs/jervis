<?php

class ManagerSchedule extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function getScheduleByUserId()
    {
        $sql = 'SELECT 	working_hours, add_break, service FROM schedule
        WHERE userid = :userid';

        return self::fetch($sql, [
            ':userid' => self::$mId,
        ]);
    }

    public function addService(array $formData)
    {
        $service = self::fetch('SELECT service FROM schedule WHERE  userid = :userid', [
            ':userid' => self::$mId,
        ]);
        if (empty($service['service'])) {
            $data[] = $formData['servicename'];
            $data = json_encode($data);
        } else {
            $serviceData = json_decode($service['service']);
            array_push($serviceData, $formData['servicename']);
            $data = json_encode($serviceData);
        }
        self::$db->beginTransaction();
        $this->update('schedule', [
            'service' => $data,
        ], self::$mId, 'userid');
        self::$db->commit();
    }

    public function editService(array $formData)
    {
        $service = self::fetch('SELECT service FROM schedule WHERE  userid = :userid', [
            ':userid' => self::$mId,
        ]);
        $retriveServices = json_decode($service['service']);
        $retriveServices[$formData['servicekey']] = $formData['servicename'];
        $serviceData = array_values($retriveServices);
        $serviceData = json_encode($serviceData);
        self::$db->beginTransaction();
        $this->update('schedule', [
            'service' => $serviceData,
        ], self::$mId, 'userid');
        self::$db->commit();
    }

    public function destroyService(array $formData)
    {
        $service = self::fetch('SELECT service FROM schedule WHERE  userid = :userid', [
            ':userid' => self::$mId,
        ]);
        $retriveServices = json_decode($service['service']);
        $key = array_search($formData['service'], $retriveServices);
        unset($retriveServices[$key]);
        $serviceData = array_values($retriveServices);
        $serviceData = json_encode($serviceData);
        self::$db->beginTransaction();
        $this->update('schedule', [
            'service' => $serviceData,
        ], self::$mId, 'userid');
        self::$db->commit();
    }

    public function getDayoffByUserId()
    {
        $sql = 'SELECT id, dayoff FROM dayoff 
        WHERE userid = :userid  order by id';

        return self::fetchAll($sql, [
            ':userid' => self::$mId,
        ]);
    }

    public function updateWorkingHours(array $formData)
    {
        $final_arr = array();
        for ($i = 0; $i <= 6; ++$i) {
            $arr = array();
            $arr['wrkdayname'] = $formData['wrkdayname'][$i];
            if (!empty($formData['status'][$i])) {
                $arr['status'] = $formData['status'][$i];
            } else {
                $arr['status'] = 'off';
            }
            $arr['starttime'] = $formData['starttime'][$i];
            $arr['endtime'] = $formData['endtime'][$i];

            $final_arr[] = $arr;
        }
        $wrkhrsdata = json_encode($final_arr);

        self::$db->beginTransaction();
        $this->update('schedule', [
            'working_hours' => $wrkhrsdata,
        ], self::$mId, 'userid');
        self::$db->commit();
    }

    public function updateAddbreak(array $formData)
    {
        foreach ($formData as $key => $data) {
            $addbreakdata[$key] = $data;
        }
        $addbreak = json_encode($addbreakdata);
        self::$db->beginTransaction();
        $this->update('schedule', [
            'add_break' => $addbreak,
        ], self::$mId, 'userid');
        self::$db->commit();
    }

    public function addDayOff(array $formData)
    {
        $dayoffdata = json_encode($formData);

        return $this->insert('dayoff', [
            'userid' => self::$mId,
            'dayoff' => $dayoffdata,
        ]);
    }

    public function deleteTimeOff($timeoffId)
    {
        return $this->delete('dayoff', ['id' => $timeoffId]);
    }

    public function getWorkingHours($userid)
    {
        $workinhours = self::fetchAll('SELECT working_hours FROM schedule WHERE userid = :userid', [
            ':userid' => $userid,
        ]);
        foreach ($workinhours as $key => $wrkdata) {
            foreach ($wrkdata as $data) {
                $workinhoursdata = json_decode($data);
            }
        }
        foreach ($workinhoursdata as $key => $wrkdaydata) {
            $starttime = $wrkdaydata->starttime;
            $endtime = $wrkdaydata->endtime;
            $time = 30;
            $array_of_time = $this->timeslotconverter($starttime, $endtime, $time);
            $workingHours[$wrkdaydata->wrkdayname] = $array_of_time;
            if ($wrkdaydata->status == 'off') {
                $wrktoggleoff = $wrkdaydata->wrkdayname;
                $workingHours['wrkoff'][] = $wrktoggleoff;
            }
        }

        return $workingHours;
    }

    public function getAddBreak($userid)
    {
        $addbreak = self::fetchAll('SELECT add_break FROM schedule WHERE userid = :userid', [
            ':userid' => $userid,
        ]);
        foreach ($addbreak as $key => $breakdata) {
            foreach ($breakdata as $data) {
                $addBreakData = json_decode($data);
            }
        }
        foreach ($addBreakData as $key => $breakdata) {
            foreach ($breakdata as $breakdatas) {
                $starttime = $breakdatas->start;
                $endtime = $breakdatas->end;
                $time = 1;
                $array_of_time = $this->timeslotconverter($starttime, $endtime, $time);
                $break[$key][] = $array_of_time;
            }
        }

        return $break;
    }

    public function getDayoff($userid)
    {
        $dayoff = self::fetchAll('SELECT dayoff FROM dayoff WHERE userid = :userid', [
            ':userid' => $userid,
        ]);
        if (!empty($dayoff)) {
            foreach ($dayoff as $key => $dayoffdata) {
                foreach ($dayoffdata as $dayoffdatas) {
                    $dayoffdetails[] = json_decode($dayoffdatas);
                }
            }

            foreach ($dayoffdetails as $daydataoff) {
                if (array_key_exists('startofftime', $daydataoff) == 'true') {
                    $filter_timeoffdata[] = $daydataoff;
                } else {
                    $filter_alldayoff[] = (array) $daydataoff;
                }
            }
            if (!empty($filter_timeoffdata)) {
                foreach ($filter_timeoffdata as $filter_timeoffdata_key => $filter_timeoffdata_data) {
                    $starttime = $filter_timeoffdata_data->startofftime;
                    $endtime = $filter_timeoffdata_data->endofftime;
                    $time = 30;
                    $array_of_time = $this->timeslotconverter($starttime, $endtime, $time);
                    $filter_timeoffdatas['timedata'][] = $array_of_time;
                    $filter_timeoffdatas['startdate'][] = $filter_timeoffdata_data->startdate;
                    $filter_timeoffdatas['enddate'][] = $filter_timeoffdata_data->enddate;
                    $filter_timeoffdatas['alldayoff'][] = '';
                }
            } else {
                $filter_timeoffdatas['alldayoff'][] = $filter_alldayoff;
            }
        } else {
            $filter_timeoffdatas = '';
        }

        return $filter_timeoffdatas;
    }

    public function getFilterData($workingHourData, $breakData)
    {
        unset($workingHourData['wrkoff']);
        foreach ($workingHourData as $workingHourkey => $workingHourDay) {
            foreach ($workingHourDay as $day => $daydetails) {
                foreach ($breakData as $dataBreak) {
                    foreach ($dataBreak as $break) {
                        if (isset($breakData[$workingHourkey])) {
                            $filter_data = array_diff($workingHourDay, $break);
                            $filter_data = array_values($filter_data);
                        } else {
                            $filter_data = $workingHourData[$workingHourkey];
                        }
                    }
                }
                $filterdata[$workingHourkey] = $filter_data;
            }
            $filter_data_all[$workingHourkey][$day] = $filter_data;
        }

        return $filter_data_all;
    }

    public function getTimeslotData(array $Data, $filter_data_all, $dayoff, $propertyId, $timezone)
    {
        $userdata = self::fetch('SELECT email,mobile FROM manager WHERE id = :id', [
            ':id' => $Data['userid'],
        ]);
        $companayName = self::fetch('SELECT title FROM property WHERE id = :id', [
            ':id' => $propertyId,
        ]);
        $todayday = $Data['todayday'];
        $date = $Data['date'];
        $userid = $Data['userid'];
        $myInt = $Data['myInt'];
        $year = $Data['year'];
        $datatimeslot = $filter_data_all[$todayday];
        $datatimeslot = array_values($datatimeslot);
        if (!empty($dayoff['startdate'])) {
            foreach ($dayoff['startdate'] as $key => $data) {
                $startdate = (strtotime($data) * 1000) - 19800000;
                $enddate = (strtotime($dayoff['enddate'][$key]) * 1000) - 19800000;
            }
            if ($myInt >= $startdate && $myInt <= $enddate) {
                foreach ($dayoff['timedata'] as $key => $data) {
                    foreach ($datatimeslot as $timeslotdata) {
                        $datatimeslot1[] = array_diff($timeslotdata, $dayoff['timedata'][$key]);
                        $datatimeslot = array_values($datatimeslot1);
                    }
                }
            }
        }

        $appoinmentData = self::fetchAll('SELECT date,time FROM appoinment_booking WHERE userid = :userid', [
            ':userid' => $Data['userid'],
        ]);
        foreach ($appoinmentData as $key => $dataappoinment) {
            $newdate = strtotime($dataappoinment['date']);
            $dateformat = date('n/j/Y', $newdate);
            $integerdate = (strtotime($dateformat) * 1000) - 19800000;
            if ($myInt == $integerdate) {
                $keyremove = array_search($dataappoinment['time'], $datatimeslot[$key]);
                unset($datatimeslot[$key][$keyremove]);
            }
        }

        foreach ($datatimeslot as $datatimeslots) {
            $datatimeslotAM = preg_grep('/(1[0-2]|0?[1-9]):[0-5][0-9] (AM)$/i', $datatimeslots);
            $datatimeslotPM = preg_grep('/(1[0-2]|0?[1-9]):[0-5][0-9] (PM)$/i', $datatimeslots);
        }
        if (!empty($datatimeslotPM)) {
            $stylepm = 'block';
        } else {
            $stylepm = 'none';
        }
        if (!empty($datatimeslotAM)) {
            $styleam = 'block';
        } else {
            $styleam = 'none';
        }

        $scheduleData = self::fetch('SELECT working_hours, add_break, service FROM schedule WHERE userid = :userid', [
            ':userid' => $Data['userid'],
        ]);
        $service = '';
        if (isset($scheduleData['service'])) {
            $service = json_decode($scheduleData['service'], true);
        }
        $propertyDetails = self::fetch('SELECT * FROM property WHERE id = :id', [
            ':id' => $propertyId,
        ]);
        $timeblogstr = '<center><label style="font-weight:10;"> Select a time slot to book an appoinment</label>
        <div><label class="slotdata">'.$todayday.'</label></diV>
        <label class="slotdate">'.$date.'</label> , <label class="slotyear">'.$year.'</label>
        <link href="/manager/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    	<link href="/manager/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
        <script>
        $(".slotborder").on("click", function() {
            var time = $(this).data("value");
            $(".selecttime").text(time); 
            $(".selecttime").val(time);
        });
		$("#propertytype [value='.$propertyDetails['type'].']").attr("selected", "true");
        var reason = "'.$propertyDetails['reason_for_cleaning'].'";
        $("#cleaningreason").val(reason);
        var frequency = "'.$propertyDetails['frequency_of_cleanings'].'";
        $("#cleaningfrequency").val(frequency);


        var country = "'.$propertyDetails['country'].'";
        var state = "'.$propertyDetails['state'].'";
        var city = "'.$propertyDetails['city'].'";
        </script>
        <div>
            <table>
            <tr>
            <td><label class="slotday" style="display:'.$styleam.';"';
        $timeblogstr .= '> Morning:</label> </td>';
        $timeblogstr .= '<td class="a">';
        foreach ($datatimeslotAM as $slottime) {
            $timeblogstr .= '<button type="button" class="slotborder btn btn-outline-brand btn-pill" style="margin:6px;" href="#" data-toggle="modal" data-target="#appoinmentbooking"  data-value="'.$slottime.'">'.$slottime.'</button>';
        }
        $timeblogstr .= '</td>
            </tr>
            <tr>
            <td><label class="slotday" style="display:'.$stylepm.';"';
        $timeblogstr .= '> Afternoon:</label> </td>';
        $timeblogstr .= '<td class="a">';
        foreach ($datatimeslotPM as $slottime) {
            $timeblogstr .= '<button type="button" class="slotborder btn btn-outline-brand btn-pill" href="#" data-toggle="modal" data-target="#appoinmentbooking" style="margin:6px;" data-value="'.$slottime.'">'.$slottime.'</button>';
        }
        $timeblogstr .= '</td>
            </tr>
            </table>
        </div></center>
        <div class="modal fade" id="appoinmentbooking" tabindex="-1" role="dialog" aria-labelledby="Appoinment Booking">
            <form method="POST" action="" id="notificationdata">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Appoinment Booking</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin:0px;"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                               <input type="hidden" name="propertyid" class="propertyid" value="'.$propertyId.'">
                               <input type="hidden" name="email" class="email" value="'.$userdata['email'].'">
                               <input type="hidden" name="userid"  value="'.$userid.'">
                               <input type="hidden" name="mobile" value="'.$userdata['mobile'].'">
                               <input type="hidden" name="companyname" value="'.$companayName['title'].'">
                               <input type="hidden" name="date" value="'.$date.', '.$year.'">
                               <input type="hidden" class="selecttime" name="time">
                               <div>Day:<label class="slotdata" style="margin:5px;">'.$todayday.'</label></div>
                               <div>Date:<label class="slotdate" style="margin:5px;">'.$date.'</label>, <label class="slotyear">'.$year.'</label></div>
                               <div>Time:<label class="selecttime" style="margin:5px;"> </label></div>
                                <div class="form-group">
                                    <label for="services"></label>
                                    <select class="form-control" name="service" class="services">';
        if (isset($service)) {
            foreach ($service as $services) {
                $timeblogstr .= '<option value="'.$services.'">'.$services.'</option>';
            }
        }

        $timeblogstr .= '</select>
                                </div>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input class="form-control" type="text" name="name" />
                                </div>
                                <div class="form-group">
                                    <label for="propertytitle">Property Title/Name</label>
                                    <input class="form-control"  type="text" value="'.$propertyDetails['title'].'"   name="propertytitle"/>
                                </div>
                                <div class="form-group">
                                    <label for="PropertyType">Property Type</label>
                                    <select class="form-control" id="propertytype" name="propertytype">
                                        <option value="Single Family">Single Family</option>
                                        <option value="Townhouse">Townhouse</option>
                                        <option value="Apartment">Apartment</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="PropertyType">TimeZone</label>
                                    <select name ="user_time_zone" id ="user_time_zone" class="form-control">
                                       '.$timezone.'
                                    </select>
                                </div>
                                <div><label for="PropertyType">General Loacation of Property</label></div>
                                <div class="form-group">
                                    <label for="propertyCountry">Country</label>
                                    <select class="form-control countries" name="country" id="countryId">
                                        <option value="">Select Country</option>
                                    </select>
                               </div>        
                                <div class="form-group">
                                    <label for="propertyState">State</label>
                                    <select class="form-control states" name="state"  id="stateId">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="propertyCity">City</label>
                                    <select class="form-control cities" name="city" id="cityId">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="propertyZipcode">Zipcode</label>
                                    <input class="form-control" type="text" value="'.$propertyDetails['zipcode'].'" name="zipcode" />
                                </div>
                                <div class="form-group">
                                    <label for="propertyBedroom">Property Square Footage</label>
                                    <input class="form-control" type="text" value="'.$propertyDetails['property_square_footage'].'" name="propertySqft"/>
                                </div>
                                <div class="form-group">
                                    <label for="bedroomsqft">Number of Bedrooms</label>
                                    <input class="form-control" type="text" value="'.$propertyDetails['no_of_bedroom'].'" name="bedroomsqft"/>
                                </div>
                                <div class="form-group">
                                <label for="bathroomsqft">Number of Bathrooms</label>
                                     <input class="form-control" type="text" value="'.$propertyDetails['no_of_bathroom'].'" name="bathroomsqft"/>
                                </div>
                                <div class="form-group">
                                    <label for="reason">Reason For Cleaning</label>
                                    <select class="form-control" id="cleaningreason" name="cleaningreason">
                                        <option value="Standard">Standard</option>
                                        <option value="Short-Term Rental Turnover">Short-Term Rental Turnover</option>
                                        <option value="Short-Term Rental Mid-Stay Cleaning">Short-Term Rental Mid-Stay Cleaning</option>
                                        <option value="Deep Clean">Deep Clean</option>
                                        <option value="Seasonal Cleaning">Seasonal Cleaning</option>
                                        <option value="Special Occasion/Party Cleaning">Special Occasion/Party Cleaning</option>
                                        <option value="Annual Cleaning">Annual Cleaning</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="frequency">Frequency of Cleanings</label>
                                    <select class="form-control" id="cleaningfrequency" name="cleaningfrequency">
                                        <option value="one-time">one-time</option>
                                        <option value="per booking reservation">per booking reservation</option>
                                        <option value="weekly">weekly</option>
                                        <option value="bi-monthly">bi-monthly</option>
                                        <option value="monthly">monthly</option>
                                        <option value="annually">annually</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="estimate">Cleaning Turnover Estimate (For Vacation Rentals)</label>
                                    <input class="form-control" type="text" name="estimate" value="'.$propertyDetails['cleaning_estimate'].'" data-container="body" data-toggle="kt-tooltip" data-skin="brand" data-placement="left" title data-original-title="If submitting this locally to cleaning companies in our marketplace, this estimation better helps to match you with a cleaning company with an hourly rate that fits your needs, based on the job that needs to be done. If unsure, feel free to leave blank. This estimate can be determined during your cleaning company consultation."/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="requestbtn btn btn-primary">
                                Send Request
                            </button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript" src="js/countrystatecity.js"></script>';

        return $timeblogstr;
    }

    public function timeslotconverter($starttime, $endtime, $time)
    {
        $array_of_time = array();
        $starttime = $starttime;
        $endtime = $endtime;
        $duration = $time; // split by 30 mins
        $array_of_time = array();
        $start_time = strtotime($starttime); //change to strtotime
        $end_time = strtotime($endtime); //change to strtotime
        $add_mins = $duration * 60;
        while ($start_time <= $end_time) { // loop between time
            $array_of_time[] = date('h:i A', $start_time);
            $start_time += $add_mins; // to check endtie=me
        }

        return $array_of_time;
    }

    public function addAppoinmentData(array $formData)
    {
        $selfData = self::fetch('SELECT email,mobile FROM manager WHERE id = :id', [
            ':id' => self::$mId,
        ]);
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405);
            echo '<h1>Method not allowed</h1>';
            echo 'Invite to Role requires the POST HTTP method.';
            exit;
        }
        $email = $formData['email'];
        try {
            $output = [];
            $mail = Helpers::createEmail($output);
            $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
            $mail->addAddress($email);
            $mail->isHTML(true);

            $modelAdminNotification = new AdminNotification();
            $emailTemplate = $modelAdminNotification->getTemplateByTitle('Appointment - request');

            $body = str_replace('{PROPERTY_NAME}', $formData['companyname'], $emailTemplate['body']);
            $body = str_replace('{APPOINTMENT_DATE}', $formData['date'], $body);
            $body = str_replace('{APPOINTMENT_TIME}', $formData['time'], $body);
            $body = str_replace('{SERVICE}', $formData['service'], $body);
            $body = str_replace('{NAME}', $formData['name'], $body);
            $body = str_replace('{PROPERTY_NAME}', $formData['propertytitle'], $body);
            $body = str_replace('{PROPERTY_TYPE}', $formData['propertytype'], $body);
            $body = str_replace('{CITY}', $formData['city'], $body);
            $body = str_replace('{STATE}', $formData['state'], $body);
            $body = str_replace('{COUNTRY}', $formData['country'], $body);
            $body = str_replace('{ZIPCODE}', $formData['zipcode'], $body);
            $body = str_replace('{TIMEZONE}', $formData['user_time_zone'], $body);
            $body = str_replace('{PROPERTY_SQFT}', $formData['propertySqft'], $body);
            $body = str_replace('{NO_OF_BEDROOMS}', $formData['bedroomsqft'], $body);
            $body = str_replace('{NO_OF_BATHROOMS}', $formData['bathroomsqft'], $body);
            $body = str_replace('{CLEANING_REASON}', $formData['cleaningreason'], $body);
            $body = str_replace('{CLEANING_FREQUENCY}', $formData['cleaningfrequency'], $body);
            $body = str_replace('{ESTIMATE}', $formData['estimate'], $body);

            $mail->Subject = str_replace('{PROPERTY_NAME}', $formData['companyname'], $emailTemplate['subject']);
            $mail->Body = $body;
            $mail->send();
        } catch (Exception $e) {
            // Message could not be sent. Mailer Error: {$mail->ErrorInfo}
            http_response_code(500);
            echo json_encode([
                    'error' => 'Failed to send email.',
                ]);
            exit;
        }

        return $this->insert('appoinment_booking', [
            'userid' => $formData['userid'],
            'property_id' => $formData['propertyid'],
            'comapny_name' => $formData['companyname'],
            'email' => $selfData['email'],
            'mobile' => $selfData['mobile'],
            'date' => $formData['date'],
            'time' => $formData['time'],
            'other_details' => json_encode($formData),
            'user_time_zone' => $formData['user_time_zone'],
        ], );
    }

    public function deleteAppoinment($appoinmentId)
    {
        return $this->delete('appoinment_booking', ['id' => $appoinmentId]);
    }

    public function getAppoinmentData($propertyId)
    {
        $sql = 'SELECT appoinment_booking.*, CONCAT(manager.first_name," ", manager.last_name) user_name, 0 as is_owner FROM appoinment_booking 
                LEFT JOIN manager ON manager.id = appoinment_booking.userid
                WHERE appoinment_booking.userid = :userid';

        $appoinment_booking = self::fetchAll($sql, [
            ':userid' => self::$mId,
        ]);

        if (empty($appoinment_booking)) {
            $sql = 'SELECT appoinment_booking.*, CONCAT(manager.first_name," ", manager.last_name) user_name, 1 as is_owner FROM appoinment_booking 
                    LEFT JOIN manager ON manager.id = appoinment_booking.userid
                    WHERE property_id = :property_id';

            $appoinment_booking = self::fetchAll($sql, [
                ':property_id' => $propertyId,
            ]);
        }

        return $appoinment_booking;
    }

    public function confirmAppoinment(array $formData)
    {
        $this->update('appoinment_booking', [
            'status' => '1',
        ], $formData['id'], 'id');

        $firstName = Model::sessionGet('managerFirstName');
        $lastName = Model::sessionGet('managerLastName');

        $email = $formData['email'];
        try {
            $output = [];
            $mail = Helpers::createEmail($output);
            $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
            $mail->addAddress($email);
            $mail->isHTML(true);

            $modelAdminNotification = new AdminNotification();
            $emailTemplate = $modelAdminNotification->getTemplateByTitle('Appointment - confirmed');

            $mail->Subject = $emailTemplate['subject'];
            $body = str_replace('{USER_NAME}', $firstName.' '.$lastName, $emailTemplate['body']);
            $body = str_replace('{APPOINTMENT_DATE}', $formData['date'], $body);
            $body = str_replace('{APPOINTMENT_TIME}', $formData['time'], $body);
            $mail->Body = $body;
            $mail->send();
        } catch (Exception $e) {
            // Message could not be sent. Mailer Error: {$mail->ErrorInfo}
            http_response_code(500);
            echo json_encode([
                'error' => 'Failed to send email.',
            ]);
            exit;
        }
    }
}
