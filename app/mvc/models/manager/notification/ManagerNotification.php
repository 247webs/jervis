<?php

class ManagerNotification extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function createNotification($propertyId, $reservationId)
    {
        $managerReservationModel = new ManagerReservation();

        $reservation = $managerReservationModel->getReservationById($reservationId);
        $email = $reservation['email'];
        $phone = $reservation['phone'];

        $property = $managerReservationModel->getPropertyById($propertyId);
        $ownerId = $property['owner_id'];

        $managerModel = new Manager();
        $manager = $managerModel->getManagerById($ownerId);
        $managerEmail = $manager['email'];

        $managerNotificationTemplateModel = new ManagerNotificationTemplate();
        $templates = $managerNotificationTemplateModel->getTemplatesByPropertyId($propertyId);

        if (count($templates)) {
            foreach ($templates as $template) {
                if (in_array($template['title'], ['Cleaning Request Email']) || in_array($template['title'], ['Cleaning Request SMS']) || in_array($template['title'], ['Cleaning Request Email - Reminder']) || in_array($template['title'], ['Cleaning Request SMS - Reminder'])) {
                    $managerRoleModel = new ManagerRole();
                    $user = $managerRoleModel->getCleaningCompanyUserIdByPropertyId($propertyId);

                    $manager = $managerModel->getManagerById($user['user_id']);
                    $email = $manager['email'];
                    $phone = $manager['mobile'];
                }

                $subject = NotificationHelpers::replaceEmailToken($template['subject'], $reservation, $manager, [], $property);
                $content = NotificationHelpers::replaceEmailToken($template['content'], $reservation, $manager, [], $property);

                if (!empty($template['beforeAfter']) && !empty($template['duration'])) {
                    if ($template['event'] == 'Check-in') {
                        $date = $reservation['check_in_date'];
                    } elseif ($template['event'] == 'Check-out') {
                        $date = $reservation['check_out_date'];
                    } else {
                        $date = $reservation['reservation_date'];
                    }

                    if ($template['beforeAfter'] == 'Before') {
                        $scheduleDate = date('Y-m-d H:i:s', strtotime('-'.$template['duration'], strtotime($date)));
                    } else {
                        $scheduleDate = date('Y-m-d H:i:s', strtotime('+'.$template['duration'], strtotime($date)));
                    }

                    $this->insert('notification', [
                        'reservation_id' => $reservationId,
                        'email' => $email,
                        'cc_email' => $managerEmail,
                        'phone' => $phone,
                        'schedule_date' => $scheduleDate,
                        'title' => $template['title'],
                        'subject' => $subject,
                        'content' => $content,
                        'send_via' => $template['send_via'],
                    ]);
                }
            }
        }
    }

    public function editNotification($propertyId, $reservationId)
    {
        $managerReservationModel = new ManagerReservation();

        $reservation = $managerReservationModel->getReservationById($reservationId);
        $email = $reservation['email'];
        $phone = $reservation['phone'];

        $property = $managerReservationModel->getPropertyById($propertyId);
        $ownerId = $property['owner_id'];

        $managerModel = new Manager();
        $manager = $managerModel->getManagerById($ownerId);
        $managerEmail = $manager['email'];

        $managerNotificationTemplateModel = new ManagerNotificationTemplate();
        $templates = $managerNotificationTemplateModel->getTemplatesByPropertyId($propertyId);
        if (count($templates)) {
            foreach ($templates as $template) {
                if (in_array($template['title'], ['Cleaning Request Email']) || in_array($template['title'], ['Cleaning Request SMS']) || in_array($template['title'], ['Cleaning Request Email - Reminder']) || in_array($template['title'], ['Cleaning Request SMS - Reminder'])) {
                    $managerRoleModel = new ManagerRole();
                    $user = $managerRoleModel->getCleaningCompanyUserIdByPropertyId($propertyId);

                    $manager = $managerModel->getManagerById($user['user_id']);
                    $email = $manager['email'];
                    $phone = $manager['mobile'];
                    $subject = NotificationHelpers::replaceEmailToken($template['subject'], $reservation, $manager, [], $property);
                    $content = NotificationHelpers::replaceEmailToken($template['content'], $reservation, $manager, [], $property);
    
                    if (!empty($template['beforeAfter']) && !empty($template['duration'])) {
                        if ($template['event'] == 'Check-in') {
                            $date = $reservation['check_in_date'];
                        } elseif ($template['event'] == 'Check-out') {
                            $date = $reservation['check_out_date'];
                        } else {
                            $date = $reservation['reservation_date'];
                        }
    
                        if ($template['beforeAfter'] == 'Before') {
                            $scheduleDate = date('Y-m-d H:i:s', strtotime('-'.$template['duration'], strtotime($date)));
                        } else {
                            $scheduleDate = date('Y-m-d H:i:s', strtotime('+'.$template['duration'], strtotime($date)));
                        }
    
                        $this->insert('notification', [
                            'reservation_id' => $reservationId,
                            'email' => $email,
                            'cc_email' => $managerEmail,
                            'phone' => $phone,
                            'schedule_date' => $scheduleDate,
                            'title' => $template['title'],
                            'subject' => $subject,
                            'content' => $content,
                            'send_via' => $template['send_via'],
                        ]);
                    }
                }
            }
        }

    }

    public function getNotificationByReservationId($reservationId)
    {
        $sql = 'SELECT * FROM notification WHERE reservation_id = :reservation_id';

        return self::fetchAll($sql, [
            ':reservation_id' => $reservationId,
        ]);
    }

    public function logNotification($reservationId, $email, $managerEmail, $phone, $scheduleDate, $title, $subject, $content, $sendVia, $status)
    {
        $this->insert('notification', [
            'reservation_id' => $reservationId,
            'email' => $email,
            'cc_email' => $managerEmail,
            'phone' => $phone,
            'schedule_date' => $scheduleDate,
            'title' => $title,
            'subject' => $subject,
            'content' => $content,
            'send_via' => $sendVia,
            'status' => $status,
        ]);
    }

    public function sendNotification($property, array $formValues)
    {
        $roleName = $formValues['to_whom'];

        $propertyId = $property['id'];
        $users = $this->getUser($propertyId, $roleName);

        $ownerId = $property['owner_id'];
        $managerModel = new Manager();
        $manager = $managerModel->getManagerById($ownerId);
        $managerEmail = $manager['email'];

        if ($users) {
            $sendVia = $formValues['send_via'];
            if ($sendVia == 'SMS') {
                $subject = '';
                $content = $formValues['sms_content'];
                if (!empty($users['mobile'])) {
                    Helpers::sendSMSNotification($content, '+1'.$users['mobile']);

                    $status = 'Sent';
                    $this->addCustomNotification($users, $propertyId, $managerEmail, $subject, $content, $sendVia, $status);
                }
            } else {
                $subject = $formValues['subject'];
                $content = $formValues['content'];
                if (!empty($users['email'])) {
                    $send = Helpers::sendEmailNotification($users['email'], $subject, $content);
                    $status = 'Failed';
                    if ($send) {
                        $status = 'Sent';
                    }
                    $this->addCustomNotification($users, $propertyId, $managerEmail, $subject, $content, $sendVia, $status);
                }
            }
        }

        return true;
    }

    protected function getUser($propertyId, $roleName)
    {
        $users = [];

        $propertyAccesss = self::fetchAll('SELECT * FROM `property_access` WHERE `property_id` = :property_id ', [
            ':property_id' => $propertyId,
        ]);

        foreach ($propertyAccesss as $propertyAccesssKey => $propertyAccesssKeyValue) {
            $decodedJson = json_decode($propertyAccesssKeyValue['access'], true);
            foreach ($decodedJson as $roleId => $access) {
                if ($access) {
                    $roleModel = new ManagerRole();
                    $role = $roleModel->getRoleById($roleId);
                    if ($role['role_name'] == $roleName) {
                        $users = self::fetch('SELECT * FROM `manager` WHERE `id` = :id ', [
                            ':id' => $propertyAccesssKeyValue['user_id'],
                        ]);
                        break;
                    }
                }
            }
        }

        return $users;
    }

    public function addCustomNotification($users, $propertyId, $managerEmail, $subject, $content, $sendVia, $status)
    {
        $this->insert('custom_notification', [
            'property_id' => $propertyId,
            'user_id' => $users[id],
            'email' => $users['email'],
            'cc_email' => $managerEmail,
            'phone' => $users['mobile'],
            'subject' => $subject,
            'content' => $content,
            'send_via' => $sendVia,
            'status' => $status,
        ]);
    }

    public function getCustomNotificationsByPropertyId($propertyId)
    {
        $sql = "SELECT custom_notification.id, custom_notification.status, manager.first_name as first_name, manager.last_name as last_name,  CASE WHEN send_via = 'Email' THEN custom_notification.email ELSE custom_notification.phone END send_via, CASE WHEN send_via = 'Email' THEN custom_notification.subject ELSE custom_notification.content END subject FROM custom_notification 
                LEFT JOIN manager ON manager.id = custom_notification.user_id
                WHERE property_id = :property_id";

        return self::fetchAll($sql, [
            ':property_id' => $propertyId,
        ]);
    }
}
