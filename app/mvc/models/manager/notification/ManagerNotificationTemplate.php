<?php

class ManagerNotificationTemplate extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function saveTemplate($propertyId, $templateId, array $formValues, array &$outErrors = [])
    {
        $tableValues = [
            'title' => $formValues['title'],
            'duration' => $formValues['duration'],
            'beforeAfter' => $formValues['beforeAfter'],
            'event' => $formValues['event'],
            'subject' => $formValues['subject'],
            'content' => $formValues['content'],
        ];

        if (isset($formValues['send_via'])) {
            $tableValues['send_via'] = $formValues['send_via'];
        }

        if (!$templateId) {
            $tableValues['property_id'] = $propertyId;
            $affectedRows = self::insert('notification_template', $tableValues);
            $tableValues['template_id'] = self::lastInsertId();

            if ($affectedRows) {
                return $tableValues;
            } else {
                $outErrors[] = 'No affected rows.';

                return false;
            }
        } else {
            $affectedRows = self::update(
                'notification_template',
                $tableValues,
                $templateId,
                'id'
            );
            $tableValues['property_id'] = $propertyId;
            $tableValues['template_id'] = $templateId;

            return $tableValues;
        }
    }

    public function getDurations()
    {
        $durations = array('1 min', '5 min', '15 min', '30 min', '45 min', '1 hour', '2 hour', '3 hour', '4 hour', '5 hour', '6 hour', '7 hour', '8 hour', '9 hour', '10 hour', '11 hour', '12 hour', '15 hour', '18 hour', '20 hour', '22 hour', '1 Day', '2 Day', '3 Day', '4 Day', '5 Day', '6 Day', '1 week');

        return $durations;
    }

    public function getTemplatesByPropertyId($propertyId)
    {
        $templates = self::fetchAll(
            'SELECT * FROM notification_template WHERE property_id = :property_id',
            [':property_id' => $propertyId]
        );

        return $templates;
    }

    public function getTemplateById($templateId)
    {
        return self::fetch(
            'SELECT * FROM notification_template WHERE id = :id',
            [':id' => $templateId]
        );
    }

    public function getTemplatesByPropertyIdWithTitle($propertyId, $title)
    {
        return self::fetch(
            'SELECT * FROM notification_template WHERE property_id = :property_id AND title = :title',
            [
                ':property_id' => $propertyId,
                ':title' => $title,
            ]
        );
    }

    public function deleteNotificationTemplate($id)
    {
        $sql = 'DELETE FROM notification_template WHERE id = :id';

        return (bool) $this->rowCount($sql, [
            ':id' => $id,
        ]);
    }
}
