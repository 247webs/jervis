<?php

class ManagerAgreement extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAgreementByCode($code)
    {
        $sql = 'SELECT * FROM reservation WHERE code = :code';

        return self::fetch($sql, [
            ':code' => $code,
        ]);
    }

    public function createAgreement($code, $output)
    {
        $reservation = $this->getAgreementByCode($code);
        if ($reservation) {
            $this->insert('agreement', [
                'property_id' => $reservation['property_id'],
                'guest_id' => $reservation['guest_id'],
                'reservation_id' => $reservation['id'],
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'document' => $output,
            ]);
        }
    }

    public function getDocumentByCode($code)
    {
        $reservation = $this->getAgreementByCode($code);
        if ($reservation) {
            return $this->getAgreementByReservationId($reservation['id']);
        }

        return false;
    }

    public function getAgreementByReservationId($reservationId)
    {
        $sql = 'SELECT * FROM agreement WHERE reservation_id = :reservation_id';

        return self::fetch($sql, [
            ':reservation_id' => $reservationId,
        ]);
    }
}
