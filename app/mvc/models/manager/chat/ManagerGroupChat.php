<?php

class ManagerGroupChat extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }
    
    public function getChatGroupById($id)
    {
        $sql = 'SELECT * FROM chat_group WHERE id = :id';
        return self::fetch($sql, [
            ':id' => $id,
        ]);
    }

    public function getGroupChatNotification()
    {
        return $sql = self::fetchAll('SELECT count(id) as countData, sender_id, group_id FROM chat_notification WHERE receiver_id = :id AND group_id != :group_id GROUP BY sender_id', [
            ':id' => self::$mId,
            ':group_id' => 0,
        ]);
    }

    public function getNonGroupPerson(array $formData)
    {
        $sql = self::fetch('SELECT * FROM chat_group WHERE id = :id', [
            ':id' => $formData['groupid'],
        ]);
        $userid = json_decode($sql['userid']);
        $user = implode(', ', $userid);
        $sql = self::fetchAll('SELECT manager.id, manager.first_name, manager.last_name  
                                FROM property_access 
                                INNER JOIN manager ON manager.id = property_access.user_id
                                    WHERE property_access.`property_id` in (SELECT `property_id` FROM `property_access` WHERE `user_id` = '.self::$mId.')
                                    AND manager.id   NOT IN('.$user.')');
        $nonUserGroup = '';
        foreach ($sql as $nonGroups) {
            $nonUserGroup .= '<div>
                <label>'.$nonGroups['first_name'].'  '.$nonGroups['last_name'].'</label>						
                <input type="checkbox" style="float:right" name="id[]" value="'.$nonGroups['id'].'">
                <input type="hidden" value="'.$formData['groupid'].'" name="groupid">
                <br>
            </div>
            <hr>';
        }

        return $nonUserGroup;
    }

    public function getGroupLeatestChatId(array $formData)
    {
        $from_user_id = self::$mId;
        $group_id = $formData['groupid'];

        return $sql = self::fetch('SELECT id FROM group_chat_message WHERE group_id = :group_id ORDER BY id DESC limit 1', [
            ':group_id' => $group_id,
        ]);
    }

    public function getGroupChatMessage(array $formData)
    {
        $from_user_id = self::$mId;
        $group_id = $formData['groupid'];
        $sql = self::fetchAll('SELECT * FROM group_chat_message WHERE group_id = :group_id ORDER BY timestamp ASC', [
            ':group_id' => $group_id,
        ]);

        $output = '<div class="kt-chat__messages groupchatdiv">';
        foreach ($sql as $sqlData) {
            $chat_id =$sqlData['id'];
            $status = $sqlData['status'];
            $chat_message = '';
            $edited = "";
            $tooltips = "";
            if ($sqlData['from_user_id'] == $from_user_id) {
                $chat_message = $sqlData['message'];
                $user = 'You';
                $class = 'chat-right';
                $chat_class = 'kt-bg-light-brand';
                $close = 'display:block';
                if ($status == 1) {
                    $chat_message = "<small><i class=\"fa fa-ban\" aria-hidden=\"true\"></i> You deleted this message</small>";
                } else {
                    if (strtotime(date('Y-m-d')) == strtotime((date('y-m-d', strtotime($sqlData['timestamp']))))) {
                        $tooltips = " data-toggle=\"tooltip\" title=\"<span class='edit_group_chat_message' id='$chat_id'>Edit</span>\"";
                    }
                }
            } else {
                $name = self::fetch('SELECT first_name,last_name FROM manager WHERE id = :id', [
                        ':id' => $sqlData['from_user_id'],
                    ]);
                $chat_message = $sqlData['message'];
                $user = $name['first_name'].' '.$name['last_name'];
                $class = 'chat-left';
                $chat_class = 'kt-bg-light-success';
                $close = 'display:none';
                if ($status == 1) {
                    $chat_message = "<small><i class=\"fa fa-ban\" aria-hidden=\"true\"></i> This message was deleted</small>";
                }
            }
            $chat_message = nl2br($chat_message);
            if ($status == 1) {
                $close = 'display:none';
                $tooltips = "";
            }
            if ($status == 2) {
                $edited = '<b>Edited</b>';
            }
            $timestamp = date('Y-m-d H:i', strtotime($sqlData['timestamp']));
            $output .= '
                <div class="chat__message '.$class.' text'.$chat_id.' chat__message" data-chatid = "'.$chat_id.'">
                <div class="chat-body">
                    <div class="chat-content edit_chat_message">
                        <p><strong>'.$user.'</strong></p>
                        <p class="chat_text">'.$chat_message.'</p>
                        <p class="chat-time text-right chat__datetime">'.$timestamp.' '. $edited.'</p>
                        <p> <i style="'.$close.'" class="fa flaticon-delete deletegroupchat" data-id="'.$chat_id.'"></i></p>
                    </div>
                </div>
            </div>';
        }

        return $output;
    }

    public function addgroup(array $formData)
    {
        array_push($formData['id'], self::$mId);
        foreach ($formData['id'] as $formDatas) {
            $userDetails = self::fetch('SELECT first_name,last_name FROM manager WHERE id = :id', [
                ':id' => $formDatas,
            ]);
            $username = $userDetails['first_name'].'  '.$userDetails['last_name'];
            $usernameData[] = $username;
        }
        $username = json_encode($usernameData);
        $group_id = json_encode($formData['id']);

        return $this->insert('chat_group', [
            'userid' => $group_id,
            'name' => $username,
        ]);
    }

    public function editgroup(array $formData)
    {
        foreach ($formData['id'] as $formDatas) {
            $userDetails = self::fetch('SELECT first_name,last_name FROM manager WHERE id = :id', [
                ':id' => $formDatas,
            ]);
            $username = $userDetails['first_name'].'  '.$userDetails['last_name'];
            $usernameData[] = $username;
        }
        $retriveGroup = self::fetch('SELECT userid, name FROM chat_group WHERE id = :id', [
            ':id' => $formData['groupid'],
        ]);
        $username = json_decode($retriveGroup['name']);
        $group_id = json_decode($retriveGroup['userid']);
        $names = array_merge($username, $usernameData);
        $groupid = array_merge($group_id, $formData['id']);
        $username = json_encode($names);
        $group_id = json_encode($groupid);

        self::$db->beginTransaction();
        $this->update('chat_group', [
          'userid' => $group_id,
          'name' => $username,
      ], $formData['groupid'], 'id');
        self::$db->commit();
    }

    public function getGroupDetails()
    {
        $sql = self::fetchAll('SELECT * FROM chat_group');
        foreach ($sql as $sqlData) {
            $data = json_decode($sqlData['userid']);
            if (in_array(self::$mId, (array) $data)) {
                $id[] = $sqlData['id'];
            }
        }
        if (!empty($id)) {
            foreach ($id as $ids) {
                $groupDetail = self::fetch('SELECT * FROM chat_group WHERE id = :id', [
                    ':id' => $ids,
                ]);
                $names = json_decode($groupDetail['name']);
                $groupDetails['id'] = $groupDetail['id'];
                $groupDetails['names'] = $names;
                $groupDetailsData[] = $groupDetails;
            }

            return $groupDetailsData;
        }
    }

    public function saveGroupChatNotification(array $formData)
    {
        $sql = self::fetch('SELECT userid FROM chat_group WHERE id = :id', [
            ':id' => $formData['groupid'],
        ]);
        $groupid = json_decode($sql['userid']);
        $key = array_search(self::$mId, $groupid);
        unset($groupid[$key]);
        foreach ($groupid as $groupids) {
            $this->insert('chat_notification', [
                'sender_id' => self::$mId,
                'receiver_id' => $groupids,
                'group_id' => $formData['groupid'],
            ]);
        }
    }

    public function deleteGroupChatNotification(array $formData)
    {
        $group_id = $formData['groupid'];

        return $this->delete('chat_notification', ['receiver_id' => self::$mId, 'group_id' => $group_id]);
    }

    public function deleteGroupChat(array $formData)
    {
        $id = $formData['id'];
        
        $this->createGroupChatMessageModified($formData);
        
        return $this->update('group_chat_message', [
            'status' => 1,
        ], $id, 'id');

        //return $this->delete('group_chat_message', ['id' => $id]);
    }
    
    public function updateGroupChatMessage(array $formData)
    {
        $id = $formData['id'];
        
        $this->createGroupChatMessageModified($formData);
        
        return $this->update('group_chat_message', [
            'message' => $formData['message'],
            'status' => 2,
        ], $id, 'id');
    }

    public function getRefreshGroupChatMessage(array $formData)
    {
        $output = [];
        $chat_id = $formData['leatest_chat_id'];
        $from_user_id = self::$mId;
        $group_id = $formData['groupid'];
        
        $chatData = self::fetch(
            'SELECT GROUP_CONCAT(group_chat_message_id) AS chat_ids FROM group_chat_message_modified WHERE group_id = :group_id AND to_user_id = :to_user_id',
            [
                    ':group_id' => $group_id,
                    ':to_user_id' => $from_user_id
                    ]
        );

        if (!empty($chatData['chat_ids'])) {
            $chat_ids = $chatData['chat_ids'];
            $result1 = self::fetchAll('SELECT * FROM group_chat_message WHERE id IN (' . $chat_ids . ')');
            $this->query('DELETE FROM group_chat_message_modified WHERE to_user_id = '.$from_user_id.' AND group_chat_message_id IN (' . $chat_ids . ')');
        }

        $result = self::fetchAll('SELECT * FROM group_chat_message WHERE group_id = :group_id AND id > :id ORDER BY id DESC limit 1', [
            ':group_id' => $group_id,
            ':id' => $chat_id,
        ]);
        
        if (isset($result1) && count($result1)) {
            $result = array_merge($result, $result1);
        }
         
        foreach ($result as $sqlData) {
            $status = $sqlData['status'];
            $chat_id = $sqlData['id'];
            $chat_message = $sqlData['message'];
            $edited = "";
            $tooltips = "";
            $delete = "";
            if ($sqlData['from_user_id'] == $from_user_id) {
                $user = 'You';
                $class = 'chat-right';
                $delete  = '<i style="display:block" class="fa flaticon-delete deletegroupchat" data-id="'.$chat_id.'"></i>';
                if ($status == 1) {
                    $chat_message = "<small><i class=\"fa fa-ban\" aria-hidden=\"true\"></i> You deleted this message</small>";
                } else {
                    if (strtotime(date('Y-m-d')) == strtotime((date('y-m-d', strtotime($sqlData['timestamp']))))) {
                        $tooltips = " data-toggle=\"tooltip\" title=\"<span class='edit_group_chat_message' id='$chat_id'>Edit</span>\"";
                    }
                }
            } else {
                $name = self::fetch('SELECT first_name,last_name FROM manager WHERE id = :id', [
                        ':id' => $sqlData['from_user_id'],
                    ]);
                $user = $name['first_name'].' '.$name['last_name'];
                $class = 'chat-left';
                if ($status == 1) {
                    $chat_message = "<small><i class=\"fa fa-ban\" aria-hidden=\"true\"></i> This message was deleted</small>";
                }
            }
            $chat_message = nl2br($chat_message);
            if ($status == 1) {
                $tooltips = "";
                $delete = "";
            }
            if ($status == 2) {
                $edited = '<b>Edited</b>';
            }
            $timestamp = date('Y-m-d H:i', strtotime($sqlData['timestamp']));
            $output[$chat_id] = [
                'status' => $status,
                'message' => $chat_message,
                'new_message' => '<div class="chat__message '.$class.' text'.$chat_id.' chat__message" data-chatid = "'.$chat_id.'">
                <div class="chat-body">
                    <div class="chat-content edit_chat_message">
                        <p><strong>'.$user.'</strong></p>
                        <p class="chat_text">'.$chat_message.'</p>
                        <p class="chat-time text-right chat__datetime">'.$timestamp.' '. $edited.'</p>
                    </div>
                </div>
            </div>'
            ];
        }

        return  $output;
    }
    
    public function createGroupChatMessageModified(array $formData)
    {
        $chatGroup = $this->getChatGroupById($formData['groupid']);
        $chatGroupIds = json_decode($chatGroup['userid']);
        foreach ($chatGroupIds as $userId) {
            if ($userId != self::$mId) {
                $this->insert('group_chat_message_modified', [
                    'group_chat_message_id' => $formData['id'],
                    'group_id' => $formData['groupid'],
                    'from_user_id' => self::$mId,
                    'to_user_id' => $userId
                ]);
            }
        }
    }
}
