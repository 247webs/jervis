<?php

class ManagerChat extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function getChatUsers()
    {
        $sql = 'SELECT manager.id, manager.first_name, manager.last_name  
                FROM property_access 
                INNER JOIN manager ON manager.id = property_access.user_id
                    WHERE property_access.`property_id` in (SELECT `property_id` FROM `property_access` WHERE `user_id` = :id1)
                    AND manager.id != :id';

        return self::fetchAll($sql, [
            ':id' => self::$mId,
            ':id1' => self::$mId,
        ]);
    }

    public function getChatNotification()
    {
        return $sql = self::fetchAll('SELECT count(id) as countData, sender_id FROM chat_notification WHERE receiver_id = :id  GROUP BY sender_id', [
            ':id' => self::$mId,
        ]);
    }

    public function getChatMessage(array $formData)
    {
        $from_user_id = self::$mId;
        $to_user_id = $formData['id'];
        $sql = self::fetchAll('SELECT * FROM chat_message WHERE (from_user_id = :from_user_id AND to_user_id = :to_user_id)
        OR (from_user_id = :to_user_id1 AND to_user_id = :from_user_id1) ORDER BY timestamp ASC', [
            ':from_user_id' => $from_user_id,
            ':to_user_id' => $to_user_id,
            ':from_user_id1' => $from_user_id,
            ':to_user_id1' => $to_user_id,
        ]);
        $output = '<div class="chat__message chatdiv">';
        foreach ($sql as $sqlData) {
            $chat_id = $sqlData['chat_message_id'];
            $status = $sqlData['status'];
            $chat_message = '';
            $edited = '';
            $tooltips = '';
            $delete = '';

            if ($sqlData['from_user_id'] == $from_user_id) {
                $chat_message = $sqlData['chat_message'];
                $user = 'You';
                $class = 'chat-right';
                $delete = '<i style="display:block" class="fa flaticon-delete deletechat" data-id="'.$chat_id.'"></i>';
                if ($status == 1) {
                    $chat_message = '<small><i class="fa fa-ban" aria-hidden="true"></i> You deleted this message</small>';
                } else {
                    if (strtotime(date('Y-m-d')) == strtotime((date('y-m-d', strtotime($sqlData['timestamp']))))) {
                        $tooltips = " data-toggle=\"tooltip\" title=\"<span class='edit_chat_message' id='$chat_id'>Edit</span>\"";
                    }
                }
            } else {
                $name = self::fetch('SELECT first_name,last_name FROM manager WHERE id = :id', [
                        ':id' => $sqlData['from_user_id'],
                    ]);
                $chat_message = $sqlData['chat_message'];
                $user = $name['first_name'].' '.$name['last_name'];
                $class = 'chat-left';
                if ($status == 1) {
                    $chat_message = '<small><i class="fa fa-ban" aria-hidden="true"></i> This message was deleted</small>';
                }
            }
            $chat_message = nl2br($chat_message);
            if ($status == 1) {
                $tooltips = '';
                $delete = '';
            }
            if ($status == 2) {
                $edited = '<b>Edited</b>';
            }
            $timestamp = date('Y-m-d H:i', strtotime($sqlData['timestamp']));
            $output .= '
                    <div class="chat '.$class.' text'.$chat_id.' chat__message" data-chatid = "'.$chat_id.'">
                    <div class="chat-body">
                        <div class="chat-content edit_chat_message">
                            <p><strong>'.$user.'</strong></p>
                            <p class="chat_text">'.$chat_message.'</p>
                            <p class="chat-time text-right chat__datetime">'.$timestamp.' '.$edited.'</p>
                        </div>
                    </div>
                </div>';
        }
        $leatest_id = $this->getLeatestChatId($_POST);
        $returnOutput = array('output' => $output, 'to_user_id' => $to_user_id, 'leatest_id' => $leatest_id);

        return $returnOutput;
    }

    public function saveChatMessage(array $formData)
    {
        if ($formData['id'] != '') {
            $result = $this->insert('chat_message', [
                'to_user_id' => $formData['id'],
                'from_user_id' => self::$mId,
                'chat_message' => $formData['message'],
            ]);

            $chat_id = $this->lastInsertId();
            $formData['id'] = $chat_id;
            $this->createChatMessageModified($formData);

            return $chat_id;
        } else {
            $result = $this->insert('group_chat_message', [
                'group_id' => $formData['groupid'],
                'from_user_id' => self::$mId,
                'message' => $formData['message'],
            ]);

            $chat_id = $this->lastInsertId();
            $formData['id'] = $chat_id;
            $managerGroupChatModel = new ManagerGroupChat();
            $managerGroupChatModel->createGroupChatMessageModified($formData);

            return $chat_id;
        }
    }

    public function saveChatNotification(array $formData)
    {
        return $this->insert('chat_notification', [
            'receiver_id' => $formData['id'],
            'sender_id' => self::$mId,
        ]);
    }

    public function deleteChatNotification(array $formData)
    {
        $receiver_id = self::$mId;
        $sender_id = $formData['id'];

        return $this->delete('chat_notification', ['receiver_id' => $receiver_id, 'sender_id' => $sender_id]);
    }

    public function deleteChat(array $formData)
    {
        $id = $formData['id'];
        $this->createChatMessageModified($formData);

        return $this->update('chat_message', [
            'status' => 1,
        ], $id, 'chat_message_id');

        //return $this->delete('chat_message', ['chat_message_id' => $id]);
    }

    public function updateChatMessage(array $formData)
    {
        $id = $formData['id'];
        $this->createChatMessageModified($formData);

        return $this->update('chat_message', [
            'chat_message' => $formData['message'],
            'status' => 2,
        ], $id, 'chat_message_id');
    }

    public function getLeatestChatId(array $formData)
    {
        $from_user_id = self::$mId;
        $to_user_id = $formData['id'];

        return $sql = self::fetch('SELECT chat_message_id FROM chat_message WHERE (from_user_id = :from_user_id AND to_user_id = :to_user_id)
        OR (from_user_id = :to_user_id1 AND to_user_id = :from_user_id1) ORDER BY chat_message_id DESC limit 1', [
            ':from_user_id' => $from_user_id,
            ':to_user_id' => $to_user_id,
            ':from_user_id1' => $from_user_id,
            ':to_user_id1' => $to_user_id,
        ]);
    }

    public function getLatestChatFromUserId()
    {
        $to_user_id = self::$mId;
        $sql = 'SELECT from_user_id FROM chat_message WHERE to_user_id = :to_user_id ORDER BY chat_message_id DESC limit 1';
        $result = self::fetch($sql,
                [
                    ':to_user_id' => $to_user_id,
                ]
        );

        return $result['from_user_id'];
    }

    public function getRefreshChatMessage(array $formData)
    {
        $chat_id = $formData['leatest_chat_id'];
        $output = [];
        $from_user_id = self::$mId;
        $to_user_id = $formData['id'];

        $chatData = self::fetch('SELECT GROUP_CONCAT(chat_message_id) AS chat_ids FROM chat_message_modified WHERE to_user_id = :to_user_id AND from_user_id = :from_user_id',
                                [
                                    ':to_user_id' => self::$mId,
                                    ':from_user_id' => $to_user_id,
                                ]
        );

        if (!empty($chatData['chat_ids'])) {
            $chat_ids = $chatData['chat_ids'];
            $result1 = self::fetchAll('SELECT * FROM chat_message WHERE chat_message_id IN ('.$chat_ids.')');
            $this->query('DELETE FROM chat_message_modified WHERE chat_message_id IN ('.$chat_ids.')');
        }

        $result = self::fetchAll('SELECT * FROM chat_message WHERE chat_message_id > :chat_id
                                AND ((from_user_id = :from_user_id AND to_user_id = :to_user_id)
                                OR (from_user_id = :to_user_id1 AND to_user_id = :from_user_id1))',
                                [
                                    ':chat_id' => $chat_id,
                                    ':from_user_id' => $from_user_id,
                                    ':to_user_id' => $to_user_id,
                                    ':from_user_id1' => $from_user_id,
                                    ':to_user_id1' => $to_user_id,
                                ]
        );
        /*
        $sql = self::fetchAll('SELECT * FROM chat_message WHERE (chat_message_id > :chat_message_id) AND ((from_user_id = :from_user_id AND to_user_id = :to_user_id)
        OR (from_user_id = :to_user_id1 AND to_user_id = :from_user_id1))', [
            ':chat_message_id' => $chat_id,
            ':from_user_id' => $from_user_id,
            ':to_user_id' => $to_user_id,
            ':from_user_id1' => $from_user_id,
            ':to_user_id1' => $to_user_id,
        ]);
         *
         */

        if (isset($result1) && count($result1)) {
            $result = array_merge($result, $result1);
        }
        foreach ($result as $sqlData) {
            $status = $sqlData['status'];
            $chat_id = $sqlData['chat_message_id'];
            $chat_message = $sqlData['chat_message'];
            $edited = '';
            $tooltips = '';
            $delete = '';
            if ($sqlData['from_user_id'] == $from_user_id) {
                $user = '';
                $class = 'chat-right';
                $delete = '<i style="display:block" class="fa flaticon-delete deletechat" data-id="'.$chat_id.'"></i>';
                if ($status == 1) {
                    $chat_message = '<small><i class="fa fa-ban" aria-hidden="true"></i> You deleted this message</small>';
                } else {
                    if (strtotime(date('Y-m-d')) == strtotime((date('Y-m-d', strtotime($sqlData['timestamp']))))) {
                        $tooltips = " data-toggle=\"tooltip\" title=\"<span class='edit_chat_message' id='$chat_id'>Edit</span>\"";
                    }
                }
            } else {
                $name = self::fetch('SELECT first_name,last_name FROM manager WHERE id = :id', [
                        ':id' => $sqlData['from_user_id'],
                    ]);
                $user = $name['first_name'].' '.$name['last_name'];
                $class = 'chat-left';
                if ($status == 1) {
                    $chat_message = '<small><i class="fa fa-ban" aria-hidden="true"></i> This message was deleted</small>';
                }
            }
            $chat_message = nl2br($chat_message);
            if ($status == 1) {
                $tooltips = '';
            }
            if ($status == 2) {
                $edited = '<b>Edited</b>';
            }
            $timestamp = date('Y-m-d H:i', strtotime($sqlData['timestamp']));
            $output[$chat_id] = [
                'status' => $status,
                'message' => $chat_message,
                'new_message' => '<div class="chat__message '.$class.' text'.$chat_id.' chat__message" data-chatid = "'.$chat_id.'">
                <div class="chat-body">
                    <div class="chat-content edit_chat_message">
                        <p><strong>'.$user.'</strong></p>
                        <p class="chat_text">'.$chat_message.'</p>
                        <p class="chat-time text-right chat__datetime">'.$timestamp.' '.$edited.'</p>
                    </div>
                </div>
            </div>',
            ];
        }
        $leatest_id = $this->getLeatestChatId($_POST);

        return  array('output' => $output, 'leatest_id' => $chat_id);
    }

    public function createChatMessageModified(array $formData)
    {
        $this->insert('chat_message_modified', [
            'chat_message_id' => $formData['id'],
            'to_user_id' => $formData['to_user_id'],
            'from_user_id' => self::$mId,
        ]);
    }

    public function getRecentChat()
    {
        $datas = array();
        $from_user_id = self::$mId;
        $sql = self::fetchAll('SELECT * FROM chat_message WHERE (from_user_id = :from_user_id)
        OR (to_user_id = :from_user_id1) ORDER BY timestamp DESC LIMIT 3', [
            ':from_user_id' => $from_user_id,
            ':from_user_id1' => $from_user_id,
        ]);
        if (count($sql) > 0) {
            foreach ($sql as $sqlData) {
                $name = self::fetch('SELECT first_name,last_name FROM manager WHERE id = :id', [
                        ':id' => $sqlData['from_user_id'],
                    ]);
                $chat_message = $sqlData['chat_message'];
                $user = $name['first_name'].' '.$name['last_name'];
                $timestamp = date('Y-m-d H:i', strtotime($sqlData['timestamp']));
                $data[] = array('name' => $name, 'chat_message' => $chat_message, 'user' => $user, 'timestamp' => $timestamp);
            }
            $datas = $data;
        }

        return $datas;
    }
}
