<?php

class ManagerUserApiController extends ManagerController
{
    public function deleteNotification()
    {
        $notificationId = $_POST['id'];

        $ManagerUserModal = self::model('ManagerUser');
        $ManagerUserModal->deleteNotification($notificationId);
    }
}
