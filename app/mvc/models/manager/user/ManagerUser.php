<?php

class ManagerUser extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function searchUsers(array $formData)
    {
        $conditionString = '';
        $conditionList = array();
        foreach ($formData as $key => $data) {
            if (!empty($data)) {
                $conditionString .= " {$key} = :{$key} AND";

                $conditionList[":{$key}"] = $data;
            }
        }
        $conditionString = trim(rtrim($conditionString, 'AND'));

        $sql = "SELECT first_name,last_name,email
                FROM manager
                WHERE $conditionString limit 50";

        return self::fetchAll($sql, $conditionList);
    }

    public function getRoleName(array $formData)
    {
        foreach ($formData as $formDatas) {
            $Name = array();
            foreach ($formDatas['access'] as $key => $data) {
                $Name[] = self::fetch('SELECT id,role_name FROM role WHERE id = :roleId', [
                    ':roleId' => $key,
                ]);
            }
            $Names[] = $Name;
        }

        foreach ($Names as $userNames) {
            $roleName[] = implode(', ', array_map(function ($userNames) {
                return $userNames['role_name'];
            }, $userNames));

            $roleId[] = implode(',', array_map(function ($userNames) {
                return $userNames['id'];
            }, $userNames));
        }

        $roleNames = $roleName;
        $roleId = $roleId;

        $Output = array('id' => $roleId, 'Name' => $roleNames);

        return $Output;
    }

    public function getRolesByPropertyId($propertyId)
    {
        $sql = 'SELECT id,role_name
                FROM role
                WHERE property_id = :property_id';

        return self::fetchAll($sql, [
            ':property_id' => $propertyId,
        ]);
    }

    public function deleteUser($userId)
    {
        return $this->delete('manager', ['id' => $userId]);
    }

    public function updateRole(array $formData)
    {
        $userId = $formData['userId'];
        $roleId = $formData['role_id'];
        $str = '"'.implode('":true,"', $roleId).'":true';
        $roleData = '{'.$str.'}';
        self::$db->beginTransaction();
        $this->update('property_access', [
          'access' => $roleData,
      ], $userId, 'user_id');
        self::$db->commit();
    }

    public function getRoleByPropertyIdAndRoleId($userId, $propertyId)
    {
        return self::fetch('SELECT access FROM property_access WHERE property_id = :propertyId and user_id = :userId', [
            ':userId' => $userId,
            ':propertyId' => $propertyId,
        ]);
    }

    public function getInvitedUserData($propertyId)
    {
        return self::fetchAll('SELECT * FROM property_access_invited WHERE property_id = :propertyId', [
            ':propertyId' => $propertyId,
        ]);
    }

    public function deleteInvitedUsers($id)
    {
        $userData = self::fetch('SELECT email FROM manager WHERE id = :id', [
            ':id' => $id,
        ]);

        return $this->delete('property_access_invited', ['email' => $userData['email']]);
    }

    public function getNotificationDetails()
    {
        $sql = 'SELECT  id,link,message FROM user_notification WHERE user_id = :userid AND message != :null';

        return self::fetchAll($sql, [
            ':userid' => self::$mId,
            'null' => '',
        ]);
    }

    public function getAppoinmentCountNotification()
    {
        $data = self::fetchAll('SELECT * FROM user_notification WHERE user_id = :userid AND type = :type', [
            ':userid' => self::$mId,
            ':type' => 'appoinment',
        ]);

        return count($data);
    }

    public function getChecklistCountNotification()
    {
        $data = self::fetchAll('SELECT * FROM user_notification WHERE user_id = :userid AND type = :type', [
            ':userid' => self::$mId,
            ':type' => 'checklist',
        ]);

        return count($data);
    }

    public function getReservationCountNotification()
    {
        $data = self::fetchAll('SELECT * FROM user_notification WHERE user_id = :userid AND type = :type', [
            ':userid' => self::$mId,
            ':type' => 'reservation',
        ]);

        return count($data);
    }
	
	public function getChatCountNotification()
    {
        $data = self::fetch('SELECT count(id) AS chat_count FROM chat_notification WHERE receiver_id = :receiver_id', [
            ':receiver_id' => self::$mId
        ]);
        return $data['chat_count'];
    }

    public function saveNotificationByEmail($email, $message = '', $url = '', $type = '')
    {
        $userData = self::fetch('SELECT id,first_name,last_name FROM manager WHERE email = :email', [
            ':email' => $email,
        ]);
        if (isset($userData)) {
            return $this->insert('user_notification', [
                'user_id' => $userData['id'],
                'message' => $message,
                'link' => $url,
                'type' => $type,
            ], );
        }
    }

    public function saveNotificationByUserId($user_id, $message = '', $url = '', $type = '')
    {
        return $this->insert('user_notification', [
            'user_id' => $user_id,
            'message' => $message,
            'link' => $url,
            'type' => $type,
        ], );
    }

    public function deleteNotification($notificationId = '', $type = '')
    {
        if ($notificationId == '') {
            $notificationId = self::$mId;

            return $this->delete('user_notification', ['user_id' => $notificationId, 'type' => $type]);
        } else {
            return $this->delete('user_notification', ['id' => $notificationId]);
        }
    }
}
