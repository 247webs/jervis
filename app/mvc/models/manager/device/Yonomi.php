<?php
class Yonomi extends Model
{
    private $tenantId;
    private $clientId;
    private $thirdPartyClientId;

    public $accessToken;
    public $refreshToken;

    public $authenticated;
    public $logs;
    
    public function __construct()
    {
        parent::__construct();
        $this->tenantId = "5ed57d0daaa22330437b3993";
        $this->clientId = "5ed57d4faaa22330437b3994";
        $this->thirdPartyClientId = "5ed57d76aaa22330437b3995";
        $this->authenticated = false;
        $this->logs = [];
    }
    public function recoverSession($accessToken, $refreshToken)
    {
        if (!empty($accessToken) and !empty($refreshToken)) {
            $this->accessToken = $accessToken;
            $this->refreshToken = $refreshToken;
            $this->authenticated = $this->checkMe();
            if ($this->authenticated === false) {
                $this->refreshToken();
                $this->authenticated = $this->checkMe();
            }
        }
    }
    public function recoverSessionOrlogin($username, $password, $accessToken, $refreshToken)
    {
        recoverSession($accessToken, $refreshToken);
        if ($this->authenticated === false) {
            $this->login($username, $password);
        }
    }
    public function login($username, $password)
    {
        $payload = [
            "grant_type"=>"password",
            "username"=>$username,
            "password"=>$password,
            "client_id"=>$this->clientId
        ];
        $json = $this->request("https://auth.yonomi.co/oauth2/token", $payload, "POST");
        $r = json_decode($json);
        if (isset($r->access_token) and isset($r->refresh_token)) {
            $this->accessToken = $r->access_token;
            $this->refreshToken = $r->refresh_token;
            $this->authenticated = $this->checkMe();
            return true;
        } else {
            return false;
        }
    }
    private function refreshToken()
    {
        $payload = [
            "grant_type"=>"refresh_token",
            "refresh_token"=>$this->refreshToken,
            "client_id"=>$this->clientId
        ];
        $json = $this->request("https://auth.yonomi.co/oauth2/token", $payload, "POST");
        $r = json_decode($json);
        if (isset($r->access_token)) {
            $this->accessToken = $r->access_token;
            $this->refreshToken = $r->refresh_token;
            return true;
        } else {
            return false;
        }
    }
    private function checkMe()
    {
        $json = $this->request("https://api.yonomi.co/users/@me");
        $r = json_decode($json);
        if (!empty($r)) {
            return true;
        } else {
            return false;
        }
    }
    public function getDevices($filters=[])
    {
        $json = $this->request("https://api.yonomi.co/devices?".http_build_query($filters));
        $r = json_decode($json);
        return $r;
    }
    public function getDeviceTypes($filters=[])
    {
        $json = $this->request("https://api.yonomi.co/devicetypes?".http_build_query($filters));
        $r = json_decode($json);
        return $r;
    }
    public function getActions($deviceId)
    {
        $json = $this->request("https://api.yonomi.co/devices/{$deviceId}/actions");
        $r = json_decode($json);
        return $r;
    }
    public function actionRequest($deviceId, $actionId)
    {
        $payload = [
            'action_id'=>$actionId,
            'device_id'=>$deviceId,
            'params'=>[]
        ];
        $json = $this->request("https://api.yonomi.co/actionrequests", $payload, "POST");
        $r = json_decode($json);
        return $r;
    }
    public function stateRequest($deviceId, $stateId = [])
    {
        $payload = [
            'devicetype_state_ids'=>$stateId,
            'device_id'=>$deviceId
        ];
        $json = $this->request("https://api.yonomi.co/staterequests", $payload, "POST");
        $r = json_decode($json);
        return $r;
    }

    public function actionResponse($requestId)
    {
        $json = $this->request("https://api.yonomi.co/actionrequests/{$requestId}");
        $r = json_decode($json);
        return $r;
    }
    
    public function stateResponse($requestId)
    {
        $json = $this->request("https://api.yonomi.co/staterequests/{$requestId}");
        $r = json_decode($json);
        return $r;
    }
    public function getLockByName($deviceName)
    {
        $devices = $this->getDevices(["type"=>"schlage_sense"]);
        foreach ($devices as $device) {
            if ($device->name == $deviceName) {
                $res['id'] = $device->_id;
                $actions = $this->getActions($device->_id);
                foreach ($actions as $action) {
                    $res[strtolower($action->action_def->name)] = $action->_id;
                }
            }
        }
        if (isset($res['id']) and isset($res['lock']) and isset($res['unlock'])) {
            return $res;
        } else {
            return false;
        }
    }
    public function deleteUser()
    {
        return $this->request("https://api.yonomi.co/users/@me?immediate=true", [], 'DELETE');
    }
    public function logoutDevice($deviceId)
    {
        return $this->request("https://api.yonomi.co/devices/{$deviceId}/logout", [], 'POST');
    }
    public function deleteDevice($deviceId)
    {
        return $this->request("https://api.yonomi.co/devices/{$deviceId}", [], 'DELETE');
    }
    private function getAccessTokenFor3rdPartyAccount()
    {
        $payload = [
            "grant_type"=>"password",
            "access_token"=>$this->accessToken,
            "client_id"=>$this->thirdPartyClientId
        ];
        $json = $this->request("https://auth.yonomi.co/oauth2/token", $payload, "POST");
        $r = json_decode($json);
        return $r->access_token;
    }
    public function getAuthUri($deviceTypeId, $deviceId)
    {
        $r = $this->getDeviceTypes(['id'=>$deviceTypeId]);
        $base = $r->authorization->uri;
        $parsedUrl = parse_url($base);
        $sign = isset(parse_url($base)['query'])?"&":"?";
        $token = $this->getAccessTokenFor3rdPartyAccount();
        return "{$base}{$sign}device_id={$deviceId}&token={$token}";
    }
    public function createDevice($deviceType)
    {
        $json = $this->request("https://api.yonomi.co/devices", ['name'=>$deviceType,'type'=>$deviceType], "POST");
        $r = json_decode($json);
        return $r;
    }
    public function createUser($locale, $timezone)
    {
        $json = $this->request("https://api.yonomi.co/users", ['locale'=>$locale,'tenant_id'=>$this->tenantId,'timezone'=>$timezone], "POST");
        $r = json_decode($json);
        //$r = (object)['_id'=>'xxx','password'=>'yyyy'];
        if (!isset($r->password)) {
            return false;
        } else {
            return $r;
        }
    }
    private function request($url, $fields=[], $reqType = 'GET')
    {
        $log = [];
        $log['URL'] = $url;
        $log['REQUEST_TYPE'] = $reqType;
        
        $header = ['Content-Type: application/json', 'cache-control: no-cache'];
        if ($url == "https://api.yonomi.co/actionrequests") {
            $header[] = 'x-request-key: api';
        }

        if (($url == "https://api.yonomi.co/staterequests") or ($url == "https://api.yonomi.co/users/@me")) {
            $header[] = 'accept-version: v1.0.0';
            $header[] = 'accept: application/json';
        }

        if ($url != "https://auth.yonomi.co/oauth2/token") {
            $header[] = 'Authorization: Bearer '.$this->accessToken;
        }
        $log["HEADER"] = $header;

        $ch = curl_init();
        
        if (!empty($fields)) {
            $log['DATA'] = $fields;
            $fields = json_encode($fields);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $reqType);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);

        $log["RESULT"] = $result;
    
        $this->logs[] = $log;
        return $result;
    }
}
