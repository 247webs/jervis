<?php

class ManagerDevice extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function setYonomi()
    {
        $yonomi = new Yonomi();
        $mId = parent::sessionGet('managerId');
        $yonomiUser = self::getYonomiUser($mId);
        if (!isset($yonomiUser['id'])) {
            $r = $yonomi->createUser('en-US', 'America/New_York');

            if (false === $r) {
                return (object) ['errMsg' => 'Connection Error with Lock Gateway','logs' => $yonomi->logs];
            }
            $r = $yonomi->login($r->_id, $r->password);
            
            if ($yonomi->authenticated === false) {
                return (object) ['errMsg' => 'Authentication Error with Lock Gateway','logs' => $yonomi->logs];
            }

            $this->createYonomiUser($mId, $yonomi->accessToken, $yonomi->refreshToken);
        }
        $yonomi->recoverSession($yonomiUser['accessToken'], $yonomiUser['refreshToken']);
        if ($yonomi->authenticated === false) {
            return (object) ['errMsg' => 'Session Recover Error','logs' => $yonomi->logs];
        }
        $this->updateYonomiToken($mId, $yonomi->accessToken, $yonomi->refreshToken);
        return $yonomi;
    }


    public function createYonomiUser($managerId, $accessToken, $refreshToken)
    {
        return $result = $this->insert('yonomi', [
            'manager_id' => $managerId,
            'accessToken' => $accessToken,
            'refreshToken' => $refreshToken,
        ]);
    }

    public function updateYonomiToken($managerId, $accessToken, $refreshToken)
    {
        $r = $result = $this->update('yonomi', [
            'accessToken' => $accessToken,
            'refreshToken' => $refreshToken,
        ], $managerId, 'manager_id');
    }

    public function getYonomiUser($managerId)
    {
        $sql = 'SELECT *
                FROM yonomi
                WHERE manager_id = :manager_id';

        return
            self::fetch(
                $sql,
                [':manager_id' => $managerId]
            );
    }

    public function updateYonomiDevices()
    {
        $mId = parent::sessionGet('managerId');
        $yonomi = $this->setYonomi();
        $devices = $yonomi->getDevices();
        foreach ($devices as $device) {
            if (empty($device->parent_ids)) {
                continue;
            }
            $sql = 'INSERT INTO device
                (manager_id, device_id, name, vendor_name, type, subtype, description, actions)
                VALUES
                (:manager_id, :device_id, :name, :vendor_name, :type, :subtype, :description, :actions)
                ON DUPLICATE KEY UPDATE
                name = VALUES(name), vendor_name = VALUES(vendor_name), type = VALUES(type), subtype = VALUES(subtype), description = VALUES(description), actions = VALUES(actions)
            ';
            $this->query(
                $sql,
                [
                    'manager_id' => $mId,
                    'device_id' => $device->_id,
                    'name' => $device->name,
                    'vendor_name' => $device->vendor_name,
                    'type' => $device->type,
                    'subtype' => (isset($device->subtype) ? $device->subtype : ''),
                    'description' => $device->description,
                    'actions' => json_encode($device->actions),
                ]
            );
            $deviceType = $yonomi->getDeviceTypes(["id"=>$device->type]);
            $sql = 'INSERT INTO device_type
                (device_type, states)
                VALUES
                (:device_type, :states)
                ON DUPLICATE KEY UPDATE
                states = VALUES(states)
            ';
            $this->query(
                $sql,
                [
                    'device_type' => $device->type,
                    'states' => json_encode($deviceType->states),
                ]
            );
        }
    }

    public function deleteDevice($deviceId)
    {
        $mId = parent::sessionGet('managerId');
        $sql = 'DELETE FROM device WHERE device_id = :device_id AND manager_id = :manager_id';
        $this->query(
            $sql,
            [
                'device_id' => $deviceId,
                'manager_id' => $mId,
            ]
        );
    }

    public function assignDeviceToProperty($deviceId, $propertyId)
    {
        $mId = parent::sessionGet('managerId');
        if ($propertyId == 0) {
            $propertyId = null;
        }
        $sql = 'UPDATE device SET property_id = :property_id WHERE id = :device_id AND manager_id = :manager_id';

        return $this->query($sql, ['device_id' => $deviceId, 'property_id' => $propertyId, 'manager_id' => $mId]);
    }

    public function getDevicesByManagerId()
    {
        $mId = parent::sessionGet('managerId');
        $sql = 'SELECT 
                device.id as deviceId,
                device.*,
                device_type.*,
                property.title as assignedProperty,
                property.id as assignedPropertyId
                FROM device
                LEFT JOIN property
                ON device.property_id = property.id
                LEFT JOIN device_type
                ON device.type = device_type.device_type
                WHERE device.manager_id = :manager_id
                ORDER BY device.type,device.name';

        $devices = self::fetchAll($sql, [
            ':manager_id' => $mId,
        ]);
        for ($i = 0; $i < count($devices); ++$i) {
            $devices[$i]['actions'] = json_decode($devices[$i]['actions']);
            $devices[$i]['states'] = json_decode($devices[$i]['states']);
        }
        return $devices;
    }

    public function getDevicesByPropertyId($propertyId)
    {
        $mId = parent::sessionGet('managerId');
        $sql = 'SELECT 
                device.id as device_id,
                property.title as assignedProperty,
                property.id as assignedPropertyId,
                device.description,
                device.type,
                device.subtype,
                device.vendor_name,
                device.name
                FROM device
                LEFT JOIN property
                ON device.property_id = :device_id
                WHERE device.manager_id = :manager_id
                ORDER BY device.type,device.name';

        return self::fetchAll($sql, [
            ':device_id' => $propertyId,
            ':manager_id' => $mId,
        ]);
    }

    public function getDeviceById($deviceId)
    {
        $sql = 'SELECT device.id as device_id, device.property_id as property_id, title, description, type
                FROM device
                WHERE device.id = :device_id';

        return self::fetch($sql, [
            ':device_id' => $deviceId,
        ]);
    }
}
