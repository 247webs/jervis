<?php

class ManagerRciAccount extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function get($id)
    {
        $r = ['userName' => '', 'id' => $id];

        $account = $this->getRow($id);
        if (isset($account['user_name'])) {
            $r['userName'] = $account['user_name'];
        }

        return json_encode($r);
    }

    public function getAll()
    {
        $r = [
            'rows' => '',
        ];
        $rows = $this->getRows();
        foreach ((array) $rows as $row) {
            $r['rows'][] = [
                'id' => $row['id'],
                'userName' => $row['user_name'],
                'ops' => "<a href='javascript:get({$row['id']});'>Edit</a> | <a href='javascript:del({$row['id']});'>Delete</a>",
            ];
        }

        return json_encode($r);
    }

    public function set($in)
    {
        $r = ['ok' => false, 'errMsg' => 'Error'];
        if (isset($in['userName']) and isset($in['password']) and !empty($in['userName']) and !empty($in['password'])) {
            if ($in['userId'] == 0) {
                $r = [
                    'ok' => $this->newRow($in['userName'], $in['password']) ? true : false,
                    'errMsg' => '',
                ];
            } else {
                $r = [
                    'ok' => $this->updateRow($in['userName'], $in['password'], $in['userId']) ? true : false,
                    'errMsg' => '',
                ];
            }
        } else {
            $r = [
                'ok' => false,
                'errMsg' => 'Form Error',
            ];
        }

        return json_encode($r);
    }

    public function del($id)
    {
        return json_encode(['ok' => $this->delRow($id)]);
    }

    private function delRow($id)
    {
        $sql = 'DELETE FROM rci_account WHERE id = :id AND manager_id = :manager_id';

        return (bool) $this->rowCount($sql, [
            ':id' => $id,
            ':manager_id' => self::$mId,
        ]);
    }

    private function getRows()
    {
        $sql = 'SELECT * FROM rci_account WHERE manager_id = :manager_id';

        return self::fetchAll($sql, [
            ':manager_id' => self::$mId,
        ]);
    }

    private function getRow($id)
    {
        $sql = 'SELECT * FROM rci_account WHERE id = :id AND manager_id = :manager_id';

        return self::fetch($sql, [
            ':id' => $id,
            ':manager_id' => self::$mId,
        ]);
    }

    private function newRow($userName, $password)
    {
        $sql = 'INSERT INTO rci_account SET user_name = :user_name, password = :password, manager_id = :manager_id';
        self::query($sql, [
            ':user_name' => $userName,
            ':password' => $password,
            ':manager_id' => self::$mId,
        ]);

        return self::lastInsertId();
    }

    private function updateRow($userName, $password, $id)
    {
        $sql = 'UPDATE rci_account SET user_name = :user_name, password = :password WHERE id = :id AND manager_id = :manager_id';

        return (bool) self::rowCount($sql, [
            ':user_name' => $userName,
            ':password' => $password,
            ':id' => $id,
            ':manager_id' => self::$mId,
        ]);
    }
}
