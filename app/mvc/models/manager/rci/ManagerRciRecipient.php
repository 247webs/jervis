<?php

class ManagerRciRecipient extends Model
{
    public static $mId;

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function getAll()
    {
        $r = [
            'rows' => '',
        ];
        $rows = $this->getRows();
        foreach ((array) $rows as $row) {
            $r['rows'][] = [
                'id' => $row['id'],
                'firstName' => $row['first_name'],
                'lastName' => $row['last_name'],
                'email' => $row['email'],
                'ops' => "<a href='javascript:del({$row['id']});'>Delete</a>",
            ];
        }

        return json_encode($r);
    }

    public function set($in)
    {
        $r = ['ok' => false, 'errMsg' => 'Error'];
        if (isset($in['firstName']) and isset($in['lastName']) and isset($in['email']) and !empty($in['firstName']) and !empty($in['lastName']) and !empty($in['email'])) {
            $r = [
                'ok' => $this->newRow($in['firstName'], $in['lastName'], $in['email']) ? true : false,
                'errMsg' => '',
            ];
        } else {
            $r = [
                'ok' => false,
                'errMsg' => 'Form Error',
            ];
        }

        return json_encode($r);
    }

    public function del($id)
    {
        return json_encode(['ok' => $this->delRow($id)]);
    }

    private function delRow($id)
    {
        $sql = 'DELETE FROM rci_recipient WHERE id = :id AND manager_id = :manager_id';

        return (bool) $this->rowCount($sql, [
            ':id' => $id,
            ':manager_id' => self::$mId,
        ]);
    }

    private function getRows()
    {
        $sql = 'SELECT * FROM rci_recipient WHERE manager_id = :manager_id';

        return self::fetchAll($sql, [
            ':manager_id' => self::$mId,
        ]);
    }

    private function newRow($firstName, $lastName, $email)
    {
        $sql = 'INSERT INTO rci_recipient SET first_name = :first_name, last_name = :last_name, email = :email, manager_id = :manager_id';
        self::query($sql, [
            ':first_name' => $firstName,
            ':last_name' => $lastName,
            ':email' => $email,
            ':manager_id' => self::$mId,
        ]);

        return self::lastInsertId();
    }
}
