<?php

class ManagerRole extends Model
{
    public static $mId;
    protected $permissionCache = [];

    public function __construct()
    {
        parent::__construct();
        self::$mId = $this->sessionGet('managerId');
    }

    public function getAllRaw()
    {
        return $this->getRows();
    }

    private function getRows()
    {
        $sql = 'SELECT manager.id as user_id, first_name, last_name, email, mobile 
                FROM manager';

        return self::fetchAll($sql, [
        ]);
    }

    public function getRolesByPropertyId($propertyId)
    {
        $sql = 'SELECT role.id as role_id, role_name, property_id
                FROM role
                WHERE role.property_id = :property_id';

        return self::fetchAll($sql, [
            ':property_id' => $propertyId,
        ]);
    }

    public function createRole(array $formData, $user_id = null)
    {
        self::$db->beginTransaction();
        $this->insert('role', [
            'role_name' => $formData['roleName'],
            'property_id' => $formData['propertyId'],
        ]);
        $role_id = $this->lastInsertId();
        if ($role_id) {
            $permissions = $formData['permissions'];
            foreach ($permissions as $permission) {
                $this->insert('role_perm', [
                    'role_id' => $role_id,
                    'perm_id' => $permission,
                    'property_id' => $formData['propertyId'],
                ]);
            }
        }
        self::$db->commit();
    }

    public function updateRole($roleId, array $formData)
    {
        self::$db->beginTransaction();

        $this->update('role', [
            'role_name' => $formData['roleName'],
        ], $roleId, 'id');

        self::query('DELETE FROM role_perm WHERE role_id = :role_id', [
            ':role_id' => $roleId,
        ]);

        $permissions = $formData['permissions'];
        foreach ($permissions as $permission) {
            $this->insert('role_perm', [
                'role_id' => $roleId,
                'perm_id' => $permission,
                'property_id' => $formData['propertyId'],
            ]);
        }

        self::$db->commit();
    }

    public function getPermissions()
    {
        $sql = 'SELECT id, perm_group, perm_code, description FROM permissions';

        return self::fetchAll($sql);
    }

    public function getAssignedPermissions($roleId)
    {
        $sql = 'SELECT perm_id FROM role_perm WHERE role_id = :role_id';

        $permissionsResult = self::fetchAll($sql, [
            ':role_id' => $roleId,
        ]);

        $assignedPermission = array();
        foreach ($permissionsResult as $key => $permissionResult) {
            $permissionsResult[] = $permissionResult['perm_id'];
        }

        return $permissionsResult;
    }

    public function getRoleById($roleId)
    {
        $sql = 'SELECT role.id as role_id, role_name, property_id
                FROM role
                WHERE role.id = :role_id';

        return self::fetch($sql, [
            ':role_id' => $roleId,
        ]);
    }

    public function deleteRole($roleId)
    {
        return $this->delRow($roleId);
    }

    private function delRow($id)
    {
        $sql = 'DELETE FROM role WHERE id = :id';

        return (bool) $this->rowCount($sql, [
            ':id' => $id,
        ]);
    }

    public function currentUserHasPermission(array $property, $permissionName)
    {
        $userId = Model::sessionGet('managerId');
        if ($userId == $property['owner_id']) {
            return true;
        }
        $permissions = $this->getCachedUserPropertyPermissions($userId, $property['id']);

        return $permissions[$permissionName];
    }

    public function currentUserHasAnyPermission(array $property, array $permissionNames)
    {
        $userId = Model::sessionGet('managerId');
        if ($userId == $property['owner_id']) {
            return true;
        }
        $permissions = $this->getCachedUserPropertyPermissions($userId, $property['id']);
        foreach ($permissionNames as $permissionName) {
            if ($permissions[$permissionName]) {
                return true;
            }
        }

        return false;
    }

    public function currentUserHasAllPermissions(array $property, array $permissionNames)
    {
        $userId = Model::sessionGet('managerId');
        if ($userId == $property['owner_id']) {
            return true;
        }
        $permissions = $this->getCachedUserPropertyPermissions($userId, $property['id']);
        foreach ($permissionNames as $permissionName) {
            if (!$permissions[$permissionName]) {
                return false;
            }
        }

        return true;
    }

    public function getCachedUserPropertyPermissions($userId, $propertyId)
    {
        if (empty($this->permissionCache[$propertyId][$userId])) {
            $this->permissionCache[$propertyId][$userId] = $this->getUserPropertyPermissions($userId, $propertyId);
        }

        return $this->permissionCache[$propertyId][$userId];
    }

    public function getUserPropertyPermissions($userId, $propertyId)
    {
        $propertyAccess = $this->fetch('SELECT * FROM `property_access`
            WHERE user_id = :user_id
            AND property_id = :property_id
        ', [
            ':user_id' => $userId,
            ':property_id' => $propertyId,
        ]);
        // We default it to 0 so the query won't fail when a user has no roles, we want to always return an object of permissions
        $roleIds = [0];
        if ($propertyAccess) {
            $decodedJson = json_decode($propertyAccess['access'], true);
            if ($decodedJson !== null && $decodedJson) {
                $roleIds = array_keys(array_filter($decodedJson));
            }
        }
        $quotedRoleIds = array_map([$this, 'quote'], $roleIds);
        $lookup = $this->fetchAll('SELECT p.perm_code, IF(rp.role_id IS NULL, FALSE, TRUE) perm  FROM `permissions` p
            LEFT JOIN `role_perm` rp ON (rp.perm_id = p.id AND rp.role_id IN ('.implode(',', $quotedRoleIds).'))
        ');
        $mapped = [];
        foreach ($lookup as $entry) {
            $mapped[$entry['perm_code']] = boolval($entry['perm']);
        }

        return $mapped;
    }

    public function getPropertiesRelevantForCurrentUser()
    {
        $userId = self::$mId;
        if (empty($this->propertiesCach[$userId])) {
            $managerPropertyModel = new ManagerProperty();

            return $managerPropertyModel->getPropertiesRelevantForUser($userId);
        }
    }

    public function getCleaningCompanyUserIdByPropertyId($propertyId)
    {
        $sql = 'SELECT user_id FROM property_access pa 
        INNER JOIN role_perm rp 
        ON rp.property_id = pa.property_id 
        AND perm_id = (SELECT id FROM permissions WHERE perm_code = "CleaningCompanyChecklist.complete")
        WHERE pa.property_id = :property_id 
        AND JSON_EXTRACT(access, concat("$.",role_id)) = TRUE';

        return self::fetch($sql, [
            ':property_id' => $propertyId,
        ]);
    }

    public function getPropertyRoles($currentProperty, $user_id)
    {
        $roles = [];
        $property_id = '';
        if (isset($currentProperty['id'])) {
            $property_id = $currentProperty['id'];
        }
        $propertyModel = new ManagerProperty();
        $userRoles = $propertyModel->getUserRoles($property_id, $user_id);

        if (count($userRoles)) {
            foreach ($userRoles as $key => $role) {
                $role_name = preg_replace('/Property|(C|c)ompany/', '', $role);
                array_push($roles, ucfirst(trim($role_name)));
            }
        } else {
            $roles = ['Owner'];
        }

        return array_unique($roles);
    }
}
