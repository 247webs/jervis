<?php

class Api extends Model
{
    public function checkBearer()
    {
        $headers = getallheaders();
        if ((!isset($headers['Authorization'])) or ($headers['Authorization'] != 'Bearer afa9598c8c1c87823193904dcb016f38')) {
            return false;
        } else {
            return true;
        }
    }

    public function pullFromBitbucket()
    {
        return shell_exec('cd '.ROOT_DIR.'&&/usr/bin/git pull');
    }
}
