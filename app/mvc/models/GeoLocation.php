<?php

class GeoLocation extends Model
{
    public function getCountries()
    {
        $sql = 'SELECT id, name FROM countries order by case when name = "United States" then 0 else 1 end';

        return self::fetchAll($sql);
    }

    public function getStatesByCountryId($countryId)
    {
        $sql = 'SELECT id, name, country_id AS countryId FROM states WHERE country_id=:country_id';

        return self::fetchAll($sql, [
            'country_id' => $countryId,
        ]);
    }

    public function getCitiesByStateId($stateId)
    {
        $sql = 'SELECT id, name, state_id AS stateId FROM cities  WHERE state_id = :state_id';

        return self::fetchAll($sql, [
            ':state_id' => $stateId,
        ]);
    }
}
