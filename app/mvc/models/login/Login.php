<?php

class Login extends Model
{
    public function doLogin($email, $password, $secret)
    {
        $payload = [
            ':email' => $email,
            ':password' => Helpers::createHash($password),
        ];
        $sql = 'SELECT * FROM manager WHERE email = :email AND password = :password';
        $row = self::fetch($sql, $payload);
        if ($row['id'] > 0) {
            if ($row['status'] == '1') {
                self::sessionSet('managerId', $row['id']);
                self::sessionSet('managerFirstName', $row['first_name']);
                self::sessionSet('managerLastName', $row['last_name']);
                self::sessionSet('managerEmail', $row['email']);
                self::sessionSet('managerMobile', $row['mobile']);
                self::sessionSet('managerstatus', $row['status']);
                self::sessionSet('managerAccess', json_decode($row['access']));
                self::sessionSet('managerRole', json_decode($row['role']));
                self::sessionSet('managerLogged', true);
                self::sessionSet('managerType', $row['access_admin']);
                self::sessionSet('managerGoogleAuthCode', ($row['google_auth_code'] !== '') ? $row['google_auth_code'] : $secret);
                self::sessionSet('managerMfa', $row['mfa']);
                return true;
            } else {
                $response['error'] = true;
                $response['description'] = 'Your account is not verified, please check your email for verification email.';
                exit(json_encode($response));
            }
        } else {
            self::sessionSet('managerLogged', false);
            $response['error'] = true;
            $response['description'] = 'Your username and password do not match. Please try again.';
            exit(json_encode($response));
        }
    }

    public function verifyAccout($userId)
    {
        self::$db->beginTransaction();
        $this->update('manager', [
            'status' => '1',
        ], $userId, 'id');
        self::$db->commit();
        /*
        STARTS EMAIL VERIFICATION CONFIRMATION FOR BETA TESTERS.
        BUT, WE CAN CONTINUE TO USING THE CODES AS WELL.
         */
        try {
            $payload = [
                ':id' => $userId,
            ];
            $sql = 'SELECT * FROM manager WHERE id = :id';
            $row = self::fetch($sql, $payload);
            $output = [];
            $mail = Helpers::createEmail($output);
            $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
            $mail->addAddress($row['email']);
            $mail->isHTML(true);
            $modelAdminNotification = new AdminNotification();
            $template = $modelAdminNotification->getTemplateByTitle('Signup - Email Verification Confirmation');
            $mail->Subject = $template['subject'];
            $mail->Body = str_replace('{LOGIN_URL}', URL . '/login', $template['body']);
            $mail->send();
        } catch (Exception $e) {
            // Message could not be sent. Mailer Error: {$mail->ErrorInfo}
            http_response_code(500);
            echo json_encode([
                'error' => 'Failed to send email.',
            ]);
            exit;
        }
        /*
    ENDS EMAIL VERIFICATION CONFIRMATION FOR BETA TESTERS.
     */
    }
}
