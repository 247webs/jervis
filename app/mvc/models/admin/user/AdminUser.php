<?php

class AdminUser extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function updateUserPlan($managerId, array $formData)
    {
        $plan['property'] = $formData['property'];
        $plan['user'] = $formData['user'];
        $plan['reservation'] = ['per_month' => $formData['per_month'], 'per_year' => $formData['per_year']];
        $trial_end_date =  $formData['trial_end_date'];
        $user = [
            'plan' => json_encode($plan),
        ];

        if (isset($formData['access_admin']) && self::sessionGet('managerType') == 1) {
            $user['access_admin'] = $formData['access_admin'];
        }

        if(!empty($trial_end_date))
        {
                self::$db->beginTransaction();
                $this->update('manager', [
                'trial_end_date' => $trial_end_date,
            ], $managerId, 'id');
                self::$db->commit();
        }
        self::$db->beginTransaction();
        $this->update('manager', $user, $managerId, 'id');
        self::$db->commit();
    }

    public function deleteUser($managerId)
    {
        return $this->delRow($managerId);
    }

    private function delRow($id)
    {
        $sql = 'DELETE FROM manager WHERE id = :id';

        return (bool) $this->rowCount($sql, [
            ':id' => $id,
        ]);
    }
}
