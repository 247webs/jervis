<?php

class AdminNotification extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function saveTemplate($templateId, array $formData)
    {
        $tableValues = [
            'title' => $formData['title'],
            'subject' => $formData['subject'],
            'body' => $formData['body'],
        ];
        
        if (!$templateId) {
            $affectedRows = self::insert('admin_notification', $tableValues);
            $tableValues['template_id'] = self::lastInsertId();
            if ($affectedRows) {
                return $tableValues;
            } else {
                $outErrors[] = 'No affected rows.';
                return false;
            }
        } else {
            $affectedRows = self::update(
                'admin_notification', $tableValues, $templateId, 'id'
            );
            $tableValues['template_id'] = $templateId;

            return $tableValues;
        }
    }

    public function getTemplate()
    {
        $sql = 'SELECT * FROM admin_notification';
        return self::fetchAll($sql);
    }

    public function getTemplateById($templateId)
    {
        return self::fetch(
            'SELECT * FROM admin_notification WHERE id = :id',
            [':id' => $templateId]
        );
    }

    public function getTemplateByTitle($title)
    {
        return self::fetch(
            'SELECT * FROM admin_notification WHERE title = :title',
            [':title' => $title]
        );
    }

    public function deleteTemplate(array $formData)
    {
        $id = $formData[0];
        return $this->delete('admin_notification', ['id' => $id]);
    }
}