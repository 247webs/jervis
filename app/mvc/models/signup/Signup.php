<?php

class Signup extends Model
{
    public function saveUser(array $formData)
    {
        $profiles['company'] = [
            'name' => $formData['companyName'],
            'address' => '',
            'email' => '',
            'phone' => '',
        ];

        $this->insert('manager', [
            'first_name' => $formData['firstName'],
            'last_name' => $formData['lastName'],
            'country' => $formData['country'],
            'state' => $formData['state'],
            'city' => $formData['city'],
            'zipcode' => $formData['zipcode'],
            'email' => $formData['email'],
            'mobile' => '+1 ' . $formData['mobile'],
            'password' => Helpers::createHash($formData['password']),
            'role' => json_encode([$formData['role'] => true]),
            'status' => '0',
            'profile' => json_encode($profiles),
            'user_time_zone' => $formData['user_time_zone'],
            'trial_end_date' => date('Y-m-d', strtotime('+' . TRIAL_DAY)),
            'google_auth_code' => isset($formData['secret']) ? $formData['secret'] : '',
            'mfa' => isset($formData['mfa']) ? $formData['mfa'] : 0,
            'access' => '{}',
            'plan' => '',
        ]);

        $user_id = $this->lastInsertId();
        $token = sha1(DB_SALT . $user_id);
        $email = $formData['email'];
        $email = MAIL_ADMIN;
        $url = URL . '/login/email-verification?token=' . $token . '_' . $user_id;
        try {
            $output = [];
            $mail = Helpers::createEmail($output);
            $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
            $mail->addAddress($email);
            $mail->isHTML(true);

            $modelAdminNotification = new AdminNotification();
            $template = $modelAdminNotification->getTemplateByTitle('Signup - Email Verification');

            $mail->Subject = $template['subject'];
            $mail->Body = str_replace('{SIGNUP_EMAIL_VERIFICATION_URL}', $url . ' ', $template['body']);
            $mail->Body .= '<br /><br />';
            $mail->Body .= '<u><b>Beta Tester Registration Data</b></u><br />';
            $mail->Body .= "<b>First Name:</b> {$formData['firstName']}<br />";
            $mail->Body .= "<b>Last Name:</b> {$formData['lastName']}<br />";
            $mail->Body .= "<b>E-Mail:</b> {$formData['email']}<br />";
            $mail->Body .= "<b>Mobile:</b> {$formData['mobile']}<br />";
            $mail->Body .= "<b>IP Address:</b> {$_SERVER['REMOTE_ADDR']}<br />";
            // $mail->Subject = 'Email verification.';
            // $mail->Body = "<a href=\"{$url}\">Click here for account verification</a><br>";
            $mail->send();
        } catch (Exception $e) {
            // Message could not be sent. Mailer Error: {$mail->ErrorInfo}
            return [
                'description' => 'Failed to send email.',
                'error' => true,
            ];
        }

        $sql = "INSERT IGNORE INTO schedule(userid, working_hours, add_break) SELECT $user_id, `working_hours`, `add_break` FROM `schedule_meta`";
        self::query($sql);

        return [
//            'description'=>'Your user profile has been successfully created! Please check your email for verification link.',
            'description' => 'Currently we are only allowing pre-approved beta testers to sign up. If you have been approved previously, you will receive an email from Jervis Systems once your account has been approved.',
            'error' => false,
        ];
    }

    public function userExists($email)
    {
        $sql = 'SELECT * FROM manager WHERE email = :email';

        return self::fetch($sql, [
            ':email' => $email,
        ]);
    }

    public function getStripePlans()
    {
        return self::fetchAll('SELECT * FROM `plans`');
    }

    public function createSubscriptions($user_id, $formData)
    {
        try {
            $plans = self::fetch('SELECT * FROM plans WHERE id = :id', [
                ':id' => $formData['plan'],
            ]);

            \Stripe\Stripe::setApiKey(SECRET_KEY);
            $customer = \Stripe\Customer::create(array(
                'email' => $formData['email'],
                'source' => $formData['stripeToken'],
            ));
            if (isset($customer->id)) {
                /* Setting Stripe Subscription */
                $trial_days = $plans['trial_days'];
                $trial_ends = strtotime("+$trial_days day");
                $subscription = \Stripe\Subscription::create([
                    'customer' => $customer->id,
                    'items' => [
                        [
                            'plan' => $plans['plan_id'],
                        ],
                    ],
                    'trial_end' => $trial_ends,
                ]);

                $this->insert('subscriptions', [
                    'user_id' => $user_id,
                    'plan_id' => $plans['id'],
                    'subscription_id' => $subscription->id,
                    'stripe_id' => $customer->id,
                ]);
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }

    public function userExistsByEmailAndPassword($email, $password)
    {
        $payload = [
            ':email' => $email,
            ':password' => Helpers::createHash($password),
        ];
        $sql = 'SELECT * FROM manager WHERE email = :email AND password = :password';

        return self::fetch($sql, $payload);
    }
}
