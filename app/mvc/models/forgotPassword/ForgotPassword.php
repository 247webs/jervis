<?php

class ForgotPassword extends Model
{
    public function userExists($email)
    {
        $sql = 'SELECT * FROM manager WHERE email = :email';

        return self::fetch($sql, [
            ':email' => $email,
        ]);
    }

    public function tokenExists($token)
    {
        $sql = 'SELECT * FROM manager WHERE code = :code';

        return self::fetch($sql, [
            ':code' => $token,
        ]);
    }

    public function setToken($email, $token)
    {
        self::$db->beginTransaction();
        $this->update('manager', [
            'code' => $token,
        ], $email, 'email');
        self::$db->commit();
    }

    public function resetPasswordByToken($token, $password)
    {
        self::$db->beginTransaction();
        $this->update('manager', [
            'password' => Helpers::createHash($password),
        ], $token, 'code');

        $this->update('manager', [
            'code' => null,
        ], $token, 'code');

        self::$db->commit();
    }
}
