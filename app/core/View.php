<?php

class View
{
    public static function render($view, array $params = [], $template = 'site')
    {
        if (!empty($templateFolder)) {
            $templateFolder .= '/';
        }
        if (file_exists($file = VDIR."/pages/{$view}.html")) {
            extract($params);
            $_T = file_get_contents(VDIR.'/templates/'.$template.'.html');
            $_T = str_replace('<!CONTENT!>', file_get_contents($file), $_T);
            $_T = @eval('?>'.$_T.'<?');
        } else {
            http_response_code(404);
            exit;
        }
    }

    public static function renderTwig($templateName, array $parameters = [], $controller = '', $controllerAction = '')
    {
        $loader = new \Twig\Loader\FilesystemLoader(VDIR.'/twig');
        $twig = new \Twig\Environment($loader, [
            'cache' => APP_DIR.'/cache/twig',
            'debug' => defined('DEBUG') && DEBUG,
            'strict_variables' => true,
        ]);
        // Add permission utility function
        $roleModel = new ManagerRole();
        $twig->addFunction(
            new \Twig\TwigFunction(
                'hasPermission',
                function (array $property = null, $permissionName) use ($roleModel) {
                    if (!$property) {
                        return false;
                    }

                    return $roleModel->currentUserHasPermission($property, $permissionName);
                }
            )
        );
        $twig->addFunction(
            new \Twig\TwigFunction(
                'hasAnyPermission',
                function (array $property = null, array $permissionNames) use ($roleModel) {
                    if (!$property) {
                        return false;
                    }

                    return $roleModel->currentUserHasAnyPermission($property, $permissionNames);
                }
            )
        );
        $twig->addFunction(
            new \Twig\TwigFunction(
                'hasAllPermissions',
                function (array $property = null, array $permissionNames) use ($roleModel) {
                    if (!$property) {
                        return false;
                    }

                    return $roleModel->currentUserHasAllPermissions($property, $permissionNames);
                }
            )
        );

        $twig->addFilter(new Twig_SimpleFilter('json_decode', function ($string) {
            return json_decode($string, true);
        }));

        // $escaper = new \Twig\Extension\EscaperExtension('html');
        // $twig->addExtension($escaper);

        $user = [
            'managerFirstName' => Model::sessionGet('managerFirstName'),
            'managerLastName' => Model::sessionGet('managerLastName'),
            'id' => Model::sessionGet('managerId'),
        ];

        $propertyLists = $roleModel->getPropertiesRelevantForCurrentUser();

        $currentProperty = array();
        if (!empty($_GET['property_id'])) {
            foreach ($propertyLists as $property) {
                if ($property['id'] == $_GET['property_id']) {
                    $currentProperty = $property;
                }
            }
        } else {
            if ($propertyLists) {
                $currentProperty = $propertyLists[0];
            }
        }

        $managerModel = new Manager();
        $manager = $managerModel->getManagerById(Model::sessionGet('managerId'));
        if ($manager['access_admin'] != 1 && $manager['access_admin'] != 2) {
            $profileModel = new ManagerProfile();
            $subscriptions = $profileModel->getSubscriptionsByOwnerId(Model::sessionGet('managerId'));
            $managerSubscriptions = ($subscriptions) ? true : false;

            if (!$managerSubscriptions) {
                //$managerModel = new Manager();
                // $manager = $managerModel->getManagerById(Model::sessionGet('managerId'));
                if (!empty($manager['trial_end_date'])) {
                    $date1 = new DateTime($manager['created']);
                    $date2 = $date1->diff(new DateTime($manager['trial_end_date']));
                    $difference = $date2->days.' days';
                } else {
                    $difference = TRIAL_DAY;
                }
                $freeTrialEndDate = date('d F', strtotime($manager['created'].' + '.$difference));
                $diff = strtotime(date('Y-m-d', strtotime($manager['created'].' +'.$difference))) - strtotime(date('Y-m-d'));
                $freeTrialDaysRemaining = abs(round($diff / 86400));
                if (strtotime(date('Y-m-d', strtotime($manager['created'].' + '.$difference))) <= strtotime(date('Y-m-d'))) {
                    $freeTrialEnd = true;
                    Model::sessionSet('freeTrialEnd', true);
                }
            }
        } else {
            $managerSubscriptions = 'admin';
        }

        $managerUserModel = new ManagerUser();
        $userNotification = $managerUserModel->getNotificationDetails();
        $appoinmentCountNotification = $managerUserModel->getAppoinmentCountNotification();
        $checklistCountNotification = $managerUserModel->getChecklistCountNotification();
        $reservationCountNotification = $managerUserModel->getReservationCountNotification();
        $chatCountNotification = $managerUserModel->getChatCountNotification();
        $propertyRoles = $roleModel->getPropertyRoles($currentProperty, Model::sessionGet('managerId'));

        $parameters = array_merge([
            'WEB_DIR' => WEB_DIR,
            'URL' => URL,
            'siteTitle' => 'Jervis Systems',
            'managerAccess' => Model::sessionGet('managerAccess'),
            'controller' => $controller,
            'controllerAction' => $controllerAction,
            'user' => $user,
            'propertyLists' => $propertyLists,
            'request_property_id' => isset($_GET['property_id']) ? $_GET['property_id'] : null,
            'currentProperty' => $currentProperty,
            'managerType' => Model::sessionGet('managerType'),
            'userRole' => Model::sessionGet('managerRole'),
            'managerSubscriptions' => $managerSubscriptions,
            'freeTrialEndDate' => isset($freeTrialEndDate) ? $freeTrialEndDate : null,
            'freeTrialDaysRemaining' => isset($freeTrialDaysRemaining) ? $freeTrialDaysRemaining : null,
            'freeTrialEnd' => isset($freeTrialEnd) ? $freeTrialEnd : null,
            'userNotification' => $userNotification,
            'appoinmentCountNotification' => $appoinmentCountNotification,
            'checklistCountNotification' => $checklistCountNotification,
            'reservationCountNotification' => $reservationCountNotification,
            'chatCountNotification' => $chatCountNotification,
            'userPropertyRole' => (array) Model::sessionGet('managerRole'),
            'propertyRoles' => $propertyRoles,
            'PLAN' => PLAN,
        ], $parameters);
        echo $twig->render($templateName, $parameters);
        exit;
    }

    public static function apiRender($apiResponse)
    {
        echo $apiResponse;
    }
}
