<?php

class Model
{
    protected static $db;

    public static function getDb()
    {
        self::initializeDb();

        return self::$db;
    }

    protected static function initializeDb()
    {
        if (self::$db) {
            return;
        }
        try {
            self::$db = new PDO(
                DB_DSN,
                DB_USR,
                DB_PWD,
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
                ]
            );
            self::$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            if (defined('DEBUG') && DEBUG) {
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } else {
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
            self::$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (\Exception $e) {
            // It's important to output error 500 in case Google bots are indexing. This will tell them not to index
            // because we are having a problem right now.
            header($_SERVER['SERVER_PROTOCOL'].' 500 Internal Server Error', true, 500);
            echo "Internal database error occured<br>\r\n";

            if (defined('DEBUG') && DEBUG) {
                // We re-throw the exception to show it to the developer
                throw $e;
            }
            exit;
        }
    }

    public function __construct()
    {
        if (session_id() === '') {
            session_start();
        }
        self::initializeDb();
    }

    /**
     * Tek satırlık veri döndüren sorgu çalıştırır.
     *
     * @param string $sql    SQL sorgusu
     * @param array  $params varsa parametreler
     *
     * @return array
     */
    public function fetch($sql, array $params = [])
    {
        $sth = self::$db->prepare($sql);
        if ($sth === false) {
            echo 'Database error occured.<br>';
            if (defined('DEBUG') && DEBUG) {
                Helpers::dump(self::$db->errorInfo());
                Helpers::dump(debug_backtrace());
            }
            exit;
        }
        $sth->execute($params);

        return $sth->fetch();
    }

    /**
     * Tek bir değer döndüren sorgu çalıştırır.
     *
     * @param string $sql    SQL sorgusu
     * @param array  $params varsa parametreler
     *
     * @return array
     */
    public function fetchValue($sql, array $params = [])
    {
        $sth = self::$db->prepare($sql);
        $sth->execute($params);

        return $sth->fetch(PDO::FETCH_COLUMN);
    }

    /**
     * Get num rows.
     *
     * @param string $sql    SQL sorgusu
     * @param array  $params varsa parametreler
     *
     * @return array
     */
    public function rowCount($sql, array $params = [])
    {
        $sth = self::$db->prepare($sql);
        $sth->execute($params);

        return $sth->rowCount();
    }

    /**
     * Birden fazla satır döndüren sorgu çalıştırır.
     *
     * @param string $sql    SQL sorgusu
     * @param array  $params varsa parametreler
     *
     * @return array
     */
    public function fetchAll($sql, array $params = [])
    {
        $sth = self::$db->prepare($sql);
        if ($sth === false) {
            echo 'Database error occured.<br>';
            if (defined('DEBUG') && DEBUG) {
                Helpers::dump(self::$db->errorInfo());
                Helpers::dump(debug_backtrace());
            }
            exit;
        }

        $sth->execute($params);

        return $sth->fetchAll();
    }

    /**
     * Birden fazla gruplu satır döndüren sorgu çalıştırır.
     *
     * @param string $sql    SQL sorgusu
     * @param array  $params varsa parametreler
     *
     * @return array
     */
    public function fetchAllGroup($sql, array $params = [])
    {
        $sth = self::$db->prepare($sql);
        if ($sth === false) {
            echo 'Database error occured.<br>';
            if (defined('DEBUG') && DEBUG) {
                Helpers::dump(self::$db->errorInfo());
                Helpers::dump(debug_backtrace());
            }
            exit;
        }
        $sth->execute($params);

        return $sth->fetchAll(PDO::FETCH_GROUP | PDO::FETCH_COLUMN);
    }

    /**
     * Sorgu çalıştırır.
     *
     * @param string $sql    SQL sorgusu
     * @param array  $params varsa parametreler
     *
     * @return array
     */
    public function query($sql, array $params = [])
    {
        $sth = self::$db->prepare($sql);
        return $sth->execute($params);
    }

    /**
     * Sanitize/escape/quote identifiers such as table and column names.
     *
     * @param string $id The identifier to escape
     *
     * @return string
     */
    public function quoteId($id)
    {
        return preg_replace('/[^A-Za-z0-9_]+/', '', $id);
    }

    /**
     * Alias to sanitize/escape/quote strings for database inserts.
     *
     * @param string $str The string to escape
     *
     * @return string
     */
    public function quote($str)
    {
        return self::$db->quote($str);
    }

    /**
     * Insert one or multiple row/rows into a table.
     *
     * @param string    Table name
     * @param array     Row or array of rows to insert
     *
     * @return bool
     */
    public function insert($table, array $rows, $action = 'INSERT')
    {
        switch ($action) {
            case 'INSERT':
            case 'INSERT IGNORE':
            case 'REPLACE':
                $action = $action;
                break;
            default:
                $action = 'INSERT';
                break;
        }
        if (!$rows) {
            return false;
        }
        // If it is a single row, convert it to multiple rows.
        if (!is_array($rows[array_key_first($rows)])) {
            $rows = [$rows];
        }
        $keys = array_keys(reset($rows));
        $joinedColumns = implode(', ', array_map([$this, 'quoteId'], $keys));
        // For each row, we quote its values and join them to a string.
        $quotedRows = array_map(function (array $row) {
            $quotedValues = array_map([$this, 'quote'], $row);

            return '('.implode(', ', $quotedValues).')';
        }, $rows);
        // Combine the row strings to a single string
        $joinedQuotedRows = implode(', ', $quotedRows);
        $quotedTable = $this->quoteId($table);
        $query = "{$action} INTO $quotedTable (".$joinedColumns.') VALUES '.$joinedQuotedRows;

        return self::$db->exec($query);
    }

    public function update($table, array $values, $id, $idColumn = 'id')
    {
        if (!$values) {
            return false;
        }
        $quotedTable = $this->quoteId($table);
        $query = "UPDATE {$quotedTable} SET ";
        $updates = [];
        foreach (array_keys($values) as $column) {
            $updates[] = $this->quoteId($column).' = '.$this->quote($values[$column]);
        }
        $query .= implode(' , ', $updates);
        $query .= ' WHERE '.$this->quoteId($idColumn).' = '.$this->quote($id);

        return self::$db->exec($query);
    }

    public function delete($table, array $conditions)
    {
        $quotedTable = $this->quoteId($table);
        if (!$conditions) {
            return false;
        }
        $quotedConditions = [];
        foreach ($conditions as $column => $value) {
            $quotedConditions[] = $this->quoteId($column).' = '.$this->quote($value);
        }
        $joinedConditions = implode(' AND ', $quotedConditions);

        return self::$db->exec("DELETE FROM {$quotedTable} WHERE {$joinedConditions}");
    }

    /**
     * Son eklenen satırın ID'sini döndürür.
     *
     * @return string
     */
    public function lastInsertId()
    {
        return self::$db->lastInsertId();
    }

    public static function sessionSet($key, $val)
    {
        $_SESSION[SESSION_NAME][$key] = $val;
    }

    public static function sessionGet($key)
    {
        return (isset($_SESSION[SESSION_NAME][$key])) ? $_SESSION[SESSION_NAME][$key] : null;
    }

    public static function sessionGetAll()
    {
        return (isset($_SESSION[SESSION_NAME])) ? $_SESSION[SESSION_NAME] : null;
    }

    public static function sessionUnset($key)
    {
        unset($_SESSION[SESSION_NAME][$key]);
    }

    public static function sessionClear()
    {
        $_SESSION[SESSION_NAME] = [];
    }
}
