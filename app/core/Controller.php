<?php

abstract class Controller
{
    public $controllerAction = null;

    /**
     * View dosyası çağırmamıza yarayan metod.
     *
     * @param string $file   dosyasını adını alır
     * @param array  $params parametreleri alır
     */
    public function view($file, array $params = [], $templateFolder = 'site')
    {
        return View::render($file, $params, $templateFolder);
    }

    public function viewTwig($templateName, array $parameters = [])
    {
        return View::renderTwig($templateName, $parameters, get_class($this), $this->controllerAction);
    }

    abstract public function notFound($error);

    abstract public function badRequest($error);

    abstract public function forbidden($error);

    abstract public function methodNotAllowed($error);

    public function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function isPost()
    {
        return $this->getMethod() == 'POST';
    }

    /**
     * Model dizininden model dosyası çağırır.
     *
     * @param string $model model dosyası adı
     */
    public function model($model)
    {
        return new $model();
    }
}
