<?php

class NotificationHelpers
{
    public function __construct()
    {
    }

    public static function sendEmailNotification($reservationId, $emailType)
    {
        $managerReservationModel = new ManagerReservation();
        $reservation = $managerReservationModel->getReservationById($reservationId);
        $propertyId = $reservation['property_id'];
        $email = $reservation['email'];

        $property = $managerReservationModel->getPropertyById($propertyId);
        $ownerId = $property['owner_id'];

        $managerAgreementModel = new ManagerAgreement();
        $agreement = $managerAgreementModel->getAgreementByReservationId($reservationId);

        $managerModel = new Manager();
        $manager = $managerModel->getManagerById($ownerId);
        $managerEmail = $manager['email'];

        $managerNotificationTemplateModel = new ManagerNotificationTemplate();
        $notification = $managerNotificationTemplateModel->getTemplatesByPropertyIdWithTitle($propertyId, $emailType);
        $emailSubject = self::replaceEmailToken($notification['subject'], $reservation, $manager, $agreement, $property);
        $emailBody = self::replaceEmailToken($notification['content'], $reservation, $manager, $agreement, $property);

        $mail = Helpers::createEmail();
        $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
        $mail->addAddress($email);
        $mail->AddCC($managerEmail);
        $mail->isHTML(true);
        $mail->Subject = $emailSubject;
        $mail->Body = $emailBody;

        $status = 'failed';

        if ($mail->send()) {
            $status = 'sent';
        }

        $managerNotificationModel = new ManagerNotification();
        $managerNotificationModel->logNotification($reservationId, $email, $managerEmail, '', date('Y-m-d H:i:s'), $emailType, $emailSubject, $emailBody, 'Email', $status);
    }

    public static function replaceEmailToken($emailBody, $reservation, $manager, $agreement, $property)
    {
        $guestFirstName = isset($reservation['first_name']) ? $reservation['first_name'] : '';
        $guestLastName = isset($reservation['last_name']) ? $reservation['last_name'] : '';
        $ownerFirstName = isset($manager['first_name']) ? $manager['first_name'] : '';
        $ownerLastName = isset($manager['last_name']) ? $manager['last_name'] : '';
        $businessOwnerName = trim($ownerFirstName.' '.$ownerLastName);
        $guestIPAddress = isset($agreement['ip_address']) ? $agreement['ip_address'] : '';
        $reservationCode = isset($reservation['code']) ? $reservation['code'] : '';
        $reservationId = isset($reservation['reservation_id']) ? $reservation['reservation_id'] : '';
        $reservationDate = isset($reservation['reservation_date']) ? $reservation['reservation_date'] : '';
        $reservationCheckInDate = isset($reservation['check_in_date']) ? $reservation['check_in_date'] : '';
        $reservationCheckOutDate = isset($reservation['check_out_date']) ? $reservation['check_out_date'] : '';
        $propertyTitle = isset($property['title']) ? $property['title'] : '';
        $propertyAddress = isset($property['address']) ? $property['address'] : '';

        $agreementUrl = URL.'/manager/agreement/index?code='.$reservationCode;
        $reservationLink = '<a href="'.$agreementUrl.'" target="_blank" title="Reservation Detail Link">'.$agreementUrl.'</a>';
        $agreementLink = '<a href="'.$agreementUrl.'" target="_blank" title="Agreement Detail Link">'.$agreementUrl.'</a>';

        $reservationDetailLink = URL.'/manager/reservation/view/index?code='.$reservationCode;
        $googleCalendarLink = 'https://calendar.google.com/calendar/r/eventedit?text='.urlencode($propertyTitle).'&location='.urlencode($propertyAddress).'&details='.urlencode('<a href="'.$reservationDetailLink.'">Your Reservation Link</a>').'&dates='.date('Ymd', strtotime($reservationCheckInDate)).'T'.date('His', strtotime($reservationCheckInDate)).'/'.date('Ymd', strtotime($reservationCheckOutDate)).'T'.date('His', strtotime($reservationCheckOutDate)).'&pli=1';

        $checkoutUrl = URL.'/manager/reservation/view/checkout?code='.$reservationCode;
        if (preg_match('/\{CHECK_OUT_DETAILS_LINK\}/', $emailBody)) {
            $checkoutUrl = Helpers::makeShortUrl($checkoutUrl);
        }

        $emailBody = str_replace('{GUEST_FIRST_NAME}', $guestFirstName, $emailBody);
        $emailBody = str_replace('{GUEST_NAME}', trim($guestFirstName.' '.$guestLastName), $emailBody);
        $emailBody = str_replace('{BUSINESS_OWNER_NAME}', $businessOwnerName, $emailBody);
        $emailBody = str_replace('{AGREEMENT_DETAIL_LINK}', $agreementLink, $emailBody);

        $emailBody = str_replace('{RESERVATION_ID}', $reservationId.' ', $emailBody);
        $emailBody = str_replace('{RESERVATION_DATE_TIME}', $reservationDate, $emailBody);
        $emailBody = str_replace('{GUEST_IP_ADDRESS}', $guestIPAddress, $emailBody);
        $emailBody = str_replace('{HOUSE_RULES_SIGNED_LINK}', $reservationLink, $emailBody);

        $emailBody = str_replace('{PROPERTY_NAME}', $propertyTitle, $emailBody);
        $emailBody = str_replace('{PROPERTY_ADDRESS}', $propertyAddress, $emailBody);
        $emailBody = str_replace('{GOOGLE_CALENDAR_LINK}', '<a href="'.$googleCalendarLink.'">Google Calendar</a>', $emailBody);
        $emailBody = str_replace('{RESERVATION_DETAIL_LINK}', '<a href="'.$reservationDetailLink.'">Reservation Detail</a>', $emailBody);
        $emailBody = str_replace('{CHECK_IN_TIME}', date('d M Y g:i A', strtotime($reservationCheckInDate)), $emailBody);
        $emailBody = str_replace('{CHECK_OUT_TIME}', date('d M Y g:i A', strtotime($reservationCheckOutDate)), $emailBody);
        $emailBody = str_replace('{CHECK_OUT_DATE}', date('d M Y', strtotime($reservationCheckOutDate)), $emailBody);

        $emailBody = str_replace('{CHECK_OUT_DETAILS_LINK}', $checkoutUrl, $emailBody);

        return $emailBody;
    }
}
