<?php

use PHPMailer\PHPMailer\PHPMailer;
use Aws\Pinpoint\PinpointClient;

class Helpers
{
    public function __construct()
    {
    }

    public static function redirectRelative($path)
    {
        header("Location: {$path}");
        exit;
    }

    public static function redirect($path = '')
    {
        header('Location: '.WEB_DIR.$path);
        exit;
    }

    public static function url()
    {
        return URL.'/?url='.implode('/', func_get_args());
    }

    public static function createHash($v)
    {
        return hash('SHA512', DB_SALT.$v.DB_SALT, false);
    }

    public static function arrayToOptions($array, $keyField, $valField, $defaultVal)
    {
        $options = '';
        foreach ($array as $v) {
            $options .= "<option value='{$v[$keyField]}'".($defaultVal == $v[$keyField] ? ' selected' : '').">{$v[$valField]}</option>";
        }

        return $options;
    }

    public static function camelCase($str)
    {
        $str = preg_split("#\-+#", $str);
        if (count($str) == 1) {
            return $str[0];
        }

        return $str[0].implode('', array_map('ucfirst', array_slice($str, 1)));
    }

    public static function dump($value)
    {
        if (defined('DEBUG') && DEBUG) {
            echo '<pre>', json_encode($value, JSON_PRETTY_PRINT), '</pre>';
        }
    }

    public static function createEmail(array &$output = [])
    {
        $mail = new PHPMailer(true);
        if (MAIL_SMTP) {
            if (defined('DEBUG') && DEBUG) {
                $mail->SMTPDebug = 2;
                $mail->Debugoutput = function ($str, $level) use (&$output) {
                    $output[] = $str;
                };
            }
            $mail->isSMTP();
            $mail->Host = MAIL_SMTP_SERVERS;
            $mail->SMTPAuth = true;
            $mail->Username = MAIL_SMTP_USERNAME;
            $mail->Password = MAIL_SMTP_PASSWORD;
            $mail->SMTPSecure = 'tls';
            $mail->Port = MAIL_SMTP_PORT;
            $mail->CharSet = 'UTF-8';
        }

        return $mail;
    }

    public static function createSMS()
    {
        $params = array('credentials' => array(
                'key' => SMS_KEY,
                'secret' => SMS_SECRET,
            ),
            'region' => 'us-east-1',
            'version' => 'latest',
        );
        $pinpoint = new PinpointClient($params);

        return $pinpoint;
    }

    public static function mysqlDate($timestamp)
    {
        return date('Y-m-d', $timestamp);
    }

    public static function mysqlDatetime($timestamp)
    {
        return date('Y-m-d H:i:s', $timestamp);
    }

    public static function isAssociative(array $arr)
    {
        return !self::isIndexed($arr);
    }

    public static function isIndexed(array $arr)
    {
        foreach ($arr as $key => $val) {
            if (!is_int($key)) {
                return false;
            }
        }

        return true;
    }

    public static function getRequestUrl()
    {
        // Source: https://stackoverflow.com/questions/6768793/get-the-full-url-in-php?answertab=votes#tab-top
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http')."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    public static function makeShortUrl($url)
    {
        $url = urlencode($url);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://www.securemysanity.com/php/api/url-shortener.php');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'longurl='.$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $headers = array();
        $headers[] = 'Sec-Fetch-Mode: cors';
        $headers[] = 'Origin: https://www.securemysanity.com';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            //echo 'Error:'.curl_error($ch);
        }
        curl_close($ch);

        $resultJson = json_decode($result);

        if (isset($resultJson->shorturl)) {
            return $resultJson->shorturl;
        }

        return $url;
    }

    public static function sendEmailNotification($to, $subject, $message, $ccEmail = null, $altBody = null)
    {
        $mail = self::createEmail();
        $mail->setFrom(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
        $mail->addAddress($to);
        if (!empty($ccEmail)) {
            $mail->AddCC($ccEmail);
        }
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $message;
        if (!empty($altBody)) {
            $mail->AltBody = $altBody;
        }

        return $mail->send();
    }

    public static function randomNumber($length)
    {
        $result = '';
        for ($i = 0; $i < $length; ++$i) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public static function randomString($length)
    {
        $result = '';
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        for ($i = 0; $i < $length; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $result .= $characters[$index];
        }

        return $result;
    }

    public static function getFileExtension($fileName)
    {
        $fileArray = explode(".", $fileName);

        return end($fileArray);
    }

    public static function sendSMSNotification($smsBody, $to)
    {
        $pinpoint = self::createSMS();

        $args = array('ApplicationId' => SMS_APPLICATION_ID,
            'MessageRequest' => array(
                'Addresses' => array(
                    $to => array('ChannelType' => 'SMS'),
                ),
                'MessageConfiguration' => array(
                    'SMSMessage' => array(
                        'Body' => $smsBody,
                        'MessageType' => 'TRANSACTIONAL',
                        'OriginationNumber' => SMS_ORIGINATION_NUMBER,
                        'SenderId' => 'Jervis',
                    ),
                ),
            ),
        );

        return $pinpoint->sendMessages($args);
    }

    public function splitTime($startTime, $endTime, $duration = '60')
    {
        $ReturnArray = array();
        $startTime = strtotime(date('Y-m-d').' '.$startTime);
        $endTime = strtotime(date('Y-m-d').' '.$endTime);

        $AddMins = $duration * 60;

        while ($startTime <= $endTime) {
            $ReturnArray[] = date('g:i A', $startTime);
            $startTime += $AddMins;
        }

        return $ReturnArray;
    }

    public static function timezone()
    {
        $timestamp = time();
        $timezone = '';
        $timezone_name = self::getUserTimeZonebyIpApi();
        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            if ($zone == $timezone_name && !empty($timezone_name)) {
                $timezone .= '<option  value='.$zone.' selected>UTC/GMT '.date('P', $timestamp).'-'.$zone.'</option>';
            } else {
                $timezone .= '<option  value='.$zone.'>UTC/GMT '.date('P', $timestamp).'-'.$zone.'</option>';
            }
        }

        return $timezone;
    }

    public static function getUserTimeZonebyIpApi()
    {
        $timezone = '';
        try {
            $ip = $_SERVER['REMOTE_ADDR'];
            if ($ip) {
                $pageSource = file_get_contents("http://ip-api.com/json/$ip");
                $pageSource = json_decode($pageSource);
                if (isset($pageSource->timezone)) {
                    $timezone = $pageSource->timezone;
                }
            }
        } catch (Exception $ex) {
        }

        return $timezone;
    }
}
