<?php

class App
{
    public $controller;
    public $action;
    public $params;
    public $routes;

    public function __construct(array $routes)
    {
        $url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_SPECIAL_CHARS);
        $url = !empty($url) ? trim($url, '/') : 'index/index';
        @list($beforeApi, $afterApi) = explode('/api/', $url);
        if (isset($afterApi)) {
            /*
                URLs containing the string "/api/" are interpreted here.
                We allow links of the form [/my/controller/many/names] "/api/" [do-set-value] "/" [x/y/z] which is interpreted as:
                controller: MyControllerManyNamesApi
                action: doSetValue
                params: ["x", "y", "z"]
            */
            $afterApiParts = explode('/', $afterApi);
            $this->action = Helpers::camelCase(array_shift($afterApiParts));
            $this->params = $afterApiParts;

            $this->controller = $this->controllerNameFromUri($beforeApi).'Api'.'Controller';
        } else {
            /*
                URLs that do not contain the string "/api/" are interpreted here.
                We allow links of the form [/my/controller/many/names] "/" [do-whatever-action] which is interpreted as:
                controller: MyControllerManyNames
                action: doSetValue
            */
            $requestParts = explode('/', $url);
            if (count($requestParts) > 1) {
                $this->action = Helpers::camelCase(array_pop($requestParts));
            } else {
                $this->action = 'index';
            }
            $this->controller = $this->controllerNameFromUriParts($requestParts).'Controller';
        }
        $this->routes = $routes;
        self::run();
    }

    protected function controllerNameFromUri($uri)
    {
        return $this->controllerNameFromUriParts(explode('/', $uri));
    }

    protected function controllerNameFromUriParts(array $parts)
    {
        return implode('', array_map('ucfirst', $parts));
    }

    public function run()
    {
        if (class_exists($this->controller)) {
            $controller = new $this->controller();
            $controller->controllerAction = $this->action;
            // Eğer metod varsa ve yaratılmışsa
            if (method_exists($controller, $this->action)) {
                // call_user_func ile controller ve metodu çağırıyoruz
                call_user_func_array([$controller, $this->action], [$this->params]);
            // Eğer method yoksa rotacıyı başlat
            } else {
                return $this->startRouter();
            }
        } else {
            return $this->startRouter();
        }
    }

    protected function startRouter()
    {
        // Eğer $routes doluysa (boş değilse) ve dize (array) ise
        if (!empty($this->routes) && is_array($this->routes)) {
            // URL'yi alıyoruz
            $url = rtrim(filter_input(INPUT_GET, 'url', FILTER_SANITIZE_SPECIAL_CHARS), '/');

            // index.php dosyasındaki $routes dizesindeki değerleri işlemek için
            // foreach döngüsüne sokuyoruz bunun manası şu
            // "/rota" => "kontrolcü:aksiyon"
            // $path => $controller : $action
            foreach ($this->routes as $path => $controllerAction) {
                // list fonksiyonu ve explode fonksiyonu ne işe yarar öğrenin
                list($controller, $action) = explode(':', $controllerAction);

                $path = str_replace(':param', '([^/]+)', $path);

                // Eğer ki orta URL'deki değerle eşleşirse işlem yapalım
                if (!preg_match("@^$path$@ixs", $url, $params)) {
                    continue;
                }
                // Eğer controller dosyası varsa
                $file = CDIR."/{$controller}.php";
                if (!file_exists($file)) {
                    continue;
                }
                // Dosyayı çağır
                require_once $file;
                // Eğer sınıf mevcutsa
                if (!class_exists($controller)) {
                    continue;
                }
                // sınıfı $class değişkenine ata
                $class = new $controller();
                // Eğer method mevcutsa her şey tamam
                if (!method_exists($class, $action)) {
                    continue;
                }
                // $params dizesinden ilk öğeyi at/çıkar
                array_shift($params);

                // controller ve aksiyonu çalıştır! bitti.
                return call_user_func_array([$class, $action], array_values($params));
            }
        }

        /*
         * checks the view file exists, if yes; hook render function
         * maybe later we can remove that codes.
         */
        http_response_code(404);

        if (stripos($url, 'manager/') !== false) {
            if (defined('DEBUG') && DEBUG) {
                echo "Tried to load controller '{$this->controller}' with action '{$this->action}'.<br>\r\n";
            }
            View::renderTwig('manager/index.html');
        } else {
            // View::render('index', ['title' => ''], 'site');
            View::renderTwig('index.html');
        }
    }
}
